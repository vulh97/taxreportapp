﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;
using System.Windows.Forms;
using System.Data;
using System.Configuration;
using System.IO;
using TaxReportApp.Model;

namespace TaxReportApp.Controller
{
    class BaseController
    {
        public static OleDbConnection getConnection(String path)
        {
            var connection = new OleDbConnection("provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=" + path);
            return connection;
        }
    }
}