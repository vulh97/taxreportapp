﻿using Spire.Doc;
using Spire.Doc.Documents;
using Spire.Doc.Fields;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TaxReportApp.Model;

namespace TaxReportApp.Controller
{
    class GTTKController
    {
        private static string dir = Directory.GetCurrentDirectory().ToString() + @"\" + ConfigurationManager.AppSettings["directoryDatabase"];
        private static string S12_filePath = dir + @"\" + ConfigurationManager.AppSettings["s12File"];
        private static string chungtu_filePath = dir + @"\" + ConfigurationManager.AppSettings["licenseFile"];
        private static string danhBa_filePath = dir + @"\" + ConfigurationManager.AppSettings["phoneBookFile"];
        private static string tkNganHang_filePath = dir + @"\" + ConfigurationManager.AppSettings["accountBankFile"];
        private static string dsTheoDoiCCTK_filePath = dir + @"\" + ConfigurationManager.AppSettings["dsTheoDoiCCTK"];
        private static string cctk_trenTMS_filePath = dir + @"\" + ConfigurationManager.AppSettings["cctkTMSFile"];
        private static string s14_filePath = dir + @"\" + ConfigurationManager.AppSettings["s14File"];
        private static string dirReport = Application.StartupPath + @"\" + ConfigurationManager.AppSettings["ReportFile"];
        private static string patermNotification3Day = Application.StartupPath + @"/documents/input/CCHD/thongbao3ngay.doc";
        private static string patermNotification5Day = Application.StartupPath + @"/documents/input/CCHD/thongbao5ngay.doc";
        public static string Get_NoiDungTheoTieuMuc(String TieuMuc)
        {
            switch (TieuMuc)
            {
                case "1001":
                    return "Thuế TNCN";
                case "1004":
                    return "Thuế TNCN";
                case "1502":
                    return "Thuế TNDN";
                case "1701":
                    return "Thuế GTGT";
                case "2862":
                    return "Lệ phí MB";
                case "2863":
                    return "Lệ phí MB";
                case "2864":
                    return "Lệ phí MB";
                case "4254":
                    return "Tiền phạt";
                case "4268":
                    return "Tiền phạt";
                case "4272":
                    return "Tiền chậm nộp";
                case "4917":
                    return "Tiền chậm nộp";
                case "4918":
                    return "Tiền chậm nộp";
                case "4931":
                    return "Tiền chậm nộp";
                case "4944":
                    return "Tiền chậm nộp";
                default:
                    return "";
            }
        }

        public static void Create_BaoCao(String maSoThue, Array createdDate)
        {
            var fileNguonStatusModel = FileNguonStatusController.get_FileNguonStatus();

            if (!fileNguonStatusModel.CC_S12)
            {
                Common.Common.error("Không tìm thấy tài liệu S12.XLSX! Vui lòng nhập tài liệu");
            }
            else if (!fileNguonStatusModel.CC_ChungTu)
            {
                Common.Common.error("Không tìm thấy file Chứng từ ! Vui lòng nhập tài liệu");
            }
            else if (!fileNguonStatusModel.CC_DanhBa)
            {
                Common.Common.error("Không tìm thấy file Danh bạ ! Vui lòng nhập tài liệu");
            }
            else if (!fileNguonStatusModel.CC_TaiKhoanNganHang)
            {
                Common.Common.error("Không tìm thấy file Tài khoản ngân hàng ! Vui lòng nhập tài liệu");
            }
            else if (!fileNguonStatusModel.CC_S14)
            {
                Common.Common.error("Không tìm thấy file S14 ! Vui lòng nhập tài liệu");
            }
            else
            {
                var tkNganHangTable = NganHangController.GetTaiKhoanNganHang(tkNganHang_filePath, maSoThue);
                if (tkNganHangTable.Rows.Count == 0)
                {
                    MessageBox.Show("Đơn vị không có tài khoản ngân hàng !");
                    return;
                }

                if (String.IsNullOrEmpty(maSoThue) || String.IsNullOrWhiteSpace(maSoThue))
                {
                    // Tạo báo cáo cho tất cả các công ty thỏa mãn
                }
                else
                {
                    var s12FileContent = S12Controller.SearchMST(S12_filePath, maSoThue);
                    var isS12File_NotEmpty = s12FileContent.AsEnumerable().Any(row => row["F2"] != null);

                    if (!isS12File_NotEmpty)
                    {
                        Common.Common.error("Không tìm thấy dữ liệu!");
                    }
                    else
                    {
                        #region Load dữ liệu 
                        // từ file S12 : Tên công ty, mã số thuế, path file để save báo cáo
                        GiaiToaTaiKhoanViewModel giaiToaTaiKhoanViewModel = new GiaiToaTaiKhoanViewModel();
                        giaiToaTaiKhoanViewModel.TenCongTy = s12FileContent.Rows[0]["F3"].ToString();
                        giaiToaTaiKhoanViewModel.MaSoThue = s12FileContent.Rows[0]["F2"].ToString();
                        giaiToaTaiKhoanViewModel.tongTienNo = 0;
                        giaiToaTaiKhoanViewModel.tongTienDaDong = 0;

                        string timeFolder = S12Controller.SearchMonthReport(S12_filePath);
                        var containerFilePath = dirReport + @"\" + timeFolder + @"\" + giaiToaTaiKhoanViewModel.MaSoThue + @"\Giải tỏa tài khoản\";
                        if (!Directory.Exists(containerFilePath))
                        {
                            Directory.CreateDirectory(containerFilePath);
                        }
                        giaiToaTaiKhoanViewModel.ContainterFilePath = containerFilePath;

                        for (int i = 0; i < s12FileContent.Rows.Count; i++)
                        {
                            // Các khoản tiền nợ
                            var TienNoItem = new KhoanTienModel();
                            TienNoItem.TenCongTy = s12FileContent.Rows[0]["F3"].ToString();
                            TienNoItem.MaSoThue = s12FileContent.Rows[0]["F2"].ToString();
                            TienNoItem.SoTienTheoSo = Convert.ToDecimal(s12FileContent.Rows[i]["F7"].ToString().Replace(",", ""));
                            TienNoItem.TieuMuc = s12FileContent.Rows[i][5].ToString();
                            TienNoItem.MaChuong = s12FileContent.Rows[i][3].ToString();
                            giaiToaTaiKhoanViewModel.listKhoanTienNo.Add(TienNoItem);
                            giaiToaTaiKhoanViewModel.tongTienNo += TienNoItem.SoTienTheoSo;
                        }

                        // Từ file Chứng từ: các khoản tiền đã đóng
                        foreach (DataRow rowItem in s12FileContent.Rows)
                        {
                            var soTienDaDongTable = ChungTuController.GetListChungTu(chungtu_filePath, rowItem[1].ToString(), rowItem[5].ToString());
                            if (soTienDaDongTable.Rows.Count > 0)
                            {
                                for (int j = 0; j < soTienDaDongTable.Rows.Count; j++)
                                {
                                    var TienDaDongItem = new KhoanTienModel();
                                    TienDaDongItem.TenCongTy = s12FileContent.Rows[0]["F3"].ToString();
                                    TienDaDongItem.MaSoThue = s12FileContent.Rows[0]["F2"].ToString();

                                    TienDaDongItem.MaChuong = soTienDaDongTable.Rows[j][5].ToString();
                                    TienDaDongItem.TieuMuc = soTienDaDongTable.Rows[j][2].ToString();
                                    TienDaDongItem.SoTienTheoSo = Convert.ToDecimal(soTienDaDongTable.Rows[j][3].ToString().Replace(",", ""));
                                    giaiToaTaiKhoanViewModel.listKhoanTienDaDong.Add(TienDaDongItem);
                                    giaiToaTaiKhoanViewModel.tongTienDaDong += TienDaDongItem.SoTienTheoSo;
                                }
                            }
                        }

                        // Từ file danh bạ: Địa chỉ trụ sở, Địa chỉ nhận thông báo
                        var danhBaTable = DanhBaController.GetDanhba(danhBa_filePath, giaiToaTaiKhoanViewModel.MaSoThue);
                        if (danhBaTable.Rows.Count > 0)
                        {
                            var diachi = danhBaTable.Rows[0][7].ToString();
                            var phuongxa = danhBaTable.Rows[0][9].ToString();
                            var quanhuyen = danhBaTable.Rows[0][11].ToString();
                            var tinhthanhpho = danhBaTable.Rows[0][13].ToString();

                            giaiToaTaiKhoanViewModel.DiaChiTruSo = diachi + ", " + phuongxa + ", " + quanhuyen + ", " + tinhthanhpho;
                            giaiToaTaiKhoanViewModel.DiaChiNhanThongBao = giaiToaTaiKhoanViewModel.DiaChiTruSo;
                        }
                        else
                        {
                            MessageBox.Show("Không tìm thấy thông tin công ty trong danh bạ !", "Lỗi");
                        }

                        // Từ file Tài khoản ngân hàng: Danh sách ngân hàng
                        if (tkNganHangTable.Rows.Count > 0)
                        {
                            List<String> dsTKNganHang = new List<string>();
                            for (int i = 0; i < tkNganHangTable.Rows.Count; i++)
                            {
                                dsTKNganHang.Add(tkNganHangTable.Rows[i][4].ToString().Replace("NH", "Ngân hàng").Replace("VN", "Việt Nam").Replace("CN", "Chi nhánh").Replace("PGD", "Phòng giao dịch").Replace(@"\s+", " "));
                            }
                            giaiToaTaiKhoanViewModel.DanhSachNganHang = String.Join("; ", dsTKNganHang.ToArray());
                        }
                        else
                        {
                            MessageBox.Show("Không tìm thấy thông tin tài khoản ngân hàng của công ty !", "Lỗi");
                            return;
                        }

                        // Từ file theo dõi CCTK: Ngày, tháng, năm, số quyết định
                        //var theoDoiCCTKTable = DanhSachTheoDoiCCTKController.SearchMST(dsTheoDoiCCTK_filePath, giaiToaTaiKhoanViewModel.MaSoThue);
                        //if (theoDoiCCTKTable.Rows.Count > 0)
                        //{
                        //    var _ngayBanHanh = theoDoiCCTKTable.Rows[0][5].ToString();
                        //    if (_ngayBanHanh.Contains('.'))
                        //    {
                        //        giaiToaTaiKhoanViewModel.Ngay = _ngayBanHanh.Split('.')[0];
                        //        giaiToaTaiKhoanViewModel.Thang = _ngayBanHanh.Split('.')[1];
                        //        giaiToaTaiKhoanViewModel.Nam = _ngayBanHanh.Split('.')[2];
                        //    }
                        //    else
                        //    {
                        //        giaiToaTaiKhoanViewModel.Ngay = _ngayBanHanh.Split('/')[1];
                        //        giaiToaTaiKhoanViewModel.Thang = _ngayBanHanh.Split('/')[0];
                        //        giaiToaTaiKhoanViewModel.Nam = _ngayBanHanh.Split('/')[2];
                        //    }

                        //    giaiToaTaiKhoanViewModel.SoQuyetDinh = theoDoiCCTKTable.Rows[0][4].ToString();
                        //    giaiToaTaiKhoanViewModel.TongTienNoCCTK = Convert.ToDecimal(theoDoiCCTKTable.Rows[0][8].ToString().Replace(",", ""));
                        //}
                        //else
                        //{
                        //    MessageBox.Show("Không tìm thấy thông tin GTTK trong file Theo dõi CCTK !", "Lỗi");
                        //    return;
                        //}

                        //var s14FileContent = S14Controller.GetDataS14FileByMST(s14_filePath, giaiToaTaiKhoanViewModel.MaSoThue);
                        //if (s14FileContent.Rows.Count > 0)
                        //{
                        //    var _ngayTB07 = s14FileContent.Rows[0][4].ToString();
                        //    if (_ngayTB07.Contains('.'))
                        //    {
                        //        giaiToaTaiKhoanViewModel.Ngay = _ngayTB07.Split('.')[0];
                        //        giaiToaTaiKhoanViewModel.Thang = _ngayTB07.Split('.')[1];
                        //        giaiToaTaiKhoanViewModel.Nam = _ngayTB07.Split('.')[2].Split(' ')[0];
                        //    }
                        //    else
                        //    {
                        //        giaiToaTaiKhoanViewModel.Ngay = _ngayTB07.Split('/')[1];
                        //        giaiToaTaiKhoanViewModel.Thang = _ngayTB07.Split('/')[0];
                        //        giaiToaTaiKhoanViewModel.Nam = _ngayTB07.Split('/')[2].Split(' ')[0];
                        //    }
                        //}
                        //else
                        //{
                        //    MessageBox.Show("Không tìm thấy thông tin GTTK trong file S14 !", "Lỗi");
                        //}
                        #endregion

                        #region Check điều kiện giải tỏa
                        var isAllowedToCreateReport = (giaiToaTaiKhoanViewModel.tongTienDaDong >= giaiToaTaiKhoanViewModel.tongTienNo) ? true : false;
                        #endregion

                        #region Xử lý tạo báo cáo 
                        if (!isAllowedToCreateReport)
                        {
                            MessageBox.Show("Công ty với mã số thuế đã nhập không đủ điều kiện tạo báo cáo giải tỏa cưỡng chế tài khoản", "Thông báo");
                            return;
                        }
                        else
                        {
                            // set path biểu mẫu
                            var quyetDinh_pathFile = Environment.CurrentDirectory + @"/documents/input/GTTK/Quyet_Dinh.doc";
                            var toTrinh_pathFile = Environment.CurrentDirectory + @"/documents/input/GTTK/To_Trinh_Giai_Toa_CCTK.doc";
                            var fileTongHop_pathFile = Environment.CurrentDirectory + @"/documents/input/GTTK/File_tong_hop_GTTK.docx";

                            // Kiểm tra file biểu mẫu
                            if (!File.Exists(quyetDinh_pathFile))
                            {
                                MessageBox.Show("Không tìm thấy file biểu mẫu của Quyết định. Vui lòng nhập tài liệu !", "Lỗi");
                                return;
                            }
                            else if (!File.Exists(toTrinh_pathFile))
                            {
                                MessageBox.Show("Không tìm thấy file biểu mẫu của Tờ trình. Vui lòng nhập tài liệu !", "Lỗi");
                                return;
                            }
                            else
                            {
                                var thongTinDonVi = DonViController.GetThongTinDonVi();

                                #region Tạo file Quyết định: Tên công ty, mã số thuế, trụ sở, Địa chỉ nhận thông báo, Danh sách ngân hàng, 
                                Document quyetDinh_doc = new Document();
                                quyetDinh_doc.LoadFromFile(quyetDinh_pathFile);

                                quyetDinh_doc.Replace("{{TENCONGTY}}", giaiToaTaiKhoanViewModel.TenCongTy, false, true);
                                quyetDinh_doc.Replace("{{MST}}", giaiToaTaiKhoanViewModel.MaSoThue, false, true);
                                quyetDinh_doc.Replace("{{TRUSO}}", giaiToaTaiKhoanViewModel.DiaChiTruSo, false, true);
                                quyetDinh_doc.Replace("{{DIACHINHANTHONGBAO}}", giaiToaTaiKhoanViewModel.DiaChiNhanThongBao, false, true);
                                quyetDinh_doc.Replace("{{DSNGANHANG}}", giaiToaTaiKhoanViewModel.DanhSachNganHang, false, true);
                                //quyetDinh_doc.Replace("{{NGAY}}", giaiToaTaiKhoanViewModel.Ngay, false, true);
                                //quyetDinh_doc.Replace("{{THANG}}", giaiToaTaiKhoanViewModel.Thang, false, true);
                                //quyetDinh_doc.Replace("{{NAM}}", giaiToaTaiKhoanViewModel.Nam, false, true);
                                quyetDinh_doc.Replace("{{SOQĐ}}", giaiToaTaiKhoanViewModel.SoQuyetDinh, false, true);
                                quyetDinh_doc.Replace("{{CCTHUE}}", thongTinDonVi.tenChiNhanh.ToUpper().Replace("QUẬN ", "Q.").Replace("HUYỆN ", "H.").Replace("KHU VỰC ", "KV."), false, true);
                                quyetDinh_doc.Replace("{{CHICUCTRUONG}}", thongTinDonVi.tenChiCucTruong, false, true);
                                //quyetDinh_doc.Replace("{{CHICUCTRUONG}}", thongTinDonVi.)
                                //quyetDinh_doc.Replace("{{DAY}}", createdDate.GetValue(0).ToString(), false, true);
                                //quyetDinh_doc.Replace("{{MONTH}}", createdDate.GetValue(1).ToString(), false, true);
                                //quyetDinh_doc.Replace("{{YEAR}}", createdDate.GetValue(2).ToString(), false, true);

                                quyetDinh_doc.SaveToFile(giaiToaTaiKhoanViewModel.ContainterFilePath + @"/Quyết định.docx", FileFormat.Docx);

                                Document toTrinh_doc = new Document();
                                #endregion

                                #region Tạo File Tờ Trình
                                toTrinh_doc.LoadFromFile(toTrinh_pathFile);

                                toTrinh_doc.Replace("{{TENCONGTY}}", giaiToaTaiKhoanViewModel.TenCongTy, false, true);
                                toTrinh_doc.Replace("{{MST}}", giaiToaTaiKhoanViewModel.MaSoThue, false, true);
                                toTrinh_doc.Replace("{{TRUSO}}", giaiToaTaiKhoanViewModel.DiaChiTruSo, false, true);
                                toTrinh_doc.Replace("{{DSNGANHANG}}", giaiToaTaiKhoanViewModel.DanhSachNganHang, false, true);
                                //toTrinh_doc.Replace("{{NGAY}}", giaiToaTaiKhoanViewModel.Ngay, false, true);
                                //toTrinh_doc.Replace("{{THANG}}", giaiToaTaiKhoanViewModel.Thang, false, true);
                                //toTrinh_doc.Replace("{{NAM}}", giaiToaTaiKhoanViewModel.Nam, false, true);
                                toTrinh_doc.Replace("{{DATE}}", giaiToaTaiKhoanViewModel.Ngay + @"/" + giaiToaTaiKhoanViewModel.Thang + @"/" + giaiToaTaiKhoanViewModel.Nam, false, true);
                                toTrinh_doc.Replace("{{SOQĐ}}", giaiToaTaiKhoanViewModel.SoQuyetDinh, false, true);
                                toTrinh_doc.Replace("{{TONGTIENPHAT}}", giaiToaTaiKhoanViewModel.TongTienNoCCTK.ToString("#,###"), false, true);
                                toTrinh_doc.Replace("{{CCTHUE}}", thongTinDonVi.tenChiNhanh.ToUpper().Replace("QUẬN ", "Q.").Replace("HUYỆN ", "H.").Replace("KHU VỰC ", "KV."), false, true);
                                toTrinh_doc.Replace("{{TENCANBO}}", thongTinDonVi.tenCanBo, false, true);
                                toTrinh_doc.Replace("{{TENDOI}}", thongTinDonVi.tenDoi.ToUpper(), false, true);
                                toTrinh_doc.Replace("{{TENDOITRUONG}}", thongTinDonVi.tenDoiTruong, false, true);
                                toTrinh_doc.Replace("{{CHICUCTRUONG}}", thongTinDonVi.tenChiCucTruong, false, true);
                                toTrinh_doc.Replace("{{PCHICUCTRUONG}}", thongTinDonVi.tenPhoChiCucTruong, false, true);
                                //toTrinh_doc.Replace("{{DAY}}", createdDate.GetValue(0).ToString(), false, true);
                                //toTrinh_doc.Replace("{{MONTH}}", createdDate.GetValue(1).ToString(), false, true);
                                //toTrinh_doc.Replace("{{YEAR}}", createdDate.GetValue(2).ToString(), false, true);

                                //Tạo table
                                Section section = toTrinh_doc.Sections[0];
                                TextSelection selection = toTrinh_doc.FindString("{{TABLE}}", true, true);
                                TextRange range = selection.GetAsOneRange();
                                Paragraph paragraph = range.OwnerParagraph;
                                Body body = paragraph.OwnerTextBody;
                                int index = body.ChildObjects.IndexOf(paragraph);
                                Table tableNo = section.AddTable(true);
                                String[] header = new string[] { "STT", "TM", "Nội dung", "Số tiền (đ)" };

                                int sttCT = 0;
                                tableNo.ResetCells(giaiToaTaiKhoanViewModel.listKhoanTienNo.Count + 2, header.Length);
                                TableRow FRow = tableNo.Rows[0];
                                FRow.IsHeader = true;
                                for (int i = 0; i < header.Length; i++)
                                {
                                    //Cell Alignment
                                    Paragraph p = FRow.Cells[i].AddParagraph();
                                    FRow.Cells[i].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                    p.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
                                    //Data Format
                                    TextRange TR = p.AppendText(header[i]);
                                    TR.CharacterFormat.FontName = "Times New Roman";
                                    TR.CharacterFormat.Bold = true;
                                    TR.CharacterFormat.FontSize = 13;
                                    switch (i)
                                    {
                                        case 0:
                                            FRow.Cells[i].SetCellWidth(10, CellWidthType.Percentage); break;
                                        case 1:
                                            FRow.Cells[i].SetCellWidth(20, CellWidthType.Percentage); break;
                                        case 2:
                                            FRow.Cells[i].SetCellWidth(30, CellWidthType.Percentage); break;
                                        case 3:
                                            FRow.Cells[i].SetCellWidth(40, CellWidthType.Percentage); break;
                                    }
                                }

                                //Table rows data
                                for (int r = 0; r < giaiToaTaiKhoanViewModel.listKhoanTienNo.Count; r++)
                                {
                                    TableRow DataRow = tableNo.Rows[r + 1];
                                    // 1. STT
                                    DataRow.Cells[0].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                    Paragraph p0 = DataRow.Cells[0].AddParagraph();
                                    TextRange TR0 = p0.AppendText((r + 1).ToString());
                                    p0.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
                                    TR0.CharacterFormat.FontName = "Times New Roman";
                                    TR0.CharacterFormat.FontSize = 13;
                                    DataRow.Cells[0].SetCellWidth(10, CellWidthType.Percentage);

                                    //2. Tiểu mục
                                    DataRow.Cells[1].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                    Paragraph p1 = DataRow.Cells[1].AddParagraph();
                                    TextRange TR1 = p1.AppendText("TM " + giaiToaTaiKhoanViewModel.listKhoanTienNo[r].TieuMuc);
                                    p1.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
                                    TR1.CharacterFormat.FontName = "Times New Roman";
                                    TR1.CharacterFormat.FontSize = 13;
                                    DataRow.Cells[1].SetCellWidth(20, CellWidthType.Percentage);

                                    //3. Nội dung
                                    DataRow.Cells[2].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                    Paragraph p2 = DataRow.Cells[2].AddParagraph();
                                    TextRange TR2 = p2.AppendText(Get_NoiDungTheoTieuMuc(giaiToaTaiKhoanViewModel.listKhoanTienNo[r].TieuMuc));
                                    p2.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
                                    TR2.CharacterFormat.FontName = "Times New Roman";
                                    TR2.CharacterFormat.FontSize = 13;
                                    DataRow.Cells[2].SetCellWidth(30, CellWidthType.Percentage);

                                    //4.Số tiền
                                    DataRow.Cells[3].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                    Paragraph p3 = DataRow.Cells[3].AddParagraph();
                                    TextRange TR3 = p3.AppendText(giaiToaTaiKhoanViewModel.listKhoanTienNo[r].SoTienTheoSo.ToString("#,###"));
                                    p3.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
                                    TR3.CharacterFormat.FontName = "Times New Roman";
                                    TR3.CharacterFormat.FontSize = 13;
                                    DataRow.Cells[3].SetCellWidth(40, CellWidthType.Percentage);
                                }

                                //Số tiền tổng cộng
                                TableRow DataRowTongCong = tableNo.Rows[giaiToaTaiKhoanViewModel.listKhoanTienNo.Count + 1];
                                ////1. STT
                                //DataRowTongCong.Cells[0].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                //Paragraph pNo0 = DataRowTongCong.Cells[0].AddParagraph();
                                //TextRange TRNo0 = pNo0.AppendText("");
                                //pNo0.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
                                //TRNo0.CharacterFormat.FontName = "Times New Roman";
                                //TRNo0.CharacterFormat.FontSize = 13;
                                //DataRowTongCong.Cells[0].SetCellWidth(10, CellWidthType.Percentage);
                                ////2. Tiểu mục
                                //DataRowTongCong.Cells[1].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                //Paragraph pNo1 = DataRowTongCong.Cells[1].AddParagraph();
                                //TextRange TRNo1 = pNo1.AppendText("");
                                //pNo1.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
                                //TRNo1.CharacterFormat.FontName = "Times New Roman";
                                //TRNo1.CharacterFormat.FontSize = 13;
                                //DataRowTongCong.Cells[1].SetCellWidth(20, CellWidthType.Percentage);
                                //3. Nội dung 
                                DataRowTongCong.Cells[2].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                Paragraph pNo2 = DataRowTongCong.Cells[2].AddParagraph();
                                TextRange TRNo2 = pNo2.AppendText("Tổng cộng");
                                pNo2.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
                                TRNo2.CharacterFormat.FontName = "Times New Roman";
                                TRNo2.CharacterFormat.FontSize = 13;
                                TRNo2.CharacterFormat.Bold = true;
                                DataRowTongCong.Cells[2].SetCellWidth(30, CellWidthType.Percentage);

                                //4.Số tiền
                                DataRowTongCong.Cells[3].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                Paragraph pNo3 = DataRowTongCong.Cells[3].AddParagraph();
                                TextRange TRNo3 = pNo3.AppendText(giaiToaTaiKhoanViewModel.tongTienNo.ToString("#,###"));
                                pNo3.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
                                TRNo3.CharacterFormat.FontName = "Times New Roman";
                                TRNo3.CharacterFormat.FontSize = 13;
                                DataRowTongCong.Cells[3].SetCellWidth(40, CellWidthType.Percentage);
                                body.ChildObjects.Remove(paragraph);
                                body.ChildObjects.Insert(index, tableNo);

                                toTrinh_doc.SaveToFile(giaiToaTaiKhoanViewModel.ContainterFilePath + @"/Tờ trình.docx", FileFormat.Docx);
                                #endregion

                                #region Tạo file tổng hợp
                                Document fileTongHop = new Document();
                                fileTongHop.LoadFromFile(fileTongHop_pathFile);

                                fileTongHop.Replace("{{TENCONGTY}}", giaiToaTaiKhoanViewModel.TenCongTy, false, true);
                                fileTongHop.Replace("{{MST}}", giaiToaTaiKhoanViewModel.MaSoThue, false, true);
                                fileTongHop.Replace("{{TRUSO}}", giaiToaTaiKhoanViewModel.DiaChiTruSo, false, true);
                                fileTongHop.Replace("{{DIACHINHANTHONGBAO}}", giaiToaTaiKhoanViewModel.DiaChiNhanThongBao, false, true);
                                fileTongHop.Replace("{{DSNGANHANG}}", giaiToaTaiKhoanViewModel.DanhSachNganHang, false, true);
                                //fileTongHop.Replace("{{NGAY}}", giaiToaTaiKhoanViewModel.Ngay, false, true);
                                //fileTongHop.Replace("{{THANG}}", giaiToaTaiKhoanViewModel.Thang, false, true);
                                //fileTongHop.Replace("{{NAM}}", giaiToaTaiKhoanViewModel.Nam, false, true);
                                //fileTongHop.Replace("{{SOQĐ}}", giaiToaTaiKhoanViewModel.SoQuyetDinh, false, true);
                                fileTongHop.Replace("{{CCTHUE}}", thongTinDonVi.tenChiNhanh.ToUpper().Replace("QUẬN ", "Q.").Replace("HUYỆN ", "H.").Replace("KHU VỰC ", "KV."), false, true);

                                //fileTongHop.Replace("{{DATE}}", giaiToaTaiKhoanViewModel.Ngay + @"/" + giaiToaTaiKhoanViewModel.Thang + @"/" + giaiToaTaiKhoanViewModel.Nam, false, true);
                                fileTongHop.Replace("{{TONGTIENPHAT}}", giaiToaTaiKhoanViewModel.TongTienNoCCTK.ToString("#,###"), false, true);
                                fileTongHop.Replace("{{CCTHUE}}", thongTinDonVi.tenChiNhanh.ToUpper().Replace("QUẬN ", "Q.").Replace("HUYỆN ", "H.").Replace("KHU VỰC ", "KV."), false, true);
                                fileTongHop.Replace("{{TENCANBO}}", thongTinDonVi.tenCanBo, false, true);
                                fileTongHop.Replace("{{TENDOI}}", thongTinDonVi.tenDoi.ToUpper(), false, true);

                                //fileTongHop.Replace("{{DAY}}", createdDate.GetValue(0).ToString(), false, true);
                                //fileTongHop.Replace("{{MONTH}}", createdDate.GetValue(1).ToString(), false, true);
                                //fileTongHop.Replace("{{YEAR}}", createdDate.GetValue(2).ToString(), false, true);

                                // Tạo table
                                Section section1 = fileTongHop.Sections[0];
                                TextSelection selection1 = fileTongHop.FindString("{{TABLE}}", true, true);
                                TextRange range1 = selection1.GetAsOneRange();
                                Paragraph paragraph1 = range1.OwnerParagraph;
                                Body body1 = paragraph1.OwnerTextBody;
                                int index1 = body1.ChildObjects.IndexOf(paragraph1);
                                Table tableNo1 = section1.AddTable(true);
                                String[] header1 = new string[] { "STT", "TM", "Nội dung", "Số tiền (đ)" };

                                int sttCT1 = 0;
                                tableNo1.ResetCells(giaiToaTaiKhoanViewModel.listKhoanTienNo.Count + 2, header1.Length);
                                TableRow FRow1 = tableNo1.Rows[0];
                                FRow1.IsHeader = true;
                                for (int i = 0; i < header1.Length; i++)
                                {
                                    //Cell Alignment
                                    Paragraph p = FRow1.Cells[i].AddParagraph();
                                    FRow1.Cells[i].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                    p.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
                                    //Data Format
                                    TextRange TR = p.AppendText(header1[i]);
                                    TR.CharacterFormat.FontName = "Times New Roman";
                                    TR.CharacterFormat.Bold = true;
                                    TR.CharacterFormat.FontSize = 13;
                                    switch (i)
                                    {
                                        case 0:
                                            FRow1.Cells[i].SetCellWidth(10, CellWidthType.Percentage); break;
                                        case 1:
                                            FRow1.Cells[i].SetCellWidth(20, CellWidthType.Percentage); break;
                                        case 2:
                                            FRow1.Cells[i].SetCellWidth(30, CellWidthType.Percentage); break;
                                        case 3:
                                            FRow1.Cells[i].SetCellWidth(40, CellWidthType.Percentage); break;
                                    }
                                }

                                //Table rows data
                                for (int r = 0; r < giaiToaTaiKhoanViewModel.listKhoanTienNo.Count; r++)
                                {
                                    TableRow DataRow = tableNo1.Rows[r + 1];
                                    // 1. STT
                                    DataRow.Cells[0].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                    Paragraph p0 = DataRow.Cells[0].AddParagraph();
                                    TextRange TR0 = p0.AppendText((r + 1).ToString());
                                    p0.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
                                    TR0.CharacterFormat.FontName = "Times New Roman";
                                    TR0.CharacterFormat.FontSize = 13;
                                    DataRow.Cells[0].SetCellWidth(10, CellWidthType.Percentage);

                                    //2. Tiểu mục
                                    DataRow.Cells[1].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                    Paragraph p1 = DataRow.Cells[1].AddParagraph();
                                    TextRange TR1 = p1.AppendText("TM " + giaiToaTaiKhoanViewModel.listKhoanTienNo[r].TieuMuc);
                                    p1.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
                                    TR1.CharacterFormat.FontName = "Times New Roman";
                                    TR1.CharacterFormat.FontSize = 13;
                                    DataRow.Cells[1].SetCellWidth(20, CellWidthType.Percentage);

                                    //3. Nội dung
                                    DataRow.Cells[2].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                    Paragraph p2 = DataRow.Cells[2].AddParagraph();
                                    TextRange TR2 = p2.AppendText(Get_NoiDungTheoTieuMuc(giaiToaTaiKhoanViewModel.listKhoanTienNo[r].TieuMuc));
                                    p2.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
                                    TR2.CharacterFormat.FontName = "Times New Roman";
                                    TR2.CharacterFormat.FontSize = 13;
                                    DataRow.Cells[2].SetCellWidth(30, CellWidthType.Percentage);

                                    //4.Số tiền
                                    DataRow.Cells[3].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                    Paragraph p3 = DataRow.Cells[3].AddParagraph();
                                    TextRange TR3 = p3.AppendText(giaiToaTaiKhoanViewModel.listKhoanTienNo[r].SoTienTheoSo.ToString("#,###"));
                                    p3.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
                                    TR3.CharacterFormat.FontName = "Times New Roman";
                                    TR3.CharacterFormat.FontSize = 13;
                                    DataRow.Cells[3].SetCellWidth(40, CellWidthType.Percentage);
                                }

                                //Số tiền tổng cộng
                                TableRow DataRowTongCong1 = tableNo1.Rows[giaiToaTaiKhoanViewModel.listKhoanTienNo.Count + 1];
                                ////1. STT
                                //DataRowTongCong.Cells[0].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                //Paragraph pNo0 = DataRowTongCong.Cells[0].AddParagraph();
                                //TextRange TRNo0 = pNo0.AppendText("");
                                //pNo0.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
                                //TRNo0.CharacterFormat.FontName = "Times New Roman";
                                //TRNo0.CharacterFormat.FontSize = 13;
                                //DataRowTongCong.Cells[0].SetCellWidth(10, CellWidthType.Percentage);
                                ////2. Tiểu mục
                                //DataRowTongCong.Cells[1].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                //Paragraph pNo1 = DataRowTongCong.Cells[1].AddParagraph();
                                //TextRange TRNo1 = pNo1.AppendText("");
                                //pNo1.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
                                //TRNo1.CharacterFormat.FontName = "Times New Roman";
                                //TRNo1.CharacterFormat.FontSize = 13;
                                //DataRowTongCong.Cells[1].SetCellWidth(20, CellWidthType.Percentage);
                                //3. Nội dung 
                                DataRowTongCong1.Cells[2].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                Paragraph pNo21 = DataRowTongCong1.Cells[2].AddParagraph();
                                TextRange TRNo21 = pNo21.AppendText("Tổng cộng");
                                pNo21.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
                                TRNo21.CharacterFormat.FontName = "Times New Roman";
                                TRNo21.CharacterFormat.FontSize = 13;
                                TRNo21.CharacterFormat.Bold = true;
                                DataRowTongCong1.Cells[2].SetCellWidth(30, CellWidthType.Percentage);

                                //4.Số tiền
                                DataRowTongCong1.Cells[3].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                Paragraph pNo31 = DataRowTongCong1.Cells[3].AddParagraph();
                                TextRange TRNo31 = pNo31.AppendText(giaiToaTaiKhoanViewModel.tongTienNo.ToString("#,###"));
                                pNo31.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
                                TRNo31.CharacterFormat.FontName = "Times New Roman";
                                TRNo31.CharacterFormat.FontSize = 13;
                                DataRowTongCong1.Cells[3].SetCellWidth(40, CellWidthType.Percentage);
                                body1.ChildObjects.Remove(paragraph1);
                                body1.ChildObjects.Insert(index1, tableNo1);

                                fileTongHop.SaveToFile(giaiToaTaiKhoanViewModel.ContainterFilePath + @"/File tổng hợp.docx", FileFormat.Docx);

                                #endregion

                                MessageBox.Show("Tạo báo cáo thành công !", "Thông báo");
                            }
                        }
                        #endregion
                    }
                }
            }
        }

        public static void CreateNotifiction(string MST, string chicuc)
        {
            try
            {
                
                if (!File.Exists(patermNotification3Day))
                {
                    Common.Common.error("Không tìm thấy file biểu mẫu cho thông báo 3 ngày! Vui lòng thêm tài liệu mẫu!");
                    return;
                }
                if (!File.Exists(patermNotification5Day))
                {
                    Common.Common.error("Không tìm thấy file biểu mẫu cho thông báo 5 ngày! Vui lòng thêm tài liệu mẫu!");
                    return;
                }

                var fileNguonStatusModel = FileNguonStatusController.get_FileNguonStatus();

                if (!fileNguonStatusModel.CC_S12)
                {
                    Common.Common.error("Không tìm thấy tài liệu S12.XLSX! Vui lòng nhập tài liệu");
                }
                else if (!fileNguonStatusModel.CC_ChungTu)
                {
                    Common.Common.error("Không tìm thấy file Chứng từ ! Vui lòng nhập tài liệu");
                }
                else if (!fileNguonStatusModel.CC_DanhBa)
                {
                    Common.Common.error("Không tìm thấy file Danh bạ ! Vui lòng nhập tài liệu");
                }
                else if (!fileNguonStatusModel.CC_TaiKhoanNganHang)
                {
                    Common.Common.error("Không tìm thấy file Tài khoản ngân hàng ! Vui lòng nhập tài liệu");
                }
                else if (!fileNguonStatusModel.dsTheoDoiCCTK)
                {
                    Common.Common.error("Không tìm thấy file Danh sách theo dõi Cưỡng chế tài khoản ! Vui lòng nhập tài liệu");
                }
                else if (!fileNguonStatusModel.cctkTMS)
                {
                    Common.Common.error("Không tìm thấy file Cưỡng chế tài khoản trên TMS ! Vui lòng nhập tài liệu");
                }
                else
                {
                    var s12FileContent = S12Controller.SearchMST(S12_filePath, MST);
                    if (s12FileContent.Rows.Count <= 0)
                    {
                        Common.Common.error("Không tìm thấy dữ liệu!");
                    }
                    else
                    {
                        string timeFolder = S12Controller.SearchMonthReport(S12_filePath);
                        var containerFilePath = dirReport + @"\" + timeFolder + @"\" + s12FileContent.Rows[0]["F2"].ToString() + @"\Giải tỏa tài khoản\";
                        if (!Directory.Exists(containerFilePath))
                        {
                            Directory.CreateDirectory(containerFilePath);
                        }
                        var tableDanhba = DanhBaController.GetDanhba(danhBa_filePath, s12FileContent.Rows[0]["F2"].ToString());
                        if (tableDanhba.Rows.Count <= 0)
                        {
                            Common.Common.error("Không tìm thấy thông tin công ty trong Danh bạ. Vui lòng kiểm tra lại!");
                        }
                        else
                        {
                            string pathReportNotification3Day = containerFilePath + ConfigurationManager.AppSettings["thongbao3ngay"];
                            string pathReportNotification5Day = containerFilePath + ConfigurationManager.AppSettings["thongbao5ngay"];
                            if (File.Exists(pathReportNotification3Day))
                            {
                                File.Delete(pathReportNotification3Day);
                            }
                            if (File.Exists(pathReportNotification5Day))
                            {
                                File.Delete(pathReportNotification5Day);
                            }


                            #region Add Model
                            ThongBaoViewModel tb = new ThongBaoViewModel();
                            tb.chicuc = chicuc;
                            tb.tennnt = s12FileContent.Rows[0]["F3"].ToString();
                            tb.mst = s12FileContent.Rows[0]["F2"].ToString();
                            tb.diachinhanthongbao = tableDanhba.Rows[0][7].ToString()
                                                    + ", " + tableDanhba.Rows[0][9].ToString()
                                                    + ", " + tableDanhba.Rows[0][11].ToString()
                                                    + ", " + tableDanhba.Rows[0][13].ToString();
                            tb.diachitruso = tableDanhba.Rows[0][7].ToString()
                                                    + ", " + tableDanhba.Rows[0][9].ToString()
                                                    + ", " + tableDanhba.Rows[0][11].ToString()
                                                    + ", " + tableDanhba.Rows[0][13].ToString();
                            // Tính toán tổng nợ
                            decimal tongtiennop = 0;
                            decimal sotiennop = 0;
                            decimal notren90ngay = 0;
                            foreach (DataRow rowS12 in s12FileContent.Rows)
                            {
                                decimal notren91ngay = 0;
                                var tableLicense = ChungTuController.GetListChungTu(chungtu_filePath, rowS12[1].ToString(), rowS12[5].ToString()); // 1: MST; 2: Tiểu mục
                                decimal col11 = !string.IsNullOrWhiteSpace(rowS12[10].ToString()) ? Convert.ToDecimal(rowS12[10].ToString()) : 0;
                                decimal col12 = !string.IsNullOrWhiteSpace(rowS12[11].ToString()) ? Convert.ToDecimal(rowS12[11].ToString()) : 0;
                                notren90ngay = col11 + col12;
                                if (tableLicense.Rows.Count > 0)
                                {
                                    sotiennop = !string.IsNullOrWhiteSpace(tableLicense.Rows[0][2].ToString()) ? Convert.ToDecimal(tableLicense.Rows[0][2].ToString()) : 0;
                                    tongtiennop = tongtiennop + sotiennop;
                                }
                                if (notren90ngay > 0)
                                {
                                    notren91ngay = Math.Abs(notren90ngay - sotiennop);
                                    tb.tongno = tb.tongno + notren91ngay;
                                }
                            }
                            #endregion

                            #region Đọc file
                            Document docNotify3Day = new Document();
                            docNotify3Day.LoadFromFile(patermNotification3Day);
                            // Xử lý tờ trình có hoá đơn
                            Document docNotify5Day = new Document();
                            docNotify5Day.LoadFromFile(patermNotification3Day);
                            #endregion

                            #region Replace
                            // Thống báo 3 ngày
                            docNotify3Day.Replace("{{CHICUC}}", tb.chicuc.ToUpper().Replace("HUYỆN", "H.").Replace("QUẬN", "Q.").Replace("KHU VỰC", "KV."), false, true);
                            docNotify3Day.Replace("{{TENNNT}}", tb.tennnt, false, true);
                            docNotify3Day.Replace("{{MST}}", tb.mst, false, true);
                            docNotify3Day.Replace("{{DIACHINHANTHU}}", tb.diachinhanthongbao, false, true);
                            docNotify3Day.Replace("{{DIACHITRUSO}}", tb.diachitruso, false, true);
                            docNotify3Day.Replace("{{TONGNO}}", tb.tongno > 0 ? tb.tongno.ToString("#,###") : "0", false, true);
                            // Thông báo 5 ngày
                            docNotify5Day.Replace("{{CHICUC}}", tb.chicuc.ToUpper().Replace("HUYỆN", "H.").Replace("QUẬN", "Q.").Replace("KHU VỰC", "KV."), false, true);
                            docNotify3Day.Replace("{{TENNNT}}", tb.tennnt, false, true);
                            docNotify3Day.Replace("{{MST}}", tb.mst, false, true);
                            docNotify3Day.Replace("{{DIACHINHANTHU}}", tb.diachinhanthongbao, false, true);
                            docNotify3Day.Replace("{{DIACHITRUSO}}", tb.diachitruso, false, true);
                            #endregion

                            #region Save file
                            // Save: Thông báo 3 ngày
                            docNotify3Day.SaveToFile(pathReportNotification3Day, FileFormat.Doc);
                            // Save: Thông báo 5 ngày
                            docNotify5Day.SaveToFile(pathReportNotification5Day, FileFormat.Doc);
                            #endregion

                            Common.Common.success("Tạo thông báo thành công!");
                        }

                    }
                }
            }
            catch (Exception)
            {
                Common.Common.error("Lỗi hệ thống! Vui lòng kiểm tra lại!");
            }
            
        }
    }
}
