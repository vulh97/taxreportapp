﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxReportApp.Controller
{
    class S14Controller : BaseController
    {
        public static DataTable GetDataS14FileByMST(string pathFileS14, string textMST)
        {
            try
            {
                string sLine = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=";
                sLine += pathFileS14;
                sLine += ";Extended Properties=Excel 8.0";
                using (var connection = BaseController.getConnection(sLine))
                {
                    connection.Open();
                    DataTable dt = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    string Sheet = dt.Rows[0].Field<string>("TABLE_NAME");
                    sLine = "Select * From [" + Sheet + "] where [F2] = @mst";
                    OleDbDataAdapter dataAdapter = new OleDbDataAdapter(sLine, connection);
                    dataAdapter.SelectCommand.Parameters.AddWithValue("@mst", textMST);
                    DataSet dataSet = new DataSet();
                    dataAdapter.Fill(dataSet);
                    connection.Close();
                    return dataSet.Tables[0];
                }
            }
            catch (Exception ex)
            {
                Common.Common.error(ex.ToString());
                return null;
            }
        }
    }
}
