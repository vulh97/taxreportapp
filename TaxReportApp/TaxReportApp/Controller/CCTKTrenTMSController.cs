﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaxReportApp.Controller
{
    class CCTKTrenTMSController : BaseController
    {
        public static DataTable GetContent(string sFilePathAndName, string textSearch)
        {
            string sLine = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=";
            sLine += sFilePathAndName;
            sLine += ";Extended Properties=Excel 8.0";
            var connection = BaseController.getConnection(sLine);
            try
            {
                connection.Open();
                DataTable dt = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string Sheet = dt.Rows[0].Field<string>("TABLE_NAME");
                sLine = "Select * From [" + Sheet + "] Where [Mã số thuế] = @mst";
                OleDbDataAdapter dataAdapter = new OleDbDataAdapter(sLine, connection);
                dataAdapter.SelectCommand.Parameters.AddWithValue("@mst", textSearch);
                DataSet dataSet = new DataSet();
                dataAdapter.Fill(dataSet);
                return dataSet.Tables[0];
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Lỗi");
                return null;
            }
            finally
            {
                connection.Close();
            }

        }
    }
}
