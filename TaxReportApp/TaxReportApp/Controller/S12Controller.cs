﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TaxReportApp.Common;

namespace TaxReportApp.Controller
{
    class S12Controller : BaseController
    {
        public static DataTable SearchAll(string sFilePathAndName)
        {
            try
            {
                string sLine = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=";
                sLine += sFilePathAndName;
                sLine += ";Extended Properties=Excel 8.0";
                using (var connection = BaseController.getConnection(sLine))
                {
                    connection.Open();
                    DataTable dt = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    string Sheet = dt.Rows[0].Field<string>("TABLE_NAME");
                    sLine = "Select * From [" + Sheet + "] where [F2] IS NOT NULL AND [F2] <> @headerName";
                    OleDbDataAdapter dataAdapter = new OleDbDataAdapter(sLine, connection);
                    dataAdapter.SelectCommand.Parameters.AddWithValue("@headerName", "Mã số thuế");
                    DataSet dataSet = new DataSet();
                    dataAdapter.Fill(dataSet);
                    connection.Close();
                    return dataSet.Tables[0];
                }
            }
            catch (Exception ex)
            {
                Common.Common.error(ex.ToString());
                return null;
            }
            
        }
        public static DataTable SearchMST(string sFilePathAndName, string textSearch)
        {
            string sLine = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=";
            sLine += sFilePathAndName;
            sLine += ";Extended Properties=Excel 8.0";
            var connection = BaseController.getConnection(sLine);
            try
            { 
                connection.Open();
                DataTable dt = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string Sheet = dt.Rows[0].Field<string>("TABLE_NAME");
                sLine = "Select top 1 * From [" + Sheet + "]";
                OleDbDataAdapter dataAdapter = new OleDbDataAdapter(sLine, connection);
                DataSet dataSet = new DataSet();
                dataAdapter.Fill(dataSet);
                // [F2, F3]: Column Name
                string sLine2 = "Select * From [" + Sheet + "] Where [" + dataSet.Tables[0].Columns[1].ColumnName.ToString() + "] = @mst";
                OleDbDataAdapter dataAdapter2 = new OleDbDataAdapter(sLine2, connection);
                dataAdapter2.SelectCommand.Parameters.AddWithValue("@mst", textSearch);
                DataSet dataSet2 = new DataSet();
                dataAdapter2.Fill(dataSet2);
                return dataSet2.Tables[0];
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Lỗi");
                return null;
            }
            finally
            {
                connection.Close();
            }
        }
        public static string SearchMonthReport(string sFilePathAndName)
        {
            string sLine = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=";
            sLine += sFilePathAndName;
            sLine += ";Extended Properties=Excel 8.0";
            var connection = BaseController.getConnection(sLine);
            try
            {
                connection.Open();
                DataTable dt = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string Sheet = dt.Rows[0].Field<string>("TABLE_NAME");
                sLine = "Select top 1 * From [" + Sheet + "]";
                OleDbDataAdapter dataAdapter = new OleDbDataAdapter(sLine, connection);
                DataSet dataSet = new DataSet();
                dataAdapter.Fill(dataSet);
                string sLine2 = "Select [" + dataSet.Tables[0].Columns[0].ColumnName.ToString() + "] From [" + Sheet + "] where [" + dataSet.Tables[0].Columns[0].ColumnName.ToString() + "] like 'Kỳ báo cáo%'";
                OleDbDataAdapter dataAdapter2 = new OleDbDataAdapter(sLine2, connection);
                DataSet dataSet2 = new DataSet();
                dataAdapter2.Fill(dataSet2);
                var data2 = dataSet2.Tables[0].Rows[0][dataSet.Tables[0].Columns[0].ColumnName.ToString()].ToString().Replace("Kỳ báo cáo: Tháng ", "").Replace(" năm ", "");
                return data2;
            }
            catch (Exception ex)
            {
                Common.Common.error(ex.ToString());
                return "";
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
