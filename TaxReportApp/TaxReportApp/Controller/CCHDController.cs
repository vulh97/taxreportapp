﻿using Microsoft.Office.Interop.Excel;
using Spire.Doc;
using Spire.Doc.Documents;
using Spire.Doc.Fields;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxReportApp.Model;

namespace TaxReportApp.Controller
{
    class CCHDController : BaseController
    {
        public static string dir = Directory.GetCurrentDirectory().ToString() + @"\" + ConfigurationManager.AppSettings["directoryDatabase"];
        public static string dirReport = System.Windows.Forms.Application.StartupPath + @"\" + ConfigurationManager.AppSettings["ReportFile"];
        private static string pathS12File = dir + @"\" + ConfigurationManager.AppSettings["s12File"];
        private static string pathDanhba = dir + @"\" + ConfigurationManager.AppSettings["phoneBookFile"];
        private static string pathChungtu = dir + @"\" + ConfigurationManager.AppSettings["licenseFile"];
        private static string pathTaikhoan = dir + @"\" + ConfigurationManager.AppSettings["accountBankFile"];
        private static string pathCCHDTMS = dir + @"\" + ConfigurationManager.AppSettings["cchd_tren_tms"];
        private static string pathS14File = dir + @"\" + ConfigurationManager.AppSettings["s14File"];
        private static string qdcohoadon = System.Windows.Forms.Application.StartupPath + @"\documents\input\CCHD\QuyetDinh_ThongBao_ToTrinh\CC_co_hoadon\QD_TB_cohoadon.doc";
        private static string ttcohoadon = System.Windows.Forms.Application.StartupPath + @"\documents\input\CCHD\QuyetDinh_ThongBao_ToTrinh\CC_co_hoadon\To_trinh_cohoadon.doc";
        private static string qdkhonghoadon = System.Windows.Forms.Application.StartupPath + @"\documents\input\CCHD\QuyetDinh_ThongBao_ToTrinh\CC_khongco_hoadon\QD_TB_khonghoadon.doc";
        private static string ttkhonghoadon = System.Windows.Forms.Application.StartupPath + @"\documents\input\CCHD\QuyetDinh_ThongBao_ToTrinh\CC_khongco_hoadon\To_trinh_khonghoadon.doc";
        public static System.Data.DataTable GetCCHDOnTMSByTaxCode(string sFilePathAndName, string searchMST)
        {
            string sLine = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=";
            sLine += sFilePathAndName;
            sLine += ";Extended Properties=Excel 8.0";
            var connection = BaseController.getConnection(sLine);
            try
            {
                connection.Open();
                //DataTable dt = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                //string Sheet = dt.Rows[1].Field<string>("TABLE_NAME");
                sLine = "Select top 1 * From [CCHĐ$] where [Mã số thuế] = @mst";
                OleDbDataAdapter dataAdapter = new OleDbDataAdapter(sLine, connection);
                dataAdapter.SelectCommand.Parameters.AddWithValue("@mst", searchMST);
                DataSet dataSet = new DataSet();
                dataAdapter.Fill(dataSet);
                return dataSet.Tables[0];
            }
            catch (Exception ex)
            {
                //Common.Common.error("Có lỗi xảy ra khi kiểm tra dữ liệu đã tồn tại trong chứng từ đã nộp! Vui lòng kiểm tra lại!");
                Common.Common.error(ex.ToString());
                connection.Close();
                return null;
            }
            finally {
                connection.Close();
            }
        }
        public static bool CCHD_CreateReport(string textSearchMST, string chinhanh, string tencb, string doitruong, string chicuctruong, string chicucpho, DateTime ngaycohieuluc, DateTime ngayhethieuluc)
        {
            try
            {
                if (!File.Exists(pathS12File))
                {
                    Common.Common.error("Không tìm thấy tài liệu S12.XLSX! Vui lòng nhập tài liệu");
                    return false;
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(textSearchMST))
                    {
                        Common.Common.error("Vui lòng nhập mã số thuế!");
                        return false;
                    }
                    else
                    {
                        var tableS12Data = S12Controller.SearchMST(pathS12File, textSearchMST.Trim());
                        if (tableS12Data.Rows.Count <= 0)
                        {
                            Common.Common.error("Không tìm thấy dữ liệu trong file S12!");
                            return false;
                        }
                        else
                        {
                            var tableDanhba = DanhBaController.GetDanhba(pathDanhba, tableS12Data.Rows[0][1].ToString());
                            // Get Folder Time
                            var timeFolder = S12Controller.SearchMonthReport(pathS12File);
                            // Get Directory: Bao Cao/062020/Công ty .../CCTK
                            var path = dirReport + @"\" + timeFolder + @"\" + tableS12Data.Rows[0][1].ToString() + @"\CCHD";
                            // Check Cty có nằm trong bảng CCHD trên TMS
                            var tableCCHDTMS = CCHDController.GetCCHDOnTMSByTaxCode(pathCCHDTMS, tableS12Data.Rows[0]["F2"].ToString());
                            // Check file s14
                            var tableS14Data = S14Controller.GetDataS14FileByMST(pathS14File, tableS12Data.Rows[0]["F2"].ToString());
                            // Check Cty có nằm trong bảng Chứng từ đã nộp
                            var tableCTDN = ChungTuController.GetListChungTu(pathChungtu, tableS12Data.Rows[0]["F2"].ToString(), tableS12Data.Rows[0][5].ToString());

                            if (tableS14Data.Rows.Count <= 0)
                            {
                                Common.Common.error("Không tồn tại dữ liệu trong file S14!");
                                return false;
                            } else
                            {
                                if (tableCCHDTMS.Rows.Count > 0)
                                {
                                    #region Kiểm tra các file có tồn tại: Có => Delete
                                    if (!Directory.Exists(path))
                                    {
                                        Directory.CreateDirectory(path);
                                    }
                                    string fileQDCoHD = path + @"\" + ConfigurationManager.AppSettings["cuongchecohoadon"];
                                    string fileTTCoHD = path + @"\" + ConfigurationManager.AppSettings["totrinhcohoadon"];
                                    string fileQDKhongHD = path + @"\" + ConfigurationManager.AppSettings["cuongchekhonghoadon"];
                                    string fileTTKhongHD = path + @"\" + ConfigurationManager.AppSettings["totrinhkhonghoadon"];
                                    string fileNotification3Day = path + @"\" + ConfigurationManager.AppSettings["thongbao3ngay"];
                                    string fileNotification5Day = path + @"\" + ConfigurationManager.AppSettings["thongbao5ngay"];
                                    if (File.Exists(fileQDCoHD))
                                    {
                                        File.Delete(fileQDCoHD);
                                    }
                                    if (File.Exists(fileTTCoHD))
                                    {
                                        File.Delete(fileTTCoHD);
                                    }
                                    if (File.Exists(fileQDKhongHD))
                                    {
                                        File.Delete(fileQDKhongHD);
                                    }
                                    if (File.Exists(fileTTKhongHD))
                                    {
                                        File.Delete(fileTTKhongHD);
                                    }
                                    if (File.Exists(fileNotification3Day))
                                    {
                                        File.Delete(fileNotification3Day);
                                    }
                                    if (File.Exists(fileNotification5Day))
                                    {
                                        File.Delete(fileNotification5Day);
                                    }
                                    #endregion

                                    #region [hoa.qn] Replace Text
                                    // Xử lý QĐ+TB có hoá đơn
                                    Document docQDCoHD = new Document();
                                    docQDCoHD.LoadFromFile(qdcohoadon);
                                    // Xử lý tờ trình có hoá đơn
                                    Document docTTCoHD = new Document();
                                    docTTCoHD.LoadFromFile(ttcohoadon);
                                    //  Xử lý QĐ+TB không có hoá đơn
                                    Document docQDKhongHD = new Document();
                                    docQDKhongHD.LoadFromFile(qdkhonghoadon);
                                    // Xử lý tờ trình không có hoá đơn
                                    Document docTTKhongHD = new Document();
                                    docTTKhongHD.LoadFromFile(ttkhonghoadon);
                                    CCHDModel cchdModel = new CCHDModel();
                                    if (tableDanhba.Rows.Count <= 0)
                                    {
                                        Common.Common.error("Không tìm thấy dữ liệu tương ứng trong file danh bạ !");
                                        return false;
                                    }
                                    else
                                    {
                                        int sttCT = 0;
                                        foreach (DataRow rowS12 in tableS12Data.Rows)
                                        {
                                            var thue = new Thue();
                                            sttCT++;
                                            thue.stt = sttCT;
                                            thue.tieumuc = rowS12[5].ToString();
                                            #region Xử lý loại thuế
                                            if (Array.IndexOf(new string[] { "1001", "1004" }, thue.tieumuc) > -1)
                                            {
                                                thue.loaithue = "Thuế TNCN";
                                            }
                                            else if (Array.IndexOf(new string[] { "1052", "1053" }, thue.tieumuc) > -1)
                                            {
                                                thue.loaithue = "Thuế TNDN";
                                            }
                                            else if (Array.IndexOf(new string[] { "1701" }, thue.tieumuc) > -1)
                                            {
                                                thue.loaithue = "Thuế GTGT";
                                            }
                                            else if (Array.IndexOf(new string[] { "1757" }, thue.tieumuc) > -1)
                                            {
                                                thue.loaithue = "Thuế TTĐB";
                                            }
                                            else if (Array.IndexOf(new string[] { "2862", "2863", "2864" }, thue.tieumuc) > -1)
                                            {
                                                thue.loaithue = "Lệ phí MB";
                                            }
                                            else if (Array.IndexOf(new string[] { "4254", "4268" }, thue.tieumuc) > -1)
                                            {
                                                thue.loaithue = "Tiền phạt";
                                            }
                                            else if (Array.IndexOf(new string[] { "4272", "4917", "4918", "4931", "4944" }, thue.tieumuc) > -1)
                                            {
                                                thue.loaithue = "Tiền chậm nộp";
                                            }
                                            else
                                            {
                                                thue.loaithue = "";
                                            }
                                            #endregion
                                            thue.kythue = "";
                                            // [Tiểu mục]: Bảng Chứng từ
                                            decimal col11 = !string.IsNullOrWhiteSpace(rowS12[10].ToString()) ? Convert.ToDecimal(rowS12[10].ToString()) : 0;
                                            decimal col12 = !string.IsNullOrWhiteSpace(rowS12[11].ToString()) ? Convert.ToDecimal(rowS12[11].ToString()) : 0;
                                            thue.notren90ngay = col11 + col12;
                                            cchdModel.tongtientren90ngay = cchdModel.tongtientren90ngay + thue.notren90ngay;

                                            if (thue.notren90ngay > 0)
                                            {
                                                var tableLicense = ChungTuController.GetListChungTu(pathChungtu, rowS12[1].ToString(), rowS12[5].ToString()); // 1: MST; 2: Tiểu mục
                                                if (tableLicense.Rows.Count > 0)
                                                {
                                                    thue.sotiennop = !string.IsNullOrWhiteSpace(tableLicense.Rows[0][2].ToString()) ? Convert.ToDecimal(tableLicense.Rows[0][2].ToString()) : 0;
                                                    cchdModel.tongsonop = cchdModel.tongsonop + thue.sotiennop;
                                                }
                                                thue.notren91ngaycuongche = thue.notren90ngay > thue.sotiennop ? Math.Abs(thue.notren90ngay - thue.sotiennop) : 0;
                                                cchdModel.tongtiencuongche = cchdModel.tongtiencuongche + thue.notren91ngaycuongche;
                                                cchdModel.listThue.Add(thue);
                                            }
                                        }

                                        #region Add Model
                                        cchdModel.chicuc = !string.IsNullOrWhiteSpace(chinhanh) ? chinhanh : "";
                                        cchdModel.thongbaoso = tableS14Data.Rows.Count > 0 ? tableS14Data.Rows[0][3].ToString() : "";
                                        cchdModel.ngaythongbao = tableS14Data.Rows.Count > 0 ? tableS14Data.Rows[0][4].ToString() : "";
                                        cchdModel.tennnt = tableDanhba.Rows[0][1].ToString();
                                        cchdModel.dctbt_diachi = tableDanhba.Rows[0][7].ToString();
                                        cchdModel.dctbt_phuongxa = tableDanhba.Rows[0][9].ToString();
                                        cchdModel.dctbt_quanhuyen = tableDanhba.Rows[0][11].ToString();
                                        cchdModel.dctbt_tinhthanhpho = tableDanhba.Rows[0][13].ToString();
                                        cchdModel.dcts_diachi = tableDanhba.Rows[0][7].ToString();
                                        cchdModel.dcts_phuongxa = tableDanhba.Rows[0][9].ToString();
                                        cchdModel.dcts_quanhuyen = tableDanhba.Rows[0][11].ToString();
                                        cchdModel.dcts_tinhthanhpho = tableDanhba.Rows[0][13].ToString();
                                        cchdModel.mst = tableDanhba.Rows[0][0].ToString();
                                        cchdModel.sodangkykd = tableDanhba.Rows[0][29].ToString();
                                        cchdModel.ngaycapdangkykd = tableDanhba.Rows[0][30].ToString();
                                        cchdModel.ngaycohieuluc = ngaycohieuluc;
                                        cchdModel.ngayhethieuluc = ngayhethieuluc;
                                        cchdModel.nganhnghekd = tableDanhba.Rows[0][3].ToString();
                                        cchdModel.ngayqdcctk = tableCCHDTMS.Rows.Count > 0 && tableDanhba.Rows[0][40].ToString() != "" ? tableDanhba.Rows[0][40].ToString() : DateTime.Now.ToString("dd/MM/yyyy");
                                        cchdModel.soqdcctk = tableCCHDTMS.Rows.Count > 0 ? tableCCHDTMS.Rows[0]["Số QĐ cưỡng chế"].ToString() : "";
                                        cchdModel.quy = "Quý 3";
                                        cchdModel.canbo = !string.IsNullOrWhiteSpace(tencb) ? tencb : "";
                                        cchdModel.chicuctruong = !string.IsNullOrWhiteSpace(chicuctruong) ? chicuctruong : "";
                                        cchdModel.chicucpho = !string.IsNullOrWhiteSpace(chicucpho) ? chicucpho : "";
                                        cchdModel.doitruong = !string.IsNullOrWhiteSpace(doitruong) ? doitruong : "";
                                        #endregion

                                        #region Replace
                                        // QĐ+TB có hoá đơn
                                        docQDCoHD.Replace("{{CHICUC}}", cchdModel.chicuc.ToUpper().Replace("HUYỆN", "H.").Replace("QUẬN", "Q.").Replace("KHU VỰC", "KV."), false, true);
                                        docQDCoHD.Replace("{{THONGBAOSO}}", cchdModel.thongbaoso, false, true);
                                        docQDCoHD.Replace("{{NGAYTHONGBAO}}", cchdModel.ngaythongbao, false, true);
                                        docQDCoHD.Replace("{{chicuc}}", cchdModel.chicuc, false, true);
                                        docQDCoHD.Replace("{{TENNNT}}", cchdModel.tennnt, false, true);
                                        docQDCoHD.Replace("{{DCTBT_DIACHI}}", cchdModel.dctbt_diachi, false, true);
                                        docQDCoHD.Replace("{{DCTBT_PHUONGXA}}", cchdModel.dctbt_phuongxa, false, true);
                                        docQDCoHD.Replace("{{DCTBT_QUANHUYEN}}", cchdModel.dctbt_quanhuyen, false, true);
                                        docQDCoHD.Replace("{{DCTBT_TINHTHANHPHO}}", cchdModel.dctbt_tinhthanhpho, false, true);
                                        docQDCoHD.Replace("{{DCTS_DIACHI}}", cchdModel.dcts_diachi, false, true);
                                        docQDCoHD.Replace("{{DCTS_PHUONGXA}}", cchdModel.dcts_phuongxa, false, true);
                                        docQDCoHD.Replace("{{DCTS_QUANHUYEN}}", cchdModel.dcts_quanhuyen, false, true);
                                        docQDCoHD.Replace("{{DCTS_TINHTHANHPHO}}", cchdModel.dcts_tinhthanhpho, false, true);
                                        docQDCoHD.Replace("{{NGANHNGHEKD}}", cchdModel.nganhnghekd, false, true);
                                        docQDCoHD.Replace("{{NGAYCOHIEULUC}}", cchdModel.ngaycohieuluc.ToString("dd/MM/yyyy"), false, true);
                                        docQDCoHD.Replace("{{NGAYHETHIEULUC}}", cchdModel.ngayhethieuluc.ToString("dd/MM/yyyy"), false, true);
                                        docQDCoHD.Replace("{{SODANGKYKD}}", cchdModel.sodangkykd, false, true);
                                        docQDCoHD.Replace("{{NGAYCAPDANGKYKD}}", cchdModel.ngaycapdangkykd, false, true);
                                        docQDCoHD.Replace("{{MST}}", cchdModel.mst, false, true);
                                        docQDCoHD.Replace("({{NGAYCOHIEULUCDANGTEXT}}", "ngày " + cchdModel.ngaycohieuluc.Day + " tháng " + cchdModel.ngaycohieuluc.Month + " năm " + cchdModel.ngaycohieuluc.Year, false, true);
                                        docQDCoHD.Replace("{{CHICUCTRUONG}}", cchdModel.chicuctruong, false, true);
                                        // QĐ + TB không hoá đơn
                                        docQDKhongHD.Replace("{{CHICUC}}", cchdModel.chicuc.ToUpper().Replace("HUYỆN", "H.").Replace("QUẬN", "Q.").Replace("KHU VỰC", "KV."), false, true);
                                        docQDKhongHD.Replace("{{THONGBAOSO}}", cchdModel.thongbaoso, false, true);
                                        docQDKhongHD.Replace("{{NGAYTHONGBAO}}", cchdModel.ngaythongbao, false, true);
                                        docQDKhongHD.Replace("{{chicuc}}", cchdModel.chicuc, false, true);
                                        docQDKhongHD.Replace("{{TENNNT}}", cchdModel.tennnt, false, true);
                                        docQDKhongHD.Replace("{{DCTBT_DIACHI}}", cchdModel.dctbt_diachi, false, true);
                                        docQDKhongHD.Replace("{{DCTBT_PHUONGXA}}", cchdModel.dctbt_phuongxa, false, true);
                                        docQDKhongHD.Replace("{{DCTBT_QUANHUYEN}}", cchdModel.dctbt_quanhuyen, false, true);
                                        docQDKhongHD.Replace("{{DCTBT_TINHTHANHPHO}}", cchdModel.dctbt_tinhthanhpho, false, true);
                                        docQDKhongHD.Replace("{{DCTS_DIACHI}}", cchdModel.dcts_diachi, false, true);
                                        docQDKhongHD.Replace("{{DCTS_PHUONGXA}}", cchdModel.dcts_phuongxa, false, true);
                                        docQDKhongHD.Replace("{{DCTS_QUANHUYEN}}", cchdModel.dcts_quanhuyen, false, true);
                                        docQDKhongHD.Replace("{{DCTS_TINHTHANHPHO}}", cchdModel.dcts_tinhthanhpho, false, true);
                                        docQDKhongHD.Replace("{{NGANHNGHEKD}}", cchdModel.nganhnghekd, false, true);
                                        docQDKhongHD.Replace("{{NGAYCOHIEULUC}}", cchdModel.ngaycohieuluc.ToString("dd/MM/yyyy"), false, true);
                                        docQDKhongHD.Replace("{{NGAYHETHIEULUC}}", cchdModel.ngayhethieuluc.ToString("dd/MM/yyyy"), false, true);
                                        docQDKhongHD.Replace("{{SODANGKYKD}}", cchdModel.sodangkykd, false, true);
                                        docQDKhongHD.Replace("{{NGAYCAPDANGKYKD}}", cchdModel.ngaycapdangkykd, false, true);
                                        docQDKhongHD.Replace("{{MST}}", cchdModel.mst, false, true);
                                        docQDKhongHD.Replace("({{NGAYCOHIEULUCDANGTEXT}}", "ngày " + cchdModel.ngaycohieuluc.Day + " tháng " + cchdModel.ngaycohieuluc.Month + " năm " + cchdModel.ngaycohieuluc.Year, false, true);
                                        docQDKhongHD.Replace("{{CHICUCTRUONG}}", cchdModel.chicuctruong, false, true);
                                        // Tờ trình có hoá đơn
                                        docTTCoHD.Replace("{{CHICUC}}", cchdModel.chicuc.ToUpper().Replace("HUYỆN", "H.").Replace("QUẬN", "Q.").Replace("KHU VỰC", "KV."), false, true);
                                        docTTCoHD.Replace("{{NGAYQDCCTK}}", cchdModel.ngayqdcctk, false, true);
                                        docTTCoHD.Replace("{{SOQDCCTK}}", cchdModel.soqdcctk, false, true);
                                        docTTCoHD.Replace("{{TENNNT}}", cchdModel.tennnt, false, true);
                                        docTTCoHD.Replace("{{MST}}", cchdModel.mst, false, true);
                                        docTTCoHD.Replace("{{DCTBT_DIACHI}}", cchdModel.dctbt_diachi, false, true);
                                        docTTCoHD.Replace("{{DCTBT_PHUONGXA}}", cchdModel.dctbt_phuongxa, false, true);
                                        docTTCoHD.Replace("{{DCTBT_QUANHUYEN}}", cchdModel.dctbt_quanhuyen, false, true);
                                        docTTCoHD.Replace("{{DCTBT_TINHTHANHPHO}}", cchdModel.dctbt_tinhthanhpho, false, true);
                                        docTTCoHD.Replace("{{DCTS_DIACHI}}", cchdModel.dcts_diachi, false, true);
                                        docTTCoHD.Replace("{{DCTS_PHUONGXA}}", cchdModel.dcts_phuongxa, false, true);
                                        docTTCoHD.Replace("{{DCTS_QUANHUYEN}}", cchdModel.dcts_quanhuyen, false, true);
                                        docTTCoHD.Replace("{{DCTS_TINHTHANHPHO}}", cchdModel.dcts_tinhthanhpho, false, true);
                                        docTTCoHD.Replace("{{THONGBAOSO}}", cchdModel.thongbaoso, false, true);
                                        docTTCoHD.Replace("{{NGAYTHONGBAO}}", cchdModel.ngaythongbao, false, true);
                                        docTTCoHD.Replace("{{QUY}}", cchdModel.quy, false, true);
                                        docTTCoHD.Replace("{{CHICUCTRUONG}}", cchdModel.chicuctruong, false, true);
                                        docTTCoHD.Replace("{{CHICUCPHO}}", cchdModel.chicucpho, false, true);
                                        docTTCoHD.Replace("{{DOITRUONG}}", cchdModel.doitruong, false, true);
                                        // Tờ trình không có hoá đơn
                                        docTTKhongHD.Replace("{{CHICUC}}", cchdModel.chicuc.ToUpper().Replace("HUYỆN", "H.").Replace("QUẬN", "Q.").Replace("KHU VỰC", "KV."), false, true);
                                        docTTKhongHD.Replace("{{NGAYQDCCTK}}", cchdModel.ngayqdcctk, false, true);
                                        docTTKhongHD.Replace("{{SOQDCCTK}}", cchdModel.soqdcctk, false, true);
                                        docTTKhongHD.Replace("{{TENNNT}}", cchdModel.tennnt, false, true);
                                        docTTKhongHD.Replace("{{MST}}", cchdModel.mst, false, true);
                                        docTTKhongHD.Replace("{{DCTBT_DIACHI}}", cchdModel.dctbt_diachi, false, true);
                                        docTTKhongHD.Replace("{{DCTBT_PHUONGXA}}", cchdModel.dctbt_phuongxa, false, true);
                                        docTTKhongHD.Replace("{{DCTBT_QUANHUYEN}}", cchdModel.dctbt_quanhuyen, false, true);
                                        docTTKhongHD.Replace("{{DCTBT_TINHTHANHPHO}}", cchdModel.dctbt_tinhthanhpho, false, true);
                                        docTTKhongHD.Replace("{{DCTS_DIACHI}}", cchdModel.dcts_diachi, false, true);
                                        docTTKhongHD.Replace("{{DCTS_PHUONGXA}}", cchdModel.dcts_phuongxa, false, true);
                                        docTTKhongHD.Replace("{{DCTS_QUANHUYEN}}", cchdModel.dcts_quanhuyen, false, true);
                                        docTTKhongHD.Replace("{{DCTS_TINHTHANHPHO}}", cchdModel.dcts_tinhthanhpho, false, true);
                                        docTTKhongHD.Replace("{{THONGBAOSO}}", cchdModel.thongbaoso, false, true);
                                        docTTKhongHD.Replace("{{NGAYTHONGBAO}}", cchdModel.ngaythongbao, false, true);
                                        docTTKhongHD.Replace("{{QUY}}", cchdModel.quy, false, true);
                                        docTTKhongHD.Replace("{{NGAYCOHIEULUC}}", cchdModel.ngaycohieuluc.ToString("dd/MM/yyyy"), false, true);
                                        docTTKhongHD.Replace("{{NGAYHETHIEULUC}}", cchdModel.ngayhethieuluc.ToString("dd/MM/yyyy"), false, true);
                                        docTTKhongHD.Replace("{{CHICUCTRUONG}}", cchdModel.chicuctruong, false, true);
                                        docTTKhongHD.Replace("{{CHICUCPHO}}", cchdModel.chicucpho, false, true);
                                        docTTKhongHD.Replace("{{DOITRUONG}}", cchdModel.doitruong, false, true);
                                        #endregion

                                        #region Replace Table: QĐ+TB có hoá đơn + Tờ trình có hoá đơn bảng 2
                                        Section section = docQDCoHD.Sections[0];
                                        TextSelection selection = docQDCoHD.FindString("{{TABLE}}", true, true);
                                        TextRange range = selection.GetAsOneRange();
                                        Paragraph paragraph = range.OwnerParagraph;
                                        Body body = paragraph.OwnerTextBody;
                                        int index = body.ChildObjects.IndexOf(paragraph);
                                        Table tableNo = section.AddTable(true);

                                        Section sectionTTCoHDTB2 = docTTCoHD.Sections[0];
                                        TextSelection selectionTTCoHDTB2 = docTTCoHD.FindString("{{TABLE2}}", true, true);
                                        TextRange rangeTTCoHDTB2 = selectionTTCoHDTB2.GetAsOneRange();
                                        Paragraph paragraphTTCoHDTB2 = rangeTTCoHDTB2.OwnerParagraph;
                                        Body bodyTTCoHDTB2 = paragraphTTCoHDTB2.OwnerTextBody;
                                        int indexTTCoHDTB2 = bodyTTCoHDTB2.ChildObjects.IndexOf(paragraphTTCoHDTB2);
                                        Table tableNoTTCoHDTB2 = sectionTTCoHDTB2.AddTable(true);

                                        String[] headerQDCoHD = new String[] { "STT", "Loại hoá đơn", "Ký hiệu mẫu", "Ký hiệu hoá đơn", "Từ số - đến số", "Ghi chú" };
                                        // Tạo dữ liệu demo
                                        cchdModel.hoadons = new List<HoaDon>();
                                        var hoadon = new HoaDon();
                                        hoadon.stt = 1;
                                        hoadon.loaihoadon = "";
                                        hoadon.kyhieumau = "";
                                        hoadon.kyhieuhoadon = "";
                                        hoadon.tusodenso = tableCCHDTMS.Rows.Count > 0 ? tableCCHDTMS.Rows[0]["Số hóa đơn"].ToString() : "";
                                        hoadon.ghichu = "";
                                        cchdModel.hoadons.Add(hoadon);
                                        tableNo.ResetCells(cchdModel.hoadons.Count + 1, headerQDCoHD.Length);
                                        TableRow FRow = tableNo.Rows[0];
                                        FRow.IsHeader = true;

                                        tableNoTTCoHDTB2.ResetCells(cchdModel.hoadons.Count + 1, headerQDCoHD.Length);
                                        TableRow FRowTB2 = tableNoTTCoHDTB2.Rows[0];
                                        FRowTB2.IsHeader = true;
                                        // Header Row
                                        for (int i = 0; i < headerQDCoHD.Length; i++)
                                        {
                                            //Cell Alignment
                                            Paragraph p = FRow.Cells[i].AddParagraph();
                                            FRow.Cells[i].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                            p.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                            //Data Format
                                            TextRange TR = p.AppendText(headerQDCoHD[i]);
                                            TR.CharacterFormat.FontName = "Times New Roman";
                                            TR.CharacterFormat.FontSize = 13;

                                            Paragraph pTB2 = FRowTB2.Cells[i].AddParagraph();
                                            FRowTB2.Cells[i].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                            pTB2.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                            TextRange TRTB2 = pTB2.AppendText(headerQDCoHD[i]);
                                            TRTB2.CharacterFormat.FontName = "Times New Roman";
                                            TRTB2.CharacterFormat.FontSize = 13;

                                            switch (i)
                                            {
                                                case 0:
                                                    FRow.Cells[i].SetCellWidth(5, CellWidthType.Percentage);
                                                    FRowTB2.Cells[i].SetCellWidth(5, CellWidthType.Percentage);
                                                    break;
                                                case 1:
                                                    FRow.Cells[i].SetCellWidth(10, CellWidthType.Percentage);
                                                    FRowTB2.Cells[i].SetCellWidth(10, CellWidthType.Percentage);
                                                    break;
                                                case 2:
                                                    FRow.Cells[i].SetCellWidth(20, CellWidthType.Percentage);
                                                    FRowTB2.Cells[i].SetCellWidth(20, CellWidthType.Percentage);
                                                    break;
                                                case 3:
                                                    FRow.Cells[i].SetCellWidth(20, CellWidthType.Percentage);
                                                    FRowTB2.Cells[i].SetCellWidth(20, CellWidthType.Percentage);
                                                    break;
                                                case 4:
                                                    FRow.Cells[i].SetCellWidth(20, CellWidthType.Percentage);
                                                    FRowTB2.Cells[i].SetCellWidth(20, CellWidthType.Percentage);
                                                    break;
                                                case 5:
                                                    FRow.Cells[i].SetCellWidth(25, CellWidthType.Percentage);
                                                    FRowTB2.Cells[i].SetCellWidth(25, CellWidthType.Percentage);
                                                    break;
                                            }
                                        }

                                        //Data Row
                                        for (int r = 0; r < cchdModel.hoadons.Count; r++)
                                        {
                                            #region QĐ+TB có hoá đơn
                                            TableRow DataRow = tableNo.Rows[r + 1];
                                            // 1. STT
                                            DataRow.Cells[0].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                            Paragraph p0 = DataRow.Cells[0].AddParagraph();
                                            TextRange TR0 = p0.AppendText(cchdModel.hoadons[r].stt.ToString());
                                            p0.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                            TR0.CharacterFormat.FontName = "Times New Roman";
                                            TR0.CharacterFormat.FontSize = 13;
                                            DataRow.Cells[0].SetCellWidth(5, CellWidthType.Percentage);
                                            // 2. Loại hoá đơn
                                            DataRow.Cells[1].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                            Paragraph p1 = DataRow.Cells[1].AddParagraph();
                                            TextRange TR1 = p1.AppendText(cchdModel.hoadons[r].loaihoadon);
                                            p1.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                            TR1.CharacterFormat.FontName = "Times New Roman";
                                            TR1.CharacterFormat.FontSize = 13;
                                            DataRow.Cells[1].SetCellWidth(10, CellWidthType.Percentage);
                                            // 3. Ký hiệu mẫu
                                            DataRow.Cells[2].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                            Paragraph p2 = DataRow.Cells[2].AddParagraph();
                                            TextRange TR2 = p2.AppendText(cchdModel.hoadons[r].kyhieumau);
                                            p2.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                            TR2.CharacterFormat.FontName = "Times New Roman";
                                            TR2.CharacterFormat.FontSize = 13;
                                            DataRow.Cells[2].SetCellWidth(20, CellWidthType.Percentage);
                                            // 4. Ký hiệu hoá đơn
                                            DataRow.Cells[3].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                            Paragraph p3 = DataRow.Cells[3].AddParagraph();
                                            TextRange TR3 = p3.AppendText(cchdModel.hoadons[r].kyhieuhoadon);
                                            p3.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                            TR3.CharacterFormat.FontName = "Times New Roman";
                                            TR3.CharacterFormat.FontSize = 13;
                                            DataRow.Cells[3].SetCellWidth(20, CellWidthType.Percentage);
                                            // 5. Từ ngày tới ngày
                                            DataRow.Cells[4].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                            Paragraph p4 = DataRow.Cells[4].AddParagraph();
                                            TextRange TR4 = p4.AppendText(cchdModel.hoadons[r].tusodenso);
                                            p4.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                            TR4.CharacterFormat.FontName = "Times New Roman";
                                            TR4.CharacterFormat.FontSize = 13;
                                            DataRow.Cells[4].SetCellWidth(20, CellWidthType.Percentage);
                                            // 6. Ghi chú
                                            DataRow.Cells[5].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                            Paragraph p5 = DataRow.Cells[5].AddParagraph();
                                            TextRange TR5 = p5.AppendText(cchdModel.hoadons[r].ghichu);
                                            p5.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                            TR5.CharacterFormat.FontName = "Times New Roman";
                                            TR5.CharacterFormat.FontSize = 13;
                                            DataRow.Cells[5].SetCellWidth(25, CellWidthType.Percentage);
                                            #endregion

                                            #region Tờ trình có hoá đơn Table 2
                                            TableRow DataRowTTCoHDTB2 = tableNoTTCoHDTB2.Rows[r + 1];
                                            // 1. STT
                                            DataRowTTCoHDTB2.Cells[0].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                            Paragraph p0TTCoHDTB2 = DataRowTTCoHDTB2.Cells[0].AddParagraph();
                                            TextRange TR0TTCoHDTB2 = p0TTCoHDTB2.AppendText(cchdModel.hoadons[r].stt.ToString());
                                            p0TTCoHDTB2.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                            TR0TTCoHDTB2.CharacterFormat.FontName = "Times New Roman";
                                            TR0TTCoHDTB2.CharacterFormat.FontSize = 13;
                                            DataRowTTCoHDTB2.Cells[0].SetCellWidth(5, CellWidthType.Percentage);
                                            // 2. Loại hoá đơn
                                            DataRowTTCoHDTB2.Cells[1].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                            Paragraph p1TTCoHDTB2 = DataRowTTCoHDTB2.Cells[1].AddParagraph();
                                            TextRange TR1TTCoHDTB2 = p1TTCoHDTB2.AppendText(cchdModel.hoadons[r].loaihoadon);
                                            p1TTCoHDTB2.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                            TR1TTCoHDTB2.CharacterFormat.FontName = "Times New Roman";
                                            TR1TTCoHDTB2.CharacterFormat.FontSize = 13;
                                            DataRowTTCoHDTB2.Cells[1].SetCellWidth(10, CellWidthType.Percentage);
                                            // 3. Ký hiệu mẫu
                                            DataRowTTCoHDTB2.Cells[2].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                            Paragraph p2TTCoHDTB2 = DataRowTTCoHDTB2.Cells[2].AddParagraph();
                                            TextRange TR2TTCoHDTB2 = p2TTCoHDTB2.AppendText(cchdModel.hoadons[r].kyhieumau);
                                            p2TTCoHDTB2.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                            TR2TTCoHDTB2.CharacterFormat.FontName = "Times New Roman";
                                            TR2TTCoHDTB2.CharacterFormat.FontSize = 13;
                                            DataRowTTCoHDTB2.Cells[2].SetCellWidth(20, CellWidthType.Percentage);
                                            // 4. Ký hiệu hoá đơn
                                            DataRowTTCoHDTB2.Cells[3].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                            Paragraph p3TTCoHDTB2 = DataRowTTCoHDTB2.Cells[3].AddParagraph();
                                            TextRange TR3TTCoHDTB2 = p3TTCoHDTB2.AppendText(cchdModel.hoadons[r].kyhieuhoadon);
                                            p3TTCoHDTB2.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                            TR3TTCoHDTB2.CharacterFormat.FontName = "Times New Roman";
                                            TR3TTCoHDTB2.CharacterFormat.FontSize = 13;
                                            DataRowTTCoHDTB2.Cells[3].SetCellWidth(20, CellWidthType.Percentage);
                                            // 5. Từ ngày tới ngày
                                            DataRowTTCoHDTB2.Cells[4].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                            Paragraph p4TTCoHDTB2 = DataRowTTCoHDTB2.Cells[4].AddParagraph();
                                            TextRange TR4TTCoHDTB2 = p4TTCoHDTB2.AppendText(cchdModel.hoadons[r].tusodenso);
                                            p4TTCoHDTB2.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                            TR4TTCoHDTB2.CharacterFormat.FontName = "Times New Roman";
                                            TR4TTCoHDTB2.CharacterFormat.FontSize = 13;
                                            DataRowTTCoHDTB2.Cells[4].SetCellWidth(20, CellWidthType.Percentage);
                                            // 6. Ghi chú
                                            DataRowTTCoHDTB2.Cells[5].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                            Paragraph p5TTCoHDTB2 = DataRowTTCoHDTB2.Cells[5].AddParagraph();
                                            TextRange TR5TTCoHDTB2 = p5TTCoHDTB2.AppendText(cchdModel.hoadons[r].ghichu);
                                            p5TTCoHDTB2.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                            TR5TTCoHDTB2.CharacterFormat.FontName = "Times New Roman";
                                            TR5TTCoHDTB2.CharacterFormat.FontSize = 13;
                                            DataRowTTCoHDTB2.Cells[5].SetCellWidth(25, CellWidthType.Percentage);
                                            #endregion

                                        }

                                        body.ChildObjects.Remove(paragraph);
                                        body.ChildObjects.Insert(index, tableNo);
                                        bodyTTCoHDTB2.ChildObjects.Remove(paragraphTTCoHDTB2);
                                        bodyTTCoHDTB2.ChildObjects.Insert(indexTTCoHDTB2, tableNoTTCoHDTB2);
                                        #endregion

                                        #region Replace Table: Số tiền cưỡng chế nợ kỳ này (Tờ trình có hoá đơn & không có hoá đơn)
                                        // Tờ trình có hoá đơn
                                        Section sectionTTCoHD = docTTCoHD.Sections[0];
                                        TextSelection selectionTTCoHD = docTTCoHD.FindString("{{TABLEST}}", true, true);
                                        TextRange rangeTTCoHD = selectionTTCoHD.GetAsOneRange();
                                        Paragraph paragraphTTCoHD = rangeTTCoHD.OwnerParagraph;
                                        Body bodyTTCoHD = paragraphTTCoHD.OwnerTextBody;
                                        int indexTTCoHD = bodyTTCoHD.ChildObjects.IndexOf(paragraphTTCoHD);
                                        Table tableNoTTCoHD = sectionTTCoHD.AddTable(true);
                                        // Tờ trình không có hoá đơn
                                        Section sectionTTKhongHD = docTTKhongHD.Sections[0];
                                        TextSelection selectionTTKhongHD = docTTKhongHD.FindString("{{TABLE}}", true, true);
                                        TextRange rangeTTKhongHD = selectionTTKhongHD.GetAsOneRange();
                                        Paragraph paragraphTTKhongHD = rangeTTKhongHD.OwnerParagraph;
                                        Body bodyTTKhongHD = paragraphTTKhongHD.OwnerTextBody;
                                        int indexTTKhongHD = bodyTTKhongHD.ChildObjects.IndexOf(paragraphTTKhongHD);
                                        Table tableNoTTKhongHD = sectionTTKhongHD.AddTable(true);

                                        String[] headerLicence = new string[] { "TT", "Tiểu mục", "Loại thuế", "Trong đó số nợ trên 90 ngày trên QĐCC trích tiền TKNH(đ)", "Số đã nộp từ thời điểm ban hành QĐCC trích tiền TKNH đến ngày đề nghị cưỡng chế TB ngừng sử dụng hóa đơn (đ)", "Số nợ trên 120 ngày đề nghị cưỡng chế TB ngừng sử dụng hóa đơn (đ)" };


                                        // Tờ trình có hoá đơn
                                        tableNoTTCoHD.ResetCells(cchdModel.listThue.Count + 2, headerLicence.Length);
                                        TableRow FRowTTCoHD = tableNoTTCoHD.Rows[0];
                                        FRowTTCoHD.IsHeader = true;
                                        //Tờ trình không có hoá đơn
                                        tableNoTTKhongHD.ResetCells(cchdModel.listThue.Count + 2, headerLicence.Length);
                                        TableRow FRowTTKhongHD = tableNoTTKhongHD.Rows[0];
                                        FRowTTKhongHD.IsHeader = true;
                                        for (int i = 0; i < headerLicence.Length; i++)
                                        {
                                            //Cell Alignment
                                            // Tờ trình có hoá đơn
                                            Paragraph p = FRowTTCoHD.Cells[i].AddParagraph();
                                            FRowTTCoHD.Cells[i].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                            p.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                            //Data Format
                                            TextRange TR = p.AppendText(headerLicence[i]);
                                            TR.CharacterFormat.FontName = "Times New Roman";
                                            TR.CharacterFormat.FontSize = 13;
                                            //Tờ trình không có hoá đơn
                                            Paragraph pNo = FRowTTKhongHD.Cells[i].AddParagraph();
                                            FRowTTKhongHD.Cells[i].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                            pNo.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                            TextRange TRNo = pNo.AppendText(headerLicence[i]);
                                            TRNo.CharacterFormat.FontName = "Times New Roman";
                                            TRNo.CharacterFormat.FontSize = 13;
                                            switch (i)
                                            {
                                                case 0:
                                                    FRowTTCoHD.Cells[i].SetCellWidth(5, CellWidthType.Percentage);
                                                    FRowTTKhongHD.Cells[i].SetCellWidth(5, CellWidthType.Percentage); break;
                                                case 1:
                                                    FRowTTCoHD.Cells[i].SetCellWidth(10, CellWidthType.Percentage);
                                                    FRowTTKhongHD.Cells[i].SetCellWidth(10, CellWidthType.Percentage); break;
                                                case 2:
                                                    FRowTTCoHD.Cells[i].SetCellWidth(20, CellWidthType.Percentage);
                                                    FRowTTKhongHD.Cells[i].SetCellWidth(20, CellWidthType.Percentage); break;
                                                case 3:
                                                    FRowTTCoHD.Cells[i].SetCellWidth(20, CellWidthType.Percentage);
                                                    FRowTTKhongHD.Cells[i].SetCellWidth(20, CellWidthType.Percentage); break;
                                                case 4:
                                                    FRowTTCoHD.Cells[i].SetCellWidth(25, CellWidthType.Percentage);
                                                    FRowTTKhongHD.Cells[i].SetCellWidth(25, CellWidthType.Percentage); break;
                                                case 5:
                                                    FRowTTCoHD.Cells[i].SetCellWidth(20, CellWidthType.Percentage);
                                                    FRowTTKhongHD.Cells[i].SetCellWidth(20, CellWidthType.Percentage); break;
                                            }
                                        }

                                        //Data Row
                                        for (int r = 0; r < cchdModel.listThue.Count; r++)
                                        {
                                            #region Tờ trình có hoá đơn
                                            TableRow DataRow = tableNoTTCoHD.Rows[r + 1];
                                            // 1. STT
                                            DataRow.Cells[0].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                            Paragraph p0 = DataRow.Cells[0].AddParagraph();
                                            TextRange TR0 = p0.AppendText(cchdModel.listThue[r].stt.ToString());
                                            p0.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                            TR0.CharacterFormat.FontName = "Times New Roman";
                                            TR0.CharacterFormat.FontSize = 13;
                                            DataRow.Cells[0].SetCellWidth(5, CellWidthType.Percentage);
                                            // 2. Tiểu mục
                                            DataRow.Cells[1].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                            Paragraph p1 = DataRow.Cells[1].AddParagraph();
                                            TextRange TR1 = p1.AppendText(cchdModel.listThue[r].tieumuc.ToString());
                                            p1.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                            TR1.CharacterFormat.FontName = "Times New Roman";
                                            TR1.CharacterFormat.FontSize = 13;
                                            DataRow.Cells[1].SetCellWidth(10, CellWidthType.Percentage);
                                            // 3. Tiểu mục
                                            DataRow.Cells[2].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                            Paragraph p2 = DataRow.Cells[2].AddParagraph();
                                            TextRange TR2 = p2.AppendText(cchdModel.listThue[r].loaithue.ToString());
                                            p2.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                            TR2.CharacterFormat.FontName = "Times New Roman";
                                            TR2.CharacterFormat.FontSize = 13;
                                            DataRow.Cells[2].SetCellWidth(20, CellWidthType.Percentage);
                                            // 4. Trong đó số nợ từ 90 ngày trở lên (đ)
                                            DataRow.Cells[3].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                            Paragraph p3 = DataRow.Cells[3].AddParagraph();
                                            TextRange TR3 = p3.AppendText(cchdModel.listThue[r].notren90ngay > 0 ? cchdModel.listThue[r].notren90ngay.ToString("#,###") : "0");
                                            p3.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                            TR3.CharacterFormat.FontName = "Times New Roman";
                                            TR3.CharacterFormat.FontSize = 13;
                                            DataRow.Cells[3].SetCellWidth(20, CellWidthType.Percentage);
                                            // 5. Số tiền đa nộp từ thời điểm banhành thông báo đến ngày đề nghị cưỡng chế nợ (đ)
                                            DataRow.Cells[4].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                            Paragraph p4 = DataRow.Cells[4].AddParagraph();
                                            TextRange TR4 = p4.AppendText(cchdModel.listThue[r].sotiennop > 0 ? cchdModel.listThue[r].sotiennop.ToString("#,###") : "0");
                                            p4.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                            TR4.CharacterFormat.FontName = "Times New Roman";
                                            TR4.CharacterFormat.FontSize = 13;
                                            DataRow.Cells[4].SetCellWidth(25, CellWidthType.Percentage);
                                            // 6. Số nợ từ 91 ngày trở lên đề nghị cưỡng chế nợ kỳ này (d)
                                            DataRow.Cells[5].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                            Paragraph p5 = DataRow.Cells[5].AddParagraph();
                                            TextRange TR5 = p5.AppendText(cchdModel.listThue[r].notren91ngaycuongche > 0 ? cchdModel.listThue[r].notren91ngaycuongche.ToString("#,###") : "0");
                                            p5.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                            TR5.CharacterFormat.FontName = "Times New Roman";
                                            TR5.CharacterFormat.FontSize = 13;
                                            DataRow.Cells[5].SetCellWidth(20, CellWidthType.Percentage);
                                            #endregion

                                            #region Tờ trình không có hoá đơn
                                            TableRow DataRowNo = tableNoTTKhongHD.Rows[r + 1];
                                            // 1. STT
                                            DataRowNo.Cells[0].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                            Paragraph p0No = DataRowNo.Cells[0].AddParagraph();
                                            TextRange TR0No = p0No.AppendText(cchdModel.listThue[r].stt.ToString());
                                            p0No.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                            TR0No.CharacterFormat.FontName = "Times New Roman";
                                            TR0No.CharacterFormat.FontSize = 13;
                                            DataRowNo.Cells[0].SetCellWidth(5, CellWidthType.Percentage);
                                            // 2. Tiểu mục
                                            DataRowNo.Cells[1].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                            Paragraph p1No = DataRowNo.Cells[1].AddParagraph();
                                            TextRange TR1No = p1No.AppendText(cchdModel.listThue[r].tieumuc.ToString());
                                            p1No.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                            TR1No.CharacterFormat.FontName = "Times New Roman";
                                            TR1No.CharacterFormat.FontSize = 13;
                                            DataRowNo.Cells[1].SetCellWidth(10, CellWidthType.Percentage);
                                            // 3. Tiểu mục
                                            DataRowNo.Cells[2].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                            Paragraph p2No = DataRowNo.Cells[2].AddParagraph();
                                            TextRange TR2No = p2No.AppendText(cchdModel.listThue[r].loaithue.ToString());
                                            p2No.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                            TR2No.CharacterFormat.FontName = "Times New Roman";
                                            TR2No.CharacterFormat.FontSize = 13;
                                            DataRowNo.Cells[2].SetCellWidth(20, CellWidthType.Percentage);
                                            // 4. Trong đó số nợ từ 90 ngày trở lên (đ)
                                            DataRowNo.Cells[3].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                            Paragraph p3No = DataRowNo.Cells[3].AddParagraph();
                                            TextRange TR3No = p3No.AppendText(cchdModel.listThue[r].notren90ngay > 0 ? cchdModel.listThue[r].notren90ngay.ToString("#,###") : "0");
                                            p3No.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                            TR3No.CharacterFormat.FontName = "Times New Roman";
                                            TR3No.CharacterFormat.FontSize = 13;
                                            DataRowNo.Cells[3].SetCellWidth(20, CellWidthType.Percentage);
                                            // 5. Số tiền đa nộp từ thời điểm banhành thông báo đến ngày đề nghị cưỡng chế nợ (đ)
                                            DataRowNo.Cells[4].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                            Paragraph p4No = DataRowNo.Cells[4].AddParagraph();
                                            TextRange TR4No = p4No.AppendText(cchdModel.listThue[r].sotiennop > 0 ? cchdModel.listThue[r].sotiennop.ToString("#,###") : "0");
                                            p4No.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                            TR4No.CharacterFormat.FontName = "Times New Roman";
                                            TR4No.CharacterFormat.FontSize = 13;
                                            DataRowNo.Cells[4].SetCellWidth(25, CellWidthType.Percentage);
                                            // 6. Số nợ từ 91 ngày trở lên đề nghị cưỡng chế nợ kỳ này (d)
                                            DataRowNo.Cells[5].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                            Paragraph p5No = DataRowNo.Cells[5].AddParagraph();
                                            TextRange TR5No = p5No.AppendText(cchdModel.listThue[r].notren91ngaycuongche > 0 ? cchdModel.listThue[r].notren91ngaycuongche.ToString("#,###") : "0");
                                            p5No.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                            TR5No.CharacterFormat.FontName = "Times New Roman";
                                            TR5No.CharacterFormat.FontSize = 13;
                                            DataRowNo.Cells[5].SetCellWidth(20, CellWidthType.Percentage);
                                            #endregion
                                        }
                                        tableNoTTCoHD.Rows[cchdModel.listThue.Count + 1].Cells[0].SetCellWidth(5, CellWidthType.Percentage);
                                        tableNoTTCoHD.Rows[cchdModel.listThue.Count + 1].Cells[1].SetCellWidth(10, CellWidthType.Percentage);
                                        tableNoTTCoHD.Rows[cchdModel.listThue.Count + 1].Cells[2].SetCellWidth(20, CellWidthType.Percentage);
                                        tableNoTTCoHD.Rows[cchdModel.listThue.Count + 1].Cells[3].SetCellWidth(20, CellWidthType.Percentage);
                                        tableNoTTCoHD.Rows[cchdModel.listThue.Count + 1].Cells[4].SetCellWidth(25, CellWidthType.Percentage);
                                        tableNoTTCoHD.Rows[cchdModel.listThue.Count + 1].Cells[5].SetCellWidth(20, CellWidthType.Percentage);
                                        tableNoTTKhongHD.Rows[cchdModel.listThue.Count + 1].Cells[0].SetCellWidth(5, CellWidthType.Percentage);
                                        tableNoTTKhongHD.Rows[cchdModel.listThue.Count + 1].Cells[1].SetCellWidth(10, CellWidthType.Percentage);
                                        tableNoTTKhongHD.Rows[cchdModel.listThue.Count + 1].Cells[2].SetCellWidth(20, CellWidthType.Percentage);
                                        tableNoTTKhongHD.Rows[cchdModel.listThue.Count + 1].Cells[3].SetCellWidth(20, CellWidthType.Percentage);
                                        tableNoTTKhongHD.Rows[cchdModel.listThue.Count + 1].Cells[4].SetCellWidth(25, CellWidthType.Percentage);
                                        tableNoTTKhongHD.Rows[cchdModel.listThue.Count + 1].Cells[5].SetCellWidth(20, CellWidthType.Percentage);
                                        #endregion

                                        #region Xử lý dòng cuối cùng (Tổng tiền)
                                        //Merge dòng cuối cùng lấy Name: Tổng
                                        tableNoTTCoHD.ApplyHorizontalMerge(cchdModel.listThue.Count + 1, 0, 2);
                                        TableRow row = tableNoTTCoHD.Rows[cchdModel.listThue.Count + 1];
                                        row.Cells[0].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                        Paragraph pF = row.Cells[0].AddParagraph();
                                        TextRange TRF = pF.AppendText("Tổng cộng");
                                        pF.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                        TRF.CharacterFormat.FontName = "Times New Roman";
                                        TRF.CharacterFormat.FontSize = 13;
                                        TRF.CharacterFormat.Bold = true;

                                        row.Cells[1].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                        Paragraph pF1 = row.Cells[3].AddParagraph();
                                        TextRange TRF1 = pF1.AppendText(cchdModel.tongtientren90ngay > 0 ? cchdModel.tongtientren90ngay.ToString("#,###") : "0");
                                        pF1.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                        TRF1.CharacterFormat.FontName = "Times New Roman";
                                        TRF1.CharacterFormat.FontSize = 13;
                                        TRF1.CharacterFormat.Bold = true;

                                        row.Cells[2].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                        Paragraph pF2 = row.Cells[4].AddParagraph();
                                        TextRange TRF2 = pF2.AppendText(cchdModel.tongsonop > 0 ? cchdModel.tongsonop.ToString("#,###") : "0");
                                        pF2.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                        TRF2.CharacterFormat.FontName = "Times New Roman";
                                        TRF2.CharacterFormat.FontSize = 13;
                                        TRF2.CharacterFormat.Bold = true;

                                        row.Cells[3].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                        Paragraph pF3 = row.Cells[5].AddParagraph();
                                        TextRange TRF3 = pF3.AppendText(cchdModel.tongtiencuongche > 0 ? cchdModel.tongtiencuongche.ToString("#,###") : "0");
                                        pF3.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                        TRF3.CharacterFormat.FontName = "Times New Roman";
                                        TRF3.CharacterFormat.FontSize = 13;
                                        TRF3.CharacterFormat.Bold = true;

                                        tableNoTTKhongHD.ApplyHorizontalMerge(cchdModel.listThue.Count + 1, 0, 2);
                                        TableRow rowNo = tableNoTTKhongHD.Rows[cchdModel.listThue.Count + 1];
                                        rowNo.Cells[0].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                        Paragraph pFNo = rowNo.Cells[0].AddParagraph();
                                        TextRange TRFNo = pFNo.AppendText("Tổng cộng");
                                        pFNo.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                        TRFNo.CharacterFormat.FontName = "Times New Roman";
                                        TRFNo.CharacterFormat.FontSize = 13;
                                        TRFNo.CharacterFormat.Bold = true;

                                        rowNo.Cells[3].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                        Paragraph pF1No = rowNo.Cells[3].AddParagraph();
                                        TextRange TRF1No = pF1No.AppendText(cchdModel.tongtientren90ngay > 0 ? cchdModel.tongtientren90ngay.ToString("#,###") : "0");
                                        pF1No.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                        TRF1No.CharacterFormat.FontName = "Times New Roman";
                                        TRF1No.CharacterFormat.FontSize = 13;
                                        TRF1No.CharacterFormat.Bold = true;

                                        rowNo.Cells[4].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                        Paragraph pF2No = rowNo.Cells[4].AddParagraph();
                                        TextRange TRF2No = pF2No.AppendText(cchdModel.tongsonop > 0 ? cchdModel.tongsonop.ToString("#,###") : "0");
                                        pF2No.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                        TRF2No.CharacterFormat.FontName = "Times New Roman";
                                        TRF2No.CharacterFormat.FontSize = 13;
                                        TRF2No.CharacterFormat.Bold = true;

                                        rowNo.Cells[5].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                        Paragraph pF3No = rowNo.Cells[5].AddParagraph();
                                        TextRange TRF3No = pF3No.AppendText(cchdModel.tongtiencuongche > 0 ? cchdModel.tongtiencuongche.ToString("#,###") : "0");
                                        pF3No.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                        TRF3No.CharacterFormat.FontName = "Times New Roman";
                                        TRF3No.CharacterFormat.FontSize = 13;
                                        TRF3No.CharacterFormat.Bold = true;
                                        #endregion

                                        bodyTTCoHD.ChildObjects.Remove(paragraphTTCoHD);
                                        bodyTTCoHD.ChildObjects.Insert(indexTTCoHD, tableNoTTCoHD);

                                        bodyTTKhongHD.ChildObjects.Remove(paragraphTTKhongHD);
                                        bodyTTKhongHD.ChildObjects.Insert(indexTTKhongHD, tableNoTTKhongHD);

                                        #region Save File
                                        // Check Cty nằm trong Chứng từ đã nộp và trong CCHD trên TMS
                                        // Save QĐ+TB có hoá đơn
                                        docQDCoHD.SaveToFile(fileQDCoHD, FileFormat.Doc);
                                        // Save: QĐ+TB không có hoá đơn
                                        docQDKhongHD.SaveToFile(fileQDKhongHD, FileFormat.Doc);
                                        // Save: Tờ trình có hoá đơn
                                        docTTCoHD.SaveToFile(fileTTCoHD, FileFormat.Doc);
                                        // Save: Tờ trình không có hoá đơn
                                        docTTKhongHD.SaveToFile(fileTTKhongHD, FileFormat.Doc);
                                        Common.Common.success("Tạo báo cáo thành công!");
                                        #endregion
                                        return true;
                                    }
                                    #endregion
                                }
                                else
                                {
                                    Common.Common.error("Không tồn tại dữ liệu trong Cưỡng chế hoá đơn trên TMS!");
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Common.Common.error(ex.ToString());
                return false;
            }
        }
    }
}
