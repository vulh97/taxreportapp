﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TaxReportApp.Model;

namespace TaxReportApp.Controller
{
    class DonViController
    {
        public static bool SaveTenCanBo (String tenCanBo)
        {
            try
            {
                var folderPath = Environment.CurrentDirectory;
                var filePath = folderPath + @"\documents\input\Thong_tin_chung\ChiTietDonVi.txt";

                if (!File.Exists(filePath))
                {
                    Directory.CreateDirectory(filePath);
                }

                var data = File.ReadAllLines(filePath);
                var tenChiNhanh = "";
                var tenDoi = "";
                var tenDoiTruong = "";
                var tenChiCucTruong = "";
                var tenPhoChiCucTruong = "";

                if (data.Count() >= 6)
                {
                    if (!String.IsNullOrEmpty(data[1]) && !String.IsNullOrWhiteSpace(data[1]))
                    {
                        tenChiNhanh = data[1].Trim();
                    }

                    if (!String.IsNullOrEmpty(data[2]) && String.IsNullOrWhiteSpace(data[2]))
                    {
                        tenDoi = data[2].Trim();
                    }

                    if (!String.IsNullOrEmpty(data[3]) && String.IsNullOrWhiteSpace(data[3]))
                    {
                        tenDoiTruong = data[3].Trim();
                    }

                    if (!String.IsNullOrEmpty(data[4].Trim()))
                    {
                        tenChiCucTruong = data[4].Trim();
                    }

                    if (!String.IsNullOrEmpty(data[5].Trim()))
                    {
                        tenPhoChiCucTruong = data[5].Trim();
                    }
                }

                var newData = new String[6];
                newData[0] = tenCanBo;
                newData[1] = tenChiNhanh;
                newData[2] = tenDoi;
                newData[3] = tenDoiTruong;
                newData[4] = tenChiCucTruong;
                newData[5] = tenPhoChiCucTruong;

                File.WriteAllLines(filePath, newData);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool SaveChiNhanh (String tenChiNhanh)
        {
            try
            {
                var folderPath = Environment.CurrentDirectory;
                var filePath = folderPath + @"\documents\input\Thong_tin_chung\ChiTietDonVi.txt";

                if (!File.Exists(filePath))
                {
                    Directory.CreateDirectory(filePath);
                }

                var data = File.ReadAllLines(filePath);
                var tenCanBo = "";
                var tenDoi = "";
                var tenDoiTruong = "";
                var tenChiCucTruong = "";
                var tenPhoChiCucTruong = "";

                if (data.Count() >= 6)
                {
                    if (!String.IsNullOrEmpty(data[0]) && !String.IsNullOrWhiteSpace(data[0]))
                    {
                        tenCanBo = data[0];
                    }

                    if (!String.IsNullOrEmpty(data[2]) && !String.IsNullOrWhiteSpace(data[2]))
                    {
                        tenDoi = data[2];
                    }

                    if (!String.IsNullOrEmpty(data[3]) && !String.IsNullOrWhiteSpace(data[3]))
                    {
                        tenDoiTruong = data[3];
                    }

                    if (!String.IsNullOrEmpty(data[4]))
                    {
                        tenChiCucTruong = data[4];
                    }

                    if (!String.IsNullOrEmpty(data[5]))
                    {
                        tenPhoChiCucTruong = data[5];
                    }
                }

                var newData = new String[6];
                newData[0] = tenCanBo;
                newData[1] = tenChiNhanh;
                newData[2] = tenDoi;
                newData[3] = tenDoiTruong;
                newData[4] = tenChiCucTruong;
                newData[5] = tenPhoChiCucTruong;

                File.WriteAllLines(filePath, newData);
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool SaveTenDoi(String tenDoi)
        {
            try
            {
                var folderPath = Environment.CurrentDirectory;
                var filePath = folderPath + @"\documents\input\Thong_tin_chung\ChiTietDonVi.txt";

                if (!File.Exists(filePath))
                {
                    Directory.CreateDirectory(filePath);
                }

                var data = File.ReadAllLines(filePath);
                var tenCanBo = "";
                var tenChiNhanh = "";
                var tenDoiTruong = "";
                var tenChiCucTruong = "";
                var tenPhoChiCucTruong = "";

                if (data.Count() >= 6)
                {
                    if (!String.IsNullOrEmpty(data[0]) && !String.IsNullOrWhiteSpace(data[0]))
                    {
                        tenCanBo = data[0];
                    }

                    if (!String.IsNullOrEmpty(data[1]) && !String.IsNullOrWhiteSpace(data[1]))
                    {
                        tenChiNhanh = data[1];
                    }

                    if (!String.IsNullOrEmpty(data[3]) && !String.IsNullOrWhiteSpace(data[3]))
                    {
                        tenDoiTruong = data[3];
                    }

                    if (!String.IsNullOrEmpty(data[4].Trim()))
                    {
                        tenChiCucTruong = data[4];
                    }

                    if (!String.IsNullOrEmpty(data[5].Trim()))
                    {
                        tenPhoChiCucTruong = data[5];
                    }
                }

                var newData = new String[6];
                newData[0] = tenCanBo;
                newData[1] = tenChiNhanh;
                newData[2] = tenDoi;
                newData[3] = tenDoiTruong;
                newData[4] = tenChiCucTruong;
                newData[5] = tenPhoChiCucTruong;

                File.WriteAllLines(filePath, newData);
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool SaveTenDoiTruong(String tenDoiTruong)
        {
            try
            {
                var folderPath = Environment.CurrentDirectory;
                var filePath = folderPath + @"\documents\input\Thong_tin_chung\ChiTietDonVi.txt";

                if (!File.Exists(filePath))
                {
                    Directory.CreateDirectory(filePath);
                }

                var data = File.ReadAllLines(filePath);
                var tenCanBo = "";
                var tenChiNhanh = "";
                var tenDoi = "";
                var tenChiCucTruong = "";
                var tenPhoChiCucTruong = "";

                if (data.Count() >= 6)
                {
                    if (!String.IsNullOrEmpty(data[0]) && !String.IsNullOrWhiteSpace(data[0]))
                    {
                        tenCanBo = data[0];
                    }

                    if (!String.IsNullOrEmpty(data[1]) && !String.IsNullOrWhiteSpace(data[1]))
                    {
                        tenChiNhanh = data[1];
                    }

                    if (!String.IsNullOrEmpty(data[2]) && !String.IsNullOrWhiteSpace(data[2]))
                    {
                        tenDoi = data[2];
                    }

                    if (!String.IsNullOrEmpty(data[4].Trim()))
                    {
                        tenChiCucTruong = data[4].Trim();
                    }

                    if (!String.IsNullOrEmpty(data[5].Trim()))
                    {
                        tenPhoChiCucTruong = data[5].Trim();
                    }
                }

                var newData = new String[6];
                newData[0] = tenCanBo;
                newData[1] = tenChiNhanh;
                newData[2] = tenDoi;
                newData[3] = tenDoiTruong;
                newData[4] = tenChiCucTruong;
                newData[5] = tenPhoChiCucTruong;

                File.WriteAllLines(filePath, newData);
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool SaveTenChiCucTruong(String tenChiCucTruong)
        {
            try
            {
                var folderPath = Environment.CurrentDirectory;
                var filePath = folderPath + @"\documents\input\Thong_tin_chung\ChiTietDonVi.txt";

                if (!File.Exists(filePath))
                {
                    Directory.CreateDirectory(filePath);
                }

                var data = File.ReadAllLines(filePath);
                var tenCanBo = "";
                var tenChiNhanh = "";
                var tenDoi = "";
                var tenDoiTruong = "";
                var tenPhoChiCucTruong = "";

                if (data.Count() >= 6)
                {
                    if (!String.IsNullOrEmpty(data[0]) && !String.IsNullOrWhiteSpace(data[0]))
                    {
                        tenCanBo = data[0];
                    }

                    if (!String.IsNullOrEmpty(data[1]) && !String.IsNullOrWhiteSpace(data[1]))
                    {
                        tenChiNhanh = data[1];
                    }

                    if (!String.IsNullOrEmpty(data[2]) && !String.IsNullOrWhiteSpace(data[2]))
                    {
                        tenDoi = data[2];
                    }

                    if (!String.IsNullOrEmpty(data[3].Trim()))
                    {
                        tenDoiTruong = data[3].Trim();
                    }

                    if (!String.IsNullOrEmpty(data[5].Trim()))
                    {
                        tenPhoChiCucTruong = data[5].Trim();
                    }
                }

                var newData = new String[6];
                newData[0] = tenCanBo;
                newData[1] = tenChiNhanh;
                newData[2] = tenDoi;
                newData[3] = tenDoiTruong;
                newData[4] = tenChiCucTruong;
                newData[5] = tenPhoChiCucTruong;

                File.WriteAllLines(filePath, newData);
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool SaveTenPhoChiCucTruong(String tenPhoChiCucTruong)
        {
            try
            {
                var folderPath = Environment.CurrentDirectory;
                var filePath = folderPath + @"\documents\input\Thong_tin_chung\ChiTietDonVi.txt";

                if (!File.Exists(filePath))
                {
                    Directory.CreateDirectory(filePath);
                }

                var data = File.ReadAllLines(filePath);
                var tenCanBo = "";
                var tenChiNhanh = "";
                var tenDoi = "";
                var tenDoiTruong = "";
                var tenChiCucTruong = "";

                if (data.Count() >= 6)
                {
                    if (!String.IsNullOrEmpty(data[0]) && !String.IsNullOrWhiteSpace(data[0]))
                    {
                        tenCanBo = data[0];
                    }

                    if (!String.IsNullOrEmpty(data[1]) && !String.IsNullOrWhiteSpace(data[1]))
                    {
                        tenChiNhanh = data[1];
                    }

                    if (!String.IsNullOrEmpty(data[2]) && !String.IsNullOrWhiteSpace(data[2]))
                    {
                        tenDoi = data[2];
                    }

                    if (!String.IsNullOrEmpty(data[3].Trim()))
                    {
                        tenDoiTruong = data[3].Trim();
                    }

                    if (!String.IsNullOrEmpty(data[4].Trim()))
                    {
                        tenChiCucTruong = data[4].Trim();
                    }
                }

                var newData = new String[6];
                newData[0] = tenCanBo;
                newData[1] = tenChiNhanh;
                newData[2] = tenDoi;
                newData[3] = tenDoiTruong;
                newData[4] = tenChiCucTruong;
                newData[5] = tenPhoChiCucTruong;

                File.WriteAllLines(filePath, newData);
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool SaveThongTinDonVi(String tenChiNhanh, String tenCanBo, String tenDoi, String tenDoiTruong, String tenChiCucTruong, String tenPhoChiCucTruong)
        {
            try
            {
                var folderPath = Environment.CurrentDirectory;
                var filePath = folderPath + @"\documents\input\Thong_tin_chung\ChiTietDonVi.txt";

                if (!File.Exists(filePath))
                {
                    Directory.CreateDirectory(filePath);
                }

                var data = File.ReadAllLines(filePath);

                var newData = new String[6];
                newData[0] = tenCanBo;
                newData[1] = tenChiNhanh;
                newData[2] = tenDoi;
                newData[3] = tenDoiTruong;
                newData[4] = tenChiCucTruong;
                newData[5] = tenPhoChiCucTruong;

                File.WriteAllLines(filePath, newData);
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static DonViViewModel GetThongTinDonVi()
        {
            try
            {
                var donViItem = new DonViViewModel();
                var folderPath = Environment.CurrentDirectory;
                var filePath = folderPath + @"\documents\input\Thong_tin_chung\ChiTietDonVi.txt";

                if (!File.Exists(filePath))
                {
                    Directory.CreateDirectory(filePath);
                }

                var data = File.ReadAllLines(filePath);

                if (data.Count() >= 6)
                {
                    if (!String.IsNullOrEmpty(data[0]) && !String.IsNullOrWhiteSpace(data[0]))
                    {
                        donViItem.tenCanBo = data[0].Trim();
                    }

                    if (!String.IsNullOrEmpty(data[1]) && !String.IsNullOrWhiteSpace(data[1]))
                    {
                        donViItem.tenChiNhanh = data[1].Trim();
                    }

                    if (!String.IsNullOrEmpty(data[2]) && !String.IsNullOrWhiteSpace(data[2]))
                    {
                        donViItem.tenDoi = data[2].Trim();
                    }

                    if (!String.IsNullOrEmpty(data[3]) && !String.IsNullOrWhiteSpace(data[3]))
                    {
                        donViItem.tenDoiTruong = data[3].Trim();
                    }

                    if (!String.IsNullOrEmpty(data[4].Trim()))
                    {
                        donViItem.tenChiCucTruong = data[4].Trim();
                    }

                    if (!String.IsNullOrEmpty(data[5].Trim()))
                    {
                        donViItem.tenPhoChiCucTruong = data[5].Trim();
                    }
                }

                return donViItem;

            }
            catch (Exception ex)
            {
                var donViItem = new DonViViewModel();
                return donViItem;
            }
        }

        public static List<String> getAllName_ChiCuc()
        {
            var listTenChiCuc = new List<String>();
            try
            {
                var folderPath = Environment.CurrentDirectory ;
                var filePath = folderPath + @"\documents\input\Thong_tin_chung\chicuc.xlsx";

                Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
                Microsoft.Office.Interop.Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(filePath);
                Microsoft.Office.Interop.Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
                Microsoft.Office.Interop.Excel.Range xlRange = xlWorksheet.UsedRange;

                int rowCount = xlRange.Rows.Count;
                int columnCount = xlRange.Columns.Count;

                for (int i = 2; i <= rowCount; i++)
                {
                    // Cột 2 là cột tên chi cục
                    Microsoft.Office.Interop.Excel.Range xlCell = xlRange.Cells[i, 2];
                    if (xlCell != null && xlCell.Value2 != null)
                    {
                        listTenChiCuc.Add(xlCell.Value2);
                    }
                }

                xlApp.Quit();
                return listTenChiCuc;
            } catch (Exception ex)
            {
                MessageBox.Show("Có lỗi xảy ra khi load dữ liệu chi cục !", "Lỗi");
                return listTenChiCuc;
            }
        }

        public static List<String> getAllName_TenDoi()
        {
            var listTenDoi = new List<String>();

            listTenDoi.Add("Đội kiểm tra thuế số 1");
            listTenDoi.Add("Đội kiểm tra thuế số 2");

            return listTenDoi;
        }

        public static DataTable GetDSCanBo(string sFilePathAndName, string searchPhuongXa)
        {
            string sLine = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=";
            sLine += sFilePathAndName;
            sLine += ";Extended Properties=Excel 8.0";
            var connection = BaseController.getConnection(sLine);
            try
            {
                connection.Open();
                DataTable dt = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string Sheet = dt.Rows[0].Field<string>("TABLE_NAME");
                sLine = "Select top 1 * From [" + Sheet + "] where UCASE([PHƯỜNG]) = @tenphuong";
                OleDbDataAdapter dataAdapter = new OleDbDataAdapter(sLine, connection);
                dataAdapter.SelectCommand.Parameters.AddWithValue("@tenphuong", searchPhuongXa.Trim().ToUpper());
                //dataAdapter.SelectCommand.Parameters.AddWithValue("@tenPhuong", tenPhuong.Trim().ToUpper());
                DataSet dataSet = new DataSet();
                dataAdapter.Fill(dataSet);
                return dataSet.Tables[0];
            }
            catch (Exception ex)
            {
                //Common.Common.error("Có lỗi xảy ra khi kiểm tra dữ liệu đã tồn tại trong chứng từ đã nộp! Vui lòng kiểm tra lại!");
                Common.Common.error(ex.ToString());
                connection.Close();
                return null;
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
