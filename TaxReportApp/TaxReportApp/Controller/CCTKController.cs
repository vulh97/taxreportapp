﻿using Spire.Doc;
using Spire.Doc.Documents;
using Spire.Doc.Fields;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TaxReportApp.Common;
using TaxReportApp.Model;

namespace TaxReportApp.Controller
{
    class CCTKController : BaseController
    {
        #region All File
        static string dir = Directory.GetCurrentDirectory().ToString() + @"\" + ConfigurationManager.AppSettings["directoryDatabase"];
        static string dirReport = System.Windows.Forms.Application.StartupPath + @"\" + ConfigurationManager.AppSettings["ReportFile"];
        private static string pathS12File = dir + @"\" + ConfigurationManager.AppSettings["s12File"];
        private static string pathS14File = dir + @"\" + ConfigurationManager.AppSettings["s14File"];
        private static string pathDanhba = dir + @"\" + ConfigurationManager.AppSettings["phoneBookFile"];
        private static string pathChungtu = dir + @"\" + ConfigurationManager.AppSettings["licenseFile"];
        private static string pathTaikhoan = dir + @"\" + ConfigurationManager.AppSettings["accountBankFile"];
        //private static string pathCCTKTMS = dir + @"\" + ConfigurationManager.AppSettings["cctkTMSFile"];
        private static string pathDSCB = dir + @"\" + ConfigurationManager.AppSettings["dsCanBoFile"];
        private static string totrinh = System.Windows.Forms.Application.StartupPath + @"\documents\input\CCTK\To_trinh_CC_taikhoan.doc";
        private static string quyetdinh = System.Windows.Forms.Application.StartupPath + @"\documents\input\CCTK\Quyet_dinh_CC_taikhoan.doc";
        private static string lenhthu = System.Windows.Forms.Application.StartupPath + @"\documents\input\CCTK\Lenh_thu_NSNN.doc";
        #endregion
        public static System.Data.DataTable GetCCTKOnTMSByTaxCode(string sFilePathAndName, string searchMST)
        {
            string sLine = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=";
            sLine += sFilePathAndName;
            sLine += ";Extended Properties=Excel 8.0";
            var connection = BaseController.getConnection(sLine);
            try
            {
                connection.Open();
                //DataTable dt = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                //string Sheet = dt.Rows[1].Field<string>("TABLE_NAME");
                sLine = "Select top 1 * From [CCTK$] where [Mã số thuế] = @mst";
                OleDbDataAdapter dataAdapter = new OleDbDataAdapter(sLine, connection);
                dataAdapter.SelectCommand.Parameters.AddWithValue("@mst", searchMST);
                DataSet dataSet = new DataSet();
                dataAdapter.Fill(dataSet);
                return dataSet.Tables[0];
            }
            catch (Exception ex)
            {
                //Common.Common.error("Có lỗi xảy ra khi kiểm tra dữ liệu đã tồn tại trong chứng từ đã nộp! Vui lòng kiểm tra lại!");
                Common.Common.error(ex.ToString());
                connection.Close();
                return null;
            }
            finally
            {
                connection.Close();
            }
        }
        /// <summary>
        /// [Author: hoa.nq]: Tạo báo cáo Cưỡng chế tài khoản
        /// </summary>
        /// <param name="mst">Mã số thuế</param>
        /// <param name="chinhanh">Chi nhánh</param>
        /// <param name="tendoi">Tên đội</param>
        /// <param name="tencb">Tên cán bộ</param>
        /// <returns></returns>
        public static ReportSumaryModel CCTK_CreateReport(string mst, string chinhanh, string tendoi, string tencb, string doitruong, string chicuctruong, string chicucpho, DateTime ngaycohieuluc, DateTime ngayhethieuluc)
        {
            try
            {
                bool totalS12SmallerS14 = false;
                ReportSumaryModel reportSumary = new ReportSumaryModel();
                var outputPathFile = "";
                if (!File.Exists(pathS12File))
                {
                    Common.Common.error("Không tìm thấy tài liệu S12.XLSX! Vui lòng nhập tài liệu");
                    return new ReportSumaryModel();
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(mst))
                    {
                        // ALL
                        return new ReportSumaryModel();
                    }
                    else
                    {
                        var tableS12Data = S12Controller.SearchMST(pathS12File, mst.Trim());
                        if (tableS12Data.Rows.Count <= 0)
                        {
                            Common.Common.error("Không tìm thấy dữ liệu!");
                            return new ReportSumaryModel();
                        }
                        else
                        {
                            // Check => Có TK Ngân hàng => Tạo cưỡng chế
                            if (File.Exists(pathTaikhoan))
                            {
                                var tableAccBank = NganHangController.GetTaiKhoanNganHang(pathTaikhoan, tableS12Data.Rows[0]["F2"].ToString());
                                if (tableAccBank.Rows.Count > 0)
                                {
                                    if (!File.Exists(totrinh))
                                    {
                                        Common.Common.error("Mẫu tài liệu tờ trình không tồn tại! Vui lòng nhập tài liệu");
                                        return new ReportSumaryModel();
                                    }
                                    else if (!File.Exists(quyetdinh))
                                    {
                                        Common.Common.error("Mẫu tài liệu quyết định không tồn tại! Vui lòng nhập tài liệu");
                                        return new ReportSumaryModel();
                                    }
                                    else if (!File.Exists(lenhthu))
                                    {
                                        Common.Common.error("Mẫu tài liệu lệnh thu không tồn tại! Vui lòng nhập tài liệu");
                                        return new ReportSumaryModel();
                                    }
                                    else
                                    {
                                        var tableDanhba = DanhBaController.GetDanhba(pathDanhba, tableS12Data.Rows[0]["F2"].ToString());
                                        if (!File.Exists(pathS14File))
                                        {
                                            Common.Common.error("Không tồn tại file S14!");
                                            return new ReportSumaryModel();
                                        }
                                        else
                                        {
                                            var tableS14Data = S14Controller.GetDataS14FileByMST(pathS14File, tableS12Data.Rows[0]["F2"].ToString());
                                            if (tableS14Data.Rows.Count <= 0)
                                            {
                                                Common.Common.error("Không tồn tại thông tin công ty này trên file S14! Vui lòng kiểm tra lại! ");
                                                return new ReportSumaryModel();
                                            }
                                            else
                                            {
                                                #region [hoa.qn]: 1. Replace Text
                                                // Xử lý tờ trình
                                                Document doc = new Document();
                                                doc.LoadFromFile(totrinh);
                                                // Xử lý quyết định
                                                Document qd = new Document();
                                                qd.LoadFromFile(quyetdinh);

                                                // Add Data to Model
                                                ToTrinh toTrinh = new ToTrinh();
                                                if (tableDanhba.Rows.Count > 0)
                                                {
                                                    toTrinh.tencongty = tableDanhba.Rows[0][4].ToString();
                                                    toTrinh.masothue = tableDanhba.Rows[0][3].ToString();
                                                    toTrinh.diachi = tableDanhba.Rows[0][7].ToString() + ", " + tableDanhba.Rows[0][9].ToString() + ", " + tableDanhba.Rows[0][11].ToString() + ", " + tableDanhba.Rows[0][13].ToString();
                                                    toTrinh.phuongxa = tableDanhba.Rows[0][9].ToString();
                                                    //toTrinh.quanhuyen = tableDanhba.Rows[0][11].ToString();
                                                    //toTrinh.tinhthanhpho = tableDanhba.Rows[0][13].ToString();
                                                    toTrinh.thongbaoso = tableS14Data.Rows.Count > 0 && tableS14Data.Rows[0][3].ToString() != "" ? tableS14Data.Rows[0][3].ToString() : "";
                                                    toTrinh.ngaythongbao = tableS14Data.Rows.Count > 0 && tableS14Data.Rows[0][4].ToString() != "" ? tableS14Data.Rows[0][4].ToString() : "";
                                                    toTrinh.chicuc = !string.IsNullOrWhiteSpace(chinhanh) ? chinhanh : "";
                                                    toTrinh.tencanbo = "";
                                                    toTrinh.dctbt_diachi = toTrinh.diachi;
                                                    toTrinh.ngaycohieuluc = ngaycohieuluc.ToString("dd/MM/yyyy");
                                                    toTrinh.ngayhethieuluc = ngayhethieuluc.ToString("dd/MM/yyyy");
                                                    toTrinh.Email = "...................";
                                                    toTrinh.sdt = "...................";
                                                    toTrinh.tendoi = !string.IsNullOrWhiteSpace(tendoi) ? tendoi : ".......................";
                                                    toTrinh.doitruong = !string.IsNullOrWhiteSpace(doitruong) ? doitruong : "";
                                                    toTrinh.chicuctruong = !string.IsNullOrWhiteSpace(chicuctruong) ? chicuctruong : "..........";
                                                    toTrinh.chicucpho = !string.IsNullOrWhiteSpace(chicucpho) ? chicucpho : "............";
                                                    if (!string.IsNullOrWhiteSpace(toTrinh.phuongxa))
                                                    {
                                                        toTrinh.tencanbo = tencb;
                                                        var tableDSCB = DonViController.GetDSCanBo(pathDSCB, toTrinh.phuongxa);
                                                        if (tableDSCB != null && tableDSCB.Rows.Count > 0)
                                                        {
                                                            toTrinh.Email = tableDSCB.Rows[0][3].ToString();
                                                            toTrinh.sdt = tableDSCB.Rows[0][4].ToString();
                                                        }
                                                        else
                                                        {
                                                            Common.Common.error("Không tìm thấy thông tin cán bộ phù hợp với với thông tin cán bộ và chi cục đã nhập !");
                                                            return new ReportSumaryModel();
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    Common.Common.error("Không tìm thấy dữ liệu tương ứng trong file danh bạ !");
                                                    return new ReportSumaryModel();
                                                }

                                                #region Replace Text phiếu trờ trình
                                                doc.Replace("{{TENCONGTY}}", toTrinh.tencongty, false, true);
                                                doc.Replace("{{MST}}", toTrinh.masothue, false, true);
                                                doc.Replace("{{DIACHI}}", toTrinh.diachi, false, true);
                                                doc.Replace("{{THONGBAOSO}}", toTrinh.thongbaoso, false, true);
                                                doc.Replace("{{NGAY}}", toTrinh.ngaythongbao, false, true);
                                                doc.Replace("{{CHICUC}}", toTrinh.chicuc.ToUpper().Replace("HUYỆN", "H.").Replace("QUẬN", "Q.").Replace("KHU VỰC", "KV."), false, true);
                                                doc.Replace("{{TIENTREN90NGAY}}", toTrinh.tongtientren90ngay.ToString("#,###"), false, true);
                                                doc.Replace("{{TIENCUONGCHE}}", toTrinh.tongtiencuongche.ToString("#,###"), false, true);
                                                doc.Replace("{{ChiCuc}}", toTrinh.chicuc, false, true);
                                                doc.Replace("{{CANBO}}", toTrinh.tencanbo, false, true);
                                                doc.Replace("{{TENDOI}}", toTrinh.tendoi, false, true);
                                                doc.Replace("{{DOITRUONG}}", toTrinh.doitruong, false, true);
                                                doc.Replace("{{CHICUCTRUONG}}", toTrinh.chicuctruong, false, true);
                                                doc.Replace("{{CHICUCPHO}}", toTrinh.chicucpho, false, true);
                                                #endregion

                                                #region Replace Text phiếu Quyết Định
                                                qd.Replace("{{CHICUC}}", toTrinh.chicuc.ToUpper().Replace("HUYỆN", "H.").Replace("QUẬN", "Q.").Replace("KHU VỰC", "KV."), false, true);
                                                qd.Replace("{{ThongBaoSo}}", toTrinh.thongbaoso, false, true);
                                                qd.Replace("{{NgayThongBao}}", toTrinh.ngaythongbao, false, true);
                                                qd.Replace("{{TENCONGTY}}", toTrinh.tencongty, false, true);
                                                qd.Replace("{{MASOTHUE}}", toTrinh.masothue, false, true);
                                                qd.Replace("{{DCTBT_DIACHI}}", toTrinh.dctbt_diachi, false, true);
                                                qd.Replace("{{DCTS_DIACHI}}", toTrinh.diachi, false, true);
                                                qd.Replace("{{chicuc}}", toTrinh.chicuc, false, true);
                                                qd.Replace("{{NGAYCOHIEULUC}}", toTrinh.ngaycohieuluc, false, true);
                                                qd.Replace("{{NGAYHETHIEULUC}}", toTrinh.ngayhethieuluc, false, true);
                                                qd.Replace("{{CANBO}}", toTrinh.tencanbo, false, true);
                                                qd.Replace("{{EMAIL}}", toTrinh.Email, false, true);
                                                qd.Replace("{{SDT}}", toTrinh.sdt, false, true);
                                                qd.Replace("{{CHICUCTRUONG}}", toTrinh.chicuctruong, false, true);
                                                #endregion

                                                #endregion

                                                Section section = doc.Sections[0];
                                                Section section1 = qd.Sections[0];

                                                #region [hoa.qn]: 2. Phần xử lý thông tin cưỡng chế
                                                // Thông tin cưỡng chế ở Tờ trình
                                                TextSelection selection = doc.FindString("{{TABLE}}", true, true);
                                                TextRange range = selection.GetAsOneRange();
                                                Paragraph paragraph = range.OwnerParagraph;
                                                Body body = paragraph.OwnerTextBody;
                                                int index = body.ChildObjects.IndexOf(paragraph);
                                                Table tableNo = section.AddTable(true);
                                                toTrinh.listThue = new List<Thue>();
                                                if (File.Exists(pathChungtu))
                                                {
                                                    String[] headerLicence = new string[] { "TT", "Loại thuế", "Tiểu mục", "Trong đó số tiền nợ từ 90 ngày trở lên (đ)", "Số đã nộp từ thời điểm ban hành thông báo đến ngày đề nghị cưỡng chế nợ (đ)", "Số nợ từ 91 ngày trở lên đề nghị cưỡng chế nợ kỳ này (đ)" };
                                                    int sttCT = 0;
                                                    foreach (DataRow rowS12 in tableS12Data.Rows)
                                                    {
                                                        var thue = new Thue();
                                                        sttCT++;
                                                        thue.stt = sttCT;
                                                        thue.tieumuc = rowS12[5].ToString();
                                                        thue.machuong = rowS12[3].ToString();
                                                        #region Xử lý loại thuế
                                                        if (Array.IndexOf(new string[] { "1001", "1004" }, thue.tieumuc) > -1)
                                                        {
                                                            thue.loaithue = "Thuế TNCN";
                                                        }
                                                        else if (Array.IndexOf(new string[] { "1052", "1053" }, thue.tieumuc) > -1)
                                                        {
                                                            thue.loaithue = "Thuế TNDN";
                                                        }
                                                        else if (Array.IndexOf(new string[] { "1701" }, thue.tieumuc) > -1)
                                                        {
                                                            thue.loaithue = "Thuế GTGT";
                                                        }
                                                        else if (Array.IndexOf(new string[] { "1757" }, thue.tieumuc) > -1)
                                                        {
                                                            thue.loaithue = "Thuế TTĐB";
                                                        }
                                                        else if (Array.IndexOf(new string[] { "2862", "2863", "2864" }, thue.tieumuc) > -1)
                                                        {
                                                            thue.loaithue = "Lệ phí MB";
                                                        }
                                                        else if (Array.IndexOf(new string[] { "4254", "4268" }, thue.tieumuc) > -1)
                                                        {
                                                            thue.loaithue = "Tiền phạt";
                                                        }
                                                        else if (Array.IndexOf(new string[] { "4272", "4917", "4918", "4931", "4944" }, thue.tieumuc) > -1)
                                                        {
                                                            thue.loaithue = "Tiền chậm nộp";
                                                        }
                                                        else
                                                        {
                                                            thue.loaithue = "";
                                                        }
                                                        #endregion
                                                        thue.kythue = "";
                                                        // [Tiểu mục]: Bảng Chứng từ
                                                        decimal col11 = !string.IsNullOrWhiteSpace(rowS12[10].ToString()) ? Convert.ToDecimal(rowS12[10].ToString()) : 0;
                                                        decimal col12 = !string.IsNullOrWhiteSpace(rowS12[11].ToString()) ? Convert.ToDecimal(rowS12[11].ToString()) : 0;
                                                        thue.notren90ngay = col11 + col12;
                                                        toTrinh.tongtientren90ngay = toTrinh.tongtientren90ngay + thue.notren90ngay;
                                                        var tableLicense = ChungTuController.GetListChungTu(pathChungtu, rowS12[1].ToString(), rowS12[5].ToString()); // 1: MST; 2: Tiểu mục
                                                        if (thue.notren90ngay > 0)
                                                        {
                                                            if (tableLicense.Rows.Count > 0)
                                                            {
                                                                thue.sotiennop = !string.IsNullOrWhiteSpace(tableLicense.Rows[0][3].ToString()) ? Convert.ToDecimal(tableLicense.Rows[0][3].ToString()) : 0;
                                                                toTrinh.tongsonop = toTrinh.tongsonop + thue.sotiennop;
                                                            }
                                                            thue.notren91ngaycuongche = thue.notren90ngay > thue.sotiennop ? Math.Abs(thue.notren90ngay - thue.sotiennop) : 0;
                                                            toTrinh.tongtiencuongche = toTrinh.tongtiencuongche + thue.notren91ngaycuongche;
                                                            toTrinh.listThue.Add(thue);
                                                        }
                                                    }

                                                    #region Kiểm tra file S14
                                                    decimal soTienS14 = Convert.ToDecimal(tableS14Data.Rows[0]["F12"]);
                                                    if (toTrinh.tongtientren90ngay < soTienS14)
                                                    {
                                                        totalS12SmallerS14 = true;
                                                    }
                                                    #endregion

                                                    tableNo.ResetCells(toTrinh.listThue.Count + 2, headerLicence.Length);
                                                    TableRow FRow1 = tableNo.Rows[0];
                                                    FRow1.IsHeader = true;
                                                    //FRow.RowFormat.BackColor = Color.AliceBlue;
                                                    for (int i = 0; i < headerLicence.Length; i++)
                                                    {
                                                        //Cell Alignment
                                                        Paragraph p = FRow1.Cells[i].AddParagraph();
                                                        FRow1.Cells[i].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                                        p.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                                        //Data Format
                                                        TextRange TR = p.AppendText(headerLicence[i]);
                                                        TR.CharacterFormat.FontName = "Times New Roman";
                                                        TR.CharacterFormat.FontSize = 13;
                                                        switch (i)
                                                        {
                                                            case 0:
                                                                FRow1.Cells[i].SetCellWidth(5, CellWidthType.Percentage); break;
                                                            case 1:
                                                                FRow1.Cells[i].SetCellWidth(20, CellWidthType.Percentage); break;
                                                            case 2:
                                                                FRow1.Cells[i].SetCellWidth(10, CellWidthType.Percentage); break;
                                                            case 3:
                                                                FRow1.Cells[i].SetCellWidth(20, CellWidthType.Percentage);
                                                                if (totalS12SmallerS14)
                                                                {
                                                                    FRow1.Cells[i].CellFormat.BackColor = System.Drawing.Color.Yellow;
                                                                }
                                                                break;
                                                            case 4:
                                                                FRow1.Cells[i].SetCellWidth(25, CellWidthType.Percentage); break;
                                                            case 5:
                                                                FRow1.Cells[i].SetCellWidth(20, CellWidthType.Percentage); break;
                                                        }
                                                    }

                                                    //Data Row
                                                    for (int r = 0; r < toTrinh.listThue.Count; r++)
                                                    {
                                                        TableRow DataRow = tableNo.Rows[r + 1];
                                                        // 1. STT
                                                        DataRow.Cells[0].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                                        Paragraph p0 = DataRow.Cells[0].AddParagraph();
                                                        TextRange TR0 = p0.AppendText(toTrinh.listThue[r].stt.ToString());
                                                        p0.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                                        TR0.CharacterFormat.FontName = "Times New Roman";
                                                        TR0.CharacterFormat.FontSize = 13;
                                                        DataRow.Cells[0].SetCellWidth(5, CellWidthType.Percentage);
                                                        // 2. Loại thuế
                                                        DataRow.Cells[1].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                                        Paragraph p1 = DataRow.Cells[1].AddParagraph();
                                                        TextRange TR1 = p1.AppendText(toTrinh.listThue[r].loaithue.ToString());
                                                        p1.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                                        TR1.CharacterFormat.FontName = "Times New Roman";
                                                        TR1.CharacterFormat.FontSize = 13;
                                                        DataRow.Cells[1].SetCellWidth(20, CellWidthType.Percentage);
                                                        // 3. Tiểu mục
                                                        DataRow.Cells[2].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                                        Paragraph p2 = DataRow.Cells[2].AddParagraph();
                                                        TextRange TR2 = p2.AppendText(toTrinh.listThue[r].tieumuc.ToString());
                                                        p2.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                                        TR2.CharacterFormat.FontName = "Times New Roman";
                                                        TR2.CharacterFormat.FontSize = 13;
                                                        DataRow.Cells[2].SetCellWidth(10, CellWidthType.Percentage);
                                                        // 4. Trong đó số nợ từ 90 ngày trở lên (đ)
                                                        DataRow.Cells[3].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                                        Paragraph p3 = DataRow.Cells[3].AddParagraph();
                                                        TextRange TR3 = p3.AppendText(toTrinh.listThue[r].notren90ngay.ToString("#,###"));
                                                        p3.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                                        TR3.CharacterFormat.FontName = "Times New Roman";
                                                        TR3.CharacterFormat.FontSize = 13;
                                                        DataRow.Cells[3].SetCellWidth(20, CellWidthType.Percentage);
                                                        if (totalS12SmallerS14)
                                                        {
                                                            DataRow.Cells[3].CellFormat.BackColor = System.Drawing.Color.Yellow;
                                                        }
                                                        // 5. Số tiền đa nộp từ thời điểm banhành thông báo đến ngày đề nghị cưỡng chế nợ (đ)
                                                        DataRow.Cells[4].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                                        Paragraph p4 = DataRow.Cells[4].AddParagraph();
                                                        TextRange TR4 = p4.AppendText(toTrinh.listThue[r].sotiennop > 0 ? toTrinh.listThue[r].sotiennop.ToString("#,###") : "0");
                                                        p4.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                                        TR4.CharacterFormat.FontName = "Times New Roman";
                                                        TR4.CharacterFormat.FontSize = 13;
                                                        DataRow.Cells[4].SetCellWidth(25, CellWidthType.Percentage);
                                                        // 6. Số nợ từ 91 ngày trở lên đề nghị cưỡng chế nợ kỳ này (d)
                                                        DataRow.Cells[5].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                                        Paragraph p5 = DataRow.Cells[5].AddParagraph();
                                                        TextRange TR5 = p5.AppendText(toTrinh.listThue[r].notren91ngaycuongche.ToString("#,###"));
                                                        p5.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                                        TR5.CharacterFormat.FontName = "Times New Roman";
                                                        TR5.CharacterFormat.FontSize = 13;
                                                        DataRow.Cells[5].SetCellWidth(20, CellWidthType.Percentage);
                                                    }

                                                    #region Xử lý dòng cuối cùng (Tổng tiền)
                                                    // Merge dòng cuối cùng lấy Name: Tổng
                                                    tableNo.ApplyHorizontalMerge(toTrinh.listThue.Count + 1, 0, 2);
                                                    TableRow row = tableNo.Rows[toTrinh.listThue.Count + 1];
                                                    row.Cells[0].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                                    Paragraph pF = row.Cells[0].AddParagraph();
                                                    TextRange TRF = pF.AppendText("Tổng cộng");
                                                    pF.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                                    TRF.CharacterFormat.FontName = "Times New Roman";
                                                    TRF.CharacterFormat.FontSize = 13;
                                                    TRF.CharacterFormat.Bold = true;

                                                    row.Cells[3].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                                    Paragraph pF1 = row.Cells[3].AddParagraph();
                                                    TextRange TRF1 = pF1.AppendText(toTrinh.tongtientren90ngay > 0 ? toTrinh.tongtientren90ngay.ToString("#,###") : "0");
                                                    pF1.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                                    TRF1.CharacterFormat.FontName = "Times New Roman";
                                                    TRF1.CharacterFormat.FontSize = 13;
                                                    TRF1.CharacterFormat.Bold = true;
                                                    if (totalS12SmallerS14)
                                                    {
                                                        row.Cells[3].CellFormat.BackColor = System.Drawing.Color.Yellow;
                                                    }

                                                    row.Cells[4].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                                    Paragraph pF2 = row.Cells[4].AddParagraph();
                                                    TextRange TRF2 = pF2.AppendText(toTrinh.tongsonop > 0 ? toTrinh.tongsonop.ToString("#,###") : "0");
                                                    pF2.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                                    TRF2.CharacterFormat.FontName = "Times New Roman";
                                                    TRF2.CharacterFormat.FontSize = 13;
                                                    TRF2.CharacterFormat.Bold = true;

                                                    row.Cells[5].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                                    Paragraph pF3 = row.Cells[5].AddParagraph();
                                                    TextRange TRF3 = pF3.AppendText(toTrinh.tongtiencuongche > 0 ? toTrinh.tongtiencuongche.ToString("#,###") : "0");
                                                    pF3.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                                    TRF3.CharacterFormat.FontName = "Times New Roman";
                                                    TRF3.CharacterFormat.FontSize = 13;
                                                    TRF3.CharacterFormat.Bold = true;
                                                    #endregion

                                                    doc.Replace("{{TIEN90NGAY}}", toTrinh.tongtientren90ngay > 0 ? toTrinh.tongtientren90ngay.ToString("#,###") : "0", false, true);
                                                    doc.Replace("{{TIENCC}}", toTrinh.tongtiencuongche > 0 ? toTrinh.tongtiencuongche.ToString("#,###") : "0", false, true);
                                                    qd.Replace("{{SOTIENCUONGCHE}}", toTrinh.tongtiencuongche > 0 ? toTrinh.tongtiencuongche.ToString("#,###") : "0", false, true);
                                                    qd.Replace("{{SOTIENCUONGCHEBANGCHU}}", Common.ConvertCommon.cCommonFunction.DocTienBangChu(Convert.ToInt64(toTrinh.tongtiencuongche), " đồng"), false, true);
                                                }
                                                else
                                                {
                                                    Common.Common.error("Không tìm thấy dữ liệu tương ứng trong file chứng từ!");
                                                    return new ReportSumaryModel();
                                                }

                                                body.ChildObjects.Remove(paragraph);
                                                body.ChildObjects.Insert(index, tableNo);
                                                #endregion

                                                #region Xử lý các file báo cáo đã tồn tài!
                                                // Get Folder Time
                                                var timeFolder = S12Controller.SearchMonthReport(pathS12File);
                                                var path = "";
                                                // Get Directory: Bao Cao/062020/Công ty .../CCTK
                                                if (totalS12SmallerS14)
                                                {
                                                    path = dirReport + @"\" + timeFolder + @"\" + tableS12Data.Rows[0]["F2"].ToString() + "_Lỗi" + @"\CCTK";
                                                }
                                                else
                                                {
                                                    path = dirReport + @"\" + timeFolder + @"\" + tableS12Data.Rows[0]["F2"].ToString() + @"\CCTK";
                                                }
                                                outputPathFile = path + @"\" + ConfigurationManager.AppSettings["fileTongHop"];
                                                if (!Directory.Exists(path))
                                                {
                                                    Directory.CreateDirectory(path);
                                                }
                                                // Tờ trình
                                                string reportFileToTrinh = path + @"\" + ConfigurationManager.AppSettings["totrinh"];
                                                if (File.Exists(reportFileToTrinh))
                                                {
                                                    File.Delete(reportFileToTrinh);
                                                }
                                                // Quyết định
                                                string reportFileQD = path + @"\" + ConfigurationManager.AppSettings["quyetdinh"];
                                                if (File.Exists(reportFileQD))
                                                {
                                                    File.Delete(reportFileQD);
                                                }
                                                #endregion

                                                #region [hoa.nq] 3. Phần xử lý fill table Danh sách tài khoản
                                                /// [START]
                                                // File Tờ trình
                                                TextSelection selectionBank = doc.FindString("{{TABLEBANK}}", true, true);
                                                TextRange rangeBank = selectionBank.GetAsOneRange();
                                                Paragraph paragraphBank = rangeBank.OwnerParagraph;
                                                Body bodyBank = paragraphBank.OwnerTextBody;
                                                int indexBank = bodyBank.ChildObjects.IndexOf(paragraphBank);
                                                Table tableBank = section.AddTable(true);
                                                // File Quyết định
                                                TextSelection selectionQDBank = qd.FindString("{{TABLEBANK}}", true, true);
                                                TextRange rangeQDBank = selectionQDBank.GetAsOneRange();
                                                Paragraph paragraphQDBank = rangeQDBank.OwnerParagraph;
                                                Body bodyQDBank = paragraphQDBank.OwnerTextBody;
                                                int indexQDBank = bodyQDBank.ChildObjects.IndexOf(paragraphQDBank);
                                                Table tableQDBank = section1.AddTable(true);
                                                //
                                                String[] headerBank = new string[] { "TT", "Số Tài khoản", "Ngân hàng" };
                                                String[] headerQDBank = new string[] { "STT", "Số Tài khoản", "Mở tại ngân hàng" };
                                                //var tableAccBank = NganHangController.GetTaiKhoanNganHang(pathTaikhoan, tableS12Data.Rows[0]["F2"].ToString());
                                                int sttTK = 0;
                                                toTrinh.listTaiKhoan = new List<TaiKhoanNganHang>();
                                                foreach (DataRow row in tableAccBank.Rows)
                                                {
                                                    sttTK++;
                                                    var taikhoan = new TaiKhoanNganHang();
                                                    taikhoan.stt = sttTK;
                                                    taikhoan.sotaikhoan = row[3].ToString();
                                                    taikhoan.tennganhang = row[4].ToString();
                                                    var getDiachi = Common.Common.getBankAddress(taikhoan.tennganhang);
                                                    taikhoan.diachi = !string.IsNullOrWhiteSpace(getDiachi) ? getDiachi : "<Không tìm thấy địa chỉ>";
                                                    toTrinh.listTaiKhoan.Add(taikhoan);
                                                    // Lệnh thu
                                                    string reportFileLT = path + @"\Lenh_thu_" + taikhoan.sotaikhoan + "_" + Regex.Replace(Common.Common.convertToUnSign2(taikhoan.tennganhang), @"(\s+|@|&|'|,|\(|\)|<|>|#)", "_") + ".doc";
                                                    if (File.Exists(reportFileLT))
                                                    {
                                                        File.Delete(reportFileLT);
                                                    }
                                                    // Xử lý lệnh thu
                                                    Document lt = new Document();
                                                    lt.LoadFromFile(lenhthu);

                                                    lt.Replace("{{CHICUC}}", toTrinh.chicuc.ToUpper().Replace("HUYỆN", "H.").Replace("QUẬN", "Q.").Replace("KHU VỰC", "KV."), false, true);
                                                    lt.Replace("{{TENNGANHANG}}", taikhoan.tennganhang, false, true);
                                                    lt.Replace("{{DIACHINGANHANG}}", taikhoan.diachi, false, true);
                                                    lt.Replace("{{SOTK}}", taikhoan.sotaikhoan, false, true);
                                                    lt.Replace("{{TENCONGTY}}", toTrinh.tencongty, false, true);
                                                    lt.Replace("{{MST}}", toTrinh.masothue, false, true);
                                                    lt.Replace("{{DCTS_DIACHI}}", toTrinh.diachi, false, true);
                                                    lt.Replace("{{CHICUCTRUONG}}", toTrinh.chicuctruong, false, true);
                                                    // Replace Table Lệnh thu
                                                    Section section2 = lt.Sections[0];
                                                    TextSelection selectionLT = lt.FindString("{{TABLE}}", true, true);
                                                    TextRange rangeLT = selectionLT.GetAsOneRange();
                                                    Paragraph paragraphLT = rangeLT.OwnerParagraph;
                                                    Body bodyLT = paragraphLT.OwnerTextBody;
                                                    int indexLT = bodyLT.ChildObjects.IndexOf(paragraphLT);
                                                    Table tableLT = section2.AddTable(true);

                                                    String[] headerLicence = new string[] { "TT", "Loại thuế", "Mã chương", "Tiểu mục", "Kỳ thuế", "Số tiền" };
                                                    tableLT.ResetCells(toTrinh.listThue.Count + 2, headerLicence.Length);
                                                    TableRow FRowLT = tableLT.Rows[0];
                                                    FRowLT.IsHeader = true;
                                                    // Header
                                                    for (int i = 0; i < headerLicence.Length; i++)
                                                    {
                                                        //Cell Alignment
                                                        Paragraph p = FRowLT.Cells[i].AddParagraph();
                                                        FRowLT.Cells[i].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                                        p.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                                        //Data Format
                                                        TextRange TR = p.AppendText(headerLicence[i]);
                                                        TR.CharacterFormat.FontName = "Times New Roman";
                                                        TR.CharacterFormat.FontSize = 13;
                                                        switch (i)
                                                        {
                                                            case 0:
                                                                FRowLT.Cells[i].SetCellWidth(5, CellWidthType.Percentage); break;
                                                            case 1:
                                                                FRowLT.Cells[i].SetCellWidth(25, CellWidthType.Percentage); break;
                                                            case 2:
                                                                FRowLT.Cells[i].SetCellWidth(20, CellWidthType.Percentage); break;
                                                            case 3:
                                                                FRowLT.Cells[i].SetCellWidth(20, CellWidthType.Percentage); break;
                                                            case 4:
                                                                FRowLT.Cells[i].SetCellWidth(10, CellWidthType.Percentage); break;
                                                            case 5:
                                                                FRowLT.Cells[i].SetCellWidth(20, CellWidthType.Percentage); break;
                                                        }
                                                    }
                                                    // Row Data
                                                    //Data Row
                                                    for (int r = 0; r < toTrinh.listThue.Count; r++)
                                                    {
                                                        TableRow DataRow = tableLT.Rows[r + 1];
                                                        // 1. STT
                                                        DataRow.Cells[0].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                                        Paragraph p0 = DataRow.Cells[0].AddParagraph();
                                                        TextRange TR0 = p0.AppendText(toTrinh.listThue[r].stt.ToString());
                                                        p0.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                                        TR0.CharacterFormat.FontName = "Times New Roman";
                                                        TR0.CharacterFormat.FontSize = 13;
                                                        DataRow.Cells[0].SetCellWidth(5, CellWidthType.Percentage);
                                                        // 2. Loại thuế
                                                        DataRow.Cells[1].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                                        Paragraph p1 = DataRow.Cells[1].AddParagraph();
                                                        TextRange TR1 = p1.AppendText(toTrinh.listThue[r].loaithue);
                                                        p1.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                                        TR1.CharacterFormat.FontName = "Times New Roman";
                                                        TR1.CharacterFormat.FontSize = 13;
                                                        DataRow.Cells[1].SetCellWidth(25, CellWidthType.Percentage);
                                                        // 3. Mã chương
                                                        DataRow.Cells[2].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                                        Paragraph p2 = DataRow.Cells[2].AddParagraph();
                                                        TextRange TR2 = p2.AppendText(toTrinh.listThue[r].machuong);
                                                        p2.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                                        TR2.CharacterFormat.FontName = "Times New Roman";
                                                        TR2.CharacterFormat.FontSize = 13;
                                                        DataRow.Cells[2].SetCellWidth(20, CellWidthType.Percentage);
                                                        // 4. Tiểu mục
                                                        DataRow.Cells[3].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                                        Paragraph p3 = DataRow.Cells[3].AddParagraph();
                                                        TextRange TR3 = p3.AppendText(toTrinh.listThue[r].tieumuc);
                                                        p3.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                                        TR3.CharacterFormat.FontName = "Times New Roman";
                                                        TR3.CharacterFormat.FontSize = 13;
                                                        DataRow.Cells[3].SetCellWidth(20, CellWidthType.Percentage);
                                                        // 5. Kỳ thuế
                                                        DataRow.Cells[4].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                                        Paragraph p4 = DataRow.Cells[4].AddParagraph();
                                                        TextRange TR4 = p4.AppendText(toTrinh.listThue[r].kythue);
                                                        p4.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                                        TR4.CharacterFormat.FontName = "Times New Roman";
                                                        TR4.CharacterFormat.FontSize = 13;
                                                        DataRow.Cells[4].SetCellWidth(10, CellWidthType.Percentage);
                                                        // 6. Số tiền
                                                        DataRow.Cells[5].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                                        Paragraph p5 = DataRow.Cells[5].AddParagraph();
                                                        TextRange TR5 = p5.AppendText(toTrinh.listThue[r].notren91ngaycuongche.ToString("#,###"));
                                                        p5.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                                        TR5.CharacterFormat.FontName = "Times New Roman";
                                                        TR5.CharacterFormat.FontSize = 13;
                                                        DataRow.Cells[5].SetCellWidth(20, CellWidthType.Percentage);
                                                    }

                                                    #region Xử lý dòng cuối cùng (Tổng tiền)
                                                    // Merge dòng cuối cùng lấy Name: Tổng
                                                    tableLT.ApplyHorizontalMerge(toTrinh.listThue.Count + 1, 0, 3);
                                                    TableRow rowLT = tableLT.Rows[toTrinh.listThue.Count + 1];

                                                    rowLT.Cells[0].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                                    Paragraph pF = rowLT.Cells[0].AddParagraph();
                                                    TextRange TRF = pF.AppendText("Tổng cộng");
                                                    pF.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                                    TRF.CharacterFormat.FontName = "Times New Roman";
                                                    TRF.CharacterFormat.FontSize = 13;
                                                    TRF.CharacterFormat.Bold = true;

                                                    rowLT.Cells[5].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                                    Paragraph pF3 = rowLT.Cells[5].AddParagraph();
                                                    TextRange TRF3 = pF3.AppendText(toTrinh.tongtiencuongche > 0 ? toTrinh.tongtiencuongche.ToString("#,###") : "0");
                                                    pF3.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                                    TRF3.CharacterFormat.FontName = "Times New Roman";
                                                    TRF3.CharacterFormat.FontSize = 13;
                                                    TRF3.CharacterFormat.Bold = true;
                                                    #endregion
                                                    lt.Replace("{{SOTIENBANGCHU}}", ConvertCommon.cCommonFunction.DocTienBangChu(Convert.ToInt64(toTrinh.tongtiencuongche), " đồng"), false, true);
                                                    bodyLT.ChildObjects.Remove(paragraphLT);
                                                    bodyLT.ChildObjects.Insert(indexLT, tableLT);

                                                    lt.SaveToFile(reportFileLT, FileFormat.Docx);
                                                    //listInputFiles.Add(reportFileLT);
                                                }
                                                //Add Cells Phiếu Tờ trình
                                                tableBank.ResetCells(toTrinh.listTaiKhoan.Count + 1, headerBank.Length);
                                                // Add Cells Phiếu Quyết định
                                                tableQDBank.ResetCells(toTrinh.listTaiKhoan.Count + 1, headerQDBank.Length);

                                                #region [Tờ trình] Header Row
                                                //Header Row (Tờ trình)
                                                TableRow FRow = tableBank.Rows[0];
                                                FRow.IsHeader = true;
                                                for (int i = 0; i < headerBank.Length; i++)
                                                {
                                                    //Cell Alignment
                                                    Paragraph p = FRow.Cells[i].AddParagraph();
                                                    FRow.Cells[i].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                                    if (i == 0)
                                                    {
                                                        FRow.Cells[i].SetCellWidth(10, CellWidthType.Percentage);
                                                    }
                                                    else if (i == 1)
                                                    {
                                                        FRow.Cells[i].SetCellWidth(30, CellWidthType.Percentage);
                                                    }
                                                    else if (i == 2)
                                                    {
                                                        FRow.Cells[i].SetCellWidth(70, CellWidthType.Percentage);
                                                    }
                                                    p.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                                    //Data Format
                                                    TextRange TR = p.AppendText(headerBank[i]);
                                                    TR.CharacterFormat.FontName = "Times New Roman";
                                                    TR.CharacterFormat.FontSize = 13;
                                                    TR.CharacterFormat.Bold = true;
                                                }
                                                #endregion

                                                #region [Quyết định] Header Row
                                                // Header Row (Quyết định)
                                                TableRow FRowQD = tableQDBank.Rows[0];
                                                FRowQD.IsHeader = true;
                                                for (int i = 0; i < headerQDBank.Length; i++)
                                                {
                                                    Paragraph p = FRowQD.Cells[i].AddParagraph();
                                                    FRowQD.Cells[i].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                                    if (i == 0)
                                                    {
                                                        FRowQD.Cells[i].SetCellWidth(10, CellWidthType.Percentage);
                                                    }
                                                    else if (i == 1)
                                                    {
                                                        FRowQD.Cells[i].SetCellWidth(30, CellWidthType.Percentage);
                                                    }
                                                    else if (i == 2)
                                                    {
                                                        FRowQD.Cells[i].SetCellWidth(70, CellWidthType.Percentage);
                                                    }
                                                    p.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                                    TextRange TR = p.AppendText(headerQDBank[i]);
                                                    TR.CharacterFormat.FontName = "Times New Roman";
                                                    TR.CharacterFormat.FontSize = 13;
                                                    TR.CharacterFormat.Bold = true;
                                                }
                                                #endregion

                                                //Data Row
                                                for (int r = 0; r < toTrinh.listTaiKhoan.Count; r++)
                                                {
                                                    #region Tờ trình
                                                    TableRow DataRow = tableBank.Rows[r + 1];
                                                    DataRow.Cells[0].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                                    Paragraph p0 = DataRow.Cells[0].AddParagraph();
                                                    TextRange TR0 = p0.AppendText(toTrinh.listTaiKhoan[r].stt.ToString());
                                                    DataRow.Cells[0].SetCellWidth(10, CellWidthType.Percentage);
                                                    p0.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                                    TR0.CharacterFormat.FontName = "Times New Roman";
                                                    TR0.CharacterFormat.FontSize = 13;
                                                    TR0.CharacterFormat.Bold = true;

                                                    DataRow.Cells[1].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                                    Paragraph p1 = DataRow.Cells[1].AddParagraph();
                                                    TextRange TR1 = p1.AppendText(toTrinh.listTaiKhoan[r].sotaikhoan.ToString());
                                                    p1.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                                    TR1.CharacterFormat.FontName = "Times New Roman";
                                                    TR1.CharacterFormat.FontSize = 13;

                                                    DataRow.Cells[2].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                                    Paragraph p2 = DataRow.Cells[2].AddParagraph();
                                                    TextRange TR2 = p2.AppendText(toTrinh.listTaiKhoan[r].tennganhang.ToString());
                                                    p2.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                                    TR2.CharacterFormat.FontName = "Times New Roman";
                                                    TR2.CharacterFormat.FontSize = 13;
                                                    DataRow.Cells[2].SetCellWidth(70, CellWidthType.Percentage);
                                                    #endregion

                                                    #region Quyết định
                                                    TableRow DataRowQD = tableQDBank.Rows[r + 1];

                                                    DataRowQD.Cells[0].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                                    Paragraph p0QD = DataRowQD.Cells[0].AddParagraph();
                                                    TextRange TR0QD = p0QD.AppendText(toTrinh.listTaiKhoan[r].stt.ToString());
                                                    DataRowQD.Cells[0].SetCellWidth(10, CellWidthType.Percentage);
                                                    p0QD.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                                    TR0QD.CharacterFormat.FontName = "Times New Roman";
                                                    TR0QD.CharacterFormat.FontSize = 13;
                                                    TR0QD.CharacterFormat.Bold = true;

                                                    DataRowQD.Cells[1].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                                    Paragraph p1QD = DataRowQD.Cells[1].AddParagraph();
                                                    TextRange TR1QD = p1QD.AppendText(toTrinh.listTaiKhoan[r].sotaikhoan.ToString());
                                                    p1QD.Format.HorizontalAlignment = HorizontalAlignment.Center;
                                                    TR1QD.CharacterFormat.FontName = "Times New Roman";
                                                    TR1QD.CharacterFormat.FontSize = 13;
                                                    DataRowQD.Cells[1].SetCellWidth(30, CellWidthType.Percentage);

                                                    DataRowQD.Cells[2].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                                                    Paragraph p2QD = DataRowQD.Cells[2].AddParagraph();
                                                    TextRange TR2QD = p2QD.AppendText(toTrinh.listTaiKhoan[r].tennganhang.ToString());
                                                    p2QD.Format.HorizontalAlignment = HorizontalAlignment.Justify;
                                                    TR2QD.CharacterFormat.FontName = "Times New Roman";
                                                    TR2QD.CharacterFormat.FontSize = 13;
                                                    DataRowQD.Cells[2].SetCellWidth(70, CellWidthType.Percentage);
                                                    #endregion

                                                }

                                                bodyBank.ChildObjects.Remove(paragraphBank);
                                                bodyBank.ChildObjects.Insert(indexBank, tableBank);

                                                bodyQDBank.ChildObjects.Remove(paragraphQDBank);
                                                bodyQDBank.ChildObjects.Insert(indexQDBank, tableQDBank);
                                                /// [END]
                                                #endregion

                                                doc.SaveToFile(reportFileToTrinh, FileFormat.Docx);
                                                qd.SaveToFile(reportFileQD, FileFormat.Docx);
                                                reportSumary.listInputFiles.Add(reportFileToTrinh);
                                                reportSumary.listInputFiles.Add(reportFileQD);
                                                reportSumary.reportFileToTrinh = reportFileToTrinh;
                                                reportSumary.reportFileQD = reportFileQD;
                                                reportSumary.outputPathFile = outputPathFile;
                                                return reportSumary;
                                            }
                                        }
                                        
                                    }
                                } else
                                {
                                    Common.Common.error("Không tồn tại tài khoản ngân hàng nào ứng với công ty này!");
                                    return new ReportSumaryModel();
                                }
                            }
                            else
                            {
                                Common.Common.error("Không tồn tại file dữ liệu tài khoản ngân hàng!");
                                return new ReportSumaryModel();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Common.Common.error(ex.ToString());
                return new ReportSumaryModel();
            }
        }
    }
}
