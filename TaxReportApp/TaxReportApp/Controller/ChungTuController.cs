﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TaxReportApp.Model;

namespace TaxReportApp.Controller
{
    class ChungTuController : BaseController
    {
        public static DataTable GetListChungTu(string sFilePathAndName, string searchMST, string searchTieuMuc)
        {
            string sLine = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=";
            sLine += sFilePathAndName;
            sLine += ";Extended Properties=Excel 8.0";
            var connection = BaseController.getConnection(sLine);
            try
            {
                connection.Open();

                DataTable dt = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                string Sheet = dt.Rows[0].Field<string>("TABLE_NAME");

                sLine = "Select top 1 * From [" + Sheet + "]";

                OleDbDataAdapter dataAdapter = new OleDbDataAdapter(sLine, connection);

                DataSet dataSet = new DataSet();

                dataAdapter.Fill(dataSet);

                var data = dataSet.Tables[0].Columns[0].ColumnName;

                string sLine2 = "Select top 1 * From [" + Sheet + "] where [" + dataSet.Tables[0].Columns[0].ColumnName.ToString() + "] = @mst AND [" + dataSet.Tables[0].Columns[2].ColumnName.ToString() + "] = @tm";

                OleDbDataAdapter dataAdapter2 = new OleDbDataAdapter(sLine2, connection);

                dataAdapter2.SelectCommand.Parameters.AddWithValue("@mst", searchMST);

                dataAdapter2.SelectCommand.Parameters.AddWithValue("@tm", searchTieuMuc);

                DataSet dataSet2 = new DataSet();

                dataAdapter2.Fill(dataSet2);

                return dataSet2.Tables[0];

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Lỗi");
                return null;
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// Lấy danh sách dữ liệu chứng từ của công ty theo mã số thuế
        /// </summary>
        /// <param name="sFilePathAndName"></param>
        /// <param name="searchMST"></param>
        /// <returns></returns>
        public static DataTable GetListChungTuByMST(string sFilePathAndName, string searchMST)
        {
            string sLine = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=";
            sLine += sFilePathAndName;
            sLine += ";Extended Properties=Excel 8.0";
            var connection = BaseController.getConnection(sLine);
            try
            {
                connection.Open();

                DataTable dt = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                string Sheet = dt.Rows[0].Field<string>("TABLE_NAME");

                sLine = "Select * From [" + Sheet + "]";

                OleDbDataAdapter dataAdapter = new OleDbDataAdapter(sLine, connection);

                DataSet dataSet = new DataSet();

                dataAdapter.Fill(dataSet);

                var data = dataSet.Tables[0].Columns[0].ColumnName;

                string sLine2 = "Select * From [" + Sheet + "] where [" + dataSet.Tables[0].Columns[0].ColumnName.ToString() + "] = @mst";

                OleDbDataAdapter dataAdapter2 = new OleDbDataAdapter(sLine2, connection);

                dataAdapter2.SelectCommand.Parameters.AddWithValue("@mst", searchMST);

                DataSet dataSet2 = new DataSet();

                dataAdapter2.Fill(dataSet2);

                return dataSet2.Tables[0];

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Lỗi");
                return null;
            }
            finally
            {
                connection.Close();
            }
        }
        /// <summary>
        /// Kiểm tra công ty này có nằm trong Chứng từ đã nộp
        /// </summary>
        /// <returns></returns>
        public static DataTable GetLicenceSubmitedByTaxCode(string sFilePathAndName, string searchMST)
        {
            try
            {
                string sLine = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=";
                sLine += sFilePathAndName;
                sLine += ";Extended Properties=Excel 8.0";
                var connection = BaseController.getConnection(sLine);
                connection.Open();
                DataTable dt = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string Sheet = dt.Rows[1].Field<string>("TABLE_NAME");
                sLine = "Select top 1 * From [Sheet1$] where [Mã số thuế] = @mst";
                OleDbDataAdapter dataAdapter = new OleDbDataAdapter(sLine, connection);
                dataAdapter.SelectCommand.Parameters.AddWithValue("@mst", searchMST);
                DataSet dataSet = new DataSet();
                dataAdapter.Fill(dataSet);
                return dataSet.Tables[0];

            }
            catch (Exception)
            {
                Common.Common.error("Có lỗi xảy ra khi kiểm tra dữ liệu đã tồn tại trong chứng từ đã nộp! Vui lòng kiểm tra lại!");
                return null;
            }
        }
    }
}
