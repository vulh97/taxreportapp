﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;
using System.Data;
using System.Windows.Forms;

namespace TaxReportApp.Controller
{
    class NganHangController
    {
        public static DataTable GetTaiKhoanNganHang(string sFilePathAndName, string textSearch)
        {
            string sLine = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=";
            sLine += sFilePathAndName;
            sLine += ";Extended Properties=Excel 8.0";
            var connection = BaseController.getConnection(sLine);
            try
            {
                connection.Open();
                DataTable dt = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string Sheet = dt.Rows[0].Field<string>("TABLE_NAME");
                sLine = "Select * From [" + Sheet + "] Where [MST] = @mst";
                OleDbDataAdapter dataAdapter = new OleDbDataAdapter(sLine, connection);
                dataAdapter.SelectCommand.Parameters.AddWithValue("@mst", textSearch);
                DataSet dataSet = new DataSet();
                dataAdapter.Fill(dataSet);
                return dataSet.Tables[0];
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Lỗi");
                return null;
            }
            finally
            {
                connection.Close();
            }

        }
    }
}
