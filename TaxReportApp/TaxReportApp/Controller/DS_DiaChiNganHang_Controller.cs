﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaxReportApp.Controller
{
    class DS_DiaChiNganHang_Controller
    {
        public static List<DataRow> SearchMST(string sFilePathAndName, string tenNH, string chiNhanh, string PGD, string tenNHVietTat)
        {
            string sLine = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=";
            sLine += sFilePathAndName;
            sLine += ";Extended Properties=Excel 8.0";
            var connection = BaseController.getConnection(sLine);
            var listTenVietTat = tenNHVietTat.Split('|');
            try
            {
                connection.Open();
                DataTable dt = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string Sheet = dt.Rows[0].Field<string>("TABLE_NAME");
                sLine = "Select * From [" + Sheet + "]";
                OleDbDataAdapter dataAdapter = new OleDbDataAdapter(sLine, connection);
                DataSet dataSet = new DataSet();
                dataAdapter.Fill(dataSet);
                var dataTableContent = dataSet.Tables[0];
                // F1
                dataTableContent.CaseSensitive = false;
                var listNH = dataTableContent.Select("TENTCTD LIKE '%" + tenNH + "%'");
                if (listNH.Length <= 0)
                {
                    foreach(var tenVietTatItem in listTenVietTat)
                    {
                        if (!String.IsNullOrEmpty(tenNHVietTat.Trim()))
                        {
                            dataTableContent.CaseSensitive = false;
                            listNH = dataTableContent.Select("TENTCTD LIKE '%" + tenVietTatItem + "%'");
                            if (listNH.Length > 0)
                                break;
                        }
                    }
                }

                var listChiNhanh_PGD = new List<DataRow>();

                if (listNH.Length > 0)
                {
                    if (!String.IsNullOrEmpty(chiNhanh.Trim()))
                    {
                        listChiNhanh_PGD = listNH.AsEnumerable().Where(p => p.Field<string>("TENTCTD").Contains(chiNhanh.Trim())).ToList();

                        if (!String.IsNullOrEmpty(PGD.Trim()) && listChiNhanh_PGD.Count > 0)
                        {
                            listChiNhanh_PGD = listChiNhanh_PGD.Where(p => p.Field<string>("TENTCTD").Contains(PGD.Trim())).ToList();
                        }
                    } else
                    {
                        if (!String.IsNullOrEmpty(PGD.Trim()))
                        {
                            listChiNhanh_PGD = listNH.AsEnumerable().Where(p => p.Field<string>("TENTCTD").Contains(PGD.Trim())).ToList();
                        }
                    }
                }

                return listChiNhanh_PGD;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Lỗi");
                return null;
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
