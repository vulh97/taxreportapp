﻿using Spire.Doc;
using Spire.Doc.Documents;
using Spire.Doc.Fields;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TaxReportApp.Model;

namespace TaxReportApp.Controller
{
    class GTHDController
    {
        public static void create_Bao_Cao(String MST)
        {
            #region Danh sách path các file cần thiết
            string dir = Directory.GetCurrentDirectory().ToString() + @"\" + ConfigurationManager.AppSettings["directoryDatabase"];
            string documentsPath = Directory.GetCurrentDirectory().ToString() + @"\" + ConfigurationManager.AppSettings["documentsFolderName"];
            string reportPath = Directory.GetCurrentDirectory().ToString() + @"\" + ConfigurationManager.AppSettings["ReportFile"];
            string S12_FilePath = dir + @"\" + ConfigurationManager.AppSettings["s12File"];
            string chungtu_filePath = dir + @"\" + ConfigurationManager.AppSettings["licenseFile"];
            string danhBa_filePath = dir + @"\" + ConfigurationManager.AppSettings["phoneBookFile"];
            string tkNganHang_filePath = dir + @"\" + ConfigurationManager.AppSettings["accountBankFile"];
            string dsTheoDoiCCHD_filePath = dir + @"\" + ConfigurationManager.AppSettings["dsTheoDoiCCHD"];
            string s14_filePath = dir + @"\" + ConfigurationManager.AppSettings["s14File"];
            string dirReport = Application.StartupPath + @"\" + ConfigurationManager.AppSettings["ReportFile"];
            // path biểu mẫu
            string bienBanLamViecNeuCoViPham_filePath = documentsPath + @"\input\GTHD\" + ConfigurationManager.AppSettings["bienBanLamViecNeuCoViPham"];
            string QD_TB_TT_co_HD_co_vi_pham_filePath = documentsPath + @"\input\GTHD\" + ConfigurationManager.AppSettings["QD-TB-TT_co_HD_co_vi_pham"];
            string QD_TB_TT_co_HD_filePath = documentsPath + @"\input\GTHD\" + ConfigurationManager.AppSettings["QD-TB-TT_co_HD"];
            string QD_TB_TT_ko_HD_filePath = documentsPath + @"\input\GTHD\" + ConfigurationManager.AppSettings["QD-TB-TT_ko_HD"];
            GiaiToaHoaDonViewModel giaiToaHoaDonViewModel = new GiaiToaHoaDonViewModel();
            #endregion

            #region Kiểm tra các file cần
            var fileNguonStatusModel = FileNguonStatusController.get_FileNguonStatus();

            if (!fileNguonStatusModel.CC_S12)
            {
                MessageBox.Show("Không tìm thấy File S12! Mời nhập lại tài liệu.", "Lỗi");
                return;
            }
            else if (!fileNguonStatusModel.CC_ChungTu)
            {
                MessageBox.Show("Không tìm thấy file Chứng từ! Mời nhập tài liệu.", "Lỗi");
                return;
            }
            else if (!fileNguonStatusModel.CC_DanhBa)
            {
                MessageBox.Show("Không tìm thấy File Danh bạ! Mời nhập tài liệu.", "Lỗi");
                return;
            }
            else if (!fileNguonStatusModel.CC_S14)
            {
                MessageBox.Show("Không tìm thấy file S14! Mời nhập tài liệu", "Lỗi");
                return;
            }
            else if (!fileNguonStatusModel.dsTheoDoiCCTK)
            {
                MessageBox.Show("Không tìm thấy file Theo dõi CCTK! Mời nhập tài liệu", "Lỗi");
                return;
            }
            #endregion

            #region Load dữ liệu
            var s12FileContent = S12Controller.SearchMST(S12_FilePath, MST);
            var isS12File_NotEmpty = s12FileContent.AsEnumerable().Any(row => row["F2"] != null);
            if (!isS12File_NotEmpty)
            {
                Common.Common.error("Không tìm thấy dữ liệu !");
            }
            else
            {
                // từ file S12 : Tên công ty, mã số thuế, path file để save báo cáo
                giaiToaHoaDonViewModel.TenCty = s12FileContent.Rows[0]["F3"].ToString();
                giaiToaHoaDonViewModel.MaSoThue = s12FileContent.Rows[0]["F2"].ToString();
                giaiToaHoaDonViewModel.TongNo = 0;
                giaiToaHoaDonViewModel.TongTienDaTra = 0;

                string timeFolder = S12Controller.SearchMonthReport(S12_FilePath);
                var containerFilePath = reportPath + @"\" + timeFolder + @"\" + giaiToaHoaDonViewModel.MaSoThue + @"\Giải tỏa hóa đơn\";
                if (!Directory.Exists(containerFilePath))
                {
                    Directory.CreateDirectory(containerFilePath);
                } 
                giaiToaHoaDonViewModel.ContainerFolderPath = containerFilePath;

                for (int i = 0; i < s12FileContent.Rows.Count; i++)
                {
                    // Các khoản tiền nợ
                    var TienNoItem = new KhoanTienModel();
                    TienNoItem.TenCongTy = s12FileContent.Rows[0]["F3"].ToString();
                    TienNoItem.MaSoThue = s12FileContent.Rows[0]["F2"].ToString();
                    TienNoItem.SoTienTheoSo = Convert.ToDecimal(s12FileContent.Rows[i]["F7"].ToString().Replace(",", ""));
                    TienNoItem.TieuMuc = s12FileContent.Rows[i][5].ToString();
                    TienNoItem.MaChuong = s12FileContent.Rows[i][3].ToString();
                    giaiToaHoaDonViewModel.ListKhoanTienNo_CCHD.Add(TienNoItem);
                    giaiToaHoaDonViewModel.TongNo += TienNoItem.SoTienTheoSo;
                }

                decimal tongTienDaDong = 0;
                // Từ file Chứng từ: các khoản tiền đã đóng
                foreach (DataRow rowItem in s12FileContent.Rows)
                {
                    var soTienDaDongTable = ChungTuController.GetListChungTu(chungtu_filePath, rowItem[1].ToString(), rowItem[5].ToString());
                    if (soTienDaDongTable.Rows.Count > 0)
                    {
                        for (int j = 0; j < soTienDaDongTable.Rows.Count; j++)
                        {
                            var TienDaDongItem = new KhoanTienModel();
                            TienDaDongItem.TenCongTy = s12FileContent.Rows[0]["F3"].ToString();
                            TienDaDongItem.MaSoThue = s12FileContent.Rows[0]["F2"].ToString();

                            TienDaDongItem.MaChuong = soTienDaDongTable.Rows[j][5].ToString();
                            TienDaDongItem.TieuMuc = soTienDaDongTable.Rows[j][2].ToString();
                            TienDaDongItem.SoTienTheoSo = Convert.ToDecimal(soTienDaDongTable.Rows[j][3].ToString().Replace(",", ""));
                            tongTienDaDong += TienDaDongItem.SoTienTheoSo;
                        }
                    }
                }

                var isAllowedToCreateReport = tongTienDaDong >= giaiToaHoaDonViewModel.TongNo ? true : false;
                if (!isAllowedToCreateReport)
                {
                    MessageBox.Show("Công ty với mã số thuế đã nhập không đủ điều kiện tạo báo cáo giải tỏa cưỡng chế hoá đơn", "Thông báo");
                    return;
                }

                var danhBaFileContent = DanhBaController.GetDanhba(danhBa_filePath, giaiToaHoaDonViewModel.MaSoThue);
                if (danhBaFileContent.Rows.Count > 0)
                {
                    var diachi = danhBaFileContent.Rows[0][7].ToString();
                    var phuongxa = danhBaFileContent.Rows[0][9].ToString();
                    var quanhuyen = danhBaFileContent.Rows[0][11].ToString();
                    var tinhthanhpho = danhBaFileContent.Rows[0][13].ToString();

                    giaiToaHoaDonViewModel.DiaChiTruSo = diachi + ", " + phuongxa + ", " + quanhuyen + ", " + tinhthanhpho;
                    giaiToaHoaDonViewModel.DiaChiNhanThongBao = giaiToaHoaDonViewModel.DiaChiTruSo;
                    giaiToaHoaDonViewModel.So_Dang_ky_kinh_doanh = danhBaFileContent.Rows[0][31].ToString();
                    var ngayDKKD_String = danhBaFileContent.Rows[0][32].ToString().Split('/');
                    if (ngayDKKD_String.Count() == 3)
                    {
                        giaiToaHoaDonViewModel.Ngay_Dang_ky_kinh_doanh = new DateTime(Convert.ToInt32(ngayDKKD_String[2].Split(' ')[0]), Convert.ToInt32(ngayDKKD_String[1]), Convert.ToInt32(ngayDKKD_String[0]));
                    }
                }
                else
                {
                    MessageBox.Show("Không tìm thấy thông tin công ty trong danh bạ !", "Lỗi");
                    return;
                }

                var theoDoiCCHD_FileContent = DanhSachTheoDoiCCHDController.SearchMST(dsTheoDoiCCHD_filePath, giaiToaHoaDonViewModel.MaSoThue);
                if (theoDoiCCHD_FileContent.Rows.Count > 0)
                {

                    //giaiToaHoaDonViewModel.So_TB07 = theoDoiCCHD_FileContent.Rows[0][11].ToString();
                    //giaiToaHoaDonViewModel.Ngay_TB07 = new DateTime();
                    //giaiToaHoaDonViewModel.So_QD_HD = theoDoiCCHD_FileContent.Rows[0][1].ToString();
                    //giaiToaHoaDonViewModel.So_TB = theoDoiCCHD_FileContent.Rows[0][7].ToString(); ;
                    //giaiToaHoaDonViewModel.MauSo = "";
                    for (int i = 0; i < theoDoiCCHD_FileContent.Rows.Count; i++)
                    {
                        var hoaDonItem = new HoaDon();
                        hoaDonItem.kyhieuhoadon = theoDoiCCHD_FileContent.Rows[i][5].ToString();

                        hoaDonItem.tusodenso = theoDoiCCHD_FileContent.Rows[i][6].ToString();
                        giaiToaHoaDonViewModel.ListHoaDon.Add(hoaDonItem);
                    }

                    //var ngay_QD_String = theoDoiCCHD_FileContent.Rows[0][2].ToString().Split('/');
                    //if (ngay_QD_String.Count() == 3)
                    //{
                    //    giaiToaHoaDonViewModel.Ngay_QD = new DateTime(Convert.ToInt32(ngay_QD_String[2].Split(' ')[0]), Convert.ToInt32(ngay_QD_String[0]), Convert.ToInt32(ngay_QD_String[1]));
                    //}

                    var ngay_hieuLuc_String = theoDoiCCHD_FileContent.Rows[0][3].ToString().Split('/');
                    if (ngay_hieuLuc_String.Count() == 3)
                    {
                        giaiToaHoaDonViewModel.Ngay_Co_hieu_luc = new DateTime(Convert.ToInt32(ngay_hieuLuc_String[2].Split(' ')[0]), Convert.ToInt32(ngay_hieuLuc_String[0]), Convert.ToInt32(ngay_hieuLuc_String[1]));
                    }

                    var ngay_het_hieuLuc_String = theoDoiCCHD_FileContent.Rows[0][4].ToString().Split('/');
                    if (ngay_het_hieuLuc_String.Count() == 3)
                    {
                        giaiToaHoaDonViewModel.Ngay_Het_hieu_luc = new DateTime(Convert.ToInt32(ngay_het_hieuLuc_String[2].Split(' ')[0]), Convert.ToInt32(ngay_het_hieuLuc_String[0]), Convert.ToInt32(ngay_het_hieuLuc_String[1]));
                    }
                }
                else
                {
                    MessageBox.Show("Không tìm thấy dữ liệu của công ty trong file Theo dõi cưỡng chế hóa đơn !", "Lỗi");
                    return;
                }

                #region Lấy thông tin TB07
                var s14FileContent = S14Controller.GetDataS14FileByMST(s14_filePath, giaiToaHoaDonViewModel.MaSoThue);
                if (s14FileContent.Rows.Count > 0)
                {
                    giaiToaHoaDonViewModel.So_TB07 = s14FileContent.Rows[0][3].ToString();
                    var _ngayTB07 = s14FileContent.Rows[0][4].ToString();
                    giaiToaHoaDonViewModel.Ngay_TB07 = _ngayTB07;
                }
                else
                {
                    MessageBox.Show("Không tìm thấy dữ liệu của công ty/doanh nghiệp trong file S14 !", "Lỗi");
                    return;
                }
                #endregion
            }

            #endregion

            #region Thực hiện tạo báo cáo GTHĐ

            var thongTinDonVi = DonViController.GetThongTinDonVi();

            #region Tạo file QĐ-TB-TT có hợp đồng/hóa đơn
            Document QD_TB_TT_CoHopDong_doc = new Document();
            QD_TB_TT_CoHopDong_doc.LoadFromFile(QD_TB_TT_co_HD_filePath);

            QD_TB_TT_CoHopDong_doc.Replace("{{CHICUCTHUE}}", thongTinDonVi.tenChiNhanh.ToUpper().Replace("QUẬN ", "Q.").Replace("HUYỆN ", "H.").Replace("KHU VỰC ", "KV."), false, true);
            QD_TB_TT_CoHopDong_doc.Replace("{{TEN_CONGTY}}", giaiToaHoaDonViewModel.TenCty, false, true);
            QD_TB_TT_CoHopDong_doc.Replace("{{MST}}", giaiToaHoaDonViewModel.MaSoThue, false, true);
            QD_TB_TT_CoHopDong_doc.Replace("{{SO_TB07}}", giaiToaHoaDonViewModel.So_TB07 ?? "", false, true);
            QD_TB_TT_CoHopDong_doc.Replace("{{NGAY_TB07}}", (giaiToaHoaDonViewModel.Ngay_TB07 ?? ""), false, true);
            //QD_TB_TT_CoHopDong_doc.Replace("{{SO_QDHD}}", giaiToaHoaDonViewModel.So_QD_HD ?? "", false, true);
            //QD_TB_TT_CoHopDong_doc.Replace("{{SO_TB}}", giaiToaHoaDonViewModel.So_TB ?? "", false, true);
            //QD_TB_TT_CoHopDong_doc.Replace("{{NGAY_QD}}", (giaiToaHoaDonViewModel.Ngay_QD == null ? "" : giaiToaHoaDonViewModel.Ngay_QD.ToString("dd/MM/yyyy")), false, true);
            QD_TB_TT_CoHopDong_doc.Replace("{{DIACHI_NHANTB}}", giaiToaHoaDonViewModel.DiaChiNhanThongBao ?? "", false, true);
            QD_TB_TT_CoHopDong_doc.Replace("{{DIACHI_TRUSO}}", giaiToaHoaDonViewModel.DiaChiTruSo ?? "", false, true);
            QD_TB_TT_CoHopDong_doc.Replace("{{SO_DKKD}}", giaiToaHoaDonViewModel.So_Dang_ky_kinh_doanh ?? "", false, true);
            QD_TB_TT_CoHopDong_doc.Replace("{{NGAY_DKKD}}", (giaiToaHoaDonViewModel.Ngay_Dang_ky_kinh_doanh == null ? "" : giaiToaHoaDonViewModel.Ngay_Dang_ky_kinh_doanh.ToString("dd/MM/yyyy")), false, true);
            QD_TB_TT_CoHopDong_doc.Replace("{{NGAY_CO_HL}}", (giaiToaHoaDonViewModel.Ngay_Co_hieu_luc == null ? "" : giaiToaHoaDonViewModel.Ngay_Co_hieu_luc.ToString("dd/MM/yyyy")), false, true);
            QD_TB_TT_CoHopDong_doc.Replace("{{TONGNO_CCHD}}", giaiToaHoaDonViewModel.TongNo.ToString("#,###") ?? "", false, true);
            QD_TB_TT_CoHopDong_doc.Replace("{{MAU_SO}}", ((giaiToaHoaDonViewModel.ListHoaDon != null && giaiToaHoaDonViewModel.ListHoaDon.Count > 0) && giaiToaHoaDonViewModel.ListHoaDon[0].kyhieumau != null
                ? giaiToaHoaDonViewModel.ListHoaDon[0].kyhieumau : ""), false, true);
            QD_TB_TT_CoHopDong_doc.Replace("{{KY_HIEU}}", ((giaiToaHoaDonViewModel.ListHoaDon != null && giaiToaHoaDonViewModel.ListHoaDon.Count > 0)
                ? giaiToaHoaDonViewModel.ListHoaDon[0].kyhieuhoadon : ""), false, true);
            QD_TB_TT_CoHopDong_doc.Replace("{{TUSO_DENSO}}", ((giaiToaHoaDonViewModel.ListHoaDon != null && giaiToaHoaDonViewModel.ListHoaDon.Count > 0)
                ? giaiToaHoaDonViewModel.ListHoaDon[0].tusodenso : ""), false, true);

            QD_TB_TT_CoHopDong_doc.Replace("{{CHICUCTRUONG}}", (thongTinDonVi.tenChiCucTruong ?? ""), false, true);
            QD_TB_TT_CoHopDong_doc.Replace("{{PCHICUCTRUONG}}", (thongTinDonVi.tenPhoChiCucTruong ?? ""), false, true);
            QD_TB_TT_CoHopDong_doc.Replace("{{CCTHUE}}", (thongTinDonVi.tenChiNhanh ?? ""), false, true);
            QD_TB_TT_CoHopDong_doc.Replace("{{TENDOI}}", (thongTinDonVi.tenDoi ?? ""), false, true);
            QD_TB_TT_CoHopDong_doc.Replace("{{TENDOITRUONG}}", (thongTinDonVi.tenDoiTruong ?? ""), false, true);
            QD_TB_TT_CoHopDong_doc.Replace("{{TENCB}}", (thongTinDonVi.tenCanBo ?? ""), false, true);
            //Tạo table

            #region Tạo table danh sách hóa đơn
            Section section = QD_TB_TT_CoHopDong_doc.Sections[0];
            TextSelection[] hoaDonSection = QD_TB_TT_CoHopDong_doc.FindAllString("{{HOADONTABLE1}}", true, true);
            foreach (var hoaDonSection1 in hoaDonSection)
            {
                TextRange hoaDonRange1 = hoaDonSection1.GetAsOneRange();
                Paragraph hoaDonParagraph1 = hoaDonRange1.OwnerParagraph;
                Body hoaDonBody1 = hoaDonParagraph1.OwnerTextBody;
                int hoaDonIndex1 = hoaDonBody1.ChildObjects.IndexOf(hoaDonParagraph1);
                Table hoaDonTable1 = section.AddTable(true);
                String[] hoaDonHeader = new string[] { "STT", "Loại hóa đơn", "Ký hiệu mẫu", "Ký hiệu Hóa đơn", "Từ số - đến số", "Ghi chú" };

                hoaDonTable1.ResetCells(giaiToaHoaDonViewModel.ListHoaDon.Count + 1, hoaDonHeader.Count());
                TableRow hoaDonTBRow1 = hoaDonTable1.Rows[0];
                hoaDonTBRow1.IsHeader = true;

                for (int i = 0; i < hoaDonHeader.Length; i++)
                {
                    //Cell Alignment
                    Paragraph p = hoaDonTBRow1.Cells[i].AddParagraph();
                    hoaDonTBRow1.Cells[i].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                    p.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
                    //Data Format
                    TextRange TR = p.AppendText(hoaDonHeader[i]);
                    TR.CharacterFormat.FontName = "Times New Roman";
                    TR.CharacterFormat.Bold = true;
                    TR.CharacterFormat.FontSize = 13;
                    switch (i)
                    {
                        case 0:
                            hoaDonTBRow1.Cells[i].SetCellWidth(10, CellWidthType.Percentage); break;
                        case 1:
                            hoaDonTBRow1.Cells[i].SetCellWidth(20, CellWidthType.Percentage); break;
                        case 2:
                            hoaDonTBRow1.Cells[i].SetCellWidth(10, CellWidthType.Percentage); break;
                        case 3:
                            hoaDonTBRow1.Cells[i].SetCellWidth(10, CellWidthType.Percentage); break;
                        case 4:
                            hoaDonTBRow1.Cells[i].SetCellWidth(30, CellWidthType.Percentage); break;
                        case 5:
                            hoaDonTBRow1.Cells[i].SetCellWidth(20, CellWidthType.Percentage); break;
                    }
                }
                // Table data rows
                for (int r = 0; r < giaiToaHoaDonViewModel.ListHoaDon.Count; r++)
                {
                    TableRow DataRow = hoaDonTable1.Rows[r + 1];
                    // 1. STT
                    DataRow.Cells[0].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                    Paragraph p0 = DataRow.Cells[0].AddParagraph();
                    TextRange TR0 = p0.AppendText((r + 1).ToString());
                    p0.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
                    TR0.CharacterFormat.FontName = "Times New Roman";
                    TR0.CharacterFormat.FontSize = 13;
                    DataRow.Cells[0].SetCellWidth(10, CellWidthType.Percentage);

                    // 2. Loại hóa đơn
                    DataRow.Cells[1].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                    Paragraph p1 = DataRow.Cells[1].AddParagraph();
                    TextRange TR1 = p1.AppendText("Giá trị gia tăng");
                    p1.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
                    TR1.CharacterFormat.FontName = "Times New Roman";
                    TR1.CharacterFormat.FontSize = 13;
                    DataRow.Cells[1].SetCellWidth(20, CellWidthType.Percentage);

                    // 3. Ký hiệu mẫu
                    DataRow.Cells[2].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                    Paragraph p2 = DataRow.Cells[2].AddParagraph();
                    TextRange TR2 = p2.AppendText(giaiToaHoaDonViewModel.ListHoaDon[r].kyhieumau ?? "");
                    p2.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
                    TR2.CharacterFormat.FontName = "Times New Roman";
                    TR2.CharacterFormat.FontSize = 13;
                    DataRow.Cells[2].SetCellWidth(10, CellWidthType.Percentage);

                    // 4. Ký hiệu hóa đơn
                    DataRow.Cells[3].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                    Paragraph p3 = DataRow.Cells[3].AddParagraph();
                    TextRange TR3 = p3.AppendText(giaiToaHoaDonViewModel.ListHoaDon[r].kyhieuhoadon ?? "");
                    p3.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
                    TR3.CharacterFormat.FontName = "Times New Roman";
                    TR3.CharacterFormat.FontSize = 13;
                    DataRow.Cells[3].SetCellWidth(10, CellWidthType.Percentage);

                    // 5. Từ số - đến số
                    DataRow.Cells[4].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                    Paragraph p4 = DataRow.Cells[4].AddParagraph();
                    TextRange TR4 = p4.AppendText(giaiToaHoaDonViewModel.ListHoaDon[r].tusodenso ?? "");
                    p4.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
                    TR4.CharacterFormat.FontName = "Times New Roman";
                    TR4.CharacterFormat.FontSize = 13;
                    DataRow.Cells[4].SetCellWidth(30, CellWidthType.Percentage);

                    // 6. Ghi chú
                    DataRow.Cells[5].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                    Paragraph p5 = DataRow.Cells[5].AddParagraph();
                    TextRange TR5 = p5.AppendText("Tiếp tục sử dụng");
                    p5.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
                    TR5.CharacterFormat.FontName = "Times New Roman";
                    TR5.CharacterFormat.FontSize = 13;
                    DataRow.Cells[5].SetCellWidth(20, CellWidthType.Percentage);
                }
                hoaDonBody1.ChildObjects.Remove(hoaDonParagraph1);
                hoaDonBody1.ChildObjects.Insert(hoaDonIndex1, hoaDonTable1);
            }
            #endregion

            #region Tạo table danh sách nợ
            TextSelection dsNoSection = QD_TB_TT_CoHopDong_doc.FindString("{{TIENNOTABLE}}", true, true);
            TextRange dsNoRange = dsNoSection.GetAsOneRange();
            Paragraph dsNoParagraph = dsNoRange.OwnerParagraph;
            Body dsNoBody = dsNoParagraph.OwnerTextBody;
            int dsNoIndex = dsNoBody.ChildObjects.IndexOf(dsNoParagraph);
            Table dsNoTable = section.AddTable(true);
            String[] dsNoHeader = new string[] { "STT", "Tiểu mục", "Nội dung", "Số tiền" };

            dsNoTable.ResetCells(giaiToaHoaDonViewModel.ListKhoanTienNo_CCHD.Count + 2, dsNoHeader.Count());
            TableRow dsNoRow = dsNoTable.Rows[0];
            dsNoRow.IsHeader = true;
            for (int i = 0; i < dsNoHeader.Length; i++)
            {
                //Cell Alignment
                Paragraph p = dsNoRow.Cells[i].AddParagraph();
                dsNoRow.Cells[i].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                p.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
                //Data Format
                TextRange TR = p.AppendText(dsNoHeader[i]);
                TR.CharacterFormat.FontName = "Times New Roman";
                TR.CharacterFormat.Bold = true;
                TR.CharacterFormat.FontSize = 13;
                switch (i)
                {
                    case 0:
                        dsNoRow.Cells[i].SetCellWidth(10, CellWidthType.Percentage); break;
                    case 1:
                        dsNoRow.Cells[i].SetCellWidth(20, CellWidthType.Percentage); break;
                    case 2:
                        dsNoRow.Cells[i].SetCellWidth(30, CellWidthType.Percentage); break;
                    case 3:
                        dsNoRow.Cells[i].SetCellWidth(40, CellWidthType.Percentage); break;
                }
            }

            // Table data rows
            for (int r = 0; r < giaiToaHoaDonViewModel.ListKhoanTienNo_CCHD.Count; r++)
            {
                TableRow DataRow = dsNoTable.Rows[r + 1];
                // 1. STT
                DataRow.Cells[0].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                Paragraph p0 = DataRow.Cells[0].AddParagraph();
                TextRange TR0 = p0.AppendText((r + 1).ToString());
                p0.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
                TR0.CharacterFormat.FontName = "Times New Roman";
                TR0.CharacterFormat.FontSize = 13;
                DataRow.Cells[0].SetCellWidth(10, CellWidthType.Percentage);

                //2. Tiểu mục
                DataRow.Cells[1].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                Paragraph p1 = DataRow.Cells[1].AddParagraph();
                TextRange TR1 = p1.AppendText("TM " + giaiToaHoaDonViewModel.ListKhoanTienNo_CCHD[r].TieuMuc);
                p1.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
                TR1.CharacterFormat.FontName = "Times New Roman";
                TR1.CharacterFormat.FontSize = 13;
                DataRow.Cells[1].SetCellWidth(20, CellWidthType.Percentage);

                //3. Nội dung
                DataRow.Cells[2].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                Paragraph p2 = DataRow.Cells[2].AddParagraph();
                TextRange TR2 = p2.AppendText(GTTKController.Get_NoiDungTheoTieuMuc(giaiToaHoaDonViewModel.ListKhoanTienNo_CCHD[r].TieuMuc));
                p2.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
                TR2.CharacterFormat.FontName = "Times New Roman";
                TR2.CharacterFormat.FontSize = 13;
                DataRow.Cells[2].SetCellWidth(30, CellWidthType.Percentage);

                //4.Số tiền
                DataRow.Cells[3].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                Paragraph p3 = DataRow.Cells[3].AddParagraph();
                TextRange TR3 = p3.AppendText(giaiToaHoaDonViewModel.ListKhoanTienNo_CCHD[r].SoTienTheoSo.ToString("#,###"));
                p3.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
                TR3.CharacterFormat.FontName = "Times New Roman";
                TR3.CharacterFormat.FontSize = 13;
                DataRow.Cells[3].SetCellWidth(40, CellWidthType.Percentage);
            }

            //Số tiền tổng cộng
            TableRow DataRowTongCong = dsNoTable.Rows[giaiToaHoaDonViewModel.ListKhoanTienNo_CCHD.Count + 1];
            //3. Nội dung 
            DataRowTongCong.Cells[2].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            Paragraph pNo2 = DataRowTongCong.Cells[2].AddParagraph();
            TextRange TRNo2 = pNo2.AppendText("Tổng cộng");
            pNo2.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
            TRNo2.CharacterFormat.FontName = "Times New Roman";
            TRNo2.CharacterFormat.FontSize = 13;
            TRNo2.CharacterFormat.Bold = true;
            DataRowTongCong.Cells[2].SetCellWidth(30, CellWidthType.Percentage);

            //4.Số tiền
            DataRowTongCong.Cells[3].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            Paragraph pNo3 = DataRowTongCong.Cells[3].AddParagraph();
            TextRange TRNo3 = pNo3.AppendText(giaiToaHoaDonViewModel.TongNo.ToString("#,###"));
            pNo3.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
            TRNo3.CharacterFormat.FontName = "Times New Roman";
            TRNo3.CharacterFormat.FontSize = 13;
            DataRowTongCong.Cells[3].SetCellWidth(40, CellWidthType.Percentage);
            dsNoBody.ChildObjects.Remove(dsNoParagraph);
            dsNoBody.ChildObjects.Insert(dsNoIndex, dsNoTable);
            #endregion

            QD_TB_TT_CoHopDong_doc.SaveToFile(giaiToaHoaDonViewModel.ContainerFolderPath + @"\" + "QĐ-TB-TT có HĐ.doc", FileFormat.Docx);
            #endregion

            #region Tạo file QĐ-TB-TT không có hợp đồng/hóa đơn
            Document QD_TB_TT_KhongCoHopDong_doc = new Document();
            QD_TB_TT_KhongCoHopDong_doc.LoadFromFile(QD_TB_TT_ko_HD_filePath);

            QD_TB_TT_KhongCoHopDong_doc.Replace("{{CHICUCTHUE}}", thongTinDonVi.tenChiNhanh.ToUpper().Replace("QUẬN ", "Q.").Replace("HUYỆN ", "H.").Replace("KHU VỰC ", "KV."), false, true);
            QD_TB_TT_KhongCoHopDong_doc.Replace("{{TEN_CONGTY}}", giaiToaHoaDonViewModel.TenCty, false, true);
            QD_TB_TT_KhongCoHopDong_doc.Replace("{{MST}}", giaiToaHoaDonViewModel.MaSoThue, false, true);
            QD_TB_TT_KhongCoHopDong_doc.Replace("{{SO_TB07}}", giaiToaHoaDonViewModel.So_TB07 ?? "", false, true);
            QD_TB_TT_KhongCoHopDong_doc.Replace("{{NGAY_TB07}}", (giaiToaHoaDonViewModel.Ngay_TB07 ?? ""), false, true);
            //QD_TB_TT_KhongCoHopDong_doc.Replace("{{SO_QDHD}}", giaiToaHoaDonViewModel.So_QD_HD ?? "", false, true);
            //QD_TB_TT_KhongCoHopDong_doc.Replace("{{SO_TB}}", giaiToaHoaDonViewModel.So_TB ?? "", false, true);
            //QD_TB_TT_KhongCoHopDong_doc.Replace("{{NGAY_QD}}", (giaiToaHoaDonViewModel.Ngay_QD == null ? "" : giaiToaHoaDonViewModel.Ngay_QD.ToString("dd/MM/yyyy")), false, true);
            QD_TB_TT_KhongCoHopDong_doc.Replace("{{DIACHI_NHANTB}}", giaiToaHoaDonViewModel.DiaChiNhanThongBao ?? "", false, true);
            QD_TB_TT_KhongCoHopDong_doc.Replace("{{DIACHI_TRUSO}}", giaiToaHoaDonViewModel.DiaChiTruSo ?? "", false, true);
            QD_TB_TT_KhongCoHopDong_doc.Replace("{{SO_DKKD}}", giaiToaHoaDonViewModel.So_Dang_ky_kinh_doanh ?? "", false, true);
            QD_TB_TT_KhongCoHopDong_doc.Replace("{{NGAY_DKKD}}", (giaiToaHoaDonViewModel.Ngay_Dang_ky_kinh_doanh == null ? "" : giaiToaHoaDonViewModel.Ngay_Dang_ky_kinh_doanh.ToString("dd/MM/yyyy")), false, true);
            QD_TB_TT_KhongCoHopDong_doc.Replace("{{NGAY_CO_HL}}", (giaiToaHoaDonViewModel.Ngay_Co_hieu_luc == null ? "" : giaiToaHoaDonViewModel.Ngay_Co_hieu_luc.ToString("dd/MM/yyyy")), false, true);
            QD_TB_TT_KhongCoHopDong_doc.Replace("{{NGAY_HET_HL}}", (giaiToaHoaDonViewModel.Ngay_Het_hieu_luc == null ? "" : giaiToaHoaDonViewModel.Ngay_Het_hieu_luc.ToString("dd/MM/yyyy")), false, true);
            QD_TB_TT_KhongCoHopDong_doc.Replace("{{TONGNO_CCHD}}", giaiToaHoaDonViewModel.TongNo.ToString("#,###") ?? "", false, true);

            #region Tạo table danh sách nợ
            Section section1 = QD_TB_TT_KhongCoHopDong_doc.Sections[0];
            TextSelection dsNoSection1 = QD_TB_TT_KhongCoHopDong_doc.FindString("{{TIENNOTABLE}}", true, true);
            TextRange dsNoRange1 = dsNoSection1.GetAsOneRange();
            Paragraph dsNoParagraph1 = dsNoRange1.OwnerParagraph;
            Body dsNoBody1 = dsNoParagraph1.OwnerTextBody;
            int dsNoIndex1 = dsNoBody1.ChildObjects.IndexOf(dsNoParagraph1);
            Table dsNoTable1 = section1.AddTable(true);
            String[] dsNoHeader1 = new string[] { "STT", "Tiểu mục", "Nội dung", "Số tiền" };

            dsNoTable1.ResetCells(giaiToaHoaDonViewModel.ListKhoanTienNo_CCHD.Count + 2, dsNoHeader1.Count());
            TableRow dsNoRow1 = dsNoTable1.Rows[0];
            dsNoRow1.IsHeader = true;
            for (int i = 0; i < dsNoHeader1.Length; i++)
            {
                //Cell Alignment
                Paragraph p = dsNoRow1.Cells[i].AddParagraph();
                dsNoRow1.Cells[i].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                p.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
                //Data Format
                TextRange TR = p.AppendText(dsNoHeader[i]);
                TR.CharacterFormat.FontName = "Times New Roman";
                TR.CharacterFormat.Bold = true;
                TR.CharacterFormat.FontSize = 13;
                switch (i)
                {
                    case 0:
                        dsNoRow1.Cells[i].SetCellWidth(10, CellWidthType.Percentage); break;
                    case 1:
                        dsNoRow1.Cells[i].SetCellWidth(20, CellWidthType.Percentage); break;
                    case 2:
                        dsNoRow1.Cells[i].SetCellWidth(30, CellWidthType.Percentage); break;
                    case 3:
                        dsNoRow1.Cells[i].SetCellWidth(40, CellWidthType.Percentage); break;
                }
            }

            // Table data rows
            for (int r = 0; r < giaiToaHoaDonViewModel.ListKhoanTienNo_CCHD.Count; r++)
            {
                TableRow DataRow = dsNoTable1.Rows[r + 1];
                // 1. STT
                DataRow.Cells[0].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                Paragraph p0 = DataRow.Cells[0].AddParagraph();
                TextRange TR0 = p0.AppendText((r + 1).ToString());
                p0.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
                TR0.CharacterFormat.FontName = "Times New Roman";
                TR0.CharacterFormat.FontSize = 13;
                DataRow.Cells[0].SetCellWidth(10, CellWidthType.Percentage);

                //2. Tiểu mục
                DataRow.Cells[1].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                Paragraph p1 = DataRow.Cells[1].AddParagraph();
                TextRange TR1 = p1.AppendText("TM " + giaiToaHoaDonViewModel.ListKhoanTienNo_CCHD[r].TieuMuc);
                p1.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
                TR1.CharacterFormat.FontName = "Times New Roman";
                TR1.CharacterFormat.FontSize = 13;
                DataRow.Cells[1].SetCellWidth(20, CellWidthType.Percentage);

                //3. Nội dung
                DataRow.Cells[2].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                Paragraph p2 = DataRow.Cells[2].AddParagraph();
                TextRange TR2 = p2.AppendText(GTTKController.Get_NoiDungTheoTieuMuc(giaiToaHoaDonViewModel.ListKhoanTienNo_CCHD[r].TieuMuc));
                p2.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
                TR2.CharacterFormat.FontName = "Times New Roman";
                TR2.CharacterFormat.FontSize = 13;
                DataRow.Cells[2].SetCellWidth(30, CellWidthType.Percentage);

                //4.Số tiền
                DataRow.Cells[3].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                Paragraph p3 = DataRow.Cells[3].AddParagraph();
                TextRange TR3 = p3.AppendText(giaiToaHoaDonViewModel.ListKhoanTienNo_CCHD[r].SoTienTheoSo.ToString("#,###"));
                p3.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
                TR3.CharacterFormat.FontName = "Times New Roman";
                TR3.CharacterFormat.FontSize = 13;
                DataRow.Cells[3].SetCellWidth(40, CellWidthType.Percentage);
            }

            //Số tiền tổng cộng
            TableRow DataRowTongCong1 = dsNoTable1.Rows[giaiToaHoaDonViewModel.ListKhoanTienNo_CCHD.Count + 1];
            //3. Nội dung 
            DataRowTongCong1.Cells[2].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            Paragraph pNo21 = DataRowTongCong1.Cells[2].AddParagraph();
            TextRange TRNo21 = pNo21.AppendText("Tổng cộng");
            pNo21.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
            TRNo21.CharacterFormat.FontName = "Times New Roman";
            TRNo21.CharacterFormat.FontSize = 13;
            TRNo21.CharacterFormat.Bold = true;
            DataRowTongCong1.Cells[2].SetCellWidth(30, CellWidthType.Percentage);

            //4.Số tiền
            DataRowTongCong1.Cells[3].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            Paragraph pNo31 = DataRowTongCong1.Cells[3].AddParagraph();
            TextRange TRNo31 = pNo31.AppendText(giaiToaHoaDonViewModel.TongNo.ToString("#,###"));
            pNo31.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
            TRNo31.CharacterFormat.FontName = "Times New Roman";
            TRNo31.CharacterFormat.FontSize = 13;
            DataRowTongCong1.Cells[3].SetCellWidth(40, CellWidthType.Percentage);
            dsNoBody1.ChildObjects.Remove(dsNoParagraph1);
            dsNoBody1.ChildObjects.Insert(dsNoIndex1, dsNoTable1);
            #endregion
            QD_TB_TT_KhongCoHopDong_doc.SaveToFile(giaiToaHoaDonViewModel.ContainerFolderPath + @"\" + "QĐ-TB-TT ko HĐ.doc", FileFormat.Docx);
            #endregion

            //#region Tạo file QD-TB-TT_co_HD_co_vi_pham
            //Document QD_TB_TT_CoHopDong_CoViPham_doc = new Document();
            //QD_TB_TT_CoHopDong_CoViPham_doc.LoadFromFile(QD_TB_TT_co_HD_co_vi_pham_filePath);

            //QD_TB_TT_CoHopDong_CoViPham_doc.Replace("{{CHICUCTHUE}}", thongTinDonVi.tenChiNhanh.ToUpper().Replace("QUẬN ", "Q.").Replace("HUYỆN ", "H.").Replace("KHU VỰC ", "KV."), false, true);
            //QD_TB_TT_CoHopDong_CoViPham_doc.Replace("{{TEN_CONGTY}}", giaiToaHoaDonViewModel.TenCty, false, true);
            //QD_TB_TT_CoHopDong_CoViPham_doc.Replace("{{MST}}", giaiToaHoaDonViewModel.MaSoThue, false, true);
            //QD_TB_TT_CoHopDong_CoViPham_doc.Replace("{{SO_TB07}}", giaiToaHoaDonViewModel.So_TB07 ?? "", false, true);
            //QD_TB_TT_CoHopDong_CoViPham_doc.Replace("{{NGAY_TB07}}", (giaiToaHoaDonViewModel.Ngay_TB07 == null ? "" : giaiToaHoaDonViewModel.Ngay_TB07.ToString("dd/MM/yyyy")), false, true);
            //QD_TB_TT_CoHopDong_CoViPham_doc.Replace("{{SO_QDHD}}", giaiToaHoaDonViewModel.So_QD_HD ?? "", false, true);
            //QD_TB_TT_CoHopDong_CoViPham_doc.Replace("{{SO_TB}}", giaiToaHoaDonViewModel.So_TB ?? "", false, true);
            //QD_TB_TT_CoHopDong_CoViPham_doc.Replace("{{NGAY_QD}}", (giaiToaHoaDonViewModel.Ngay_QD == null ? "" : giaiToaHoaDonViewModel.Ngay_QD.ToString("dd/MM/yyyy")), false, true);
            //QD_TB_TT_CoHopDong_CoViPham_doc.Replace("{{DIACHI_NHANTB}}", giaiToaHoaDonViewModel.DiaChiNhanThongBao ?? "", false, true);
            //QD_TB_TT_CoHopDong_CoViPham_doc.Replace("{{DIACHI_TRUSO}}", giaiToaHoaDonViewModel.DiaChiTruSo ?? "", false, true);
            //QD_TB_TT_CoHopDong_CoViPham_doc.Replace("{{SO_DKKD}}", giaiToaHoaDonViewModel.So_Dang_ky_kinh_doanh ?? "", false, true);
            //QD_TB_TT_CoHopDong_CoViPham_doc.Replace("{{NGAY_DKKD}}", (giaiToaHoaDonViewModel.Ngay_Dang_ky_kinh_doanh == null ? "" : giaiToaHoaDonViewModel.Ngay_Dang_ky_kinh_doanh.ToString("dd/MM/yyyy")), false, true);
            //QD_TB_TT_CoHopDong_CoViPham_doc.Replace("{{NGAY_CO_HL}}", (giaiToaHoaDonViewModel.Ngay_Co_hieu_luc == null ? "" : giaiToaHoaDonViewModel.Ngay_Co_hieu_luc.ToString("dd/MM/yyyy")), false, true);
            //QD_TB_TT_CoHopDong_CoViPham_doc.Replace("{{TONGNO_CCHD}}", giaiToaHoaDonViewModel.TongNo.ToString("#,###") ?? "", false, true);
            //QD_TB_TT_CoHopDong_CoViPham_doc.Replace("{{MAU_SO}}", ((giaiToaHoaDonViewModel.ListHoaDon != null && giaiToaHoaDonViewModel.ListHoaDon.Count > 0) && giaiToaHoaDonViewModel.ListHoaDon[0].kyhieumau != null
            //    ? giaiToaHoaDonViewModel.ListHoaDon[0].kyhieumau : ""), false, true);
            //QD_TB_TT_CoHopDong_CoViPham_doc.Replace("{{KY_HIEU}}", ((giaiToaHoaDonViewModel.ListHoaDon != null && giaiToaHoaDonViewModel.ListHoaDon.Count > 0)
            //    ? giaiToaHoaDonViewModel.ListHoaDon[0].kyhieuhoadon : ""), false, true);
            //QD_TB_TT_CoHopDong_CoViPham_doc.Replace("{{TUSO_DENSO}}", ((giaiToaHoaDonViewModel.ListHoaDon != null && giaiToaHoaDonViewModel.ListHoaDon.Count > 0)
            //    ? giaiToaHoaDonViewModel.ListHoaDon[0].tusodenso : ""), false, true);

            //#region Taọ table danh sách hóa đơn 1
            //Section section2 = QD_TB_TT_CoHopDong_CoViPham_doc.Sections[0];
            //TextSelection hoaDonSection2 = QD_TB_TT_CoHopDong_CoViPham_doc.FindString("{{HOADONTABLE1}}", true, true);
            //TextRange hoaDonRange2 = hoaDonSection2.GetAsOneRange();
            //Paragraph hoaDonParagraph2 = hoaDonRange2.OwnerParagraph;
            //Body hoaDonBody2 = hoaDonParagraph2.OwnerTextBody;
            //int hoaDonIndex2 = hoaDonBody2.ChildObjects.IndexOf(hoaDonParagraph2);
            //Table hoaDonTable2 = section2.AddTable(true);
            //String[] hoaDonHeader2 = new string[] { "STT", "Loại hóa đơn", "Ký hiệu mẫu", "Ký hiệu Hóa đơn", "Từ số - đến số", "Ghi chú" };

            //hoaDonTable2.ResetCells(giaiToaHoaDonViewModel.ListHoaDon.Count + 1, hoaDonHeader2.Count());
            //TableRow hoaDonTBRow2 = hoaDonTable2.Rows[0];
            //hoaDonTBRow2.IsHeader = true;

            //for (int i = 0; i < hoaDonHeader2.Length; i++)
            //{
            //    //Cell Alignment
            //    Paragraph p = hoaDonTBRow2.Cells[i].AddParagraph();
            //    hoaDonTBRow2.Cells[i].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //    p.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
            //    //Data Format
            //    TextRange TR = p.AppendText(hoaDonHeader2[i]);
            //    TR.CharacterFormat.FontName = "Times New Roman";
            //    TR.CharacterFormat.Bold = true;
            //    TR.CharacterFormat.FontSize = 13;
            //    switch (i)
            //    {
            //        case 0:
            //            hoaDonTBRow2.Cells[i].SetCellWidth(10, CellWidthType.Percentage); break;
            //        case 1:
            //            hoaDonTBRow2.Cells[i].SetCellWidth(20, CellWidthType.Percentage); break;
            //        case 2:
            //            hoaDonTBRow2.Cells[i].SetCellWidth(10, CellWidthType.Percentage); break;
            //        case 3:
            //            hoaDonTBRow2.Cells[i].SetCellWidth(10, CellWidthType.Percentage); break;
            //        case 4:
            //            hoaDonTBRow2.Cells[i].SetCellWidth(30, CellWidthType.Percentage); break;
            //        case 5:
            //            hoaDonTBRow2.Cells[i].SetCellWidth(20, CellWidthType.Percentage); break;
            //    }
            //}
            //// Table data rows
            //for (int r = 0; r < giaiToaHoaDonViewModel.ListHoaDon.Count; r++)
            //{
            //    TableRow DataRow = hoaDonTable2.Rows[r + 1];
            //    // 1. STT
            //    DataRow.Cells[0].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //    Paragraph p0 = DataRow.Cells[0].AddParagraph();
            //    TextRange TR0 = p0.AppendText((r + 1).ToString());
            //    p0.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
            //    TR0.CharacterFormat.FontName = "Times New Roman";
            //    TR0.CharacterFormat.FontSize = 13;
            //    DataRow.Cells[0].SetCellWidth(10, CellWidthType.Percentage);

            //    // 2. Loại hóa đơn
            //    DataRow.Cells[1].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //    Paragraph p1 = DataRow.Cells[1].AddParagraph();
            //    TextRange TR1 = p1.AppendText("Giá trị gia tăng");
            //    p1.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
            //    TR1.CharacterFormat.FontName = "Times New Roman";
            //    TR1.CharacterFormat.FontSize = 13;
            //    DataRow.Cells[1].SetCellWidth(20, CellWidthType.Percentage);

            //    // 3. Ký hiệu mẫu
            //    DataRow.Cells[2].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //    Paragraph p2 = DataRow.Cells[2].AddParagraph();
            //    TextRange TR2 = p2.AppendText(giaiToaHoaDonViewModel.ListHoaDon[r].kyhieumau ?? "");
            //    p2.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
            //    TR2.CharacterFormat.FontName = "Times New Roman";
            //    TR2.CharacterFormat.FontSize = 13;
            //    DataRow.Cells[2].SetCellWidth(10, CellWidthType.Percentage);

            //    // 4. Ký hiệu hóa đơn
            //    DataRow.Cells[3].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //    Paragraph p3 = DataRow.Cells[3].AddParagraph();
            //    TextRange TR3 = p3.AppendText(giaiToaHoaDonViewModel.ListHoaDon[r].kyhieuhoadon ?? "");
            //    p3.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
            //    TR3.CharacterFormat.FontName = "Times New Roman";
            //    TR3.CharacterFormat.FontSize = 13;
            //    DataRow.Cells[3].SetCellWidth(10, CellWidthType.Percentage);

            //    // 5. Từ số - đến số
            //    DataRow.Cells[4].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //    Paragraph p4 = DataRow.Cells[4].AddParagraph();
            //    TextRange TR4 = p4.AppendText(giaiToaHoaDonViewModel.ListHoaDon[r].tusodenso ?? "");
            //    p4.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
            //    TR4.CharacterFormat.FontName = "Times New Roman";
            //    TR4.CharacterFormat.FontSize = 13;
            //    DataRow.Cells[4].SetCellWidth(30, CellWidthType.Percentage);

            //    // 6. Ghi chú
            //    DataRow.Cells[5].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //    Paragraph p5 = DataRow.Cells[5].AddParagraph();
            //    TextRange TR5 = p5.AppendText("Tiếp tục sử dụng");
            //    p5.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
            //    TR5.CharacterFormat.FontName = "Times New Roman";
            //    TR5.CharacterFormat.FontSize = 13;
            //    DataRow.Cells[5].SetCellWidth(20, CellWidthType.Percentage);
            //}
            //hoaDonBody2.ChildObjects.Remove(hoaDonParagraph2);
            //hoaDonBody2.ChildObjects.Insert(hoaDonIndex2, hoaDonTable2);
            //#endregion

            //#region Tạo table danh sách hóa đơn 2
            //TextSelection hoaDonSection3 = QD_TB_TT_CoHopDong_CoViPham_doc.FindString("{{HOADONTABLE2}}", true, true);
            //TextRange hoaDonRange3 = hoaDonSection3.GetAsOneRange();
            //Paragraph hoaDonParagraph3 = hoaDonRange3.OwnerParagraph;
            //Body hoaDonBody3 = hoaDonParagraph3.OwnerTextBody;
            //int hoaDonIndex3 = hoaDonBody3.ChildObjects.IndexOf(hoaDonParagraph3);
            //Table hoaDonTable3 = section2.AddTable(true);
            //String[] hoaDonHeader3 = new string[] { "STT", "Loại hóa đơn", "Ký hiệu mẫu", "Ký hiệu Hóa đơn", "Từ số - đến số", "Ghi chú" };

            //hoaDonTable3.ResetCells(giaiToaHoaDonViewModel.ListHoaDon.Count + 1, hoaDonHeader3.Count());
            //TableRow hoaDonTBRow3 = hoaDonTable3.Rows[0];
            //hoaDonTBRow3.IsHeader = true;

            //for (int i = 0; i < hoaDonHeader3.Length; i++)
            //{
            //    //Cell Alignment
            //    Paragraph p = hoaDonTBRow3.Cells[i].AddParagraph();
            //    hoaDonTBRow3.Cells[i].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //    p.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
            //    //Data Format
            //    TextRange TR = p.AppendText(hoaDonHeader3[i]);
            //    TR.CharacterFormat.FontName = "Times New Roman";
            //    TR.CharacterFormat.Bold = true;
            //    TR.CharacterFormat.FontSize = 13;
            //    switch (i)
            //    {
            //        case 0:
            //            hoaDonTBRow3.Cells[i].SetCellWidth(10, CellWidthType.Percentage); break;
            //        case 1:
            //            hoaDonTBRow3.Cells[i].SetCellWidth(20, CellWidthType.Percentage); break;
            //        case 2:
            //            hoaDonTBRow3.Cells[i].SetCellWidth(10, CellWidthType.Percentage); break;
            //        case 3:
            //            hoaDonTBRow3.Cells[i].SetCellWidth(10, CellWidthType.Percentage); break;
            //        case 4:
            //            hoaDonTBRow3.Cells[i].SetCellWidth(30, CellWidthType.Percentage); break;
            //        case 5:
            //            hoaDonTBRow3.Cells[i].SetCellWidth(20, CellWidthType.Percentage); break;
            //    }
            //}
            //// Table data rows
            //for (int r = 0; r < giaiToaHoaDonViewModel.ListHoaDon.Count; r++)
            //{
            //    TableRow DataRow = hoaDonTable3.Rows[r + 1];
            //    // 1. STT
            //    DataRow.Cells[0].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //    Paragraph p0 = DataRow.Cells[0].AddParagraph();
            //    TextRange TR0 = p0.AppendText((r + 1).ToString());
            //    p0.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
            //    TR0.CharacterFormat.FontName = "Times New Roman";
            //    TR0.CharacterFormat.FontSize = 13;
            //    DataRow.Cells[0].SetCellWidth(10, CellWidthType.Percentage);

            //    // 2. Loại hóa đơn
            //    DataRow.Cells[1].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //    Paragraph p1 = DataRow.Cells[1].AddParagraph();
            //    TextRange TR1 = p1.AppendText("Giá trị gia tăng");
            //    p1.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
            //    TR1.CharacterFormat.FontName = "Times New Roman";
            //    TR1.CharacterFormat.FontSize = 13;
            //    DataRow.Cells[1].SetCellWidth(20, CellWidthType.Percentage);

            //    // 3. Ký hiệu mẫu
            //    DataRow.Cells[2].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //    Paragraph p2 = DataRow.Cells[2].AddParagraph();
            //    TextRange TR2 = p2.AppendText(giaiToaHoaDonViewModel.ListHoaDon[r].kyhieumau ?? "");
            //    p2.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
            //    TR2.CharacterFormat.FontName = "Times New Roman";
            //    TR2.CharacterFormat.FontSize = 13;
            //    DataRow.Cells[2].SetCellWidth(10, CellWidthType.Percentage);

            //    // 4. Ký hiệu hóa đơn
            //    DataRow.Cells[3].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //    Paragraph p3 = DataRow.Cells[3].AddParagraph();
            //    TextRange TR3 = p3.AppendText(giaiToaHoaDonViewModel.ListHoaDon[r].kyhieuhoadon ?? "");
            //    p3.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
            //    TR3.CharacterFormat.FontName = "Times New Roman";
            //    TR3.CharacterFormat.FontSize = 13;
            //    DataRow.Cells[3].SetCellWidth(10, CellWidthType.Percentage);

            //    // 5. Từ số - đến số
            //    DataRow.Cells[4].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //    Paragraph p4 = DataRow.Cells[4].AddParagraph();
            //    TextRange TR4 = p4.AppendText(giaiToaHoaDonViewModel.ListHoaDon[r].tusodenso ?? "");
            //    p4.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
            //    TR4.CharacterFormat.FontName = "Times New Roman";
            //    TR4.CharacterFormat.FontSize = 13;
            //    DataRow.Cells[4].SetCellWidth(30, CellWidthType.Percentage);

            //    // 6. Ghi chú
            //    DataRow.Cells[5].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //    Paragraph p5 = DataRow.Cells[5].AddParagraph();
            //    TextRange TR5 = p5.AppendText("");
            //    p5.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
            //    TR5.CharacterFormat.FontName = "Times New Roman";
            //    TR5.CharacterFormat.FontSize = 13;
            //    DataRow.Cells[5].SetCellWidth(20, CellWidthType.Percentage);
            //}
            //hoaDonBody3.ChildObjects.Remove(hoaDonParagraph3);
            //hoaDonBody3.ChildObjects.Insert(hoaDonIndex3, hoaDonTable3);
            //#endregion

            //#region Tạo table danh sách nợ
            //TextSelection dsNoSection2 = QD_TB_TT_CoHopDong_CoViPham_doc.FindString("{{TIENNOTABLE}}", true, true);
            //TextRange dsNoRange2 = dsNoSection2.GetAsOneRange();
            //Paragraph dsNoParagraph2 = dsNoRange2.OwnerParagraph;
            //Body dsNoBody2 = dsNoParagraph2.OwnerTextBody;
            //int dsNoIndex2 = dsNoBody2.ChildObjects.IndexOf(dsNoParagraph2);
            //Table dsNoTable2 = section2.AddTable(true);
            //String[] dsNoHeader2 = new string[] { "STT", "Tiểu mục", "Nội dung", "Số tiền" };

            //dsNoTable2.ResetCells(giaiToaHoaDonViewModel.ListKhoanTienNo_CCHD.Count + 2, dsNoHeader2.Count());
            //TableRow dsNoRow2 = dsNoTable2.Rows[0];
            //dsNoRow2.IsHeader = true;
            //for (int i = 0; i < dsNoHeader2.Length; i++)
            //{
            //    //Cell Alignment
            //    Paragraph p = dsNoRow2.Cells[i].AddParagraph();
            //    dsNoRow2.Cells[i].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //    p.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
            //    //Data Format
            //    TextRange TR = p.AppendText(dsNoHeader2[i]);
            //    TR.CharacterFormat.FontName = "Times New Roman";
            //    TR.CharacterFormat.Bold = true;
            //    TR.CharacterFormat.FontSize = 13;
            //    switch (i)
            //    {
            //        case 0:
            //            dsNoRow2.Cells[i].SetCellWidth(10, CellWidthType.Percentage); break;
            //        case 1:
            //            dsNoRow2.Cells[i].SetCellWidth(20, CellWidthType.Percentage); break;
            //        case 2:
            //            dsNoRow2.Cells[i].SetCellWidth(30, CellWidthType.Percentage); break;
            //        case 3:
            //            dsNoRow2.Cells[i].SetCellWidth(40, CellWidthType.Percentage); break;
            //    }
            //}

            //// Table data rows
            //for (int r = 0; r < giaiToaHoaDonViewModel.ListKhoanTienNo_CCHD.Count; r++)
            //{
            //    TableRow DataRow = dsNoTable2.Rows[r + 1];
            //    // 1. STT
            //    DataRow.Cells[0].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //    Paragraph p0 = DataRow.Cells[0].AddParagraph();
            //    TextRange TR0 = p0.AppendText((r + 1).ToString());
            //    p0.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
            //    TR0.CharacterFormat.FontName = "Times New Roman";
            //    TR0.CharacterFormat.FontSize = 13;
            //    DataRow.Cells[0].SetCellWidth(10, CellWidthType.Percentage);

            //    //2. Tiểu mục
            //    DataRow.Cells[1].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //    Paragraph p1 = DataRow.Cells[1].AddParagraph();
            //    TextRange TR1 = p1.AppendText("TM " + giaiToaHoaDonViewModel.ListKhoanTienNo_CCHD[r].TieuMuc);
            //    p1.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
            //    TR1.CharacterFormat.FontName = "Times New Roman";
            //    TR1.CharacterFormat.FontSize = 13;
            //    DataRow.Cells[1].SetCellWidth(20, CellWidthType.Percentage);

            //    //3. Nội dung
            //    DataRow.Cells[2].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //    Paragraph p2 = DataRow.Cells[2].AddParagraph();
            //    TextRange TR2 = p2.AppendText(GTTKController.Get_NoiDungTheoTieuMuc(giaiToaHoaDonViewModel.ListKhoanTienNo_CCHD[r].TieuMuc));
            //    p2.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
            //    TR2.CharacterFormat.FontName = "Times New Roman";
            //    TR2.CharacterFormat.FontSize = 13;
            //    DataRow.Cells[2].SetCellWidth(30, CellWidthType.Percentage);

            //    //4.Số tiền
            //    DataRow.Cells[3].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //    Paragraph p3 = DataRow.Cells[3].AddParagraph();
            //    TextRange TR3 = p3.AppendText(giaiToaHoaDonViewModel.ListKhoanTienNo_CCHD[r].SoTienTheoSo.ToString("#,###"));
            //    p3.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
            //    TR3.CharacterFormat.FontName = "Times New Roman";
            //    TR3.CharacterFormat.FontSize = 13;
            //    DataRow.Cells[3].SetCellWidth(40, CellWidthType.Percentage);
            //}

            ////Số tiền tổng cộng
            //TableRow DataRowTongCong2 = dsNoTable.Rows[giaiToaHoaDonViewModel.ListKhoanTienNo_CCHD.Count + 1];
            ////3. Nội dung 
            //DataRowTongCong2.Cells[2].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //Paragraph pNo22 = DataRowTongCong2.Cells[2].AddParagraph();
            //TextRange TRNo22 = pNo22.AppendText("Tổng cộng");
            //pNo22.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
            //TRNo22.CharacterFormat.FontName = "Times New Roman";
            //TRNo22.CharacterFormat.FontSize = 13;
            //TRNo22.CharacterFormat.Bold = true;
            //DataRowTongCong2.Cells[2].SetCellWidth(30, CellWidthType.Percentage);

            ////4.Số tiền
            //DataRowTongCong2.Cells[3].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //Paragraph pNo32 = DataRowTongCong2.Cells[3].AddParagraph();
            //TextRange TRNo32 = pNo32.AppendText(giaiToaHoaDonViewModel.TongNo.ToString("#,###"));
            //pNo32.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
            //TRNo32.CharacterFormat.FontName = "Times New Roman";
            //TRNo32.CharacterFormat.FontSize = 13;
            //DataRowTongCong2.Cells[3].SetCellWidth(40, CellWidthType.Percentage);
            //dsNoBody2.ChildObjects.Remove(dsNoParagraph2);
            //dsNoBody2.ChildObjects.Insert(dsNoIndex2, dsNoTable2);
            //#endregion

            //QD_TB_TT_CoHopDong_CoViPham_doc.SaveToFile(giaiToaHoaDonViewModel.ContainerFolderPath + @"\" + "QĐ-TB-TT có HĐ có vi phạm.doc", FileFormat.Docx);
            //#endregion

            //#region Tạo file Biên bản làm việc nếu có vi phạm

            //#endregion

            MessageBox.Show("Tạo báo cáo thành công !", "Thông báo");
            #endregion
        }
    }
}
