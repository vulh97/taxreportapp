﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaxReportApp.Controller
{
    class DS_VietTatTenNH_Controller
    {
        public static DataTable SearchMST(string sFilePathAndName, string textSearch)
        {
            string sLine = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=";
            sLine += sFilePathAndName;
            sLine += ";Extended Properties=Excel 8.0";
            var connection = BaseController.getConnection(sLine);
            try
            {
                connection.Open();
                DataTable dt = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string Sheet = dt.Rows[0].Field<string>("TABLE_NAME");
                sLine = "Select top 1 * From [" + Sheet + "]";
                OleDbDataAdapter dataAdapter = new OleDbDataAdapter(sLine, connection);
                DataSet dataSet = new DataSet();
                dataAdapter.Fill(dataSet);
                // [F2, F3]: Column Name
                string sLine2 = "Select * From [" + Sheet + "] Where UCASE([" + dataSet.Tables[0].Columns[1].ColumnName.ToString() + "]) LIKE '%" + textSearch.ToUpper() + "%'";
                OleDbDataAdapter dataAdapter2 = new OleDbDataAdapter(sLine2, connection);
                //dataAdapter2.SelectCommand.Parameters.AddWithValue("@tenNH", textSearch.ToUpper());
                DataSet dataSet2 = new DataSet();
                dataAdapter2.Fill(dataSet2);
                return dataSet2.Tables[0];
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Lỗi");
                return null;
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
