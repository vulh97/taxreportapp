﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxReportApp.Model;

namespace TaxReportApp.Controller
{
    class FileNguonStatusController
    {
        // const tên file
        private static string chungTu_FileName = ConfigurationManager.AppSettings["licenseFile"];
        private static string danhBa_FileName = ConfigurationManager.AppSettings["phoneBookFile"];
        private static string diaChiNganHang_FileName = ConfigurationManager.AppSettings["listAddressBankFile"];
        private static string s12_FileName = ConfigurationManager.AppSettings["s12File"];
        private static string s14_FileName = ConfigurationManager.AppSettings["s14File"];
        private static string taiKhoanNganHang_FileName = ConfigurationManager.AppSettings["accountBankFile"];
        private static string csdl_FolderName = ConfigurationManager.AppSettings["directoryDatabase"] + @"\";
         private static string cctkTMSFile = ConfigurationManager.AppSettings["cctkTMSFile"];
        private static string dsTheodoiCCTK_FileName = ConfigurationManager.AppSettings["dsTheoDoiCCTK"];
        private static string dsTheoDoiCCHD_FileName = ConfigurationManager.AppSettings["dsTheoDoiCCHD"];

        private static string CCTK_trenTMS_FileName = ConfigurationManager.AppSettings["cctkTMSFile"];
        private static string CCHD_trenTMS_FileName = ConfigurationManager.AppSettings["cchd_tren_tms"];
        public static FileNguonStatusModel get_FileNguonStatus()
        {
            FileNguonStatusModel fileNguonStatus = new FileNguonStatusModel();
            var container_FolderPath = Environment.CurrentDirectory + @"\";

            if (File.Exists(container_FolderPath + csdl_FolderName + chungTu_FileName))
            {
                fileNguonStatus.CC_ChungTu = true;
            }

            if (File.Exists(container_FolderPath + csdl_FolderName + danhBa_FileName))
            {
                fileNguonStatus.CC_DanhBa = true;
            }

            if (File.Exists(container_FolderPath + csdl_FolderName + diaChiNganHang_FileName))
            {
                fileNguonStatus.CC_DSDiaChiNH = true;
            }

            if (File.Exists(container_FolderPath + csdl_FolderName + s12_FileName))
            {
                fileNguonStatus.CC_S12 = true;
            }

            if (File.Exists(container_FolderPath + csdl_FolderName + s14_FileName))
            {
                fileNguonStatus.CC_S14 = true;
            }

            if (File.Exists(container_FolderPath + csdl_FolderName + taiKhoanNganHang_FileName))
            {
                fileNguonStatus.CC_TaiKhoanNganHang = true;
            }

            if (File.Exists(container_FolderPath + csdl_FolderName + dsTheoDoiCCHD_FileName))
            {
                fileNguonStatus.cchdTrenTMS = true;
            }

            if (File.Exists(container_FolderPath + csdl_FolderName + dsTheodoiCCTK_FileName))
            {
                fileNguonStatus.dsTheoDoiCCTK = true;
            }

            if (File.Exists(container_FolderPath + csdl_FolderName + dsTheoDoiCCHD_FileName))
            {
                fileNguonStatus.dsTheoDoiCCHD = true;
            }
            if (File.Exists(container_FolderPath + csdl_FolderName + cctkTMSFile))
            {
                fileNguonStatus.cctkTMS = true;
            }
            if (File.Exists(container_FolderPath + csdl_FolderName + CCTK_trenTMS_FileName))
            {
                fileNguonStatus.cctkTrenTMS = true;
            }

            if (File.Exists(container_FolderPath + csdl_FolderName + CCHD_trenTMS_FileName))
            {
                fileNguonStatus.cchdTrenTMS = true;
            }
            return fileNguonStatus;
        }

        public static String get_tinhTrangCacFileNguonCCTK(FileNguonStatusModel fileNguonStatusModel)
        {
            var statusString = "";

            /// danh sách tên file còn thiếu
            List<String> danhSachTenFileConThieu = new List<string>();

            if (!fileNguonStatusModel.CC_ChungTu)
            {
                danhSachTenFileConThieu.Add("Chứng từ.XLSX");
            }

            if (!fileNguonStatusModel.CC_DanhBa)
            {
                danhSachTenFileConThieu.Add("Danh bạ.XLSX");
            }

            if (!fileNguonStatusModel.CC_DSDiaChiNH)
            {
                danhSachTenFileConThieu.Add("DS_địa chỉ nhân hàng.xlsx");
            }

            if (!fileNguonStatusModel.CC_S12)
            {
                danhSachTenFileConThieu.Add("S12.XLSX");
            }

            if (!fileNguonStatusModel.CC_S14)
            {
                danhSachTenFileConThieu.Add("S14.XLSX");
            }

            if (!fileNguonStatusModel.CC_TaiKhoanNganHang)
            {
                danhSachTenFileConThieu.Add("Tài khoản ngân hàng.XLSX");
            }

            if (danhSachTenFileConThieu.Count < 1)
            {
                statusString = "Các file đã được nhập vào đầy đủ.";
            } else {
                statusString = "Các file còn thiếu: \n - " + String.Join("\n - ", danhSachTenFileConThieu.ToArray());
            }

            return statusString;
        }

        public static String get_tinhTrangCacFileNguonCCHĐ(FileNguonStatusModel fileNguonStatusModel)
        {
            var statusString = "";

            /// danh sách tên file còn thiếu
            List<String> danhSachTenFileConThieu = new List<string>();

            if (!fileNguonStatusModel.CC_S12)
            {
                danhSachTenFileConThieu.Add(ConfigurationManager.AppSettings["s12File"]);
            }

            if (!fileNguonStatusModel.CC_DanhBa)
            {
                danhSachTenFileConThieu.Add(ConfigurationManager.AppSettings["phoneBookFile"]);
            }

            if (!fileNguonStatusModel.CC_ChungTu)
            {
                danhSachTenFileConThieu.Add(ConfigurationManager.AppSettings["licenseFile"]);
            }

            if (!fileNguonStatusModel.cchdTrenTMS)
            {
                danhSachTenFileConThieu.Add(ConfigurationManager.AppSettings["cchd_tren_tms"]);
            }

            if (danhSachTenFileConThieu.Count < 1)
            {
                statusString = "Các file đã được nhập vào đầy đủ.";
            }
            else
            {
                statusString = "Các file còn thiếu: \n - " + String.Join("\n - ", danhSachTenFileConThieu.ToArray());
            }

            return statusString;
        }

        public static String get_tinhTrangCacFileNguonGTHD(FileNguonStatusModel fileNguonStatusModel)
        {
            var statusString = "";

            /// danh sách tên file còn thiếu
            List<String> danhSachTenFileConThieu = new List<string>();

            if (!fileNguonStatusModel.CC_S12)
            {
                danhSachTenFileConThieu.Add(ConfigurationManager.AppSettings["s12File"]);
            }

            if (!fileNguonStatusModel.CC_ChungTu)
            {
                danhSachTenFileConThieu.Add(ConfigurationManager.AppSettings["licenseFile"]);
            }

            if (!fileNguonStatusModel.CC_DanhBa)
            {
                danhSachTenFileConThieu.Add(ConfigurationManager.AppSettings["phoneBookFile"]);
            }

            if (!fileNguonStatusModel.dsTheoDoiCCHD)
            {
                danhSachTenFileConThieu.Add(ConfigurationManager.AppSettings["dsTheoDoiCCHD"]);
            }

            if (danhSachTenFileConThieu.Count == 0)
            {
                statusString = "Các file đã được nhập vào đầy đủ.";
            }
            else
            {
                statusString = "Các file còn thiếu: \n - " + String.Join("\n - ", danhSachTenFileConThieu.ToArray());
            }

            return statusString;
        }
    }
}
