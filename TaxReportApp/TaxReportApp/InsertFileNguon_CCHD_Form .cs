﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TaxReportApp.Controller;

namespace TaxReportApp
{
    public partial class InsertFileNguon_CCHD_Form : Form
    {
        public InsertFileNguon_CCHD_Form()
        {
            InitializeComponent();

            // Kiểm tra các file đã có
            var fileNguonStatus = FileNguonStatusController.get_FileNguonStatus();

            if (fileNguonStatus.CC_S12)
            {
                CCHD_S12_statusMessage.Text = ConfigurationManager.AppSettings["s12File"];
            }

            if (fileNguonStatus.CC_ChungTu)
            {
                CCHD_ChungTu_statusMessage.Text = ConfigurationManager.AppSettings["licenseFile"];
            }

            if (fileNguonStatus.CC_DanhBa)
            {
                CCHD_DanhBa_statusMessage.Text = ConfigurationManager.AppSettings["phoneBookFile"];
            }

            if (fileNguonStatus.cchdTrenTMS)
            {
                CCHD_TrenTMS_statusMessage.Text = ConfigurationManager.AppSettings["cchd_tren_tms"];
            }
        }

        private void InsertFileNguon_CCTK_Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(1);
        }

        private void CCHD_S12_selectBtn_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.RestoreDirectory = true;
                openFileDialog.Title = "Chọn File";
                openFileDialog.Multiselect = false;
                openFileDialog.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";
                openFileDialog.CheckFileExists = true;
                openFileDialog.CheckPathExists = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    CCHD_S12_urlTxtBox.Text = openFileDialog.FileName;
                }
            } catch(Exception ex)
            {
                MessageBox.Show("Có lỗi xảy ra !", "Lỗi");
            }
        }

        private void CCHD_ChungTu_selectBtn_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.RestoreDirectory = true;
                openFileDialog.Title = "Chọn File";
                openFileDialog.Multiselect = false;
                openFileDialog.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";
                openFileDialog.CheckFileExists = true;
                openFileDialog.CheckPathExists = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    CCHD_ChungTu_urlTxtBox.Text = openFileDialog.FileName;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi xảy ra !", "Lỗi");
            }
        }

        private void CCHD_DanhBa_selectBtn_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.RestoreDirectory = true;
                openFileDialog.Title = "Chọn File";
                openFileDialog.Multiselect = false;
                openFileDialog.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";
                openFileDialog.CheckFileExists = true;
                openFileDialog.CheckPathExists = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    CCHD_DanhBa_urlTxtBox.Text = openFileDialog.FileName;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi xảy ra !", "Lỗi");
            }
        }

        private void CCHD_TrenTMS_selectBtn_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.RestoreDirectory = true;
                openFileDialog.Title = "Chọn File";
                openFileDialog.Multiselect = false;
                openFileDialog.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";
                openFileDialog.CheckFileExists = true;
                openFileDialog.CheckPathExists = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    CCHD_TrenTMS_urlTxtBox.Text = openFileDialog.FileName;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi xảy ra !", "Lỗi");
            }
        }

        private void CCHD_S12_deleteBtn_Click(object sender, EventArgs e)
        {
            CCHD_S12_urlTxtBox.Text = "";
        }

        private void CCHD_ChungTu_deleteBtn_Click(object sender, EventArgs e)
        {
            CCHD_ChungTu_urlTxtBox.Text = "";
        }

        private void CCHD_DanhBa_deleteBtn_Click(object sender, EventArgs e)
        {
            CCHD_DanhBa_urlTxtBox.Text = "";
        }

        private void CCHD_TrenTMS_deleteBtn_Click(object sender, EventArgs e)
        {
            CCHD_TrenTMS_urlTxtBox.Text = "";
        }

        private void CCHD_S12_loadBtn_Click(object sender, EventArgs e)
        {
            var filePath = CCHD_S12_urlTxtBox.Text;
            var isSuccessful = Common.Common.ImportFile(filePath, "s12File");

            if (isSuccessful)
            {
                CCHD_S12_statusMessage.Text = "S12.XLSX";
                CCHD_S12_urlTxtBox.Text = "";
            }
        }

        private void CCHD_ChungTu_loadBtn_Click(object sender, EventArgs e)
        {
            var filePath = CCHD_ChungTu_urlTxtBox.Text;
            var isSuccessful = Common.Common.ImportFile(filePath, "licenseFile");

            if (isSuccessful)
            {
                CCHD_ChungTu_statusMessage.Text = "Chứng từ.XLSX";
                CCHD_ChungTu_urlTxtBox.Text = "";
            }
        }

        private void CCHD_DanhBa_loadBtn_Click(object sender, EventArgs e)
        {
            var filePath = CCHD_DanhBa_urlTxtBox.Text;
            var isSuccessful = Common.Common.ImportFile(filePath, "phoneBookFile");

            if (isSuccessful)
            {
                CCHD_DanhBa_statusMessage.Text = "Danh bạ.XLSX";
                CCHD_DanhBa_urlTxtBox.Text = "";
            }
        }

        private void CCHD_TrenTMS_loadBtn_Click(object sender, EventArgs e)
        {
            var filePath = CCHD_TrenTMS_urlTxtBox.Text;
            var isSuccessful = Common.Common.ImportFile(filePath, "accountBankFile");

            if (isSuccessful)
            {
                CCHD_TrenTMS_statusMessage.Text = "Tài khoản ngân hàng.XLSX";
                CCHD_TrenTMS_urlTxtBox.Text = "";
            }
        }
    }
}
