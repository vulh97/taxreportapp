﻿namespace TaxReportApp
{
    partial class InsertFileNguon_CCHD_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InsertFileNguon_CCHD_Form));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.CCHD_S12_statusMessage = new System.Windows.Forms.Label();
            this.CCHD_S12_deleteBtn = new System.Windows.Forms.Button();
            this.CCHD_S12_loadBtn = new System.Windows.Forms.Button();
            this.CCHD_S12_selectBtn = new System.Windows.Forms.Button();
            this.CCHD_S12_urlTxtBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.CCHD_ChungTu_statusMessage = new System.Windows.Forms.Label();
            this.CCHD_ChungTu_deleteBtn = new System.Windows.Forms.Button();
            this.CCHD_ChungTu_loadBtn = new System.Windows.Forms.Button();
            this.CCHD_ChungTu_selectBtn = new System.Windows.Forms.Button();
            this.CCHD_ChungTu_urlTxtBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.CCHD_DanhBa_statusMessage = new System.Windows.Forms.Label();
            this.CCHD_DanhBa_deleteBtn = new System.Windows.Forms.Button();
            this.CCHD_DanhBa_loadBtn = new System.Windows.Forms.Button();
            this.CCHD_DanhBa_selectBtn = new System.Windows.Forms.Button();
            this.CCHD_DanhBa_urlTxtBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.CCHD_TrenTMS_statusMessage = new System.Windows.Forms.Label();
            this.CCHD_TrenTMS_deleteBtn = new System.Windows.Forms.Button();
            this.CCHD_TrenTMS_loadBtn = new System.Windows.Forms.Button();
            this.CCHD_TrenTMS_selectBtn = new System.Windows.Forms.Button();
            this.CCHD_TrenTMS_urlTxtBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.CCHD_S12_statusMessage);
            this.groupBox1.Controls.Add(this.CCHD_S12_deleteBtn);
            this.groupBox1.Controls.Add(this.CCHD_S12_loadBtn);
            this.groupBox1.Controls.Add(this.CCHD_S12_selectBtn);
            this.groupBox1.Controls.Add(this.CCHD_S12_urlTxtBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(26, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(762, 100);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "1. File S12";
            // 
            // CCHD_S12_statusMessage
            // 
            this.CCHD_S12_statusMessage.AutoSize = true;
            this.CCHD_S12_statusMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CCHD_S12_statusMessage.ForeColor = System.Drawing.SystemColors.Highlight;
            this.CCHD_S12_statusMessage.Location = new System.Drawing.Point(121, 28);
            this.CCHD_S12_statusMessage.Name = "CCHD_S12_statusMessage";
            this.CCHD_S12_statusMessage.Size = new System.Drawing.Size(199, 17);
            this.CCHD_S12_statusMessage.TabIndex = 6;
            this.CCHD_S12_statusMessage.Text = "Hiện tại không có file nào.";
            // 
            // CCHD_S12_deleteBtn
            // 
            this.CCHD_S12_deleteBtn.Location = new System.Drawing.Point(679, 57);
            this.CCHD_S12_deleteBtn.Name = "CCHD_S12_deleteBtn";
            this.CCHD_S12_deleteBtn.Size = new System.Drawing.Size(75, 23);
            this.CCHD_S12_deleteBtn.TabIndex = 5;
            this.CCHD_S12_deleteBtn.Text = "Xóa";
            this.CCHD_S12_deleteBtn.UseVisualStyleBackColor = true;
            this.CCHD_S12_deleteBtn.Click += new System.EventHandler(this.CCHD_S12_deleteBtn_Click);
            // 
            // CCHD_S12_loadBtn
            // 
            this.CCHD_S12_loadBtn.Location = new System.Drawing.Point(588, 57);
            this.CCHD_S12_loadBtn.Name = "CCHD_S12_loadBtn";
            this.CCHD_S12_loadBtn.Size = new System.Drawing.Size(75, 23);
            this.CCHD_S12_loadBtn.TabIndex = 4;
            this.CCHD_S12_loadBtn.Text = "Nhập";
            this.CCHD_S12_loadBtn.UseVisualStyleBackColor = true;
            this.CCHD_S12_loadBtn.Click += new System.EventHandler(this.CCHD_S12_loadBtn_Click);
            // 
            // CCHD_S12_selectBtn
            // 
            this.CCHD_S12_selectBtn.Location = new System.Drawing.Point(496, 57);
            this.CCHD_S12_selectBtn.Name = "CCHD_S12_selectBtn";
            this.CCHD_S12_selectBtn.Size = new System.Drawing.Size(75, 23);
            this.CCHD_S12_selectBtn.TabIndex = 3;
            this.CCHD_S12_selectBtn.Text = "Chọn";
            this.CCHD_S12_selectBtn.UseVisualStyleBackColor = true;
            this.CCHD_S12_selectBtn.Click += new System.EventHandler(this.CCHD_S12_selectBtn_Click);
            // 
            // CCHD_S12_urlTxtBox
            // 
            this.CCHD_S12_urlTxtBox.Location = new System.Drawing.Point(122, 57);
            this.CCHD_S12_urlTxtBox.Name = "CCHD_S12_urlTxtBox";
            this.CCHD_S12_urlTxtBox.Size = new System.Drawing.Size(355, 22);
            this.CCHD_S12_urlTxtBox.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Đường dẫn:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "File hiện tại:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.CCHD_ChungTu_statusMessage);
            this.groupBox3.Controls.Add(this.CCHD_ChungTu_deleteBtn);
            this.groupBox3.Controls.Add(this.CCHD_ChungTu_loadBtn);
            this.groupBox3.Controls.Add(this.CCHD_ChungTu_selectBtn);
            this.groupBox3.Controls.Add(this.CCHD_ChungTu_urlTxtBox);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Location = new System.Drawing.Point(26, 118);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(762, 100);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "2. File Chứng từ";
            // 
            // CCHD_ChungTu_statusMessage
            // 
            this.CCHD_ChungTu_statusMessage.AutoSize = true;
            this.CCHD_ChungTu_statusMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CCHD_ChungTu_statusMessage.ForeColor = System.Drawing.SystemColors.Highlight;
            this.CCHD_ChungTu_statusMessage.Location = new System.Drawing.Point(121, 28);
            this.CCHD_ChungTu_statusMessage.Name = "CCHD_ChungTu_statusMessage";
            this.CCHD_ChungTu_statusMessage.Size = new System.Drawing.Size(199, 17);
            this.CCHD_ChungTu_statusMessage.TabIndex = 9;
            this.CCHD_ChungTu_statusMessage.Text = "Hiện tại không có file nào.";
            // 
            // CCHD_ChungTu_deleteBtn
            // 
            this.CCHD_ChungTu_deleteBtn.Location = new System.Drawing.Point(679, 57);
            this.CCHD_ChungTu_deleteBtn.Name = "CCHD_ChungTu_deleteBtn";
            this.CCHD_ChungTu_deleteBtn.Size = new System.Drawing.Size(75, 23);
            this.CCHD_ChungTu_deleteBtn.TabIndex = 5;
            this.CCHD_ChungTu_deleteBtn.Text = "Xóa";
            this.CCHD_ChungTu_deleteBtn.UseVisualStyleBackColor = true;
            this.CCHD_ChungTu_deleteBtn.Click += new System.EventHandler(this.CCHD_ChungTu_deleteBtn_Click);
            // 
            // CCHD_ChungTu_loadBtn
            // 
            this.CCHD_ChungTu_loadBtn.Location = new System.Drawing.Point(588, 57);
            this.CCHD_ChungTu_loadBtn.Name = "CCHD_ChungTu_loadBtn";
            this.CCHD_ChungTu_loadBtn.Size = new System.Drawing.Size(75, 23);
            this.CCHD_ChungTu_loadBtn.TabIndex = 4;
            this.CCHD_ChungTu_loadBtn.Text = "Nhập";
            this.CCHD_ChungTu_loadBtn.UseVisualStyleBackColor = true;
            this.CCHD_ChungTu_loadBtn.Click += new System.EventHandler(this.CCHD_ChungTu_loadBtn_Click);
            // 
            // CCHD_ChungTu_selectBtn
            // 
            this.CCHD_ChungTu_selectBtn.Location = new System.Drawing.Point(496, 57);
            this.CCHD_ChungTu_selectBtn.Name = "CCHD_ChungTu_selectBtn";
            this.CCHD_ChungTu_selectBtn.Size = new System.Drawing.Size(75, 23);
            this.CCHD_ChungTu_selectBtn.TabIndex = 3;
            this.CCHD_ChungTu_selectBtn.Text = "Chọn";
            this.CCHD_ChungTu_selectBtn.UseVisualStyleBackColor = true;
            this.CCHD_ChungTu_selectBtn.Click += new System.EventHandler(this.CCHD_ChungTu_selectBtn_Click);
            // 
            // CCHD_ChungTu_urlTxtBox
            // 
            this.CCHD_ChungTu_urlTxtBox.Location = new System.Drawing.Point(122, 57);
            this.CCHD_ChungTu_urlTxtBox.Name = "CCHD_ChungTu_urlTxtBox";
            this.CCHD_ChungTu_urlTxtBox.Size = new System.Drawing.Size(355, 22);
            this.CCHD_ChungTu_urlTxtBox.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 60);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 17);
            this.label5.TabIndex = 1;
            this.label5.Text = "Đường dẫn:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "File hiện tại:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.CCHD_DanhBa_statusMessage);
            this.groupBox4.Controls.Add(this.CCHD_DanhBa_deleteBtn);
            this.groupBox4.Controls.Add(this.CCHD_DanhBa_loadBtn);
            this.groupBox4.Controls.Add(this.CCHD_DanhBa_selectBtn);
            this.groupBox4.Controls.Add(this.CCHD_DanhBa_urlTxtBox);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Location = new System.Drawing.Point(26, 224);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(762, 100);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "3. File Danh bạ";
            // 
            // CCHD_DanhBa_statusMessage
            // 
            this.CCHD_DanhBa_statusMessage.AutoSize = true;
            this.CCHD_DanhBa_statusMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CCHD_DanhBa_statusMessage.ForeColor = System.Drawing.SystemColors.Highlight;
            this.CCHD_DanhBa_statusMessage.Location = new System.Drawing.Point(121, 28);
            this.CCHD_DanhBa_statusMessage.Name = "CCHD_DanhBa_statusMessage";
            this.CCHD_DanhBa_statusMessage.Size = new System.Drawing.Size(199, 17);
            this.CCHD_DanhBa_statusMessage.TabIndex = 8;
            this.CCHD_DanhBa_statusMessage.Text = "Hiện tại không có file nào.";
            // 
            // CCHD_DanhBa_deleteBtn
            // 
            this.CCHD_DanhBa_deleteBtn.Location = new System.Drawing.Point(679, 57);
            this.CCHD_DanhBa_deleteBtn.Name = "CCHD_DanhBa_deleteBtn";
            this.CCHD_DanhBa_deleteBtn.Size = new System.Drawing.Size(75, 23);
            this.CCHD_DanhBa_deleteBtn.TabIndex = 5;
            this.CCHD_DanhBa_deleteBtn.Text = "Xóa";
            this.CCHD_DanhBa_deleteBtn.UseVisualStyleBackColor = true;
            this.CCHD_DanhBa_deleteBtn.Click += new System.EventHandler(this.CCHD_DanhBa_deleteBtn_Click);
            // 
            // CCHD_DanhBa_loadBtn
            // 
            this.CCHD_DanhBa_loadBtn.Location = new System.Drawing.Point(588, 57);
            this.CCHD_DanhBa_loadBtn.Name = "CCHD_DanhBa_loadBtn";
            this.CCHD_DanhBa_loadBtn.Size = new System.Drawing.Size(75, 23);
            this.CCHD_DanhBa_loadBtn.TabIndex = 4;
            this.CCHD_DanhBa_loadBtn.Text = "Nhập";
            this.CCHD_DanhBa_loadBtn.UseVisualStyleBackColor = true;
            this.CCHD_DanhBa_loadBtn.Click += new System.EventHandler(this.CCHD_DanhBa_loadBtn_Click);
            // 
            // CCHD_DanhBa_selectBtn
            // 
            this.CCHD_DanhBa_selectBtn.Location = new System.Drawing.Point(496, 57);
            this.CCHD_DanhBa_selectBtn.Name = "CCHD_DanhBa_selectBtn";
            this.CCHD_DanhBa_selectBtn.Size = new System.Drawing.Size(75, 23);
            this.CCHD_DanhBa_selectBtn.TabIndex = 3;
            this.CCHD_DanhBa_selectBtn.Text = "Chọn";
            this.CCHD_DanhBa_selectBtn.UseVisualStyleBackColor = true;
            this.CCHD_DanhBa_selectBtn.Click += new System.EventHandler(this.CCHD_DanhBa_selectBtn_Click);
            // 
            // CCHD_DanhBa_urlTxtBox
            // 
            this.CCHD_DanhBa_urlTxtBox.Location = new System.Drawing.Point(122, 57);
            this.CCHD_DanhBa_urlTxtBox.Name = "CCHD_DanhBa_urlTxtBox";
            this.CCHD_DanhBa_urlTxtBox.Size = new System.Drawing.Size(355, 22);
            this.CCHD_DanhBa_urlTxtBox.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(23, 60);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 17);
            this.label7.TabIndex = 1;
            this.label7.Text = "Đường dẫn:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(22, 28);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 17);
            this.label8.TabIndex = 0;
            this.label8.Text = "File hiện tại:";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.CCHD_TrenTMS_statusMessage);
            this.groupBox5.Controls.Add(this.CCHD_TrenTMS_deleteBtn);
            this.groupBox5.Controls.Add(this.CCHD_TrenTMS_loadBtn);
            this.groupBox5.Controls.Add(this.CCHD_TrenTMS_selectBtn);
            this.groupBox5.Controls.Add(this.CCHD_TrenTMS_urlTxtBox);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Location = new System.Drawing.Point(26, 330);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(762, 100);
            this.groupBox5.TabIndex = 6;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "4. File danh sách Cưỡng chế hóa đơn trên TMS";
            // 
            // CCHD_TrenTMS_statusMessage
            // 
            this.CCHD_TrenTMS_statusMessage.AutoSize = true;
            this.CCHD_TrenTMS_statusMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CCHD_TrenTMS_statusMessage.ForeColor = System.Drawing.SystemColors.Highlight;
            this.CCHD_TrenTMS_statusMessage.Location = new System.Drawing.Point(121, 28);
            this.CCHD_TrenTMS_statusMessage.Name = "CCHD_TrenTMS_statusMessage";
            this.CCHD_TrenTMS_statusMessage.Size = new System.Drawing.Size(199, 17);
            this.CCHD_TrenTMS_statusMessage.TabIndex = 9;
            this.CCHD_TrenTMS_statusMessage.Text = "Hiện tại không có file nào.";
            // 
            // CCHD_TrenTMS_deleteBtn
            // 
            this.CCHD_TrenTMS_deleteBtn.Location = new System.Drawing.Point(679, 57);
            this.CCHD_TrenTMS_deleteBtn.Name = "CCHD_TrenTMS_deleteBtn";
            this.CCHD_TrenTMS_deleteBtn.Size = new System.Drawing.Size(75, 23);
            this.CCHD_TrenTMS_deleteBtn.TabIndex = 5;
            this.CCHD_TrenTMS_deleteBtn.Text = "Xóa";
            this.CCHD_TrenTMS_deleteBtn.UseVisualStyleBackColor = true;
            this.CCHD_TrenTMS_deleteBtn.Click += new System.EventHandler(this.CCHD_TrenTMS_deleteBtn_Click);
            // 
            // CCHD_TrenTMS_loadBtn
            // 
            this.CCHD_TrenTMS_loadBtn.Location = new System.Drawing.Point(588, 57);
            this.CCHD_TrenTMS_loadBtn.Name = "CCHD_TrenTMS_loadBtn";
            this.CCHD_TrenTMS_loadBtn.Size = new System.Drawing.Size(75, 23);
            this.CCHD_TrenTMS_loadBtn.TabIndex = 4;
            this.CCHD_TrenTMS_loadBtn.Text = "Nhập";
            this.CCHD_TrenTMS_loadBtn.UseVisualStyleBackColor = true;
            this.CCHD_TrenTMS_loadBtn.Click += new System.EventHandler(this.CCHD_TrenTMS_loadBtn_Click);
            // 
            // CCHD_TrenTMS_selectBtn
            // 
            this.CCHD_TrenTMS_selectBtn.Location = new System.Drawing.Point(496, 57);
            this.CCHD_TrenTMS_selectBtn.Name = "CCHD_TrenTMS_selectBtn";
            this.CCHD_TrenTMS_selectBtn.Size = new System.Drawing.Size(75, 23);
            this.CCHD_TrenTMS_selectBtn.TabIndex = 3;
            this.CCHD_TrenTMS_selectBtn.Text = "Chọn";
            this.CCHD_TrenTMS_selectBtn.UseVisualStyleBackColor = true;
            this.CCHD_TrenTMS_selectBtn.Click += new System.EventHandler(this.CCHD_TrenTMS_selectBtn_Click);
            // 
            // CCHD_TrenTMS_urlTxtBox
            // 
            this.CCHD_TrenTMS_urlTxtBox.Location = new System.Drawing.Point(122, 57);
            this.CCHD_TrenTMS_urlTxtBox.Name = "CCHD_TrenTMS_urlTxtBox";
            this.CCHD_TrenTMS_urlTxtBox.Size = new System.Drawing.Size(355, 22);
            this.CCHD_TrenTMS_urlTxtBox.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(23, 60);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 17);
            this.label9.TabIndex = 1;
            this.label9.Text = "Đường dẫn:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(22, 28);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(84, 17);
            this.label10.TabIndex = 0;
            this.label10.Text = "File hiện tại:";
            // 
            // InsertFileNguon_CCHD_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 453);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "InsertFileNguon_CCHD_Form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Nhập file nguồn CCHD";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label CCHD_S12_statusMessage;
        private System.Windows.Forms.Button CCHD_S12_deleteBtn;
        private System.Windows.Forms.Button CCHD_S12_loadBtn;
        private System.Windows.Forms.Button CCHD_S12_selectBtn;
        private System.Windows.Forms.TextBox CCHD_S12_urlTxtBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label CCHD_ChungTu_statusMessage;
        private System.Windows.Forms.Button CCHD_ChungTu_deleteBtn;
        private System.Windows.Forms.Button CCHD_ChungTu_loadBtn;
        private System.Windows.Forms.Button CCHD_ChungTu_selectBtn;
        private System.Windows.Forms.TextBox CCHD_ChungTu_urlTxtBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label CCHD_DanhBa_statusMessage;
        private System.Windows.Forms.Button CCHD_DanhBa_deleteBtn;
        private System.Windows.Forms.Button CCHD_DanhBa_loadBtn;
        private System.Windows.Forms.Button CCHD_DanhBa_selectBtn;
        private System.Windows.Forms.TextBox CCHD_DanhBa_urlTxtBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label CCHD_TrenTMS_statusMessage;
        private System.Windows.Forms.Button CCHD_TrenTMS_deleteBtn;
        private System.Windows.Forms.Button CCHD_TrenTMS_loadBtn;
        private System.Windows.Forms.Button CCHD_TrenTMS_selectBtn;
        private System.Windows.Forms.TextBox CCHD_TrenTMS_urlTxtBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
    }
}