﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TaxReportApp.Controller;

namespace TaxReportApp
{
    public partial class InsertFileNguon_CCTK_Form : Form
    {
        public InsertFileNguon_CCTK_Form()
        {
            InitializeComponent();

            // Kiểm tra các file đã có
            var fileNguonStatus = FileNguonStatusController.get_FileNguonStatus();

            if (fileNguonStatus.CC_S12)
            {
                CCTK_S12_statusMessage.Text = "S12.XLSX";
            }

            if (fileNguonStatus.CC_S14)
            {
                CCTK_S14_statusMessage.Text = "S14.XLSX";
            }

            if (fileNguonStatus.CC_ChungTu)
            {
                CCTK_ChungTu_statusMessage.Text = "Chứng từ.XLSX";
            }

            if (fileNguonStatus.CC_DanhBa)
            {
                CCTK_DanhBa_statusMessage.Text = "Danh bạ.XLSX";
            }

            if (fileNguonStatus.CC_TaiKhoanNganHang)
            {
                CCTK_TKNganHang_statusMessage.Text = "Tài khoản ngân hàng.XLSX";
            }

            if (fileNguonStatus.CC_DSDiaChiNH)
            {
                CCTK_dsDiaChiNganHang_statusMessage.Text = "DS_địa chỉ nhân hàng.xlsx";
            }
        }

        private void InsertFileNguon_CCTK_Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(1);
        }

        private void CCTK_S12_selectBtn_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.RestoreDirectory = true;
                openFileDialog.Title = "Chọn File";
                openFileDialog.Multiselect = false;
                openFileDialog.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";
                openFileDialog.CheckFileExists = true;
                openFileDialog.CheckPathExists = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    CCTK_S12_urlTxtBox.Text = openFileDialog.FileName;
                }
            } catch(Exception ex)
            {
                MessageBox.Show("Có lỗi xảy ra !", "Lỗi");
            }
        }

        private void CCTK_S14_selectBtn_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.RestoreDirectory = true;
                openFileDialog.Title = "Chọn File";
                openFileDialog.Multiselect = false;
                openFileDialog.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";
                openFileDialog.CheckFileExists = true;
                openFileDialog.CheckPathExists = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    CCTK_S14_urlTxtBox.Text = openFileDialog.FileName;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi xảy ra !", "Lỗi");
            }
        }

        private void CCTK_ChungTu_selectBtn_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.RestoreDirectory = true;
                openFileDialog.Title = "Chọn File";
                openFileDialog.Multiselect = false;
                openFileDialog.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";
                openFileDialog.CheckFileExists = true;
                openFileDialog.CheckPathExists = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    CCTK_ChungTu_urlTxtBox.Text = openFileDialog.FileName;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi xảy ra !", "Lỗi");
            }
        }

        private void CCTK_DanhBa_selectBtn_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.RestoreDirectory = true;
                openFileDialog.Title = "Chọn File";
                openFileDialog.Multiselect = false;
                openFileDialog.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";
                openFileDialog.CheckFileExists = true;
                openFileDialog.CheckPathExists = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    CCTK_DanhBa_urlTxtBox.Text = openFileDialog.FileName;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi xảy ra !", "Lỗi");
            }
        }

        private void CCTK_TKNganHang_selectBtn_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.RestoreDirectory = true;
                openFileDialog.Title = "Chọn File";
                openFileDialog.Multiselect = false;
                openFileDialog.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";
                openFileDialog.CheckFileExists = true;
                openFileDialog.CheckPathExists = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    CCTK_TKNganHang_urlTxtBox.Text = openFileDialog.FileName;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi xảy ra !", "Lỗi");
            }
        }

        private void CCTK_dsDiaChiNganHang_selectBtn_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.RestoreDirectory = true;
                openFileDialog.Title = "Chọn File";
                openFileDialog.Multiselect = false;
                openFileDialog.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";
                openFileDialog.CheckFileExists = true;
                openFileDialog.CheckPathExists = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    CCTK_DSDiaChiNganHang_urlTxtBox.Text = openFileDialog.FileName;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi xảy ra !", "Lỗi");
            }
        }

        private void CCTK_S12_deleteBtn_Click(object sender, EventArgs e)
        {
            CCTK_S12_urlTxtBox.Text = "";
        }

        private void CCTK_S14_deleteBtn_Click(object sender, EventArgs e)
        {
            CCTK_S14_urlTxtBox.Text = "";
        }

        private void CCTK_ChungTu_deleteBtn_Click(object sender, EventArgs e)
        {
            CCTK_ChungTu_urlTxtBox.Text = "";
        }

        private void CCTK_DanhBa_deleteBtn_Click(object sender, EventArgs e)
        {
            CCTK_DanhBa_urlTxtBox.Text = "";
        }

        private void CCTK_TKNganHang_deleteBtn_Click(object sender, EventArgs e)
        {
            CCTK_TKNganHang_urlTxtBox.Text = "";
        }

        private void CCTK_DSDiaChiNganHang_deleteBtn_Click(object sender, EventArgs e)
        {
            CCTK_DSDiaChiNganHang_urlTxtBox.Text = "";
        }

        private void CCTK_S12_loadBtn_Click(object sender, EventArgs e)
        {
            var filePath = CCTK_S12_urlTxtBox.Text;
            var isSuccessful = Common.Common.ImportFile(filePath, "s12File");

            if (isSuccessful)
            {
                CCTK_S12_statusMessage.Text = "S12.XLSX";
                CCTK_S12_urlTxtBox.Text = "";
            }
        }

        private void CCTK_S14_loadBtn_Click(object sender, EventArgs e)
        {
            var filePath = CCTK_S14_urlTxtBox.Text;
            var isSuccessful = Common.Common.ImportFile(filePath, "s14File");

            if (isSuccessful)
            {
                CCTK_S14_statusMessage.Text = "S14.XLSX";
                CCTK_S14_urlTxtBox.Text = "";
            }
        }

        private void CCTK_ChungTu_loadBtn_Click(object sender, EventArgs e)
        {
            var filePath = CCTK_ChungTu_urlTxtBox.Text;
            var isSuccessful = Common.Common.ImportFile(filePath, "licenseFile");

            if (isSuccessful)
            {
                CCTK_ChungTu_statusMessage.Text = "Chứng từ.XLSX";
                CCTK_ChungTu_urlTxtBox.Text = "";
            }
        }

        private void CCTK_DanhBa_loadBtn_Click(object sender, EventArgs e)
        {
            var filePath = CCTK_DanhBa_urlTxtBox.Text;
            var isSuccessful = Common.Common.ImportFile(filePath, "phoneBookFile");

            if (isSuccessful)
            {
                CCTK_DanhBa_statusMessage.Text = "Danh bạ.XLSX";
                CCTK_DanhBa_urlTxtBox.Text = "";
            }
        }

        private void CCTK_TKNganHang_loadBtn_Click(object sender, EventArgs e)
        {
            var filePath = CCTK_TKNganHang_urlTxtBox.Text;
            var isSuccessful = Common.Common.ImportFile(filePath, "accountBankFile");

            if (isSuccessful)
            {
                CCTK_TKNganHang_statusMessage.Text = "Tài khoản ngân hàng.XLSX";
                CCTK_TKNganHang_urlTxtBox.Text = "";
            }
        }

        private void CCTK_DSDiaChiNganHang_loadBtn_Click(object sender, EventArgs e)
        {
            var filePath = CCTK_DSDiaChiNganHang_urlTxtBox.Text;
            var isSuccessful = Common.Common.ImportFile(filePath, "listAddressBankFile");

            if (isSuccessful)
            {
                CCTK_dsDiaChiNganHang_statusMessage.Text = "DS_địa chỉ nhân hàng.XLSX";
                CCTK_DSDiaChiNganHang_urlTxtBox.Text = "";
            }
        }
    }
}
