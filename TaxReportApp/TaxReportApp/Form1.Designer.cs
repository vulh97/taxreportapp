﻿namespace TaxReportApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabPage14 = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage15 = new System.Windows.Forms.TabPage();
            this.CCTK_DateTime = new System.Windows.Forms.DateTimePicker();
            this.button26 = new System.Windows.Forms.Button();
            this.btnCCTK_openForm_NhapFileNguon = new System.Windows.Forms.Button();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.tbCCTK_tenChiCucTruong = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.tbCCTK_tenPhoChiCucTruong = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.tbCCTK_TenDoiTruong = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.tbCCTK_TenCB = new System.Windows.Forms.TextBox();
            this.cbCCTK_tenDoi = new System.Windows.Forms.ComboBox();
            this.cbCCTK_Chinhanh = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txbCCTK_fileNguonStatus = new System.Windows.Forms.RichTextBox();
            this.btnCCTK_CreateReport = new System.Windows.Forms.Button();
            this.btnCCTK_checkFile = new System.Windows.Forms.Button();
            this.dgvCCTK_Search = new System.Windows.Forms.DataGridView();
            this.STT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TenCongTy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MST = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Chuong = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Khoan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TieuMuc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnCCTK_SearchMST = new System.Windows.Forms.Button();
            this.tbCCTK_SearchMST = new System.Windows.Forms.TextBox();
            this.tabPage16 = new System.Windows.Forms.TabPage();
            this.CCHD_DateTime = new System.Windows.Forms.DateTimePicker();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.tbCCHD_tenChiCucTruong = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.tbCCHD_tenPhoChiCucTruong = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.tbCCHD_TenDoiTruong = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.CCHD_TenCanBo = new System.Windows.Forms.TextBox();
            this.CCHD_listTenDoi = new System.Windows.Forms.ComboBox();
            this.CCHD_listChiNhanh = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.CCHD_FileNguonStatus = new System.Windows.Forms.RichTextBox();
            this.btnCCHD = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.CCHD_GridView = new System.Windows.Forms.DataGridView();
            this.STT1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TenCongTy1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MST1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Chuong1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Khoan1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TieuMuc1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button22 = new System.Windows.Forms.Button();
            this.tbCCHD_Search = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.tabPage17 = new System.Windows.Forms.TabPage();
            this.GTTK_DateTime = new System.Windows.Forms.DateTimePicker();
            this.btnCreateTB = new System.Windows.Forms.Button();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.tbGTTK_tenChiCucTruong = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.tbGTTK_tenPhoChiCucTruong = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.tbGTTK_TenDoiTruong = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.GTTK_TenCanBo = new System.Windows.Forms.TextBox();
            this.GTTK_listTenDoi = new System.Windows.Forms.ComboBox();
            this.GTTK_listChiNhanh = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.GTTK_CheckResult_text = new System.Windows.Forms.RichTextBox();
            this.button32 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.button29 = new System.Windows.Forms.Button();
            this.GTTK_dsCongTy_GridView = new System.Windows.Forms.DataGridView();
            this.STT2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TenCongTy2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MST2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Chuong2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Khoan2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TieuMuc2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button28 = new System.Windows.Forms.Button();
            this.GTTK_MST_textBox = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.tabPage18 = new System.Windows.Forms.TabPage();
            this.GTHD_DateTime = new System.Windows.Forms.DateTimePicker();
            this.tinhTrangFileNguon_GTHD_txt = new System.Windows.Forms.RichTextBox();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.tbGTHD_tenChiCucTruong = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.tbGTHD_tenPhoChiCucTruong = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.tbGTHD_TenDoiTruong = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.GTHD_TenCanBo = new System.Windows.Forms.TextBox();
            this.GTHD_listTenDoi = new System.Windows.Forms.ComboBox();
            this.GTHD_listChiNhanh = new System.Windows.Forms.ComboBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.CreateReport_CCHD_btn = new System.Windows.Forms.Button();
            this.button37 = new System.Windows.Forms.Button();
            this.button36 = new System.Windows.Forms.Button();
            this.FileNguonStatusCheck_GTHD_btn = new System.Windows.Forms.Button();
            this.GTHD_GridView = new System.Windows.Forms.DataGridView();
            this.STT3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TenCongTy3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MST3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Chuong3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Khoan3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TieuMuc3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button33 = new System.Windows.Forms.Button();
            this.MST_GTHD_txtBox = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.button27 = new System.Windows.Forms.Button();
            this.tb_tenChiCucTruong = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.addTenDoiTruong_btn = new System.Windows.Forms.Button();
            this.tenDoiTruong_txt = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.button34 = new System.Windows.Forms.Button();
            this.tb_tenPhoChiCucTruong = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.listTenDoi_comboBox = new System.Windows.Forms.ComboBox();
            this.saveTenDoi_btn = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.listChiCucThue_comboBox = new System.Windows.Forms.ComboBox();
            this.addChiCucThue_btn = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.saveTenCanBo_btn = new System.Windows.Forms.Button();
            this.tenCanBo_txt = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.S12FileNameTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.tbLicenseFile = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.button16 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.button19 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.tbPhonebook = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.btnDeleteCCTKTMS = new System.Windows.Forms.Button();
            this.btnImportCCTKTMS = new System.Windows.Forms.Button();
            this.btnUploadCCTKTMS = new System.Windows.Forms.Button();
            this.tbCCTKTMS = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.SaveDataTab = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.button35 = new System.Windows.Forms.Button();
            this.button38 = new System.Windows.Forms.Button();
            this.button39 = new System.Windows.Forms.Button();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.richTextBox3 = new System.Windows.Forms.RichTextBox();
            this.button40 = new System.Windows.Forms.Button();
            this.button41 = new System.Windows.Forms.Button();
            this.button42 = new System.Windows.Forms.Button();
            this.richTextBox4 = new System.Windows.Forms.RichTextBox();
            this.richTextBox5 = new System.Windows.Forms.RichTextBox();
            this.richTextBox6 = new System.Windows.Forms.RichTextBox();
            this.button43 = new System.Windows.Forms.Button();
            this.button44 = new System.Windows.Forms.Button();
            this.button45 = new System.Windows.Forms.Button();
            this.richTextBox7 = new System.Windows.Forms.RichTextBox();
            this.richTextBox8 = new System.Windows.Forms.RichTextBox();
            this.button46 = new System.Windows.Forms.Button();
            this.button47 = new System.Windows.Forms.Button();
            this.button48 = new System.Windows.Forms.Button();
            this.tabPage14.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage15.SuspendLayout();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCCTK_Search)).BeginInit();
            this.tabPage16.SuspendLayout();
            this.groupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CCHD_GridView)).BeginInit();
            this.tabPage17.SuspendLayout();
            this.groupBox13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GTTK_dsCongTy_GridView)).BeginInit();
            this.tabPage18.SuspendLayout();
            this.groupBox14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GTHD_GridView)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.SaveDataTab.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.tabPage8.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage14
            // 
            this.tabPage14.BackColor = System.Drawing.Color.Azure;
            this.tabPage14.Controls.Add(this.tabControl2);
            this.tabPage14.Location = new System.Drawing.Point(4, 28);
            this.tabPage14.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage14.Name = "tabPage14";
            this.tabPage14.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage14.Size = new System.Drawing.Size(951, 717);
            this.tabPage14.TabIndex = 4;
            this.tabPage14.Text = "Xuất báo cáo";
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage15);
            this.tabControl2.Controls.Add(this.tabPage16);
            this.tabControl2.Controls.Add(this.tabPage17);
            this.tabControl2.Controls.Add(this.tabPage18);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(3, 2);
            this.tabControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(945, 713);
            this.tabControl2.TabIndex = 0;
            // 
            // tabPage15
            // 
            this.tabPage15.BackColor = System.Drawing.Color.Azure;
            this.tabPage15.Controls.Add(this.CCTK_DateTime);
            this.tabPage15.Controls.Add(this.button26);
            this.tabPage15.Controls.Add(this.btnCCTK_openForm_NhapFileNguon);
            this.tabPage15.Controls.Add(this.groupBox10);
            this.tabPage15.Controls.Add(this.label16);
            this.tabPage15.Controls.Add(this.txbCCTK_fileNguonStatus);
            this.tabPage15.Controls.Add(this.btnCCTK_CreateReport);
            this.tabPage15.Controls.Add(this.btnCCTK_checkFile);
            this.tabPage15.Controls.Add(this.dgvCCTK_Search);
            this.tabPage15.Controls.Add(this.btnCCTK_SearchMST);
            this.tabPage15.Controls.Add(this.tbCCTK_SearchMST);
            this.tabPage15.Font = new System.Drawing.Font("Arial", 9.75F);
            this.tabPage15.Location = new System.Drawing.Point(4, 28);
            this.tabPage15.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage15.Name = "tabPage15";
            this.tabPage15.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage15.Size = new System.Drawing.Size(937, 681);
            this.tabPage15.TabIndex = 0;
            this.tabPage15.Text = "Cưỡng chế tài khoản";
            // 
            // CCTK_DateTime
            // 
            this.CCTK_DateTime.Location = new System.Drawing.Point(698, 22);
            this.CCTK_DateTime.Name = "CCTK_DateTime";
            this.CCTK_DateTime.Size = new System.Drawing.Size(112, 26);
            this.CCTK_DateTime.TabIndex = 11;
            // 
            // button26
            // 
            this.button26.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button26.Location = new System.Drawing.Point(677, 254);
            this.button26.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(192, 23);
            this.button26.TabIndex = 10;
            this.button26.Text = "Chỉnh sửa mẫu biểu";
            this.button26.UseVisualStyleBackColor = true;
            this.button26.Click += new System.EventHandler(this.button26_Click);
            // 
            // btnCCTK_openForm_NhapFileNguon
            // 
            this.btnCCTK_openForm_NhapFileNguon.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCCTK_openForm_NhapFileNguon.Location = new System.Drawing.Point(465, 254);
            this.btnCCTK_openForm_NhapFileNguon.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnCCTK_openForm_NhapFileNguon.Name = "btnCCTK_openForm_NhapFileNguon";
            this.btnCCTK_openForm_NhapFileNguon.Size = new System.Drawing.Size(192, 23);
            this.btnCCTK_openForm_NhapFileNguon.TabIndex = 9;
            this.btnCCTK_openForm_NhapFileNguon.Text = "Nhập file nguồn";
            this.btnCCTK_openForm_NhapFileNguon.UseVisualStyleBackColor = true;
            this.btnCCTK_openForm_NhapFileNguon.Click += new System.EventHandler(this.btnCCTK_openForm_NhapFileNguon_Click);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.tbCCTK_tenChiCucTruong);
            this.groupBox10.Controls.Add(this.label41);
            this.groupBox10.Controls.Add(this.tbCCTK_tenPhoChiCucTruong);
            this.groupBox10.Controls.Add(this.label42);
            this.groupBox10.Controls.Add(this.tbCCTK_TenDoiTruong);
            this.groupBox10.Controls.Add(this.label36);
            this.groupBox10.Controls.Add(this.tbCCTK_TenCB);
            this.groupBox10.Controls.Add(this.cbCCTK_tenDoi);
            this.groupBox10.Controls.Add(this.cbCCTK_Chinhanh);
            this.groupBox10.Controls.Add(this.label20);
            this.groupBox10.Controls.Add(this.label19);
            this.groupBox10.Controls.Add(this.label18);
            this.groupBox10.Location = new System.Drawing.Point(43, 366);
            this.groupBox10.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox10.Size = new System.Drawing.Size(827, 298);
            this.groupBox10.TabIndex = 8;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Chi tiết đơn vị";
            // 
            // tbCCTK_tenChiCucTruong
            // 
            this.tbCCTK_tenChiCucTruong.Font = new System.Drawing.Font("Arial", 9.75F);
            this.tbCCTK_tenChiCucTruong.Location = new System.Drawing.Point(193, 75);
            this.tbCCTK_tenChiCucTruong.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbCCTK_tenChiCucTruong.Name = "tbCCTK_tenChiCucTruong";
            this.tbCCTK_tenChiCucTruong.Size = new System.Drawing.Size(497, 26);
            this.tbCCTK_tenChiCucTruong.TabIndex = 11;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label41.Location = new System.Drawing.Point(35, 78);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(155, 19);
            this.label41.TabIndex = 10;
            this.label41.Text = "Tên chi cục trưởng:";
            // 
            // tbCCTK_tenPhoChiCucTruong
            // 
            this.tbCCTK_tenPhoChiCucTruong.Font = new System.Drawing.Font("Arial", 9.75F);
            this.tbCCTK_tenPhoChiCucTruong.Location = new System.Drawing.Point(193, 120);
            this.tbCCTK_tenPhoChiCucTruong.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbCCTK_tenPhoChiCucTruong.Name = "tbCCTK_tenPhoChiCucTruong";
            this.tbCCTK_tenPhoChiCucTruong.Size = new System.Drawing.Size(497, 26);
            this.tbCCTK_tenPhoChiCucTruong.TabIndex = 9;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label42.Location = new System.Drawing.Point(3, 122);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(187, 19);
            this.label42.TabIndex = 8;
            this.label42.Text = "Tên phó chi cục trưởng:";
            // 
            // tbCCTK_TenDoiTruong
            // 
            this.tbCCTK_TenDoiTruong.Font = new System.Drawing.Font("Arial", 9.75F);
            this.tbCCTK_TenDoiTruong.Location = new System.Drawing.Point(193, 164);
            this.tbCCTK_TenDoiTruong.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbCCTK_TenDoiTruong.Name = "tbCCTK_TenDoiTruong";
            this.tbCCTK_TenDoiTruong.Size = new System.Drawing.Size(497, 26);
            this.tbCCTK_TenDoiTruong.TabIndex = 7;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label36.Location = new System.Drawing.Point(64, 167);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(124, 19);
            this.label36.TabIndex = 6;
            this.label36.Text = "Tên đội trưởng:";
            // 
            // tbCCTK_TenCB
            // 
            this.tbCCTK_TenCB.Font = new System.Drawing.Font("Arial", 9.75F);
            this.tbCCTK_TenCB.Location = new System.Drawing.Point(193, 209);
            this.tbCCTK_TenCB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbCCTK_TenCB.Name = "tbCCTK_TenCB";
            this.tbCCTK_TenCB.Size = new System.Drawing.Size(497, 26);
            this.tbCCTK_TenCB.TabIndex = 5;
            // 
            // cbCCTK_tenDoi
            // 
            this.cbCCTK_tenDoi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCCTK_tenDoi.Font = new System.Drawing.Font("Arial", 9.75F);
            this.cbCCTK_tenDoi.FormattingEnabled = true;
            this.cbCCTK_tenDoi.Location = new System.Drawing.Point(193, 253);
            this.cbCCTK_tenDoi.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbCCTK_tenDoi.Name = "cbCCTK_tenDoi";
            this.cbCCTK_tenDoi.Size = new System.Drawing.Size(497, 26);
            this.cbCCTK_tenDoi.TabIndex = 4;
            // 
            // cbCCTK_Chinhanh
            // 
            this.cbCCTK_Chinhanh.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCCTK_Chinhanh.Font = new System.Drawing.Font("Arial", 9.75F);
            this.cbCCTK_Chinhanh.FormattingEnabled = true;
            this.cbCCTK_Chinhanh.Location = new System.Drawing.Point(193, 28);
            this.cbCCTK_Chinhanh.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbCCTK_Chinhanh.Name = "cbCCTK_Chinhanh";
            this.cbCCTK_Chinhanh.Size = new System.Drawing.Size(497, 26);
            this.cbCCTK_Chinhanh.TabIndex = 3;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label20.Location = new System.Drawing.Point(94, 211);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(96, 19);
            this.label20.TabIndex = 2;
            this.label20.Text = "Tên cán bộ:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label19.Location = new System.Drawing.Point(121, 256);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(69, 19);
            this.label19.TabIndex = 1;
            this.label19.Text = "Tên đội:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label18.Location = new System.Drawing.Point(118, 31);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(71, 19);
            this.label18.TabIndex = 0;
            this.label18.Text = "Chi cục:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(43, 23);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(89, 19);
            this.label16.TabIndex = 7;
            this.label16.Text = "Mã số thuế";
            // 
            // txbCCTK_fileNguonStatus
            // 
            this.txbCCTK_fileNguonStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txbCCTK_fileNguonStatus.Font = new System.Drawing.Font("Arial", 9.75F);
            this.txbCCTK_fileNguonStatus.Location = new System.Drawing.Point(43, 298);
            this.txbCCTK_fileNguonStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txbCCTK_fileNguonStatus.Name = "txbCCTK_fileNguonStatus";
            this.txbCCTK_fileNguonStatus.Size = new System.Drawing.Size(827, 64);
            this.txbCCTK_fileNguonStatus.TabIndex = 6;
            this.txbCCTK_fileNguonStatus.Text = "";
            // 
            // btnCCTK_CreateReport
            // 
            this.btnCCTK_CreateReport.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCCTK_CreateReport.Location = new System.Drawing.Point(253, 254);
            this.btnCCTK_CreateReport.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnCCTK_CreateReport.Name = "btnCCTK_CreateReport";
            this.btnCCTK_CreateReport.Size = new System.Drawing.Size(192, 23);
            this.btnCCTK_CreateReport.TabIndex = 4;
            this.btnCCTK_CreateReport.Text = "Tạo báo cáo";
            this.btnCCTK_CreateReport.UseVisualStyleBackColor = true;
            this.btnCCTK_CreateReport.Click += new System.EventHandler(this.btnCCTK_CreateReport_Click);
            // 
            // btnCCTK_checkFile
            // 
            this.btnCCTK_checkFile.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCCTK_checkFile.Location = new System.Drawing.Point(43, 254);
            this.btnCCTK_checkFile.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnCCTK_checkFile.Name = "btnCCTK_checkFile";
            this.btnCCTK_checkFile.Size = new System.Drawing.Size(192, 23);
            this.btnCCTK_checkFile.TabIndex = 3;
            this.btnCCTK_checkFile.Text = "Kiểm tra";
            this.btnCCTK_checkFile.UseVisualStyleBackColor = true;
            this.btnCCTK_checkFile.Click += new System.EventHandler(this.btnCCTK_checkFile_Click);
            // 
            // dgvCCTK_Search
            // 
            this.dgvCCTK_Search.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dgvCCTK_Search.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCCTK_Search.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.STT,
            this.TenCongTy,
            this.MST,
            this.Chuong,
            this.Khoan,
            this.TieuMuc});
            this.dgvCCTK_Search.Location = new System.Drawing.Point(43, 62);
            this.dgvCCTK_Search.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvCCTK_Search.Name = "dgvCCTK_Search";
            this.dgvCCTK_Search.RowTemplate.Height = 24;
            this.dgvCCTK_Search.Size = new System.Drawing.Size(827, 156);
            this.dgvCCTK_Search.TabIndex = 2;
            // 
            // STT
            // 
            this.STT.HeaderText = "STT";
            this.STT.Name = "STT";
            this.STT.Width = 50;
            // 
            // TenCongTy
            // 
            this.TenCongTy.HeaderText = "Tên Công Ty";
            this.TenCongTy.Name = "TenCongTy";
            this.TenCongTy.Width = 286;
            // 
            // MST
            // 
            this.MST.HeaderText = "Mã Số Thuế";
            this.MST.Name = "MST";
            this.MST.Width = 150;
            // 
            // Chuong
            // 
            this.Chuong.HeaderText = "Chương";
            this.Chuong.Name = "Chuong";
            // 
            // Khoan
            // 
            this.Khoan.HeaderText = "Khoản";
            this.Khoan.Name = "Khoan";
            // 
            // TieuMuc
            // 
            this.TieuMuc.HeaderText = "Tiểu Mục";
            this.TieuMuc.Name = "TieuMuc";
            // 
            // btnCCTK_SearchMST
            // 
            this.btnCCTK_SearchMST.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCCTK_SearchMST.Location = new System.Drawing.Point(553, 20);
            this.btnCCTK_SearchMST.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnCCTK_SearchMST.Name = "btnCCTK_SearchMST";
            this.btnCCTK_SearchMST.Size = new System.Drawing.Size(107, 23);
            this.btnCCTK_SearchMST.TabIndex = 1;
            this.btnCCTK_SearchMST.Text = "Tìm kiếm";
            this.btnCCTK_SearchMST.UseVisualStyleBackColor = true;
            this.btnCCTK_SearchMST.Click += new System.EventHandler(this.button22_Click);
            // 
            // tbCCTK_SearchMST
            // 
            this.tbCCTK_SearchMST.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCCTK_SearchMST.Location = new System.Drawing.Point(151, 20);
            this.tbCCTK_SearchMST.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbCCTK_SearchMST.Name = "tbCCTK_SearchMST";
            this.tbCCTK_SearchMST.Size = new System.Drawing.Size(367, 26);
            this.tbCCTK_SearchMST.TabIndex = 0;
            // 
            // tabPage16
            // 
            this.tabPage16.BackColor = System.Drawing.Color.Azure;
            this.tabPage16.Controls.Add(this.CCHD_DateTime);
            this.tabPage16.Controls.Add(this.groupBox12);
            this.tabPage16.Controls.Add(this.CCHD_FileNguonStatus);
            this.tabPage16.Controls.Add(this.btnCCHD);
            this.tabPage16.Controls.Add(this.button25);
            this.tabPage16.Controls.Add(this.button24);
            this.tabPage16.Controls.Add(this.button23);
            this.tabPage16.Controls.Add(this.CCHD_GridView);
            this.tabPage16.Controls.Add(this.button22);
            this.tabPage16.Controls.Add(this.tbCCHD_Search);
            this.tabPage16.Controls.Add(this.label22);
            this.tabPage16.Font = new System.Drawing.Font("Arial", 9.75F);
            this.tabPage16.Location = new System.Drawing.Point(4, 28);
            this.tabPage16.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage16.Name = "tabPage16";
            this.tabPage16.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage16.Size = new System.Drawing.Size(937, 681);
            this.tabPage16.TabIndex = 1;
            this.tabPage16.Text = "Cưỡng chế hóa đơn";
            // 
            // CCHD_DateTime
            // 
            this.CCHD_DateTime.Location = new System.Drawing.Point(698, 22);
            this.CCHD_DateTime.Name = "CCHD_DateTime";
            this.CCHD_DateTime.Size = new System.Drawing.Size(112, 26);
            this.CCHD_DateTime.TabIndex = 10;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.tbCCHD_tenChiCucTruong);
            this.groupBox12.Controls.Add(this.label43);
            this.groupBox12.Controls.Add(this.tbCCHD_tenPhoChiCucTruong);
            this.groupBox12.Controls.Add(this.label44);
            this.groupBox12.Controls.Add(this.tbCCHD_TenDoiTruong);
            this.groupBox12.Controls.Add(this.label37);
            this.groupBox12.Controls.Add(this.CCHD_TenCanBo);
            this.groupBox12.Controls.Add(this.CCHD_listTenDoi);
            this.groupBox12.Controls.Add(this.CCHD_listChiNhanh);
            this.groupBox12.Controls.Add(this.label25);
            this.groupBox12.Controls.Add(this.label24);
            this.groupBox12.Controls.Add(this.label23);
            this.groupBox12.Location = new System.Drawing.Point(43, 366);
            this.groupBox12.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox12.Size = new System.Drawing.Size(827, 298);
            this.groupBox12.TabIndex = 9;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Chi tiết đơn vị";
            // 
            // tbCCHD_tenChiCucTruong
            // 
            this.tbCCHD_tenChiCucTruong.Location = new System.Drawing.Point(193, 77);
            this.tbCCHD_tenChiCucTruong.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbCCHD_tenChiCucTruong.Name = "tbCCHD_tenChiCucTruong";
            this.tbCCHD_tenChiCucTruong.Size = new System.Drawing.Size(497, 26);
            this.tbCCHD_tenChiCucTruong.TabIndex = 18;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(34, 80);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(155, 19);
            this.label43.TabIndex = 17;
            this.label43.Text = "Tên chi cục trưởng:";
            // 
            // tbCCHD_tenPhoChiCucTruong
            // 
            this.tbCCHD_tenPhoChiCucTruong.Location = new System.Drawing.Point(193, 120);
            this.tbCCHD_tenPhoChiCucTruong.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbCCHD_tenPhoChiCucTruong.Name = "tbCCHD_tenPhoChiCucTruong";
            this.tbCCHD_tenPhoChiCucTruong.Size = new System.Drawing.Size(497, 26);
            this.tbCCHD_tenPhoChiCucTruong.TabIndex = 16;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(2, 122);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(187, 19);
            this.label44.TabIndex = 15;
            this.label44.Text = "Tên phó chi cục trưởng:";
            // 
            // tbCCHD_TenDoiTruong
            // 
            this.tbCCHD_TenDoiTruong.Location = new System.Drawing.Point(193, 166);
            this.tbCCHD_TenDoiTruong.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbCCHD_TenDoiTruong.Name = "tbCCHD_TenDoiTruong";
            this.tbCCHD_TenDoiTruong.Size = new System.Drawing.Size(497, 26);
            this.tbCCHD_TenDoiTruong.TabIndex = 14;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(65, 169);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(124, 19);
            this.label37.TabIndex = 13;
            this.label37.Text = "Tên đội trưởng:";
            // 
            // CCHD_TenCanBo
            // 
            this.CCHD_TenCanBo.Location = new System.Drawing.Point(193, 209);
            this.CCHD_TenCanBo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CCHD_TenCanBo.Name = "CCHD_TenCanBo";
            this.CCHD_TenCanBo.Size = new System.Drawing.Size(497, 26);
            this.CCHD_TenCanBo.TabIndex = 12;
            // 
            // CCHD_listTenDoi
            // 
            this.CCHD_listTenDoi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CCHD_listTenDoi.FormattingEnabled = true;
            this.CCHD_listTenDoi.Location = new System.Drawing.Point(193, 259);
            this.CCHD_listTenDoi.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CCHD_listTenDoi.Name = "CCHD_listTenDoi";
            this.CCHD_listTenDoi.Size = new System.Drawing.Size(497, 26);
            this.CCHD_listTenDoi.TabIndex = 11;
            // 
            // CCHD_listChiNhanh
            // 
            this.CCHD_listChiNhanh.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CCHD_listChiNhanh.FormattingEnabled = true;
            this.CCHD_listChiNhanh.Location = new System.Drawing.Point(193, 29);
            this.CCHD_listChiNhanh.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CCHD_listChiNhanh.Name = "CCHD_listChiNhanh";
            this.CCHD_listChiNhanh.Size = new System.Drawing.Size(497, 26);
            this.CCHD_listChiNhanh.TabIndex = 10;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(93, 212);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(96, 19);
            this.label25.TabIndex = 2;
            this.label25.Text = "Tên cán bộ:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(120, 262);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(69, 19);
            this.label24.TabIndex = 1;
            this.label24.Text = "Tên đội:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(118, 32);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(71, 19);
            this.label23.TabIndex = 0;
            this.label23.Text = "Chi cục:";
            // 
            // CCHD_FileNguonStatus
            // 
            this.CCHD_FileNguonStatus.Location = new System.Drawing.Point(43, 298);
            this.CCHD_FileNguonStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CCHD_FileNguonStatus.Name = "CCHD_FileNguonStatus";
            this.CCHD_FileNguonStatus.Size = new System.Drawing.Size(827, 64);
            this.CCHD_FileNguonStatus.TabIndex = 8;
            this.CCHD_FileNguonStatus.Text = "";
            // 
            // btnCCHD
            // 
            this.btnCCHD.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCCHD.Location = new System.Drawing.Point(251, 254);
            this.btnCCHD.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnCCHD.Name = "btnCCHD";
            this.btnCCHD.Size = new System.Drawing.Size(192, 23);
            this.btnCCHD.TabIndex = 7;
            this.btnCCHD.Text = "Tạo báo cáo";
            this.btnCCHD.UseVisualStyleBackColor = true;
            this.btnCCHD.Click += new System.EventHandler(this.btnCCHD_CreateReport_Click);
            // 
            // button25
            // 
            this.button25.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button25.Location = new System.Drawing.Point(465, 254);
            this.button25.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(192, 23);
            this.button25.TabIndex = 6;
            this.button25.Text = "Nhập file nguồn";
            this.button25.UseVisualStyleBackColor = true;
            this.button25.Click += new System.EventHandler(this.button25_Click);
            // 
            // button24
            // 
            this.button24.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button24.Location = new System.Drawing.Point(677, 254);
            this.button24.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(192, 23);
            this.button24.TabIndex = 5;
            this.button24.Text = "Chỉnh sửa mẫu biểu";
            this.button24.UseVisualStyleBackColor = true;
            this.button24.Click += new System.EventHandler(this.button24_Click);
            // 
            // button23
            // 
            this.button23.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button23.Location = new System.Drawing.Point(43, 254);
            this.button23.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(192, 23);
            this.button23.TabIndex = 4;
            this.button23.Text = "Kiểm tra";
            this.button23.UseVisualStyleBackColor = true;
            this.button23.Click += new System.EventHandler(this.button23_Click);
            // 
            // CCHD_GridView
            // 
            this.CCHD_GridView.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.CCHD_GridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.CCHD_GridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.STT1,
            this.TenCongTy1,
            this.MST1,
            this.Chuong1,
            this.Khoan1,
            this.TieuMuc1,
            this.Column1});
            this.CCHD_GridView.Location = new System.Drawing.Point(43, 62);
            this.CCHD_GridView.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CCHD_GridView.Name = "CCHD_GridView";
            this.CCHD_GridView.RowTemplate.Height = 24;
            this.CCHD_GridView.Size = new System.Drawing.Size(827, 156);
            this.CCHD_GridView.TabIndex = 3;
            // 
            // STT1
            // 
            this.STT1.HeaderText = "STT";
            this.STT1.Name = "STT1";
            this.STT1.Width = 50;
            // 
            // TenCongTy1
            // 
            this.TenCongTy1.HeaderText = "Tên Công Ty";
            this.TenCongTy1.Name = "TenCongTy1";
            this.TenCongTy1.Width = 286;
            // 
            // MST1
            // 
            this.MST1.HeaderText = "Mã Số Thuế";
            this.MST1.Name = "MST1";
            this.MST1.Width = 150;
            // 
            // Chuong1
            // 
            this.Chuong1.HeaderText = "Chương";
            this.Chuong1.Name = "Chuong1";
            // 
            // Khoan1
            // 
            this.Khoan1.HeaderText = "Khoản";
            this.Khoan1.Name = "Khoan1";
            // 
            // TieuMuc1
            // 
            this.TieuMuc1.HeaderText = "Tiểu Mục";
            this.TieuMuc1.Name = "TieuMuc1";
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Column1";
            this.Column1.Name = "Column1";
            // 
            // button22
            // 
            this.button22.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button22.Location = new System.Drawing.Point(560, 21);
            this.button22.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(107, 23);
            this.button22.TabIndex = 2;
            this.button22.Text = "Tìm kiếm";
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.button22_Click_1);
            // 
            // tbCCHD_Search
            // 
            this.tbCCHD_Search.Font = new System.Drawing.Font("Arial", 9.75F);
            this.tbCCHD_Search.Location = new System.Drawing.Point(149, 21);
            this.tbCCHD_Search.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbCCHD_Search.Name = "tbCCHD_Search";
            this.tbCCHD_Search.Size = new System.Drawing.Size(367, 26);
            this.tbCCHD_Search.TabIndex = 1;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label22.Location = new System.Drawing.Point(40, 23);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(94, 19);
            this.label22.TabIndex = 0;
            this.label22.Text = "Mã số thuế:";
            // 
            // tabPage17
            // 
            this.tabPage17.BackColor = System.Drawing.Color.Azure;
            this.tabPage17.Controls.Add(this.GTTK_DateTime);
            this.tabPage17.Controls.Add(this.btnCreateTB);
            this.tabPage17.Controls.Add(this.groupBox13);
            this.tabPage17.Controls.Add(this.GTTK_CheckResult_text);
            this.tabPage17.Controls.Add(this.button32);
            this.tabPage17.Controls.Add(this.button31);
            this.tabPage17.Controls.Add(this.button30);
            this.tabPage17.Controls.Add(this.button29);
            this.tabPage17.Controls.Add(this.GTTK_dsCongTy_GridView);
            this.tabPage17.Controls.Add(this.button28);
            this.tabPage17.Controls.Add(this.GTTK_MST_textBox);
            this.tabPage17.Controls.Add(this.label26);
            this.tabPage17.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage17.Location = new System.Drawing.Point(4, 28);
            this.tabPage17.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage17.Name = "tabPage17";
            this.tabPage17.Size = new System.Drawing.Size(937, 681);
            this.tabPage17.TabIndex = 0;
            this.tabPage17.Text = "Giải tỏa tài khoản";
            // 
            // GTTK_DateTime
            // 
            this.GTTK_DateTime.Location = new System.Drawing.Point(758, 18);
            this.GTTK_DateTime.Name = "GTTK_DateTime";
            this.GTTK_DateTime.Size = new System.Drawing.Size(112, 26);
            this.GTTK_DateTime.TabIndex = 11;
            // 
            // btnCreateTB
            // 
            this.btnCreateTB.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreateTB.Location = new System.Drawing.Point(612, 18);
            this.btnCreateTB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnCreateTB.Name = "btnCreateTB";
            this.btnCreateTB.Size = new System.Drawing.Size(135, 23);
            this.btnCreateTB.TabIndex = 10;
            this.btnCreateTB.Text = "Tạo thông báo";
            this.btnCreateTB.UseVisualStyleBackColor = true;
            this.btnCreateTB.Click += new System.EventHandler(this.btnCreateTB_Click);
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.tbGTTK_tenChiCucTruong);
            this.groupBox13.Controls.Add(this.label45);
            this.groupBox13.Controls.Add(this.tbGTTK_tenPhoChiCucTruong);
            this.groupBox13.Controls.Add(this.label46);
            this.groupBox13.Controls.Add(this.tbGTTK_TenDoiTruong);
            this.groupBox13.Controls.Add(this.label38);
            this.groupBox13.Controls.Add(this.GTTK_TenCanBo);
            this.groupBox13.Controls.Add(this.GTTK_listTenDoi);
            this.groupBox13.Controls.Add(this.GTTK_listChiNhanh);
            this.groupBox13.Controls.Add(this.label30);
            this.groupBox13.Controls.Add(this.label29);
            this.groupBox13.Controls.Add(this.label28);
            this.groupBox13.Location = new System.Drawing.Point(43, 366);
            this.groupBox13.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox13.Size = new System.Drawing.Size(827, 298);
            this.groupBox13.TabIndex = 9;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Chi tiết đơn vị";
            // 
            // tbGTTK_tenChiCucTruong
            // 
            this.tbGTTK_tenChiCucTruong.Location = new System.Drawing.Point(195, 78);
            this.tbGTTK_tenChiCucTruong.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbGTTK_tenChiCucTruong.Name = "tbGTTK_tenChiCucTruong";
            this.tbGTTK_tenChiCucTruong.Size = new System.Drawing.Size(497, 26);
            this.tbGTTK_tenChiCucTruong.TabIndex = 11;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(36, 81);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(155, 19);
            this.label45.TabIndex = 10;
            this.label45.Text = "Tên chi cục trưởng:";
            // 
            // tbGTTK_tenPhoChiCucTruong
            // 
            this.tbGTTK_tenPhoChiCucTruong.Location = new System.Drawing.Point(194, 123);
            this.tbGTTK_tenPhoChiCucTruong.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbGTTK_tenPhoChiCucTruong.Name = "tbGTTK_tenPhoChiCucTruong";
            this.tbGTTK_tenPhoChiCucTruong.Size = new System.Drawing.Size(497, 26);
            this.tbGTTK_tenPhoChiCucTruong.TabIndex = 9;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(3, 126);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(187, 19);
            this.label46.TabIndex = 8;
            this.label46.Text = "Tên phó chi cục trưởng:";
            // 
            // tbGTTK_TenDoiTruong
            // 
            this.tbGTTK_TenDoiTruong.Location = new System.Drawing.Point(194, 164);
            this.tbGTTK_TenDoiTruong.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbGTTK_TenDoiTruong.Name = "tbGTTK_TenDoiTruong";
            this.tbGTTK_TenDoiTruong.Size = new System.Drawing.Size(497, 26);
            this.tbGTTK_TenDoiTruong.TabIndex = 7;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(66, 167);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(124, 19);
            this.label38.TabIndex = 6;
            this.label38.Text = "Tên đội trưởng:";
            // 
            // GTTK_TenCanBo
            // 
            this.GTTK_TenCanBo.Location = new System.Drawing.Point(193, 209);
            this.GTTK_TenCanBo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.GTTK_TenCanBo.Name = "GTTK_TenCanBo";
            this.GTTK_TenCanBo.Size = new System.Drawing.Size(497, 26);
            this.GTTK_TenCanBo.TabIndex = 5;
            // 
            // GTTK_listTenDoi
            // 
            this.GTTK_listTenDoi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.GTTK_listTenDoi.FormattingEnabled = true;
            this.GTTK_listTenDoi.Location = new System.Drawing.Point(193, 256);
            this.GTTK_listTenDoi.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.GTTK_listTenDoi.Name = "GTTK_listTenDoi";
            this.GTTK_listTenDoi.Size = new System.Drawing.Size(497, 26);
            this.GTTK_listTenDoi.TabIndex = 4;
            // 
            // GTTK_listChiNhanh
            // 
            this.GTTK_listChiNhanh.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.GTTK_listChiNhanh.FormattingEnabled = true;
            this.GTTK_listChiNhanh.Location = new System.Drawing.Point(193, 34);
            this.GTTK_listChiNhanh.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.GTTK_listChiNhanh.Name = "GTTK_listChiNhanh";
            this.GTTK_listChiNhanh.Size = new System.Drawing.Size(497, 26);
            this.GTTK_listChiNhanh.TabIndex = 3;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(120, 259);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(69, 19);
            this.label30.TabIndex = 2;
            this.label30.Text = "Tên đội:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(93, 212);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(96, 19);
            this.label29.TabIndex = 1;
            this.label29.Text = "Tên cán bộ:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(118, 38);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(71, 19);
            this.label28.TabIndex = 0;
            this.label28.Text = "Chi cục:";
            // 
            // GTTK_CheckResult_text
            // 
            this.GTTK_CheckResult_text.Location = new System.Drawing.Point(43, 298);
            this.GTTK_CheckResult_text.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.GTTK_CheckResult_text.Name = "GTTK_CheckResult_text";
            this.GTTK_CheckResult_text.Size = new System.Drawing.Size(827, 64);
            this.GTTK_CheckResult_text.TabIndex = 8;
            this.GTTK_CheckResult_text.Text = "";
            // 
            // button32
            // 
            this.button32.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button32.Location = new System.Drawing.Point(251, 254);
            this.button32.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(192, 23);
            this.button32.TabIndex = 7;
            this.button32.Text = "Tạo báo cáo";
            this.button32.UseVisualStyleBackColor = true;
            this.button32.Click += new System.EventHandler(this.button32_Click);
            // 
            // button31
            // 
            this.button31.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button31.Location = new System.Drawing.Point(465, 254);
            this.button31.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(192, 23);
            this.button31.TabIndex = 6;
            this.button31.Text = "Nhập file nguồn";
            this.button31.UseVisualStyleBackColor = true;
            // 
            // button30
            // 
            this.button30.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button30.Location = new System.Drawing.Point(677, 254);
            this.button30.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(192, 23);
            this.button30.TabIndex = 5;
            this.button30.Text = "Chỉnh sửa mẫu biểu";
            this.button30.UseVisualStyleBackColor = true;
            this.button30.Click += new System.EventHandler(this.button30_Click);
            // 
            // button29
            // 
            this.button29.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button29.Location = new System.Drawing.Point(43, 254);
            this.button29.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(192, 23);
            this.button29.TabIndex = 4;
            this.button29.Text = "Kiểm tra";
            this.button29.UseVisualStyleBackColor = true;
            // 
            // GTTK_dsCongTy_GridView
            // 
            this.GTTK_dsCongTy_GridView.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.GTTK_dsCongTy_GridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GTTK_dsCongTy_GridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.STT2,
            this.TenCongTy2,
            this.MST2,
            this.Chuong2,
            this.Khoan2,
            this.TieuMuc2});
            this.GTTK_dsCongTy_GridView.Location = new System.Drawing.Point(43, 62);
            this.GTTK_dsCongTy_GridView.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.GTTK_dsCongTy_GridView.Name = "GTTK_dsCongTy_GridView";
            this.GTTK_dsCongTy_GridView.RowTemplate.Height = 24;
            this.GTTK_dsCongTy_GridView.Size = new System.Drawing.Size(827, 156);
            this.GTTK_dsCongTy_GridView.TabIndex = 3;
            // 
            // STT2
            // 
            this.STT2.HeaderText = "STT";
            this.STT2.Name = "STT2";
            this.STT2.Width = 50;
            // 
            // TenCongTy2
            // 
            this.TenCongTy2.HeaderText = "Tên Công Ty";
            this.TenCongTy2.Name = "TenCongTy2";
            this.TenCongTy2.Width = 286;
            // 
            // MST2
            // 
            this.MST2.HeaderText = "Mã Số Thuế";
            this.MST2.Name = "MST2";
            this.MST2.Width = 150;
            // 
            // Chuong2
            // 
            this.Chuong2.HeaderText = "Chương";
            this.Chuong2.Name = "Chuong2";
            // 
            // Khoan2
            // 
            this.Khoan2.HeaderText = "Khoản";
            this.Khoan2.Name = "Khoan2";
            // 
            // TieuMuc2
            // 
            this.TieuMuc2.HeaderText = "Tiểu Mục";
            this.TieuMuc2.Name = "TieuMuc2";
            // 
            // button28
            // 
            this.button28.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button28.Location = new System.Drawing.Point(490, 18);
            this.button28.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(107, 23);
            this.button28.TabIndex = 2;
            this.button28.Text = "Tìm kiếm";
            this.button28.UseVisualStyleBackColor = true;
            this.button28.Click += new System.EventHandler(this.button28_Click);
            // 
            // GTTK_MST_textBox
            // 
            this.GTTK_MST_textBox.Location = new System.Drawing.Point(149, 18);
            this.GTTK_MST_textBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.GTTK_MST_textBox.Name = "GTTK_MST_textBox";
            this.GTTK_MST_textBox.Size = new System.Drawing.Size(315, 26);
            this.GTTK_MST_textBox.TabIndex = 1;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(40, 20);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(94, 19);
            this.label26.TabIndex = 0;
            this.label26.Text = "Mã số thuế:";
            // 
            // tabPage18
            // 
            this.tabPage18.BackColor = System.Drawing.Color.Azure;
            this.tabPage18.Controls.Add(this.GTHD_DateTime);
            this.tabPage18.Controls.Add(this.tinhTrangFileNguon_GTHD_txt);
            this.tabPage18.Controls.Add(this.groupBox14);
            this.tabPage18.Controls.Add(this.CreateReport_CCHD_btn);
            this.tabPage18.Controls.Add(this.button37);
            this.tabPage18.Controls.Add(this.button36);
            this.tabPage18.Controls.Add(this.FileNguonStatusCheck_GTHD_btn);
            this.tabPage18.Controls.Add(this.GTHD_GridView);
            this.tabPage18.Controls.Add(this.button33);
            this.tabPage18.Controls.Add(this.MST_GTHD_txtBox);
            this.tabPage18.Controls.Add(this.label27);
            this.tabPage18.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage18.Location = new System.Drawing.Point(4, 28);
            this.tabPage18.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage18.Name = "tabPage18";
            this.tabPage18.Size = new System.Drawing.Size(937, 681);
            this.tabPage18.TabIndex = 0;
            this.tabPage18.Text = "Giải tỏa hóa đơn";
            // 
            // GTHD_DateTime
            // 
            this.GTHD_DateTime.Location = new System.Drawing.Point(698, 22);
            this.GTHD_DateTime.Name = "GTHD_DateTime";
            this.GTHD_DateTime.Size = new System.Drawing.Size(112, 26);
            this.GTHD_DateTime.TabIndex = 10;
            // 
            // tinhTrangFileNguon_GTHD_txt
            // 
            this.tinhTrangFileNguon_GTHD_txt.Location = new System.Drawing.Point(43, 298);
            this.tinhTrangFileNguon_GTHD_txt.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tinhTrangFileNguon_GTHD_txt.Name = "tinhTrangFileNguon_GTHD_txt";
            this.tinhTrangFileNguon_GTHD_txt.Size = new System.Drawing.Size(827, 64);
            this.tinhTrangFileNguon_GTHD_txt.TabIndex = 9;
            this.tinhTrangFileNguon_GTHD_txt.Text = "";
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.tbGTHD_tenChiCucTruong);
            this.groupBox14.Controls.Add(this.label47);
            this.groupBox14.Controls.Add(this.tbGTHD_tenPhoChiCucTruong);
            this.groupBox14.Controls.Add(this.label48);
            this.groupBox14.Controls.Add(this.tbGTHD_TenDoiTruong);
            this.groupBox14.Controls.Add(this.label39);
            this.groupBox14.Controls.Add(this.GTHD_TenCanBo);
            this.groupBox14.Controls.Add(this.GTHD_listTenDoi);
            this.groupBox14.Controls.Add(this.GTHD_listChiNhanh);
            this.groupBox14.Controls.Add(this.label33);
            this.groupBox14.Controls.Add(this.label32);
            this.groupBox14.Controls.Add(this.label31);
            this.groupBox14.Location = new System.Drawing.Point(43, 366);
            this.groupBox14.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox14.Size = new System.Drawing.Size(827, 298);
            this.groupBox14.TabIndex = 8;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Chi tiết đơn vị";
            // 
            // tbGTHD_tenChiCucTruong
            // 
            this.tbGTHD_tenChiCucTruong.Location = new System.Drawing.Point(193, 79);
            this.tbGTHD_tenChiCucTruong.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbGTHD_tenChiCucTruong.Name = "tbGTHD_tenChiCucTruong";
            this.tbGTHD_tenChiCucTruong.Size = new System.Drawing.Size(497, 26);
            this.tbGTHD_tenChiCucTruong.TabIndex = 11;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(38, 82);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(151, 19);
            this.label47.TabIndex = 10;
            this.label47.Text = "Tên chi đội trưởng:";
            // 
            // tbGTHD_tenPhoChiCucTruong
            // 
            this.tbGTHD_tenPhoChiCucTruong.Location = new System.Drawing.Point(193, 123);
            this.tbGTHD_tenPhoChiCucTruong.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbGTHD_tenPhoChiCucTruong.Name = "tbGTHD_tenPhoChiCucTruong";
            this.tbGTHD_tenPhoChiCucTruong.Size = new System.Drawing.Size(497, 26);
            this.tbGTHD_tenPhoChiCucTruong.TabIndex = 9;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(6, 126);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(183, 19);
            this.label48.TabIndex = 8;
            this.label48.Text = "Tên phó chi đội trưởng:";
            // 
            // tbGTHD_TenDoiTruong
            // 
            this.tbGTHD_TenDoiTruong.Location = new System.Drawing.Point(193, 166);
            this.tbGTHD_TenDoiTruong.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbGTHD_TenDoiTruong.Name = "tbGTHD_TenDoiTruong";
            this.tbGTHD_TenDoiTruong.Size = new System.Drawing.Size(497, 26);
            this.tbGTHD_TenDoiTruong.TabIndex = 7;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(60, 169);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(129, 19);
            this.label39.TabIndex = 6;
            this.label39.Text = "Tên đội trưởng: ";
            // 
            // GTHD_TenCanBo
            // 
            this.GTHD_TenCanBo.Location = new System.Drawing.Point(193, 210);
            this.GTHD_TenCanBo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.GTHD_TenCanBo.Name = "GTHD_TenCanBo";
            this.GTHD_TenCanBo.Size = new System.Drawing.Size(497, 26);
            this.GTHD_TenCanBo.TabIndex = 5;
            // 
            // GTHD_listTenDoi
            // 
            this.GTHD_listTenDoi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.GTHD_listTenDoi.FormattingEnabled = true;
            this.GTHD_listTenDoi.Location = new System.Drawing.Point(193, 256);
            this.GTHD_listTenDoi.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.GTHD_listTenDoi.Name = "GTHD_listTenDoi";
            this.GTHD_listTenDoi.Size = new System.Drawing.Size(497, 26);
            this.GTHD_listTenDoi.TabIndex = 4;
            // 
            // GTHD_listChiNhanh
            // 
            this.GTHD_listChiNhanh.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.GTHD_listChiNhanh.FormattingEnabled = true;
            this.GTHD_listChiNhanh.Location = new System.Drawing.Point(193, 34);
            this.GTHD_listChiNhanh.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.GTHD_listChiNhanh.Name = "GTHD_listChiNhanh";
            this.GTHD_listChiNhanh.Size = new System.Drawing.Size(497, 26);
            this.GTHD_listChiNhanh.TabIndex = 3;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(93, 213);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(96, 19);
            this.label33.TabIndex = 2;
            this.label33.Text = "Tên cán bộ:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(120, 258);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(69, 19);
            this.label32.TabIndex = 1;
            this.label32.Text = "Tên đội:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(118, 37);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(71, 19);
            this.label31.TabIndex = 0;
            this.label31.Text = "Chi cục:";
            // 
            // CreateReport_CCHD_btn
            // 
            this.CreateReport_CCHD_btn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreateReport_CCHD_btn.Location = new System.Drawing.Point(251, 254);
            this.CreateReport_CCHD_btn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CreateReport_CCHD_btn.Name = "CreateReport_CCHD_btn";
            this.CreateReport_CCHD_btn.Size = new System.Drawing.Size(192, 23);
            this.CreateReport_CCHD_btn.TabIndex = 7;
            this.CreateReport_CCHD_btn.Text = "Tạo báo cáo";
            this.CreateReport_CCHD_btn.UseVisualStyleBackColor = true;
            this.CreateReport_CCHD_btn.Click += new System.EventHandler(this.CreateReport_CCHD_btn_Click);
            // 
            // button37
            // 
            this.button37.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button37.Location = new System.Drawing.Point(465, 254);
            this.button37.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(192, 23);
            this.button37.TabIndex = 6;
            this.button37.Text = "Nhập file nguồn";
            this.button37.UseVisualStyleBackColor = true;
            this.button37.Click += new System.EventHandler(this.button37_Click);
            // 
            // button36
            // 
            this.button36.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button36.Location = new System.Drawing.Point(677, 254);
            this.button36.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(192, 23);
            this.button36.TabIndex = 5;
            this.button36.Text = "Chỉnh sửa mẫu biểu";
            this.button36.UseVisualStyleBackColor = true;
            this.button36.Click += new System.EventHandler(this.button36_Click);
            // 
            // FileNguonStatusCheck_GTHD_btn
            // 
            this.FileNguonStatusCheck_GTHD_btn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FileNguonStatusCheck_GTHD_btn.Location = new System.Drawing.Point(43, 254);
            this.FileNguonStatusCheck_GTHD_btn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.FileNguonStatusCheck_GTHD_btn.Name = "FileNguonStatusCheck_GTHD_btn";
            this.FileNguonStatusCheck_GTHD_btn.Size = new System.Drawing.Size(192, 23);
            this.FileNguonStatusCheck_GTHD_btn.TabIndex = 4;
            this.FileNguonStatusCheck_GTHD_btn.Text = "Kiểm tra";
            this.FileNguonStatusCheck_GTHD_btn.UseVisualStyleBackColor = true;
            this.FileNguonStatusCheck_GTHD_btn.Click += new System.EventHandler(this.FileNguonStatusCheck_GTHD_btn_Click);
            // 
            // GTHD_GridView
            // 
            this.GTHD_GridView.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.GTHD_GridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GTHD_GridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.STT3,
            this.TenCongTy3,
            this.MST3,
            this.Chuong3,
            this.Khoan3,
            this.TieuMuc3});
            this.GTHD_GridView.Location = new System.Drawing.Point(43, 62);
            this.GTHD_GridView.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.GTHD_GridView.Name = "GTHD_GridView";
            this.GTHD_GridView.RowTemplate.Height = 24;
            this.GTHD_GridView.Size = new System.Drawing.Size(827, 156);
            this.GTHD_GridView.TabIndex = 3;
            // 
            // STT3
            // 
            this.STT3.HeaderText = "STT";
            this.STT3.Name = "STT3";
            this.STT3.Width = 50;
            // 
            // TenCongTy3
            // 
            this.TenCongTy3.HeaderText = "Tên Công Ty";
            this.TenCongTy3.Name = "TenCongTy3";
            this.TenCongTy3.Width = 286;
            // 
            // MST3
            // 
            this.MST3.HeaderText = "Mã Số Thuế";
            this.MST3.Name = "MST3";
            this.MST3.Width = 150;
            // 
            // Chuong3
            // 
            this.Chuong3.HeaderText = "Chương";
            this.Chuong3.Name = "Chuong3";
            // 
            // Khoan3
            // 
            this.Khoan3.HeaderText = "Khoản";
            this.Khoan3.Name = "Khoan3";
            // 
            // TieuMuc3
            // 
            this.TieuMuc3.HeaderText = "Tiểu Mục";
            this.TieuMuc3.Name = "TieuMuc3";
            // 
            // button33
            // 
            this.button33.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button33.Location = new System.Drawing.Point(560, 20);
            this.button33.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(107, 23);
            this.button33.TabIndex = 2;
            this.button33.Text = "Tìm kiếm";
            this.button33.UseVisualStyleBackColor = true;
            this.button33.Click += new System.EventHandler(this.button33_Click);
            // 
            // MST_GTHD_txtBox
            // 
            this.MST_GTHD_txtBox.Location = new System.Drawing.Point(149, 20);
            this.MST_GTHD_txtBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MST_GTHD_txtBox.Name = "MST_GTHD_txtBox";
            this.MST_GTHD_txtBox.Size = new System.Drawing.Size(367, 26);
            this.MST_GTHD_txtBox.TabIndex = 1;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(40, 22);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(94, 19);
            this.label27.TabIndex = 0;
            this.label27.Text = "Mã số thuế:";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.Azure;
            this.tabPage3.Controls.Add(this.groupBox17);
            this.tabPage3.Controls.Add(this.groupBox16);
            this.tabPage3.Controls.Add(this.groupBox18);
            this.tabPage3.Controls.Add(this.groupBox11);
            this.tabPage3.Controls.Add(this.groupBox9);
            this.tabPage3.Controls.Add(this.groupBox8);
            this.tabPage3.Font = new System.Drawing.Font("Arial", 9.75F);
            this.tabPage3.Location = new System.Drawing.Point(4, 28);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage3.Size = new System.Drawing.Size(951, 717);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Chi tiết đơn vị";
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.button27);
            this.groupBox17.Controls.Add(this.tb_tenChiCucTruong);
            this.groupBox17.Controls.Add(this.label49);
            this.groupBox17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox17.Location = new System.Drawing.Point(19, 145);
            this.groupBox17.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox17.Size = new System.Drawing.Size(869, 94);
            this.groupBox17.TabIndex = 7;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Tên chi cục trưởng";
            // 
            // button27
            // 
            this.button27.Location = new System.Drawing.Point(736, 48);
            this.button27.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(107, 28);
            this.button27.TabIndex = 4;
            this.button27.Text = "Lưu";
            this.button27.UseVisualStyleBackColor = true;
            this.button27.Click += new System.EventHandler(this.button27_Click);
            // 
            // tb_tenChiCucTruong
            // 
            this.tb_tenChiCucTruong.Location = new System.Drawing.Point(152, 51);
            this.tb_tenChiCucTruong.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_tenChiCucTruong.Name = "tb_tenChiCucTruong";
            this.tb_tenChiCucTruong.Size = new System.Drawing.Size(565, 26);
            this.tb_tenChiCucTruong.TabIndex = 2;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(30, 54);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(123, 20);
            this.label49.TabIndex = 1;
            this.label49.Text = "Chi cục trưởng:";
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.addTenDoiTruong_btn);
            this.groupBox16.Controls.Add(this.tenDoiTruong_txt);
            this.groupBox16.Controls.Add(this.label40);
            this.groupBox16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox16.Location = new System.Drawing.Point(19, 370);
            this.groupBox16.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox16.Size = new System.Drawing.Size(869, 94);
            this.groupBox16.TabIndex = 5;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Tên đội trưởng";
            // 
            // addTenDoiTruong_btn
            // 
            this.addTenDoiTruong_btn.Location = new System.Drawing.Point(736, 48);
            this.addTenDoiTruong_btn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addTenDoiTruong_btn.Name = "addTenDoiTruong_btn";
            this.addTenDoiTruong_btn.Size = new System.Drawing.Size(107, 28);
            this.addTenDoiTruong_btn.TabIndex = 4;
            this.addTenDoiTruong_btn.Text = "Lưu";
            this.addTenDoiTruong_btn.UseVisualStyleBackColor = true;
            this.addTenDoiTruong_btn.Click += new System.EventHandler(this.addTenDoiTruong_btn_Click);
            // 
            // tenDoiTruong_txt
            // 
            this.tenDoiTruong_txt.Location = new System.Drawing.Point(152, 51);
            this.tenDoiTruong_txt.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tenDoiTruong_txt.Name = "tenDoiTruong_txt";
            this.tenDoiTruong_txt.Size = new System.Drawing.Size(565, 26);
            this.tenDoiTruong_txt.TabIndex = 2;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(32, 53);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(121, 20);
            this.label40.TabIndex = 1;
            this.label40.Text = "Tên đội trưởng:";
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.button34);
            this.groupBox18.Controls.Add(this.tb_tenPhoChiCucTruong);
            this.groupBox18.Controls.Add(this.label50);
            this.groupBox18.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox18.Location = new System.Drawing.Point(19, 259);
            this.groupBox18.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox18.Size = new System.Drawing.Size(869, 94);
            this.groupBox18.TabIndex = 6;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Tên phó chi cục trưởng";
            // 
            // button34
            // 
            this.button34.Location = new System.Drawing.Point(736, 48);
            this.button34.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(107, 28);
            this.button34.TabIndex = 4;
            this.button34.Text = "Lưu";
            this.button34.UseVisualStyleBackColor = true;
            this.button34.Click += new System.EventHandler(this.button34_Click);
            // 
            // tb_tenPhoChiCucTruong
            // 
            this.tb_tenPhoChiCucTruong.Location = new System.Drawing.Point(152, 51);
            this.tb_tenPhoChiCucTruong.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_tenPhoChiCucTruong.Name = "tb_tenPhoChiCucTruong";
            this.tb_tenPhoChiCucTruong.Size = new System.Drawing.Size(565, 26);
            this.tb_tenPhoChiCucTruong.TabIndex = 2;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(2, 54);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(154, 20);
            this.label50.TabIndex = 1;
            this.label50.Text = "Phó chi cục trưởng:";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.listTenDoi_comboBox);
            this.groupBox11.Controls.Add(this.saveTenDoi_btn);
            this.groupBox11.Controls.Add(this.label21);
            this.groupBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox11.Location = new System.Drawing.Point(19, 597);
            this.groupBox11.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox11.Size = new System.Drawing.Size(869, 94);
            this.groupBox11.TabIndex = 8;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Tên đội";
            // 
            // listTenDoi_comboBox
            // 
            this.listTenDoi_comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.listTenDoi_comboBox.Location = new System.Drawing.Point(152, 49);
            this.listTenDoi_comboBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.listTenDoi_comboBox.Name = "listTenDoi_comboBox";
            this.listTenDoi_comboBox.Size = new System.Drawing.Size(565, 28);
            this.listTenDoi_comboBox.TabIndex = 7;
            // 
            // saveTenDoi_btn
            // 
            this.saveTenDoi_btn.Location = new System.Drawing.Point(736, 46);
            this.saveTenDoi_btn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.saveTenDoi_btn.Name = "saveTenDoi_btn";
            this.saveTenDoi_btn.Size = new System.Drawing.Size(107, 28);
            this.saveTenDoi_btn.TabIndex = 4;
            this.saveTenDoi_btn.Text = "Lưu";
            this.saveTenDoi_btn.UseVisualStyleBackColor = true;
            this.saveTenDoi_btn.Click += new System.EventHandler(this.saveTenDoi_btn_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(84, 53);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(74, 20);
            this.label21.TabIndex = 1;
            this.label21.Text = "Tên đội: ";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.listChiCucThue_comboBox);
            this.groupBox9.Controls.Add(this.addChiCucThue_btn);
            this.groupBox9.Controls.Add(this.label17);
            this.groupBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox9.Location = new System.Drawing.Point(19, 30);
            this.groupBox9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox9.Size = new System.Drawing.Size(869, 94);
            this.groupBox9.TabIndex = 6;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Chi cục thuế";
            // 
            // listChiCucThue_comboBox
            // 
            this.listChiCucThue_comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.listChiCucThue_comboBox.Location = new System.Drawing.Point(152, 49);
            this.listChiCucThue_comboBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.listChiCucThue_comboBox.Name = "listChiCucThue_comboBox";
            this.listChiCucThue_comboBox.Size = new System.Drawing.Size(565, 28);
            this.listChiCucThue_comboBox.TabIndex = 7;
            // 
            // addChiCucThue_btn
            // 
            this.addChiCucThue_btn.Location = new System.Drawing.Point(736, 46);
            this.addChiCucThue_btn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addChiCucThue_btn.Name = "addChiCucThue_btn";
            this.addChiCucThue_btn.Size = new System.Drawing.Size(107, 28);
            this.addChiCucThue_btn.TabIndex = 4;
            this.addChiCucThue_btn.Text = "Lưu";
            this.addChiCucThue_btn.UseVisualStyleBackColor = true;
            this.addChiCucThue_btn.Click += new System.EventHandler(this.addChiCucThue_btn_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(43, 52);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(113, 20);
            this.label17.TabIndex = 1;
            this.label17.Text = "Chi cục thuế: ";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.saveTenCanBo_btn);
            this.groupBox8.Controls.Add(this.tenCanBo_txt);
            this.groupBox8.Controls.Add(this.label15);
            this.groupBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox8.Location = new System.Drawing.Point(19, 485);
            this.groupBox8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox8.Size = new System.Drawing.Size(869, 94);
            this.groupBox8.TabIndex = 1;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Cán bộ";
            // 
            // saveTenCanBo_btn
            // 
            this.saveTenCanBo_btn.Location = new System.Drawing.Point(736, 48);
            this.saveTenCanBo_btn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.saveTenCanBo_btn.Name = "saveTenCanBo_btn";
            this.saveTenCanBo_btn.Size = new System.Drawing.Size(107, 28);
            this.saveTenCanBo_btn.TabIndex = 4;
            this.saveTenCanBo_btn.Text = "Lưu";
            this.saveTenCanBo_btn.UseVisualStyleBackColor = true;
            this.saveTenCanBo_btn.Click += new System.EventHandler(this.saveTenCanBo_btn_Click);
            // 
            // tenCanBo_txt
            // 
            this.tenCanBo_txt.Location = new System.Drawing.Point(152, 51);
            this.tenCanBo_txt.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tenCanBo_txt.Name = "tenCanBo_txt";
            this.tenCanBo_txt.Size = new System.Drawing.Size(565, 26);
            this.tenCanBo_txt.TabIndex = 2;
            this.tenCanBo_txt.TextChanged += new System.EventHandler(this.tenCanBo_txt_TextChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(55, 54);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(102, 20);
            this.label15.TabIndex = 1;
            this.label15.Text = "Tên cán bộ: ";
            this.label15.Click += new System.EventHandler(this.label15_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Azure;
            this.tabPage1.Controls.Add(this.flowLayoutPanel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 28);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Size = new System.Drawing.Size(951, 717);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "File nguồn";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(7)))));
            this.flowLayoutPanel1.Controls.Add(this.groupBox1);
            this.flowLayoutPanel1.Controls.Add(this.groupBox2);
            this.flowLayoutPanel1.Controls.Add(this.groupBox3);
            this.flowLayoutPanel1.Controls.Add(this.groupBox4);
            this.flowLayoutPanel1.Controls.Add(this.groupBox5);
            this.flowLayoutPanel1.Controls.Add(this.groupBox6);
            this.flowLayoutPanel1.Controls.Add(this.groupBox7);
            this.flowLayoutPanel1.Controls.Add(this.groupBox15);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(945, 713);
            this.flowLayoutPanel1.TabIndex = 9;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.S12FileNameTextBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(3, 2);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(869, 94);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "1. File S12";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(768, 48);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 28);
            this.button3.TabIndex = 5;
            this.button3.Text = "Xóa";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(663, 48);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 28);
            this.button2.TabIndex = 4;
            this.button2.Text = "Nhập";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(557, 48);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 28);
            this.button1.TabIndex = 3;
            this.button1.Text = "Chọn";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // S12FileNameTextBox
            // 
            this.S12FileNameTextBox.Location = new System.Drawing.Point(152, 51);
            this.S12FileNameTextBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.S12FileNameTextBox.Name = "S12FileNameTextBox";
            this.S12FileNameTextBox.Size = new System.Drawing.Size(359, 26);
            this.S12FileNameTextBox.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Đường dẫn:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "File hiện tại: ";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button4);
            this.groupBox2.Controls.Add(this.button5);
            this.groupBox2.Controls.Add(this.button6);
            this.groupBox2.Controls.Add(this.textBox2);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(3, 100);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(869, 94);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "2. File danh sách điều chỉnh";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(768, 48);
            this.button4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 28);
            this.button4.TabIndex = 5;
            this.button4.Text = "Xóa";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(663, 48);
            this.button5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 28);
            this.button5.TabIndex = 4;
            this.button5.Text = "Nhập";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(557, 48);
            this.button6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 28);
            this.button6.TabIndex = 3;
            this.button6.Text = "Chọn";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(152, 51);
            this.textBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(359, 26);
            this.textBox2.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(31, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 20);
            this.label3.TabIndex = 1;
            this.label3.Text = "Đường dẫn:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(31, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(105, 20);
            this.label4.TabIndex = 0;
            this.label4.Text = "File hiện tại: ";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button7);
            this.groupBox3.Controls.Add(this.button8);
            this.groupBox3.Controls.Add(this.button9);
            this.groupBox3.Controls.Add(this.textBox3);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(3, 198);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Size = new System.Drawing.Size(869, 94);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "3. File CCTK-HĐ TMS";
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(768, 48);
            this.button7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 28);
            this.button7.TabIndex = 5;
            this.button7.Text = "Xóa";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(663, 48);
            this.button8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 28);
            this.button8.TabIndex = 4;
            this.button8.Text = "Nhập";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(557, 48);
            this.button9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 28);
            this.button9.TabIndex = 3;
            this.button9.Text = "Chọn";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(152, 51);
            this.textBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(359, 26);
            this.textBox3.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(31, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 20);
            this.label5.TabIndex = 1;
            this.label5.Text = "Đường dẫn:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(31, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 20);
            this.label6.TabIndex = 0;
            this.label6.Text = "File hiện tại: ";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button10);
            this.groupBox4.Controls.Add(this.button11);
            this.groupBox4.Controls.Add(this.button12);
            this.groupBox4.Controls.Add(this.textBox4);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(3, 296);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Size = new System.Drawing.Size(869, 94);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "4. File CCTK-HĐ";
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(768, 48);
            this.button10.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(75, 28);
            this.button10.TabIndex = 5;
            this.button10.Text = "Xóa";
            this.button10.UseVisualStyleBackColor = true;
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(663, 48);
            this.button11.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(75, 28);
            this.button11.TabIndex = 4;
            this.button11.Text = "Nhập";
            this.button11.UseVisualStyleBackColor = true;
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(557, 48);
            this.button12.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(75, 28);
            this.button12.TabIndex = 3;
            this.button12.Text = "Chọn";
            this.button12.UseVisualStyleBackColor = true;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(152, 51);
            this.textBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(359, 26);
            this.textBox4.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(31, 54);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 20);
            this.label7.TabIndex = 1;
            this.label7.Text = "Đường dẫn:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(31, 27);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(105, 20);
            this.label8.TabIndex = 0;
            this.label8.Text = "File hiện tại: ";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.button13);
            this.groupBox5.Controls.Add(this.button14);
            this.groupBox5.Controls.Add(this.button15);
            this.groupBox5.Controls.Add(this.tbLicenseFile);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(3, 394);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox5.Size = new System.Drawing.Size(869, 94);
            this.groupBox5.TabIndex = 6;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "5. File chứng từ";
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(768, 48);
            this.button13.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(75, 28);
            this.button13.TabIndex = 5;
            this.button13.Text = "Xóa";
            this.button13.UseVisualStyleBackColor = true;
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(663, 48);
            this.button14.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(75, 28);
            this.button14.TabIndex = 4;
            this.button14.Text = "Nhập";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(557, 48);
            this.button15.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(75, 28);
            this.button15.TabIndex = 3;
            this.button15.Text = "Chọn";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // tbLicenseFile
            // 
            this.tbLicenseFile.Location = new System.Drawing.Point(152, 53);
            this.tbLicenseFile.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbLicenseFile.Name = "tbLicenseFile";
            this.tbLicenseFile.Size = new System.Drawing.Size(359, 26);
            this.tbLicenseFile.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(31, 54);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(94, 20);
            this.label9.TabIndex = 1;
            this.label9.Text = "Đường dẫn:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(31, 27);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(105, 20);
            this.label10.TabIndex = 0;
            this.label10.Text = "File hiện tại: ";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.button16);
            this.groupBox6.Controls.Add(this.button17);
            this.groupBox6.Controls.Add(this.button18);
            this.groupBox6.Controls.Add(this.textBox6);
            this.groupBox6.Controls.Add(this.label11);
            this.groupBox6.Controls.Add(this.label12);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(3, 492);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox6.Size = new System.Drawing.Size(869, 94);
            this.groupBox6.TabIndex = 7;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "6. File danh sách giải tỏa";
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(768, 48);
            this.button16.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(75, 28);
            this.button16.TabIndex = 5;
            this.button16.Text = "Xóa";
            this.button16.UseVisualStyleBackColor = true;
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(663, 48);
            this.button17.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(75, 28);
            this.button17.TabIndex = 4;
            this.button17.Text = "Nhập";
            this.button17.UseVisualStyleBackColor = true;
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(557, 48);
            this.button18.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(75, 28);
            this.button18.TabIndex = 3;
            this.button18.Text = "Chọn";
            this.button18.UseVisualStyleBackColor = true;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(152, 51);
            this.textBox6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(359, 26);
            this.textBox6.TabIndex = 2;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(31, 54);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(94, 20);
            this.label11.TabIndex = 1;
            this.label11.Text = "Đường dẫn:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(31, 27);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(105, 20);
            this.label12.TabIndex = 0;
            this.label12.Text = "File hiện tại: ";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.button19);
            this.groupBox7.Controls.Add(this.button20);
            this.groupBox7.Controls.Add(this.button21);
            this.groupBox7.Controls.Add(this.tbPhonebook);
            this.groupBox7.Controls.Add(this.label13);
            this.groupBox7.Controls.Add(this.label14);
            this.groupBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox7.Location = new System.Drawing.Point(3, 590);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox7.Size = new System.Drawing.Size(869, 94);
            this.groupBox7.TabIndex = 8;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "7. File danh bạ doanh nghiệp";
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(768, 48);
            this.button19.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(75, 28);
            this.button19.TabIndex = 5;
            this.button19.Text = "Xóa";
            this.button19.UseVisualStyleBackColor = true;
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(663, 48);
            this.button20.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(75, 28);
            this.button20.TabIndex = 4;
            this.button20.Text = "Nhập";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.button20_Click);
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(557, 48);
            this.button21.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(75, 28);
            this.button21.TabIndex = 3;
            this.button21.Text = "Chọn";
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.button21_Click);
            // 
            // tbPhonebook
            // 
            this.tbPhonebook.Location = new System.Drawing.Point(152, 51);
            this.tbPhonebook.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbPhonebook.Name = "tbPhonebook";
            this.tbPhonebook.Size = new System.Drawing.Size(359, 26);
            this.tbPhonebook.TabIndex = 2;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(31, 54);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(94, 20);
            this.label13.TabIndex = 1;
            this.label13.Text = "Đường dẫn:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(31, 27);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(105, 20);
            this.label14.TabIndex = 0;
            this.label14.Text = "File hiện tại: ";
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.btnDeleteCCTKTMS);
            this.groupBox15.Controls.Add(this.btnImportCCTKTMS);
            this.groupBox15.Controls.Add(this.btnUploadCCTKTMS);
            this.groupBox15.Controls.Add(this.tbCCTKTMS);
            this.groupBox15.Controls.Add(this.label34);
            this.groupBox15.Controls.Add(this.label35);
            this.groupBox15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox15.Location = new System.Drawing.Point(3, 688);
            this.groupBox15.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox15.Size = new System.Drawing.Size(869, 94);
            this.groupBox15.TabIndex = 9;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "8. File CCTK trên TMS";
            // 
            // btnDeleteCCTKTMS
            // 
            this.btnDeleteCCTKTMS.Location = new System.Drawing.Point(768, 48);
            this.btnDeleteCCTKTMS.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnDeleteCCTKTMS.Name = "btnDeleteCCTKTMS";
            this.btnDeleteCCTKTMS.Size = new System.Drawing.Size(75, 28);
            this.btnDeleteCCTKTMS.TabIndex = 5;
            this.btnDeleteCCTKTMS.Text = "Xóa";
            this.btnDeleteCCTKTMS.UseVisualStyleBackColor = true;
            // 
            // btnImportCCTKTMS
            // 
            this.btnImportCCTKTMS.Location = new System.Drawing.Point(663, 48);
            this.btnImportCCTKTMS.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnImportCCTKTMS.Name = "btnImportCCTKTMS";
            this.btnImportCCTKTMS.Size = new System.Drawing.Size(75, 28);
            this.btnImportCCTKTMS.TabIndex = 4;
            this.btnImportCCTKTMS.Text = "Nhập";
            this.btnImportCCTKTMS.UseVisualStyleBackColor = true;
            this.btnImportCCTKTMS.Click += new System.EventHandler(this.btnImportCCTKTMS_Click);
            // 
            // btnUploadCCTKTMS
            // 
            this.btnUploadCCTKTMS.Location = new System.Drawing.Point(557, 48);
            this.btnUploadCCTKTMS.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnUploadCCTKTMS.Name = "btnUploadCCTKTMS";
            this.btnUploadCCTKTMS.Size = new System.Drawing.Size(75, 28);
            this.btnUploadCCTKTMS.TabIndex = 3;
            this.btnUploadCCTKTMS.Text = "Chọn";
            this.btnUploadCCTKTMS.UseVisualStyleBackColor = true;
            this.btnUploadCCTKTMS.Click += new System.EventHandler(this.btnUploadCCTKTMS_Click);
            // 
            // tbCCTKTMS
            // 
            this.tbCCTKTMS.Location = new System.Drawing.Point(152, 51);
            this.tbCCTKTMS.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbCCTKTMS.Name = "tbCCTKTMS";
            this.tbCCTKTMS.Size = new System.Drawing.Size(359, 26);
            this.tbCCTKTMS.TabIndex = 2;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(31, 54);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(94, 20);
            this.label34.TabIndex = 1;
            this.label34.Text = "Đường dẫn:";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(31, 27);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(105, 20);
            this.label35.TabIndex = 0;
            this.label35.Text = "File hiện tại: ";
            // 
            // SaveDataTab
            // 
            this.SaveDataTab.Controls.Add(this.tabPage1);
            this.SaveDataTab.Controls.Add(this.tabPage14);
            this.SaveDataTab.Controls.Add(this.tabPage2);
            this.SaveDataTab.Controls.Add(this.tabPage3);
            this.SaveDataTab.Controls.Add(this.tabPage4);
            this.SaveDataTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SaveDataTab.Location = new System.Drawing.Point(0, 0);
            this.SaveDataTab.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.SaveDataTab.Name = "SaveDataTab";
            this.SaveDataTab.SelectedIndex = 0;
            this.SaveDataTab.Size = new System.Drawing.Size(959, 749);
            this.SaveDataTab.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.Azure;
            this.tabPage4.Location = new System.Drawing.Point(4, 28);
            this.tabPage4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage4.Size = new System.Drawing.Size(951, 717);
            this.tabPage4.TabIndex = 5;
            this.tabPage4.Text = "Giới thiệu";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tabControl1);
            this.tabPage2.Location = new System.Drawing.Point(4, 28);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(951, 717);
            this.tabPage2.TabIndex = 6;
            this.tabPage2.Text = "Hỗ trợ quản lý nợ";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Location = new System.Drawing.Point(0, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(948, 711);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.richTextBox4);
            this.tabPage5.Controls.Add(this.button42);
            this.tabPage5.Controls.Add(this.button41);
            this.tabPage5.Controls.Add(this.button40);
            this.tabPage5.Controls.Add(this.richTextBox3);
            this.tabPage5.Location = new System.Drawing.Point(4, 28);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(940, 679);
            this.tabPage5.TabIndex = 0;
            this.tabPage5.Text = "Khả năng thu tăng";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.richTextBox2);
            this.tabPage6.Controls.Add(this.button39);
            this.tabPage6.Controls.Add(this.button38);
            this.tabPage6.Controls.Add(this.button35);
            this.tabPage6.Controls.Add(this.richTextBox1);
            this.tabPage6.Location = new System.Drawing.Point(4, 28);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(940, 679);
            this.tabPage6.TabIndex = 1;
            this.tabPage6.Text = "Nợ mới đánh giá đến từng cán bộ";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // tabPage7
            // 
            this.tabPage7.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar;
            this.tabPage7.Controls.Add(this.button45);
            this.tabPage7.Controls.Add(this.button44);
            this.tabPage7.Controls.Add(this.button43);
            this.tabPage7.Controls.Add(this.richTextBox6);
            this.tabPage7.Controls.Add(this.richTextBox5);
            this.tabPage7.Location = new System.Drawing.Point(4, 28);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(940, 679);
            this.tabPage7.TabIndex = 2;
            this.tabPage7.Text = "Cảnh báo CCHD hết hạn cưỡng chế";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.button48);
            this.tabPage8.Controls.Add(this.button47);
            this.tabPage8.Controls.Add(this.button46);
            this.tabPage8.Controls.Add(this.richTextBox8);
            this.tabPage8.Controls.Add(this.richTextBox7);
            this.tabPage8.Location = new System.Drawing.Point(4, 28);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(940, 679);
            this.tabPage8.TabIndex = 3;
            this.tabPage8.Text = "Cảnh báo các quyết định cưỡng chế chưa nhập vào TMS";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(6, 28);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(928, 136);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "Mô tả tab";
            // 
            // button35
            // 
            this.button35.Location = new System.Drawing.Point(82, 189);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(129, 23);
            this.button35.TabIndex = 1;
            this.button35.Text = "Kiểm tra file";
            this.button35.UseVisualStyleBackColor = true;
            // 
            // button38
            // 
            this.button38.Location = new System.Drawing.Point(281, 189);
            this.button38.Name = "button38";
            this.button38.Size = new System.Drawing.Size(189, 23);
            this.button38.TabIndex = 2;
            this.button38.Text = "Xuất cảnh báo";
            this.button38.UseVisualStyleBackColor = true;
            // 
            // button39
            // 
            this.button39.Location = new System.Drawing.Point(577, 189);
            this.button39.Name = "button39";
            this.button39.Size = new System.Drawing.Size(189, 23);
            this.button39.TabIndex = 3;
            this.button39.Text = "Nhập file nguồn";
            this.button39.UseVisualStyleBackColor = true;
            // 
            // richTextBox2
            // 
            this.richTextBox2.Location = new System.Drawing.Point(6, 250);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(928, 96);
            this.richTextBox2.TabIndex = 4;
            this.richTextBox2.Text = "Các files đã được nhập đầy đủ.";
            // 
            // richTextBox3
            // 
            this.richTextBox3.Location = new System.Drawing.Point(17, 15);
            this.richTextBox3.Name = "richTextBox3";
            this.richTextBox3.Size = new System.Drawing.Size(905, 135);
            this.richTextBox3.TabIndex = 0;
            this.richTextBox3.Text = "Mô tả tab";
            // 
            // button40
            // 
            this.button40.Location = new System.Drawing.Point(127, 181);
            this.button40.Name = "button40";
            this.button40.Size = new System.Drawing.Size(138, 23);
            this.button40.TabIndex = 1;
            this.button40.Text = "Kiểm tra";
            this.button40.UseVisualStyleBackColor = true;
            // 
            // button41
            // 
            this.button41.Location = new System.Drawing.Point(377, 181);
            this.button41.Name = "button41";
            this.button41.Size = new System.Drawing.Size(149, 23);
            this.button41.TabIndex = 2;
            this.button41.Text = "Xuất cảnh báo";
            this.button41.UseVisualStyleBackColor = true;
            // 
            // button42
            // 
            this.button42.Location = new System.Drawing.Point(614, 181);
            this.button42.Name = "button42";
            this.button42.Size = new System.Drawing.Size(149, 23);
            this.button42.TabIndex = 3;
            this.button42.Text = "Nhập file nguồn";
            this.button42.UseVisualStyleBackColor = true;
            // 
            // richTextBox4
            // 
            this.richTextBox4.Location = new System.Drawing.Point(17, 268);
            this.richTextBox4.Name = "richTextBox4";
            this.richTextBox4.Size = new System.Drawing.Size(905, 138);
            this.richTextBox4.TabIndex = 4;
            this.richTextBox4.Text = "Các file đã được cập nhật đầy đủ.";
            // 
            // richTextBox5
            // 
            this.richTextBox5.Location = new System.Drawing.Point(18, 18);
            this.richTextBox5.Name = "richTextBox5";
            this.richTextBox5.Size = new System.Drawing.Size(897, 120);
            this.richTextBox5.TabIndex = 0;
            this.richTextBox5.Text = "";
            // 
            // richTextBox6
            // 
            this.richTextBox6.Location = new System.Drawing.Point(18, 260);
            this.richTextBox6.Name = "richTextBox6";
            this.richTextBox6.Size = new System.Drawing.Size(897, 132);
            this.richTextBox6.TabIndex = 1;
            this.richTextBox6.Text = "";
            // 
            // button43
            // 
            this.button43.Location = new System.Drawing.Point(41, 182);
            this.button43.Name = "button43";
            this.button43.Size = new System.Drawing.Size(201, 23);
            this.button43.TabIndex = 2;
            this.button43.Text = "Kiểm tra";
            this.button43.UseVisualStyleBackColor = true;
            // 
            // button44
            // 
            this.button44.Location = new System.Drawing.Point(655, 182);
            this.button44.Name = "button44";
            this.button44.Size = new System.Drawing.Size(201, 23);
            this.button44.TabIndex = 3;
            this.button44.Text = "Nhập file nguồn";
            this.button44.UseVisualStyleBackColor = true;
            // 
            // button45
            // 
            this.button45.Location = new System.Drawing.Point(351, 182);
            this.button45.Name = "button45";
            this.button45.Size = new System.Drawing.Size(201, 23);
            this.button45.TabIndex = 4;
            this.button45.Text = "Xuất cảnh báo";
            this.button45.UseVisualStyleBackColor = true;
            // 
            // richTextBox7
            // 
            this.richTextBox7.Location = new System.Drawing.Point(29, 28);
            this.richTextBox7.Name = "richTextBox7";
            this.richTextBox7.Size = new System.Drawing.Size(872, 114);
            this.richTextBox7.TabIndex = 0;
            this.richTextBox7.Text = "";
            // 
            // richTextBox8
            // 
            this.richTextBox8.Location = new System.Drawing.Point(34, 282);
            this.richTextBox8.Name = "richTextBox8";
            this.richTextBox8.Size = new System.Drawing.Size(872, 114);
            this.richTextBox8.TabIndex = 1;
            this.richTextBox8.Text = "";
            // 
            // button46
            // 
            this.button46.Location = new System.Drawing.Point(92, 198);
            this.button46.Name = "button46";
            this.button46.Size = new System.Drawing.Size(191, 23);
            this.button46.TabIndex = 2;
            this.button46.Text = "Kiểm tra";
            this.button46.UseVisualStyleBackColor = true;
            // 
            // button47
            // 
            this.button47.Location = new System.Drawing.Point(637, 198);
            this.button47.Name = "button47";
            this.button47.Size = new System.Drawing.Size(191, 23);
            this.button47.TabIndex = 3;
            this.button47.Text = "Nhập file nguồn";
            this.button47.UseVisualStyleBackColor = true;
            // 
            // button48
            // 
            this.button48.Location = new System.Drawing.Point(354, 198);
            this.button48.Name = "button48";
            this.button48.Size = new System.Drawing.Size(191, 23);
            this.button48.TabIndex = 4;
            this.button48.Text = "Xuất cảnh báo";
            this.button48.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightCyan;
            this.ClientSize = new System.Drawing.Size(959, 749);
            this.Controls.Add(this.SaveDataTab);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "QLTDN1.1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabPage14.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage15.ResumeLayout(false);
            this.tabPage15.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCCTK_Search)).EndInit();
            this.tabPage16.ResumeLayout(false);
            this.tabPage16.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CCHD_GridView)).EndInit();
            this.tabPage17.ResumeLayout(false);
            this.tabPage17.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GTTK_dsCongTy_GridView)).EndInit();
            this.tabPage18.ResumeLayout(false);
            this.tabPage18.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GTHD_GridView)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.SaveDataTab.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.tabPage7.ResumeLayout(false);
            this.tabPage8.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage14;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.ComboBox listChiCucThue_comboBox;
        private System.Windows.Forms.Button addChiCucThue_btn;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button saveTenCanBo_btn;
        private System.Windows.Forms.TextBox tenCanBo_txt;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.TextBox tbPhonebook;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.TextBox tbLicenseFile;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox S12FileNameTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl SaveDataTab;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.ComboBox listTenDoi_comboBox;
        private System.Windows.Forms.Button saveTenDoi_btn;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.Button btnDeleteCCTKTMS;
        private System.Windows.Forms.Button btnImportCCTKTMS;
        private System.Windows.Forms.Button btnUploadCCTKTMS;
        private System.Windows.Forms.TextBox tbCCTKTMS;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage15;
        private System.Windows.Forms.DateTimePicker CCTK_DateTime;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.Button btnCCTK_openForm_NhapFileNguon;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.TextBox tbCCTK_TenCB;
        private System.Windows.Forms.ComboBox cbCCTK_tenDoi;
        private System.Windows.Forms.ComboBox cbCCTK_Chinhanh;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.RichTextBox txbCCTK_fileNguonStatus;
        private System.Windows.Forms.Button btnCCTK_CreateReport;
        private System.Windows.Forms.Button btnCCTK_checkFile;
        private System.Windows.Forms.DataGridView dgvCCTK_Search;
        private System.Windows.Forms.DataGridViewTextBoxColumn STT;
        private System.Windows.Forms.DataGridViewTextBoxColumn TenCongTy;
        private System.Windows.Forms.DataGridViewTextBoxColumn MST;
        private System.Windows.Forms.DataGridViewTextBoxColumn Chuong;
        private System.Windows.Forms.DataGridViewTextBoxColumn Khoan;
        private System.Windows.Forms.DataGridViewTextBoxColumn TieuMuc;
        private System.Windows.Forms.Button btnCCTK_SearchMST;
        private System.Windows.Forms.TextBox tbCCTK_SearchMST;
        private System.Windows.Forms.TabPage tabPage16;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.TextBox CCHD_TenCanBo;
        private System.Windows.Forms.ComboBox CCHD_listTenDoi;
        private System.Windows.Forms.ComboBox CCHD_listChiNhanh;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.RichTextBox CCHD_FileNguonStatus;
        private System.Windows.Forms.Button btnCCHD;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.DataGridView CCHD_GridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn STT1;
        private System.Windows.Forms.DataGridViewTextBoxColumn TenCongTy1;
        private System.Windows.Forms.DataGridViewTextBoxColumn MST1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Chuong1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Khoan1;
        private System.Windows.Forms.DataGridViewTextBoxColumn TieuMuc1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.TextBox tbCCHD_Search;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TabPage tabPage17;
        private System.Windows.Forms.Button btnCreateTB;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.TextBox GTTK_TenCanBo;
        private System.Windows.Forms.ComboBox GTTK_listTenDoi;
        private System.Windows.Forms.ComboBox GTTK_listChiNhanh;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.RichTextBox GTTK_CheckResult_text;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.DataGridView GTTK_dsCongTy_GridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn STT2;
        private System.Windows.Forms.DataGridViewTextBoxColumn TenCongTy2;
        private System.Windows.Forms.DataGridViewTextBoxColumn MST2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Chuong2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Khoan2;
        private System.Windows.Forms.DataGridViewTextBoxColumn TieuMuc2;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.TextBox GTTK_MST_textBox;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TabPage tabPage18;
        private System.Windows.Forms.RichTextBox tinhTrangFileNguon_GTHD_txt;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.TextBox GTHD_TenCanBo;
        private System.Windows.Forms.ComboBox GTHD_listTenDoi;
        private System.Windows.Forms.ComboBox GTHD_listChiNhanh;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Button CreateReport_CCHD_btn;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.Button FileNguonStatusCheck_GTHD_btn;
        private System.Windows.Forms.DataGridView GTHD_GridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn STT3;
        private System.Windows.Forms.DataGridViewTextBoxColumn TenCongTy3;
        private System.Windows.Forms.DataGridViewTextBoxColumn MST3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Chuong3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Khoan3;
        private System.Windows.Forms.DataGridViewTextBoxColumn TieuMuc3;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.TextBox MST_GTHD_txtBox;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.DateTimePicker CCHD_DateTime;
        private System.Windows.Forms.DateTimePicker GTTK_DateTime;
        private System.Windows.Forms.DateTimePicker GTHD_DateTime;
        private System.Windows.Forms.TextBox tbCCTK_TenDoiTruong;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox tbCCHD_TenDoiTruong;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox tbGTTK_TenDoiTruong;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox tbGTHD_TenDoiTruong;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.Button addTenDoiTruong_btn;
        private System.Windows.Forms.TextBox tenDoiTruong_txt;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox tbCCTK_tenChiCucTruong;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox tbCCTK_tenPhoChiCucTruong;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox tbCCHD_tenChiCucTruong;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox tbCCHD_tenPhoChiCucTruong;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox tbGTTK_tenChiCucTruong;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox tbGTTK_tenPhoChiCucTruong;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox tbGTHD_tenChiCucTruong;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox tbGTHD_tenPhoChiCucTruong;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.TextBox tb_tenChiCucTruong;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.TextBox tb_tenPhoChiCucTruong;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.Button button40;
        private System.Windows.Forms.RichTextBox richTextBox3;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.Button button38;
        private System.Windows.Forms.Button button35;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.RichTextBox richTextBox4;
        private System.Windows.Forms.Button button42;
        private System.Windows.Forms.Button button41;
        private System.Windows.Forms.Button button45;
        private System.Windows.Forms.Button button44;
        private System.Windows.Forms.Button button43;
        private System.Windows.Forms.RichTextBox richTextBox6;
        private System.Windows.Forms.RichTextBox richTextBox5;
        private System.Windows.Forms.Button button48;
        private System.Windows.Forms.Button button47;
        private System.Windows.Forms.Button button46;
        private System.Windows.Forms.RichTextBox richTextBox8;
        private System.Windows.Forms.RichTextBox richTextBox7;
    }
}

