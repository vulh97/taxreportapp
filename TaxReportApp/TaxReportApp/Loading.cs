﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaxReportApp
{
    public partial class Loading : Form
    {
        public Loading()
        {
            InitializeComponent();
        }

        private void Loading_Load(object sender, EventArgs e)
        {
            timer1.Start();
            //int i; //Khởi tạo biến i

            //progressBar1.Minimum = 0; //Đặt giá trị nhỏ nhất cho ProgressBar
            progressBar1.Maximum = 60; //Đặt giá trị lớn nhất cho ProgressBar
            //for (i = 0; i <= 3; i++)
            //{
            //    progressBar1.Value = i; //Gán giá trị cho ProgressBar
            //}
            //if (progressBar1.Value == 3)
            //{
            //    Form1 form = new Form1();
            //    form.Show();
            //    this.Hide();
            //}
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            progressBar1.Value = progressBar1.Value + 2;
            if (progressBar1.Value == 60)
            {
                timer1.Stop();
                Form1 form = new Form1();
                form.Show();
                this.Hide();
            }
        }
    }
}
