﻿using Microsoft.Office.Interop.Excel;
using Spire.Doc;
using Spire.Doc.Documents;
using Spire.Doc.Fields;
//using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using TaxReportApp.Common;
using TaxReportApp.Controller;
using TaxReportApp.Model;
using HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment;
using TextBox = System.Windows.Forms.TextBox;

namespace TaxReportApp
{
    public partial class Form1 : Form
    {
        #region [hoa.qn] Link File
        private string fileS12 = "";
        private string fileChungtu = "";
        private string filePhonebook = "";
        private string fileCCTKTMS = "";
        static string dir = Directory.GetCurrentDirectory().ToString() + @"\" + ConfigurationManager.AppSettings["directoryDatabase"];
        static string dirReport = System.Windows.Forms.Application.StartupPath + @"\" + ConfigurationManager.AppSettings["ReportFile"];
        private string pathS12File = dir + @"\" + ConfigurationManager.AppSettings["s12File"];
        private string pathDanhba = dir + @"\" + ConfigurationManager.AppSettings["phoneBookFile"];
        private string pathChungtu = dir + @"\" + ConfigurationManager.AppSettings["licenseFile"];
        private string pathTaikhoan = dir + @"\" + ConfigurationManager.AppSettings["accountBankFile"];
        private string pathCCTKTMS = dir + @"\" + ConfigurationManager.AppSettings["cctkTMSFile"];
        private string pathDSCB = dir + @"\" + ConfigurationManager.AppSettings["dsCanBoFile"];
        private string totrinh = System.Windows.Forms.Application.StartupPath + @"\documents\input\CCTK\To_trinh_CC_taikhoan.doc";
        private string quyetdinh = System.Windows.Forms.Application.StartupPath + @"\documents\input\CCTK\Quyet_dinh_CC_taikhoan.doc";
        private string lenhthu = System.Windows.Forms.Application.StartupPath + @"\documents\input\CCTK\Lenh_thu_NSNN.doc";
        private string qdcohoadon = System.Windows.Forms.Application.StartupPath + @"\documents\input\CCHD\QuyetDinh_ThongBao_ToTrinh\CC_co_hoadon\QD_TB_cohoadon.doc";
        private string ttcohoadon = System.Windows.Forms.Application.StartupPath + @"\documents\input\CCHD\QuyetDinh_ThongBao_ToTrinh\CC_co_hoadon\To_trinh_cohoadon.doc";
        private string qdkhonghoadon = System.Windows.Forms.Application.StartupPath + @"\documents\input\CCHD\QuyetDinh_ThongBao_ToTrinh\CC_khongco_hoadon\QD_TB_khonghoadon.doc";
        private string ttkhonghoadon = System.Windows.Forms.Application.StartupPath + @"\documents\input\CCHD\QuyetDinh_ThongBao_ToTrinh\CC_khongco_hoadon\To_trinh_khonghoadon.doc";
        private static string pathCTDN = dir + @"\" + ConfigurationManager.AppSettings["chungtunoptu_01072020"];
        private static string pathCCHDTMS = dir + @"\" + ConfigurationManager.AppSettings["cchd_tren_tms"];
        private static string pathNotification3Day = System.Windows.Forms.Application.StartupPath + @"\documents\input\CCHD\thongbao3ngay.doc";
        private static string pathNotification5Day = System.Windows.Forms.Application.StartupPath + @"\documents\input\CCHD\thongbao5ngay.doc";
        static FileNguonStatusModel fileNguonStatusModel = new FileNguonStatusModel();
        #endregion
        public Form1()
        {
            InitializeComponent();

            // Set thời gian tạo báo cáo
            CCTK_DateTime.Format = DateTimePickerFormat.Custom;
            CCTK_DateTime.CustomFormat = "dd/MM/yyyy";
            CCHD_DateTime.Format = DateTimePickerFormat.Custom;
            CCHD_DateTime.CustomFormat = "dd/MM/yyyy";
            GTTK_DateTime.Format = DateTimePickerFormat.Custom;
            GTTK_DateTime.CustomFormat = "dd/MM/yyyy";
            GTHD_DateTime.Format = DateTimePickerFormat.Custom;
            GTHD_DateTime.CustomFormat = "dd/MM/yyyy";
          
            // Load danh sách file vào tab báo cáo
            DirectoryInfo d = new DirectoryInfo(dir);
            FileInfo[] Files = d.GetFiles(); //Getting Text files
            var listFileModel = new List<FileModel>();
            int stt = 0;
            foreach (FileInfo file in Files)
            {
                stt++;
                var fileModel = new FileModel();
                fileModel.stt = stt;
                fileModel.filename = file.Name;
                fileModel.filetype = Path.GetDirectoryName(file.Name);
                listFileModel.Add(fileModel);

            }
            var bindingList = new BindingList<FileModel>(listFileModel);
            var source = new BindingSource(bindingList, null);

            // Kiếm tra các file còn thiếu
            fileNguonStatusModel = FileNguonStatusController.get_FileNguonStatus();
            txbCCTK_fileNguonStatus.Text = FileNguonStatusController.get_tinhTrangCacFileNguonCCTK(fileNguonStatusModel);
            CCHD_FileNguonStatus.Text = FileNguonStatusController.get_tinhTrangCacFileNguonCCHĐ(fileNguonStatusModel);
            tinhTrangFileNguon_GTHD_txt.Text = FileNguonStatusController.get_tinhTrangCacFileNguonGTHD(fileNguonStatusModel);

            // Load danh sách chi cục
            var listTenChiCuc = DonViController.getAllName_ChiCuc();
            foreach (var tenChiCuc in listTenChiCuc)
            {
                listChiCucThue_comboBox.Items.Add(tenChiCuc);
                cbCCTK_Chinhanh.Items.Add(tenChiCuc);
                CCHD_listChiNhanh.Items.Add(tenChiCuc);
                GTTK_listChiNhanh.Items.Add(tenChiCuc);
                GTHD_listChiNhanh.Items.Add(tenChiCuc);
            }

            // Load thông tin đội
            var listTenDoi = DonViController.getAllName_TenDoi();
            foreach (var tenDoi in listTenDoi)
            {
                listTenDoi_comboBox.Items.Add(tenDoi);
                cbCCTK_tenDoi.Items.Add(tenDoi);
                CCHD_listTenDoi.Items.Add(tenDoi);
                GTTK_listTenDoi.Items.Add(tenDoi);
                GTHD_listTenDoi.Items.Add(tenDoi);
            }

            // Load thông tin cán bộ, chi Cục và đội
            var donVi_data = DonViController.GetThongTinDonVi();
            tenCanBo_txt.Text = donVi_data.tenCanBo;
            listChiCucThue_comboBox.Text = donVi_data.tenChiNhanh;
            cbCCTK_Chinhanh.Text = donVi_data.tenChiNhanh;
            tbCCTK_TenCB.Text = donVi_data.tenCanBo;
            cbCCTK_tenDoi.Text = donVi_data.tenDoi;
            listTenDoi_comboBox.Text = donVi_data.tenDoi;
            CCHD_listChiNhanh.Text = donVi_data.tenChiNhanh;
            CCHD_TenCanBo.Text = donVi_data.tenCanBo;
            CCHD_listTenDoi.Text = donVi_data.tenDoi;
            GTTK_listChiNhanh.Text = donVi_data.tenChiNhanh;
            GTTK_listTenDoi.Text = donVi_data.tenDoi;
            GTTK_TenCanBo.Text = donVi_data.tenCanBo;
            GTHD_listChiNhanh.Text = donVi_data.tenChiNhanh;
            GTHD_listTenDoi.Text = donVi_data.tenDoi;
            GTHD_TenCanBo.Text = donVi_data.tenCanBo;
            tbCCTK_TenDoiTruong.Text = donVi_data.tenDoiTruong;
            tbCCHD_TenDoiTruong.Text = donVi_data.tenDoiTruong;
            tbGTTK_TenDoiTruong.Text = donVi_data.tenDoiTruong;
            tbGTHD_TenDoiTruong.Text = donVi_data.tenDoiTruong;
            tenDoiTruong_txt.Text = donVi_data.tenDoiTruong;
            tbCCTK_tenChiCucTruong.Text = donVi_data.tenChiCucTruong;
            tbCCHD_tenChiCucTruong.Text = donVi_data.tenChiCucTruong;
            tbGTTK_tenChiCucTruong.Text = donVi_data.tenChiCucTruong;
            tbGTHD_tenChiCucTruong.Text = donVi_data.tenChiCucTruong;
            tb_tenChiCucTruong.Text = donVi_data.tenChiCucTruong;
            tbCCTK_tenPhoChiCucTruong.Text = donVi_data.tenPhoChiCucTruong;
            tbCCHD_tenPhoChiCucTruong.Text = donVi_data.tenPhoChiCucTruong;
            tbGTTK_tenPhoChiCucTruong.Text = donVi_data.tenPhoChiCucTruong;
            tbGTHD_tenPhoChiCucTruong.Text = donVi_data.tenPhoChiCucTruong;
            tb_tenPhoChiCucTruong.Text = donVi_data.tenPhoChiCucTruong;
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            fileS12 = UploadFile(S12FileNameTextBox);
        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void tabPage5_Click(object sender, EventArgs e)
        {

        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void openFileDialog1_FileOk_1(object sender, CancelEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Common.Common.Import(fileS12, "s12File", S12FileNameTextBox);
            fileS12 = "";
        }

        private void button15_Click(object sender, EventArgs e)
        {
            fileChungtu = UploadFile(tbLicenseFile);
        }

        private void button14_Click(object sender, EventArgs e)
        {
            Common.Common.Import(fileChungtu, "licenseFile", tbLicenseFile);
            fileChungtu = "";
        }

        private void button21_Click(object sender, EventArgs e)
        {
            filePhonebook = UploadFile(tbPhonebook);
        }

        private void button20_Click(object sender, EventArgs e)
        {
            Common.Common.Import(filePhonebook, "phoneBookFile", tbPhonebook);
            filePhonebook = "";
        }

        private string UploadFile(TextBox tbFile)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.RestoreDirectory = true;
                openFileDialog.Title = "Chọn File";
                openFileDialog.Multiselect = false;
                openFileDialog.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";
                openFileDialog.CheckFileExists = true;
                openFileDialog.CheckPathExists = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    tbFile.Text = openFileDialog.FileName;
                    return tbFile.Text;
                }
                return "";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi xảy ra !", "Lỗi");
                return "";
            }
        }

        private void tsbCheckFile_Click(object sender, EventArgs e)
        {
            if (!Directory.Exists(dir))
            {
                MessageBox.Show("Không tìm thấy các file yêu cầu! Vui lòng nhập file", "Lỗi");
            }
            else
            {
                if (!File.Exists(dir + @"\" + ConfigurationManager.AppSettings["s12File"]))
                {
                    MessageBox.Show("Không tìm thấy các file yêu cầu! Vui lòng nhập file", "Lỗi");
                }
                else if (!File.Exists(dir + @"\" + ConfigurationManager.AppSettings["accountBankFile"]))
                {
                    MessageBox.Show("Không tìm thấy các file yêu cầu! Vui lòng nhập file", "Lỗi");
                }
                else if (!File.Exists(dir + @"\" + ConfigurationManager.AppSettings["listAddressBankFile"]))
                {
                    MessageBox.Show("Không tìm thấy các file yêu cầu! Vui lòng nhập file", "Lỗi");
                }
                else if (!File.Exists(dir + @"\" + ConfigurationManager.AppSettings["phoneBookFile"]))
                {
                    MessageBox.Show("Không tìm thấy các file yêu cầu! Vui lòng nhập file", "Lỗi");
                }
                else if (!File.Exists(dir + @"\" + ConfigurationManager.AppSettings["licenseFile"]))
                {
                    MessageBox.Show("Không tìm thấy các file yêu cầu! Vui lòng nhập file", "Lỗi");
                }
            }
        }

        private void tsbExport_Click(object sender, EventArgs e)
        {

        }

        private void addChiCucThue_btn_Click(object sender, EventArgs e)
        {
            var tenChiNhanh = listChiCucThue_comboBox.Text;
            var isSaveTenChiNhanhSuccessfully = DonViController.SaveChiNhanh(tenChiNhanh);
            if (isSaveTenChiNhanhSuccessfully)
            {
                setData_chiNhanh(tenChiNhanh);
                MessageBox.Show("Lưu chi nhánh thành công !", "Thông báo");
            }
            else
            {
                MessageBox.Show("Có lỗi xảy ra. Lưu chi nhánh thất bại !", "Thông báo");
            }
        }

        private void tenCanBo_txt_TextChanged(object sender, EventArgs e)
        {

        }

        private void saveTenCanBo_btn_Click(object sender, EventArgs e)
        {
            var tenCanBo = tenCanBo_txt.Text;
            var isSaveTenCanBoSuccessfully = DonViController.SaveTenCanBo(tenCanBo);
            if (isSaveTenCanBoSuccessfully)
            {
                setData_tenCanBo(tenCanBo);
                MessageBox.Show("Lưu tên cán bộ thành công !", "Thông báo");
            }
            else
            {
                MessageBox.Show("Có lỗi xảy ra. Lưu tên cán bộ thất bại !", "Thông báo");
            }
        }
        private void FindAndReplace(Microsoft.Office.Interop.Word.Application doc, object findText, object replaceWithText)
        {
            //options
            object matchCase = false;
            object matchWholeWord = true;
            object matchWildCards = false;
            object matchSoundsLike = false;
            object matchAllWordForms = false;
            object forward = true;
            object format = false;
            object matchKashida = false;
            object matchDiacritics = false;
            object matchAlefHamza = false;
            object matchControl = false;
            object read_only = false;
            object visible = true;
            object replace = 2;
            object wrap = 1;
            //execute find and replace
            doc.Selection.Find.Execute(ref findText, ref matchCase, ref matchWholeWord,
                ref matchWildCards, ref matchSoundsLike, ref matchAllWordForms, ref forward, ref wrap, ref format, ref replaceWithText, ref replace,
                ref matchKashida, ref matchDiacritics, ref matchAlefHamza, ref matchControl);
        }

        private void label18_Click(object sender, EventArgs e)
        {

        }

        private void button22_Click(object sender, EventArgs e)
        {
            if (!File.Exists(pathS12File))
            {
                Common.Common.error("Không tìm thấy tài liệu S12.XLSX! Vui lòng nhập tài liệu");
            }
            else
            {
                if (string.IsNullOrWhiteSpace(tbCCTK_SearchMST.Text))
                {
                    SearchAllDataS12(pathS12File, dgvCCTK_Search);
                }
                else
                {
                    SearchS12byMST(pathS12File, tbCCTK_SearchMST.Text.Trim(), dgvCCTK_Search);
                }
            }
        }

        private void button26_Click(object sender, EventArgs e)
        {
            var folderPath = Environment.CurrentDirectory + @"\documents\input\CCTK";
            Common.Common.OpenFolderExplorer(folderPath);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            flowLayoutPanel1.WrapContents = false;
            flowLayoutPanel1.AutoScroll = true;
            flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            #region 1. Load data S12.XLSX
            if (!File.Exists(pathS12File))
            {
                //Common.Common.error("Không tìm thấy tài liệu S12.XLSX! Vui lòng nhập tài liệu");
            }
            else
            {
                SearchAllDataS12(pathS12File, dgvCCTK_Search);
            }

            #endregion


            float widthRatio = Screen.PrimaryScreen.Bounds.Width / 1280;
            float heightRatio = Screen.PrimaryScreen.Bounds.Height / 800f;
            SizeF scale = new SizeF(widthRatio, heightRatio);
            this.Scale(scale);
            //foreach (Control control in this.Controls)
            //{
            //    control.Font = new Font("Verdana", control.Font.SizeInPoints * heightRatio * widthRatio);
            //}


        }

        #region [hoa.qn] Search
        public void SearchAllDataS12(string path, DataGridView dgv)
        {
            try
            {
                var tbS12 = S12Controller.SearchAll(path);
                var FormattedResult = Common.Common.get_ListCompanyFromDataTable(tbS12);
                dgvCCTK_Search.Rows.Clear();
                CCHD_GridView.Rows.Clear();
                GTTK_dsCongTy_GridView.Rows.Clear();
                GTHD_GridView.Rows.Clear();
                for (int i = 0; i < tbS12.Rows.Count; i++)
                {
                    if (!String.IsNullOrEmpty(FormattedResult[1, i]) && !String.IsNullOrWhiteSpace(FormattedResult[1, i]) && FormattedResult[1, i] != "Tên NNT")
                    {
                        i = dgvCCTK_Search.Rows.Add();
                        dgvCCTK_Search.Rows[i].Cells[0].Value = FormattedResult[0, i] ?? "";
                        dgvCCTK_Search.Rows[i].Cells[1].Value = FormattedResult[1, i] ?? "";
                        dgvCCTK_Search.Rows[i].Cells[2].Value = FormattedResult[2, i] ?? "";
                        dgvCCTK_Search.Rows[i].Cells[3].Value = FormattedResult[3, i] ?? "";
                        dgvCCTK_Search.Rows[i].Cells[4].Value = FormattedResult[4, i] ?? "";
                        dgvCCTK_Search.Rows[i].Cells[5].Value = FormattedResult[5, i] ?? "";

                        i = CCHD_GridView.Rows.Add();
                        CCHD_GridView.Rows[i].Cells[0].Value = FormattedResult[0, i] ?? "";
                        CCHD_GridView.Rows[i].Cells[1].Value = FormattedResult[1, i] ?? "";
                        CCHD_GridView.Rows[i].Cells[2].Value = FormattedResult[2, i] ?? "";
                        CCHD_GridView.Rows[i].Cells[3].Value = FormattedResult[3, i] ?? "";
                        CCHD_GridView.Rows[i].Cells[4].Value = FormattedResult[4, i] ?? "";
                        CCHD_GridView.Rows[i].Cells[5].Value = FormattedResult[5, i] ?? "";

                        i = GTTK_dsCongTy_GridView.Rows.Add();
                        GTTK_dsCongTy_GridView.Rows[i].Cells[0].Value = FormattedResult[0, i] ?? "";
                        GTTK_dsCongTy_GridView.Rows[i].Cells[1].Value = FormattedResult[1, i] ?? "";
                        GTTK_dsCongTy_GridView.Rows[i].Cells[2].Value = FormattedResult[2, i] ?? "";
                        GTTK_dsCongTy_GridView.Rows[i].Cells[3].Value = FormattedResult[3, i] ?? "";
                        GTTK_dsCongTy_GridView.Rows[i].Cells[4].Value = FormattedResult[4, i] ?? "";
                        GTTK_dsCongTy_GridView.Rows[i].Cells[5].Value = FormattedResult[5, i] ?? "";

                        i = GTHD_GridView.Rows.Add();
                        GTHD_GridView.Rows[i].Cells[0].Value = FormattedResult[0, i] ?? "";
                        GTHD_GridView.Rows[i].Cells[1].Value = FormattedResult[1, i] ?? "";
                        GTHD_GridView.Rows[i].Cells[2].Value = FormattedResult[2, i] ?? "";
                        GTHD_GridView.Rows[i].Cells[3].Value = FormattedResult[3, i] ?? "";
                        GTHD_GridView.Rows[i].Cells[4].Value = FormattedResult[4, i] ?? "";
                        GTHD_GridView.Rows[i].Cells[5].Value = FormattedResult[5, i] ?? "";

                    }
                }
            }
            catch (Exception ex)
            {
                Common.Common.error(ex.ToString());
            }
        }

        public static void UpdateGridView(System.Data.DataTable dataTable, String[,] FormattedResult, DataGridView dg)
        {
            dg.Rows.Clear();
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                i = dg.Rows.Add();
                dg.Rows[i].Cells[0].Value = FormattedResult[0, i] ?? "";
                dg.Rows[i].Cells[1].Value = FormattedResult[1, i] ?? "";
                dg.Rows[i].Cells[2].Value = FormattedResult[2, i] ?? "";
                dg.Rows[i].Cells[3].Value = FormattedResult[3, i] ?? "";
                dg.Rows[i].Cells[4].Value = FormattedResult[4, i] ?? "";
                dg.Rows[i].Cells[5].Value = FormattedResult[5, i] ?? "";
            }
        }

        public static bool SearchS12byMST(string path, string mstSearch, DataGridView dgv)
        {
            try
            {
                var tbS12 = S12Controller.SearchMST(path, mstSearch);
                bool haveValue = tbS12.AsEnumerable().Any(row => row["F2"] != null);
                if (haveValue)
                {
                    var FormattedResult = Common.Common.get_ListCompanyFromDataTable(tbS12);
                    UpdateGridView(tbS12, FormattedResult, dgv);
                    return true;
                }
                else
                {
                    Common.Common.error("Không tìm thấy dữ liệu!");
                    return false;
                }
            }
            catch (Exception ex)
            {
                Common.Common.error(ex.ToString());
                return false;
            }
        }
        #endregion

        private void btnCCTK_CreateReport_Click(object sender, EventArgs e)
        {
            //Common.Common.getBankAddress(@"NH NNo & PTNT VN - CN Hoàng Quốc Việt");

            var tenChiNhanh = cbCCTK_Chinhanh.Text;
            var tenDoi = cbCCTK_tenDoi.Text;
            var tenCanBo = tbCCTK_TenCB.Text;
            var tenDoiTruong = tbCCTK_TenDoiTruong.Text;
            var tenChiCucTruong = tbCCTK_tenChiCucTruong.Text;
            var tenPhoChiCucTruong = tbCCTK_tenPhoChiCucTruong.Text;

            DonViController.SaveThongTinDonVi(tenChiNhanh, tenCanBo, tenDoi, tenDoiTruong, tenChiCucTruong, tenPhoChiCucTruong);
            setData_chiNhanh(tenChiNhanh);
            setData_tenCanBo(tenCanBo);
            setData_tenDoi(tenDoi);
            setData_tenDoiTruong(tenDoiTruong);
            setData_tenChiCucTruong(tenChiCucTruong);
            setData_tenPhoChiCucTruong(tenPhoChiCucTruong);
            //List<String> listInputFiles = new List<string>();
            //var outputPathFile = "";

            #region Tạo file excel danh sách nợ
            Common.Common.createExcelFile(tbCCTK_SearchMST.Text.Trim());
            #endregion

            #region Tạo file excel trung gian CCTK
            Common.Common.createFileTrungGian(tbCCTK_SearchMST.Text.Trim());
            #endregion

            #region Tạo báo cáo Cưỡng chế tài khoản
            DateTime ngaycohieuluc = CCTK_DateTime.Value;
            DateTime ngayhethieuluc = CCTK_DateTime.Value.AddDays(30);
            var reportSumary = CCTKController.CCTK_CreateReport(tbCCTK_SearchMST.Text, tenChiNhanh, tenDoi, tenCanBo, tenDoiTruong, tenChiCucTruong, tenPhoChiCucTruong, ngaycohieuluc, ngayhethieuluc);
            #endregion

            #region Nối file
            if (reportSumary.listInputFiles.Count > 0 && !String.IsNullOrWhiteSpace(reportSumary.outputPathFile))
            {
                Common.Common.joinFiles(reportSumary.listInputFiles, reportSumary.outputPathFile);
                Common.Common.success("Tạo báo cáo thành công!");
            }
            #endregion
        }

        private void btnCCHD_CreateReport_Click(object sender, EventArgs e)
        {
            #region [hoa.nq]: Comment
            //if (!File.Exists(pathS12File))
            //{
            //    Common.Common.error("Không tìm thấy tài liệu S12.XLSX! Vui lòng nhập tài liệu");
            //}
            //else
            //{
            //    if (string.IsNullOrWhiteSpace(tbCCHD_Search.Text))
            //    {
            //        Common.Common.error("Vui lòng nhập mã số thuế!");
            //    }
            //    else
            //    {
            //        var tableS12Data = S12Controller.SearchMST(pathS12File, tbCCHD_Search.Text.Trim());
            //        if (tableS12Data.Rows.Count <= 0)
            //        {
            //            Common.Common.error("Không tìm thấy dữ liệu!");
            //            return;
            //        }
            //        else
            //        {
            //            var tableDanhba = DanhBaController.GetDanhba(pathDanhba, tableS12Data.Rows[0][1].ToString());
            //            // Get Folder Time
            //            var timeFolder = S12Controller.SearchMonthReport(pathS12File);
            //            // Get Directory: Bao Cao/062020/Công ty .../CCTK
            //            var path = dirReport + @"\" + timeFolder + @"\" + tableS12Data.Rows[0][1].ToString() + @"\CCHD";
            //            // Check Cty có nằm trong bảng CCHD trên TMS
            //            var tableCCHDTMS = CCHDController.GetCCHDOnTMSByTaxCode(pathCCHDTMS, tableS12Data.Rows[0]["F2"].ToString());
            //            // Check Cty có nằm trong bảng Chứng từ đã nộp
            //            var tableCTDN = ChungTuController.GetListChungTu(pathChungtu, tableS12Data.Rows[0]["F2"].ToString(), tableS12Data.Rows[0][5].ToString());
            //            if (!Directory.Exists(path))
            //            {
            //                Directory.CreateDirectory(path);
            //            }
            //            #region Kiểm tra các file có tồn tại: Có => Delete
            //            string fileQDCoHD = path + @"\" + ConfigurationManager.AppSettings["cuongchecohoadon"];
            //            string fileTTCoHD = path + @"\" + ConfigurationManager.AppSettings["totrinhcohoadon"];
            //            string fileQDKhongHD = path + @"\" + ConfigurationManager.AppSettings["cuongchekhonghoadon"];
            //            string fileTTKhongHD = path + @"\" + ConfigurationManager.AppSettings["totrinhkhonghoadon"];
            //            string fileNotification3Day = path + @"\" + ConfigurationManager.AppSettings["thongbao3ngay"];
            //            string fileNotification5Day = path + @"\" + ConfigurationManager.AppSettings["thongbao5ngay"];
            //            if (File.Exists(fileQDCoHD))
            //            {
            //                File.Delete(fileQDCoHD);
            //            }
            //            if (File.Exists(fileTTCoHD))
            //            {
            //                File.Delete(fileTTCoHD);
            //            }
            //            if (File.Exists(fileQDKhongHD))
            //            {
            //                File.Delete(fileQDKhongHD);
            //            }
            //            if (File.Exists(fileTTKhongHD))
            //            {
            //                File.Delete(fileTTKhongHD);
            //            }
            //            if (File.Exists(fileNotification3Day))
            //            {
            //                File.Delete(fileNotification3Day);
            //            }
            //            if (File.Exists(fileNotification5Day))
            //            {
            //                File.Delete(fileNotification5Day);
            //            }
            //            #endregion

            //            if (tableCCHDTMS.Rows.Count > 0)
            //            {
            //                #region [hoa.qn] Replace Text
            //                // Xử lý QĐ+TB có hoá đơn
            //                Document docQDCoHD = new Document();
            //                docQDCoHD.LoadFromFile(qdcohoadon);
            //                // Xử lý tờ trình có hoá đơn
            //                Document docTTCoHD = new Document();
            //                docTTCoHD.LoadFromFile(ttcohoadon);
            //                //  Xử lý QĐ+TB không có hoá đơn
            //                Document docQDKhongHD = new Document();
            //                docQDKhongHD.LoadFromFile(qdkhonghoadon);
            //                // Xử lý tờ trình không có hoá đơn
            //                Document docTTKhongHD = new Document();
            //                docTTKhongHD.LoadFromFile(ttkhonghoadon);
            //                CCHDModel cchdModel = new CCHDModel();
            //                if (tableDanhba.Rows.Count <= 0)
            //                {
            //                    Common.Common.error("Không tìm thấy dữ liệu tương ứng trong file danh bạ !");
            //                    return;
            //                }
            //                else
            //                {
            //                    int sttCT = 0;
            //                    foreach (DataRow rowS12 in tableS12Data.Rows)
            //                    {
            //                        var thue = new Thue();
            //                        sttCT++;
            //                        thue.stt = sttCT;
            //                        thue.tieumuc = rowS12[5].ToString();
            //                        #region Xử lý loại thuế
            //                        if (Array.IndexOf(new string[] { "1001", "1004" }, thue.tieumuc) > -1)
            //                        {
            //                            thue.loaithue = "Thuế TNCN";
            //                        }
            //                        else if (Array.IndexOf(new string[] { "1052", "1053" }, thue.tieumuc) > -1)
            //                        {
            //                            thue.loaithue = "Thuế TNDN";
            //                        }
            //                        else if (Array.IndexOf(new string[] { "1701" }, thue.tieumuc) > -1)
            //                        {
            //                            thue.loaithue = "Thuế GTGT";
            //                        }
            //                        else if (Array.IndexOf(new string[] { "1757" }, thue.tieumuc) > -1)
            //                        {
            //                            thue.loaithue = "Thuế TTĐB";
            //                        }
            //                        else if (Array.IndexOf(new string[] { "2862", "2863", "2864" }, thue.tieumuc) > -1)
            //                        {
            //                            thue.loaithue = "Lệ phí MB";
            //                        }
            //                        else if (Array.IndexOf(new string[] { "4254", "4268" }, thue.tieumuc) > -1)
            //                        {
            //                            thue.loaithue = "Tiền phạt";
            //                        }
            //                        else if (Array.IndexOf(new string[] { "4272", "4917", "4918", "4931", "4944" }, thue.tieumuc) > -1)
            //                        {
            //                            thue.loaithue = "Tiền chậm nộp";
            //                        }
            //                        else
            //                        {
            //                            thue.loaithue = "";
            //                        }
            //                        #endregion
            //                        thue.kythue = "";
            //                        // [Tiểu mục]: Bảng Chứng từ
            //                        decimal col11 = !string.IsNullOrWhiteSpace(rowS12[10].ToString()) ? Convert.ToDecimal(rowS12[10].ToString()) : 0;
            //                        decimal col12 = !string.IsNullOrWhiteSpace(rowS12[11].ToString()) ? Convert.ToDecimal(rowS12[11].ToString()) : 0;
            //                        thue.notren90ngay = col11 + col12;
            //                        cchdModel.tongtientren90ngay = cchdModel.tongtientren90ngay + thue.notren90ngay;

            //                        if (thue.notren90ngay > 0)
            //                        {
            //                            var tableLicense = ChungTuController.GetListChungTu(pathChungtu, rowS12[1].ToString(), rowS12[5].ToString()); // 1: MST; 2: Tiểu mục
            //                            if (tableLicense.Rows.Count > 0)
            //                            {
            //                                thue.sotiennop = !string.IsNullOrWhiteSpace(tableLicense.Rows[0][2].ToString()) ? Convert.ToDecimal(tableLicense.Rows[0][2].ToString()) : 0;
            //                                cchdModel.tongsonop = cchdModel.tongsonop + thue.sotiennop;
            //                            }
            //                            thue.notren91ngaycuongche = thue.notren90ngay > thue.sotiennop ? Math.Abs(thue.notren90ngay - thue.sotiennop) : 0;
            //                            cchdModel.tongtiencuongche = cchdModel.tongtiencuongche + thue.notren91ngaycuongche;
            //                            cchdModel.listThue.Add(thue);
            //                        }
            //                    }

            //                    //if (cchdModel.tongtientren90ngay == 0 || cchdModel.tongtiencuongche == 0)
            //                    //{

            //                    //}
            //                    //else
            //                    //{
            //                    //    Common.Common.success("Công ty này không còn tồn tại nợ thuế! Vui lòng kiểm tra lại!");
            //                    //}
            //                    #region Add Model
            //                    cchdModel.chicuc = !string.IsNullOrWhiteSpace(CCHD_listChiNhanh.Text) ? CCHD_listChiNhanh.Text : "";
            //                    cchdModel.thongbaoso = tableCCHDTMS.Rows.Count > 0 ? tableCCHDTMS.Rows[0][7].ToString() : "";
            //                    cchdModel.ngaythongbao = tableCCHDTMS.Rows.Count > 0 && tableCCHDTMS.Rows[0][44].ToString() != "" ? Convert.ToDateTime(tableCCHDTMS.Rows[0][44]).ToString("dd/MM/yyyy") : "";
            //                    cchdModel.tennnt = tableDanhba.Rows[0][1].ToString();
            //                    cchdModel.dctbt_diachi = tableDanhba.Rows[0][7].ToString();
            //                    cchdModel.dctbt_phuongxa = tableDanhba.Rows[0][9].ToString();
            //                    cchdModel.dctbt_quanhuyen = tableDanhba.Rows[0][11].ToString();
            //                    cchdModel.dctbt_tinhthanhpho = tableDanhba.Rows[0][13].ToString();
            //                    cchdModel.dcts_diachi = tableDanhba.Rows[0][7].ToString();
            //                    cchdModel.dcts_phuongxa = tableDanhba.Rows[0][9].ToString();
            //                    cchdModel.dcts_quanhuyen = tableDanhba.Rows[0][11].ToString();
            //                    cchdModel.dcts_tinhthanhpho = tableDanhba.Rows[0][13].ToString();
            //                    cchdModel.mst = tableDanhba.Rows[0][0].ToString();
            //                    cchdModel.sodangkykd = tableDanhba.Rows[0][29].ToString();
            //                    cchdModel.ngaycapdangkykd = tableDanhba.Rows[0][30].ToString();
            //                    cchdModel.ngaycohieuluc = tableCCHDTMS.Rows.Count > 0 && tableCCHDTMS.Rows[0][41].ToString() != "" ? Convert.ToDateTime(tableCCHDTMS.Rows[0][41]) : DateTime.Today;
            //                    cchdModel.ngayhethieuluc = tableCCHDTMS.Rows.Count > 0 && tableCCHDTMS.Rows[0][42].ToString() != "" ? Convert.ToDateTime(tableCCHDTMS.Rows[0][42]) : DateTime.Today;
            //                    cchdModel.nganhnghekd = tableDanhba.Rows[0][3].ToString();
            //                    cchdModel.ngayqdcctk = tableCCHDTMS.Rows.Count > 0 && tableDanhba.Rows[0][40].ToString() != "" ? tableDanhba.Rows[0][40].ToString() : DateTime.Now.ToString("dd/MM/yyyy");
            //                    cchdModel.soqdcctk = tableCCHDTMS.Rows.Count > 0 ? tableCCHDTMS.Rows[0]["Số QĐ cưỡng chế"].ToString() : "";
            //                    cchdModel.quy = "Quý 3";
            //                    cchdModel.canbo = !string.IsNullOrWhiteSpace(CCHD_TenCanBo.Text) ? CCHD_TenCanBo.Text : "";
            //                    #endregion

            //                    #region Replace
            //                    // QĐ+TB có hoá đơn
            //                    docQDCoHD.Replace("{{CHICUC}}", cchdModel.chicuc.ToUpper().Replace("HUYỆN", "H.").Replace("QUẬN", "Q.").Replace("KHU VỰC", "KV."), false, true);
            //                    docQDCoHD.Replace("{{THONGBAOSO}}", cchdModel.thongbaoso, false, true);
            //                    docQDCoHD.Replace("{{NGAYTHONGBAO}}", cchdModel.ngaythongbao, false, true);
            //                    docQDCoHD.Replace("{{chicuc}}", cchdModel.chicuc, false, true);
            //                    docQDCoHD.Replace("{{TENNNT}}", cchdModel.tennnt, false, true);
            //                    docQDCoHD.Replace("{{DCTBT_DIACHI}}", cchdModel.dctbt_diachi, false, true);
            //                    docQDCoHD.Replace("{{DCTBT_PHUONGXA}}", cchdModel.dctbt_phuongxa, false, true);
            //                    docQDCoHD.Replace("{{DCTBT_QUANHUYEN}}", cchdModel.dctbt_quanhuyen, false, true);
            //                    docQDCoHD.Replace("{{DCTBT_TINHTHANHPHO}}", cchdModel.dctbt_tinhthanhpho, false, true);
            //                    docQDCoHD.Replace("{{DCTS_DIACHI}}", cchdModel.dcts_diachi, false, true);
            //                    docQDCoHD.Replace("{{DCTS_PHUONGXA}}", cchdModel.dcts_phuongxa, false, true);
            //                    docQDCoHD.Replace("{{DCTS_QUANHUYEN}}", cchdModel.dcts_quanhuyen, false, true);
            //                    docQDCoHD.Replace("{{DCTS_TINHTHANHPHO}}", cchdModel.dcts_tinhthanhpho, false, true);
            //                    docQDCoHD.Replace("{{NGANHNGHEKD}}", cchdModel.nganhnghekd, false, true);
            //                    docQDCoHD.Replace("{{NGAYCOHIEULUC}}", cchdModel.ngaycohieuluc.ToString("dd/MM/yyyy"), false, true);
            //                    docQDCoHD.Replace("{{NGAYHETHIEULUC}}", cchdModel.ngayhethieuluc.ToString("dd/MM/yyyy"), false, true);
            //                    docQDCoHD.Replace("{{SODANGKYKD}}", cchdModel.sodangkykd, false, true);
            //                    docQDCoHD.Replace("{{NGAYCAPDANGKYKD}}", cchdModel.ngaycapdangkykd, false, true);
            //                    docQDCoHD.Replace("{{MST}}", cchdModel.mst, false, true);
            //                    docQDCoHD.Replace("({{NGAYCOHIEULUCDANGTEXT}}", "ngày " + cchdModel.ngaycohieuluc.Day + " tháng " + cchdModel.ngaycohieuluc.Month + " năm " + cchdModel.ngaycohieuluc.Year, false, true);
            //                    // QĐ + TB không hoá đơn
            //                    docQDKhongHD.Replace("{{CHICUC}}", cchdModel.chicuc.ToUpper().Replace("HUYỆN", "H.").Replace("QUẬN", "Q.").Replace("KHU VỰC", "KV."), false, true);
            //                    docQDKhongHD.Replace("{{THONGBAOSO}}", cchdModel.thongbaoso, false, true);
            //                    docQDKhongHD.Replace("{{NGAYTHONGBAO}}", cchdModel.ngaythongbao, false, true);
            //                    docQDKhongHD.Replace("{{chicuc}}", cchdModel.chicuc, false, true);
            //                    docQDKhongHD.Replace("{{TENNNT}}", cchdModel.tennnt, false, true);
            //                    docQDKhongHD.Replace("{{DCTBT_DIACHI}}", cchdModel.dctbt_diachi, false, true);
            //                    docQDKhongHD.Replace("{{DCTBT_PHUONGXA}}", cchdModel.dctbt_phuongxa, false, true);
            //                    docQDKhongHD.Replace("{{DCTBT_QUANHUYEN}}", cchdModel.dctbt_quanhuyen, false, true);
            //                    docQDKhongHD.Replace("{{DCTBT_TINHTHANHPHO}}", cchdModel.dctbt_tinhthanhpho, false, true);
            //                    docQDKhongHD.Replace("{{DCTS_DIACHI}}", cchdModel.dcts_diachi, false, true);
            //                    docQDKhongHD.Replace("{{DCTS_PHUONGXA}}", cchdModel.dcts_phuongxa, false, true);
            //                    docQDKhongHD.Replace("{{DCTS_QUANHUYEN}}", cchdModel.dcts_quanhuyen, false, true);
            //                    docQDKhongHD.Replace("{{DCTS_TINHTHANHPHO}}", cchdModel.dcts_tinhthanhpho, false, true);
            //                    docQDKhongHD.Replace("{{NGANHNGHEKD}}", cchdModel.nganhnghekd, false, true);
            //                    docQDKhongHD.Replace("{{NGAYCOHIEULUC}}", cchdModel.ngaycohieuluc.ToString("dd/MM/yyyy"), false, true);
            //                    docQDKhongHD.Replace("{{NGAYHETHIEULUC}}", cchdModel.ngayhethieuluc.ToString("dd/MM/yyyy"), false, true);
            //                    docQDKhongHD.Replace("{{SODANGKYKD}}", cchdModel.sodangkykd, false, true);
            //                    docQDKhongHD.Replace("{{NGAYCAPDANGKYKD}}", cchdModel.ngaycapdangkykd, false, true);
            //                    docQDKhongHD.Replace("{{MST}}", cchdModel.mst, false, true);
            //                    docQDKhongHD.Replace("({{NGAYCOHIEULUCDANGTEXT}}", "ngày " + cchdModel.ngaycohieuluc.Day + " tháng " + cchdModel.ngaycohieuluc.Month + " năm " + cchdModel.ngaycohieuluc.Year, false, true);
            //                    // Tờ trình có hoá đơn
            //                    docTTCoHD.Replace("{{CHICUC}}", cchdModel.chicuc.ToUpper().Replace("HUYỆN", "H.").Replace("QUẬN", "Q.").Replace("KHU VỰC", "KV."), false, true);
            //                    docTTCoHD.Replace("{{NGAYQDCCTK}}", cchdModel.ngayqdcctk, false, true);
            //                    docTTCoHD.Replace("{{SOQDCCTK}}", cchdModel.soqdcctk, false, true);
            //                    docTTCoHD.Replace("{{TENNNT}}", cchdModel.tennnt, false, true);
            //                    docTTCoHD.Replace("{{MST}}", cchdModel.mst, false, true);
            //                    docTTCoHD.Replace("{{DCTBT_DIACHI}}", cchdModel.dctbt_diachi, false, true);
            //                    docTTCoHD.Replace("{{DCTBT_PHUONGXA}}", cchdModel.dctbt_phuongxa, false, true);
            //                    docTTCoHD.Replace("{{DCTBT_QUANHUYEN}}", cchdModel.dctbt_quanhuyen, false, true);
            //                    docTTCoHD.Replace("{{DCTBT_TINHTHANHPHO}}", cchdModel.dctbt_tinhthanhpho, false, true);
            //                    docTTCoHD.Replace("{{DCTS_DIACHI}}", cchdModel.dcts_diachi, false, true);
            //                    docTTCoHD.Replace("{{DCTS_PHUONGXA}}", cchdModel.dcts_phuongxa, false, true);
            //                    docTTCoHD.Replace("{{DCTS_QUANHUYEN}}", cchdModel.dcts_quanhuyen, false, true);
            //                    docTTCoHD.Replace("{{DCTS_TINHTHANHPHO}}", cchdModel.dcts_tinhthanhpho, false, true);
            //                    docTTCoHD.Replace("{{THONGBAOSO}}", cchdModel.thongbaoso, false, true);
            //                    docTTCoHD.Replace("{{NGAYTHONGBAO}}", cchdModel.ngaythongbao, false, true);
            //                    docTTCoHD.Replace("{{QUY}}", cchdModel.quy, false, true);
            //                    docTTCoHD.Replace("{{CANBO}}", cchdModel.canbo, false, true);
            //                    // Tờ trình không có hoá đơn
            //                    docTTKhongHD.Replace("{{CHICUC}}", cchdModel.chicuc.ToUpper().Replace("HUYỆN", "H.").Replace("QUẬN", "Q.").Replace("KHU VỰC", "KV."), false, true);
            //                    docTTKhongHD.Replace("{{NGAYQDCCTK}}", cchdModel.ngayqdcctk, false, true);
            //                    docTTKhongHD.Replace("{{SOQDCCTK}}", cchdModel.soqdcctk, false, true);
            //                    docTTKhongHD.Replace("{{TENNNT}}", cchdModel.tennnt, false, true);
            //                    docTTKhongHD.Replace("{{MST}}", cchdModel.mst, false, true);
            //                    docTTKhongHD.Replace("{{DCTBT_DIACHI}}", cchdModel.dctbt_diachi, false, true);
            //                    docTTKhongHD.Replace("{{DCTBT_PHUONGXA}}", cchdModel.dctbt_phuongxa, false, true);
            //                    docTTKhongHD.Replace("{{DCTBT_QUANHUYEN}}", cchdModel.dctbt_quanhuyen, false, true);
            //                    docTTKhongHD.Replace("{{DCTBT_TINHTHANHPHO}}", cchdModel.dctbt_tinhthanhpho, false, true);
            //                    docTTKhongHD.Replace("{{DCTS_DIACHI}}", cchdModel.dcts_diachi, false, true);
            //                    docTTKhongHD.Replace("{{DCTS_PHUONGXA}}", cchdModel.dcts_phuongxa, false, true);
            //                    docTTKhongHD.Replace("{{DCTS_QUANHUYEN}}", cchdModel.dcts_quanhuyen, false, true);
            //                    docTTKhongHD.Replace("{{DCTS_TINHTHANHPHO}}", cchdModel.dcts_tinhthanhpho, false, true);
            //                    docTTKhongHD.Replace("{{THONGBAOSO}}", cchdModel.thongbaoso, false, true);
            //                    docTTKhongHD.Replace("{{NGAYTHONGBAO}}", cchdModel.ngaythongbao, false, true);
            //                    docTTKhongHD.Replace("{{QUY}}", cchdModel.quy, false, true);
            //                    docTTKhongHD.Replace("{{CANBO}}", cchdModel.canbo, false, true);
            //                    docTTKhongHD.Replace("{{NGAYCOHIEULUC}}", cchdModel.ngaycohieuluc.ToString("dd/MM/yyyy"), false, true);
            //                    docTTKhongHD.Replace("{{NGAYHETHIEULUC}}", cchdModel.ngayhethieuluc.ToString("dd/MM/yyyy"), false, true);
            //                    #endregion

            //                    #region Replace Table: QĐ+TB có hoá đơn + Tờ trình có hoá đơn bảng 2
            //                    Section section = docQDCoHD.Sections[0];
            //                    TextSelection selection = docQDCoHD.FindString("{{TABLE}}", true, true);
            //                    TextRange range = selection.GetAsOneRange();
            //                    Paragraph paragraph = range.OwnerParagraph;
            //                    Body body = paragraph.OwnerTextBody;
            //                    int index = body.ChildObjects.IndexOf(paragraph);
            //                    Table tableNo = section.AddTable(true);

            //                    Section sectionTTCoHDTB2 = docTTCoHD.Sections[0];
            //                    TextSelection selectionTTCoHDTB2 = docTTCoHD.FindString("{{TABLE2}}", true, true);
            //                    TextRange rangeTTCoHDTB2 = selectionTTCoHDTB2.GetAsOneRange();
            //                    Paragraph paragraphTTCoHDTB2 = rangeTTCoHDTB2.OwnerParagraph;
            //                    Body bodyTTCoHDTB2 = paragraphTTCoHDTB2.OwnerTextBody;
            //                    int indexTTCoHDTB2 = bodyTTCoHDTB2.ChildObjects.IndexOf(paragraphTTCoHDTB2);
            //                    Table tableNoTTCoHDTB2 = sectionTTCoHDTB2.AddTable(true);

            //                    String[] headerQDCoHD = new String[] { "STT", "Loại hoá đơn", "Ký hiệu mẫu", "Ký hiệu hoá đơn", "Từ số - đến số", "Ghi chú" };
            //                    // Tạo dữ liệu demo
            //                    cchdModel.hoadons = new List<HoaDon>();
            //                    var hoadon = new HoaDon();
            //                    hoadon.stt = 1;
            //                    hoadon.loaihoadon = "";
            //                    hoadon.kyhieumau = "";
            //                    hoadon.kyhieuhoadon = "";
            //                    hoadon.tusodenso = tableCCHDTMS.Rows.Count > 0 ? tableCCHDTMS.Rows[0]["Số hóa đơn"].ToString() : "";
            //                    hoadon.ghichu = "";
            //                    cchdModel.hoadons.Add(hoadon);
            //                    //var hoadon1 = new HoaDon();
            //                    //hoadon1.stt = 2;
            //                    //hoadon1.loaihoadon = "Hoá đơn TNCN";
            //                    //hoadon1.kyhieumau = "HD_MS_CN_00151";
            //                    //hoadon1.kyhieuhoadon = "HD_KH_CN_1021021";
            //                    //hoadon1.tusodenso = "Từ số 1 đến 3";
            //                    //hoadon1.ghichu = "Số hóa tồn trên cơ sở dữ liệu của cơ quan thuế";
            //                    //cchdModel.hoadons.Add(hoadon1);
            //                    //
            //                    tableNo.ResetCells(cchdModel.hoadons.Count + 1, headerQDCoHD.Length);
            //                    TableRow FRow = tableNo.Rows[0];
            //                    FRow.IsHeader = true;

            //                    tableNoTTCoHDTB2.ResetCells(cchdModel.hoadons.Count + 1, headerQDCoHD.Length);
            //                    TableRow FRowTB2 = tableNoTTCoHDTB2.Rows[0];
            //                    FRowTB2.IsHeader = true;
            //                    // Header Row
            //                    for (int i = 0; i < headerQDCoHD.Length; i++)
            //                    {
            //                        //Cell Alignment
            //                        Paragraph p = FRow.Cells[i].AddParagraph();
            //                        FRow.Cells[i].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //                        p.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //                        //Data Format
            //                        TextRange TR = p.AppendText(headerQDCoHD[i]);
            //                        TR.CharacterFormat.FontName = "Times New Roman";
            //                        TR.CharacterFormat.FontSize = 13;

            //                        Paragraph pTB2 = FRowTB2.Cells[i].AddParagraph();
            //                        FRowTB2.Cells[i].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //                        pTB2.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //                        TextRange TRTB2 = pTB2.AppendText(headerQDCoHD[i]);
            //                        TRTB2.CharacterFormat.FontName = "Times New Roman";
            //                        TRTB2.CharacterFormat.FontSize = 13;

            //                        switch (i)
            //                        {
            //                            case 0:
            //                                FRow.Cells[i].SetCellWidth(5, CellWidthType.Percentage);
            //                                FRowTB2.Cells[i].SetCellWidth(5, CellWidthType.Percentage);
            //                                break;
            //                            case 1:
            //                                FRow.Cells[i].SetCellWidth(10, CellWidthType.Percentage);
            //                                FRowTB2.Cells[i].SetCellWidth(10, CellWidthType.Percentage);
            //                                break;
            //                            case 2:
            //                                FRow.Cells[i].SetCellWidth(20, CellWidthType.Percentage);
            //                                FRowTB2.Cells[i].SetCellWidth(20, CellWidthType.Percentage);
            //                                break;
            //                            case 3:
            //                                FRow.Cells[i].SetCellWidth(20, CellWidthType.Percentage);
            //                                FRowTB2.Cells[i].SetCellWidth(20, CellWidthType.Percentage);
            //                                break;
            //                            case 4:
            //                                FRow.Cells[i].SetCellWidth(20, CellWidthType.Percentage);
            //                                FRowTB2.Cells[i].SetCellWidth(20, CellWidthType.Percentage);
            //                                break;
            //                            case 5:
            //                                FRow.Cells[i].SetCellWidth(25, CellWidthType.Percentage);
            //                                FRowTB2.Cells[i].SetCellWidth(25, CellWidthType.Percentage);
            //                                break;
            //                        }
            //                    }

            //                    //Data Row
            //                    for (int r = 0; r < cchdModel.hoadons.Count; r++)
            //                    {
            //                        #region QĐ+TB có hoá đơn
            //                        TableRow DataRow = tableNo.Rows[r + 1];
            //                        // 1. STT
            //                        DataRow.Cells[0].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //                        Paragraph p0 = DataRow.Cells[0].AddParagraph();
            //                        TextRange TR0 = p0.AppendText(cchdModel.hoadons[r].stt.ToString());
            //                        p0.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //                        TR0.CharacterFormat.FontName = "Times New Roman";
            //                        TR0.CharacterFormat.FontSize = 13;
            //                        DataRow.Cells[0].SetCellWidth(5, CellWidthType.Percentage);
            //                        // 2. Loại hoá đơn
            //                        DataRow.Cells[1].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //                        Paragraph p1 = DataRow.Cells[1].AddParagraph();
            //                        TextRange TR1 = p1.AppendText(cchdModel.hoadons[r].loaihoadon);
            //                        p1.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //                        TR1.CharacterFormat.FontName = "Times New Roman";
            //                        TR1.CharacterFormat.FontSize = 13;
            //                        DataRow.Cells[1].SetCellWidth(10, CellWidthType.Percentage);
            //                        // 3. Ký hiệu mẫu
            //                        DataRow.Cells[2].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //                        Paragraph p2 = DataRow.Cells[2].AddParagraph();
            //                        TextRange TR2 = p2.AppendText(cchdModel.hoadons[r].kyhieumau);
            //                        p2.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //                        TR2.CharacterFormat.FontName = "Times New Roman";
            //                        TR2.CharacterFormat.FontSize = 13;
            //                        DataRow.Cells[2].SetCellWidth(20, CellWidthType.Percentage);
            //                        // 4. Ký hiệu hoá đơn
            //                        DataRow.Cells[3].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //                        Paragraph p3 = DataRow.Cells[3].AddParagraph();
            //                        TextRange TR3 = p3.AppendText(cchdModel.hoadons[r].kyhieuhoadon);
            //                        p3.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //                        TR3.CharacterFormat.FontName = "Times New Roman";
            //                        TR3.CharacterFormat.FontSize = 13;
            //                        DataRow.Cells[3].SetCellWidth(20, CellWidthType.Percentage);
            //                        // 5. Từ ngày tới ngày
            //                        DataRow.Cells[4].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //                        Paragraph p4 = DataRow.Cells[4].AddParagraph();
            //                        TextRange TR4 = p4.AppendText(cchdModel.hoadons[r].tusodenso);
            //                        p4.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //                        TR4.CharacterFormat.FontName = "Times New Roman";
            //                        TR4.CharacterFormat.FontSize = 13;
            //                        DataRow.Cells[4].SetCellWidth(20, CellWidthType.Percentage);
            //                        // 6. Ghi chú
            //                        DataRow.Cells[5].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //                        Paragraph p5 = DataRow.Cells[5].AddParagraph();
            //                        TextRange TR5 = p5.AppendText(cchdModel.hoadons[r].ghichu);
            //                        p5.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //                        TR5.CharacterFormat.FontName = "Times New Roman";
            //                        TR5.CharacterFormat.FontSize = 13;
            //                        DataRow.Cells[5].SetCellWidth(25, CellWidthType.Percentage);
            //                        #endregion


            //                        #region Tờ trình có hoá đơn Table 2
            //                        TableRow DataRowTTCoHDTB2 = tableNoTTCoHDTB2.Rows[r + 1];
            //                        // 1. STT
            //                        DataRowTTCoHDTB2.Cells[0].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //                        Paragraph p0TTCoHDTB2 = DataRowTTCoHDTB2.Cells[0].AddParagraph();
            //                        TextRange TR0TTCoHDTB2 = p0TTCoHDTB2.AppendText(cchdModel.hoadons[r].stt.ToString());
            //                        p0TTCoHDTB2.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //                        TR0TTCoHDTB2.CharacterFormat.FontName = "Times New Roman";
            //                        TR0TTCoHDTB2.CharacterFormat.FontSize = 13;
            //                        DataRowTTCoHDTB2.Cells[0].SetCellWidth(5, CellWidthType.Percentage);
            //                        // 2. Loại hoá đơn
            //                        DataRowTTCoHDTB2.Cells[1].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //                        Paragraph p1TTCoHDTB2 = DataRowTTCoHDTB2.Cells[1].AddParagraph();
            //                        TextRange TR1TTCoHDTB2 = p1TTCoHDTB2.AppendText(cchdModel.hoadons[r].loaihoadon);
            //                        p1TTCoHDTB2.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //                        TR1TTCoHDTB2.CharacterFormat.FontName = "Times New Roman";
            //                        TR1TTCoHDTB2.CharacterFormat.FontSize = 13;
            //                        DataRowTTCoHDTB2.Cells[1].SetCellWidth(10, CellWidthType.Percentage);
            //                        // 3. Ký hiệu mẫu
            //                        DataRowTTCoHDTB2.Cells[2].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //                        Paragraph p2TTCoHDTB2 = DataRowTTCoHDTB2.Cells[2].AddParagraph();
            //                        TextRange TR2TTCoHDTB2 = p2TTCoHDTB2.AppendText(cchdModel.hoadons[r].kyhieumau);
            //                        p2TTCoHDTB2.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //                        TR2TTCoHDTB2.CharacterFormat.FontName = "Times New Roman";
            //                        TR2TTCoHDTB2.CharacterFormat.FontSize = 13;
            //                        DataRowTTCoHDTB2.Cells[2].SetCellWidth(20, CellWidthType.Percentage);
            //                        // 4. Ký hiệu hoá đơn
            //                        DataRowTTCoHDTB2.Cells[3].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //                        Paragraph p3TTCoHDTB2 = DataRowTTCoHDTB2.Cells[3].AddParagraph();
            //                        TextRange TR3TTCoHDTB2 = p3TTCoHDTB2.AppendText(cchdModel.hoadons[r].kyhieuhoadon);
            //                        p3TTCoHDTB2.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //                        TR3TTCoHDTB2.CharacterFormat.FontName = "Times New Roman";
            //                        TR3TTCoHDTB2.CharacterFormat.FontSize = 13;
            //                        DataRowTTCoHDTB2.Cells[3].SetCellWidth(20, CellWidthType.Percentage);
            //                        // 5. Từ ngày tới ngày
            //                        DataRowTTCoHDTB2.Cells[4].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //                        Paragraph p4TTCoHDTB2 = DataRowTTCoHDTB2.Cells[4].AddParagraph();
            //                        TextRange TR4TTCoHDTB2 = p4TTCoHDTB2.AppendText(cchdModel.hoadons[r].tusodenso);
            //                        p4TTCoHDTB2.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //                        TR4TTCoHDTB2.CharacterFormat.FontName = "Times New Roman";
            //                        TR4TTCoHDTB2.CharacterFormat.FontSize = 13;
            //                        DataRowTTCoHDTB2.Cells[4].SetCellWidth(20, CellWidthType.Percentage);
            //                        // 6. Ghi chú
            //                        DataRowTTCoHDTB2.Cells[5].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //                        Paragraph p5TTCoHDTB2 = DataRowTTCoHDTB2.Cells[5].AddParagraph();
            //                        TextRange TR5TTCoHDTB2 = p5TTCoHDTB2.AppendText(cchdModel.hoadons[r].ghichu);
            //                        p5TTCoHDTB2.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //                        TR5TTCoHDTB2.CharacterFormat.FontName = "Times New Roman";
            //                        TR5TTCoHDTB2.CharacterFormat.FontSize = 13;
            //                        DataRowTTCoHDTB2.Cells[5].SetCellWidth(25, CellWidthType.Percentage);
            //                        #endregion

            //                    }

            //                    body.ChildObjects.Remove(paragraph);
            //                    body.ChildObjects.Insert(index, tableNo);
            //                    bodyTTCoHDTB2.ChildObjects.Remove(paragraphTTCoHDTB2);
            //                    bodyTTCoHDTB2.ChildObjects.Insert(indexTTCoHDTB2, tableNoTTCoHDTB2);
            //                    #endregion

            //                    #region Replace Table: Số tiền cưỡng chế nợ kỳ này (Tờ trình có hoá đơn & không có hoá đơn)
            //                    // Tờ trình có hoá đơn
            //                    Section sectionTTCoHD = docTTCoHD.Sections[0];
            //                    TextSelection selectionTTCoHD = docTTCoHD.FindString("{{TABLEST}}", true, true);
            //                    TextRange rangeTTCoHD = selectionTTCoHD.GetAsOneRange();
            //                    Paragraph paragraphTTCoHD = rangeTTCoHD.OwnerParagraph;
            //                    Body bodyTTCoHD = paragraphTTCoHD.OwnerTextBody;
            //                    int indexTTCoHD = bodyTTCoHD.ChildObjects.IndexOf(paragraphTTCoHD);
            //                    Table tableNoTTCoHD = sectionTTCoHD.AddTable(true);
            //                    // Tờ trình không có hoá đơn
            //                    Section sectionTTKhongHD = docTTKhongHD.Sections[0];
            //                    TextSelection selectionTTKhongHD = docTTKhongHD.FindString("{{TABLE}}", true, true);
            //                    TextRange rangeTTKhongHD = selectionTTKhongHD.GetAsOneRange();
            //                    Paragraph paragraphTTKhongHD = rangeTTKhongHD.OwnerParagraph;
            //                    Body bodyTTKhongHD = paragraphTTKhongHD.OwnerTextBody;
            //                    int indexTTKhongHD = bodyTTKhongHD.ChildObjects.IndexOf(paragraphTTKhongHD);
            //                    Table tableNoTTKhongHD = sectionTTKhongHD.AddTable(true);

            //                    String[] headerLicence = new string[] { "TT", "Tiểu mục", "Loại thuế", "Trong đó số nợ trên 90 ngày trên QĐCC trích tiền TKNH(đ)", "Số đã nộp từ thời điểm ban hành QĐCC trích tiền TKNH đến ngày đề nghị cưỡng chế TB ngừng sử dụng hóa đơn (đ)", "Số nợ trên 120 ngày đề nghị cưỡng chế TB ngừng sử dụng hóa đơn (đ)" };


            //                    // Tờ trình có hoá đơn
            //                    tableNoTTCoHD.ResetCells(cchdModel.listThue.Count + 2, headerLicence.Length);
            //                    TableRow FRowTTCoHD = tableNoTTCoHD.Rows[0];
            //                    FRowTTCoHD.IsHeader = true;
            //                    //Tờ trình không có hoá đơn
            //                    tableNoTTKhongHD.ResetCells(cchdModel.listThue.Count + 2, headerLicence.Length);
            //                    TableRow FRowTTKhongHD = tableNoTTKhongHD.Rows[0];
            //                    FRowTTKhongHD.IsHeader = true;
            //                    for (int i = 0; i < headerLicence.Length; i++)
            //                    {
            //                        //Cell Alignment
            //                        // Tờ trình có hoá đơn
            //                        Paragraph p = FRowTTCoHD.Cells[i].AddParagraph();
            //                        FRowTTCoHD.Cells[i].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //                        p.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //                        //Data Format
            //                        TextRange TR = p.AppendText(headerLicence[i]);
            //                        TR.CharacterFormat.FontName = "Times New Roman";
            //                        TR.CharacterFormat.FontSize = 13;
            //                        //Tờ trình không có hoá đơn
            //                        Paragraph pNo = FRowTTKhongHD.Cells[i].AddParagraph();
            //                        FRowTTKhongHD.Cells[i].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //                        pNo.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //                        TextRange TRNo = pNo.AppendText(headerLicence[i]);
            //                        TRNo.CharacterFormat.FontName = "Times New Roman";
            //                        TRNo.CharacterFormat.FontSize = 13;
            //                        switch (i)
            //                        {
            //                            case 0:
            //                                FRowTTCoHD.Cells[i].SetCellWidth(5, CellWidthType.Percentage);
            //                                FRowTTKhongHD.Cells[i].SetCellWidth(5, CellWidthType.Percentage); break;
            //                            case 1:
            //                                FRowTTCoHD.Cells[i].SetCellWidth(10, CellWidthType.Percentage);
            //                                FRowTTKhongHD.Cells[i].SetCellWidth(10, CellWidthType.Percentage); break;
            //                            case 2:
            //                                FRowTTCoHD.Cells[i].SetCellWidth(20, CellWidthType.Percentage);
            //                                FRowTTKhongHD.Cells[i].SetCellWidth(20, CellWidthType.Percentage); break;
            //                            case 3:
            //                                FRowTTCoHD.Cells[i].SetCellWidth(20, CellWidthType.Percentage);
            //                                FRowTTKhongHD.Cells[i].SetCellWidth(20, CellWidthType.Percentage); break;
            //                            case 4:
            //                                FRowTTCoHD.Cells[i].SetCellWidth(25, CellWidthType.Percentage);
            //                                FRowTTKhongHD.Cells[i].SetCellWidth(25, CellWidthType.Percentage); break;
            //                            case 5:
            //                                FRowTTCoHD.Cells[i].SetCellWidth(20, CellWidthType.Percentage);
            //                                FRowTTKhongHD.Cells[i].SetCellWidth(20, CellWidthType.Percentage); break;
            //                        }
            //                    }

            //                    //Data Row
            //                    for (int r = 0; r < cchdModel.listThue.Count; r++)
            //                    {
            //                        #region Tờ trình có hoá đơn
            //                        TableRow DataRow = tableNoTTCoHD.Rows[r + 1];
            //                        // 1. STT
            //                        DataRow.Cells[0].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //                        Paragraph p0 = DataRow.Cells[0].AddParagraph();
            //                        TextRange TR0 = p0.AppendText(cchdModel.listThue[r].stt.ToString());
            //                        p0.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //                        TR0.CharacterFormat.FontName = "Times New Roman";
            //                        TR0.CharacterFormat.FontSize = 13;
            //                        DataRow.Cells[0].SetCellWidth(5, CellWidthType.Percentage);
            //                        // 2. Tiểu mục
            //                        DataRow.Cells[1].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //                        Paragraph p1 = DataRow.Cells[1].AddParagraph();
            //                        TextRange TR1 = p1.AppendText(cchdModel.listThue[r].tieumuc.ToString());
            //                        p1.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //                        TR1.CharacterFormat.FontName = "Times New Roman";
            //                        TR1.CharacterFormat.FontSize = 13;
            //                        DataRow.Cells[1].SetCellWidth(10, CellWidthType.Percentage);
            //                        // 3. Tiểu mục
            //                        DataRow.Cells[2].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //                        Paragraph p2 = DataRow.Cells[2].AddParagraph();
            //                        TextRange TR2 = p2.AppendText(cchdModel.listThue[r].loaithue.ToString());
            //                        p2.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //                        TR2.CharacterFormat.FontName = "Times New Roman";
            //                        TR2.CharacterFormat.FontSize = 13;
            //                        DataRow.Cells[2].SetCellWidth(20, CellWidthType.Percentage);
            //                        // 4. Trong đó số nợ từ 90 ngày trở lên (đ)
            //                        DataRow.Cells[3].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //                        Paragraph p3 = DataRow.Cells[3].AddParagraph();
            //                        TextRange TR3 = p3.AppendText(cchdModel.listThue[r].notren90ngay > 0 ? cchdModel.listThue[r].notren90ngay.ToString("#,###") : "0");
            //                        p3.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //                        TR3.CharacterFormat.FontName = "Times New Roman";
            //                        TR3.CharacterFormat.FontSize = 13;
            //                        DataRow.Cells[3].SetCellWidth(20, CellWidthType.Percentage);
            //                        // 5. Số tiền đa nộp từ thời điểm banhành thông báo đến ngày đề nghị cưỡng chế nợ (đ)
            //                        DataRow.Cells[4].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //                        Paragraph p4 = DataRow.Cells[4].AddParagraph();
            //                        TextRange TR4 = p4.AppendText(cchdModel.listThue[r].sotiennop > 0 ? cchdModel.listThue[r].sotiennop.ToString("#,###") : "0");
            //                        p4.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //                        TR4.CharacterFormat.FontName = "Times New Roman";
            //                        TR4.CharacterFormat.FontSize = 13;
            //                        DataRow.Cells[4].SetCellWidth(25, CellWidthType.Percentage);
            //                        // 6. Số nợ từ 91 ngày trở lên đề nghị cưỡng chế nợ kỳ này (d)
            //                        DataRow.Cells[5].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //                        Paragraph p5 = DataRow.Cells[5].AddParagraph();
            //                        TextRange TR5 = p5.AppendText(cchdModel.listThue[r].notren91ngaycuongche > 0 ? cchdModel.listThue[r].notren91ngaycuongche.ToString("#,###") : "0");
            //                        p5.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //                        TR5.CharacterFormat.FontName = "Times New Roman";
            //                        TR5.CharacterFormat.FontSize = 13;
            //                        DataRow.Cells[5].SetCellWidth(20, CellWidthType.Percentage);
            //                        #endregion

            //                        #region Tờ trình không có hoá đơn
            //                        TableRow DataRowNo = tableNoTTKhongHD.Rows[r + 1];
            //                        // 1. STT
            //                        DataRowNo.Cells[0].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //                        Paragraph p0No = DataRowNo.Cells[0].AddParagraph();
            //                        TextRange TR0No = p0No.AppendText(cchdModel.listThue[r].stt.ToString());
            //                        p0No.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //                        TR0No.CharacterFormat.FontName = "Times New Roman";
            //                        TR0No.CharacterFormat.FontSize = 13;
            //                        DataRowNo.Cells[0].SetCellWidth(5, CellWidthType.Percentage);
            //                        // 2. Tiểu mục
            //                        DataRowNo.Cells[1].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //                        Paragraph p1No = DataRowNo.Cells[1].AddParagraph();
            //                        TextRange TR1No = p1No.AppendText(cchdModel.listThue[r].tieumuc.ToString());
            //                        p1No.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //                        TR1No.CharacterFormat.FontName = "Times New Roman";
            //                        TR1No.CharacterFormat.FontSize = 13;
            //                        DataRowNo.Cells[1].SetCellWidth(10, CellWidthType.Percentage);
            //                        // 3. Tiểu mục
            //                        DataRowNo.Cells[2].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //                        Paragraph p2No = DataRowNo.Cells[2].AddParagraph();
            //                        TextRange TR2No = p2No.AppendText(cchdModel.listThue[r].loaithue.ToString());
            //                        p2No.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //                        TR2No.CharacterFormat.FontName = "Times New Roman";
            //                        TR2No.CharacterFormat.FontSize = 13;
            //                        DataRowNo.Cells[2].SetCellWidth(20, CellWidthType.Percentage);
            //                        // 4. Trong đó số nợ từ 90 ngày trở lên (đ)
            //                        DataRowNo.Cells[3].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //                        Paragraph p3No = DataRowNo.Cells[3].AddParagraph();
            //                        TextRange TR3No = p3No.AppendText(cchdModel.listThue[r].notren90ngay > 0 ? cchdModel.listThue[r].notren90ngay.ToString("#,###") : "0");
            //                        p3No.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //                        TR3No.CharacterFormat.FontName = "Times New Roman";
            //                        TR3No.CharacterFormat.FontSize = 13;
            //                        DataRowNo.Cells[3].SetCellWidth(20, CellWidthType.Percentage);
            //                        // 5. Số tiền đa nộp từ thời điểm banhành thông báo đến ngày đề nghị cưỡng chế nợ (đ)
            //                        DataRowNo.Cells[4].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //                        Paragraph p4No = DataRowNo.Cells[4].AddParagraph();
            //                        TextRange TR4No = p4No.AppendText(cchdModel.listThue[r].sotiennop > 0 ? cchdModel.listThue[r].sotiennop.ToString("#,###") : "0");
            //                        p4No.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //                        TR4No.CharacterFormat.FontName = "Times New Roman";
            //                        TR4No.CharacterFormat.FontSize = 13;
            //                        DataRowNo.Cells[4].SetCellWidth(25, CellWidthType.Percentage);
            //                        // 6. Số nợ từ 91 ngày trở lên đề nghị cưỡng chế nợ kỳ này (d)
            //                        DataRowNo.Cells[5].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //                        Paragraph p5No = DataRowNo.Cells[5].AddParagraph();
            //                        TextRange TR5No = p5No.AppendText(cchdModel.listThue[r].notren91ngaycuongche > 0 ? cchdModel.listThue[r].notren91ngaycuongche.ToString("#,###") : "0");
            //                        p5No.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //                        TR5No.CharacterFormat.FontName = "Times New Roman";
            //                        TR5No.CharacterFormat.FontSize = 13;
            //                        DataRowNo.Cells[5].SetCellWidth(20, CellWidthType.Percentage);
            //                        #endregion
            //                    }
            //                    tableNoTTCoHD.Rows[cchdModel.listThue.Count + 1].Cells[0].SetCellWidth(5, CellWidthType.Percentage);
            //                    tableNoTTCoHD.Rows[cchdModel.listThue.Count + 1].Cells[1].SetCellWidth(10, CellWidthType.Percentage);
            //                    tableNoTTCoHD.Rows[cchdModel.listThue.Count + 1].Cells[2].SetCellWidth(20, CellWidthType.Percentage);
            //                    tableNoTTCoHD.Rows[cchdModel.listThue.Count + 1].Cells[3].SetCellWidth(20, CellWidthType.Percentage);
            //                    tableNoTTCoHD.Rows[cchdModel.listThue.Count + 1].Cells[4].SetCellWidth(25, CellWidthType.Percentage);
            //                    tableNoTTCoHD.Rows[cchdModel.listThue.Count + 1].Cells[5].SetCellWidth(20, CellWidthType.Percentage);
            //                    tableNoTTKhongHD.Rows[cchdModel.listThue.Count + 1].Cells[0].SetCellWidth(5, CellWidthType.Percentage);
            //                    tableNoTTKhongHD.Rows[cchdModel.listThue.Count + 1].Cells[1].SetCellWidth(10, CellWidthType.Percentage);
            //                    tableNoTTKhongHD.Rows[cchdModel.listThue.Count + 1].Cells[2].SetCellWidth(20, CellWidthType.Percentage);
            //                    tableNoTTKhongHD.Rows[cchdModel.listThue.Count + 1].Cells[3].SetCellWidth(20, CellWidthType.Percentage);
            //                    tableNoTTKhongHD.Rows[cchdModel.listThue.Count + 1].Cells[4].SetCellWidth(25, CellWidthType.Percentage);
            //                    tableNoTTKhongHD.Rows[cchdModel.listThue.Count + 1].Cells[5].SetCellWidth(20, CellWidthType.Percentage);
            //                    #endregion

            //                    #region Xử lý dòng cuối cùng (Tổng tiền)
            //                    //Merge dòng cuối cùng lấy Name: Tổng
            //                    tableNoTTCoHD.ApplyHorizontalMerge(cchdModel.listThue.Count + 1, 0, 2);
            //                    TableRow row = tableNoTTCoHD.Rows[cchdModel.listThue.Count + 1];
            //                    row.Cells[0].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //                    Paragraph pF = row.Cells[0].AddParagraph();
            //                    TextRange TRF = pF.AppendText("Tổng cộng");
            //                    pF.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //                    TRF.CharacterFormat.FontName = "Times New Roman";
            //                    TRF.CharacterFormat.FontSize = 13;
            //                    TRF.CharacterFormat.Bold = true;

            //                    row.Cells[1].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //                    Paragraph pF1 = row.Cells[3].AddParagraph();
            //                    TextRange TRF1 = pF1.AppendText(cchdModel.tongtientren90ngay > 0 ? cchdModel.tongtientren90ngay.ToString("#,###") : "0");
            //                    pF1.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //                    TRF1.CharacterFormat.FontName = "Times New Roman";
            //                    TRF1.CharacterFormat.FontSize = 13;
            //                    TRF1.CharacterFormat.Bold = true;

            //                    row.Cells[2].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //                    Paragraph pF2 = row.Cells[4].AddParagraph();
            //                    TextRange TRF2 = pF2.AppendText(cchdModel.tongsonop > 0 ? cchdModel.tongsonop.ToString("#,###") : "0");
            //                    pF2.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //                    TRF2.CharacterFormat.FontName = "Times New Roman";
            //                    TRF2.CharacterFormat.FontSize = 13;
            //                    TRF2.CharacterFormat.Bold = true;

            //                    row.Cells[3].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //                    Paragraph pF3 = row.Cells[5].AddParagraph();
            //                    TextRange TRF3 = pF3.AppendText(cchdModel.tongtiencuongche > 0 ? cchdModel.tongtiencuongche.ToString("#,###") : "0");
            //                    pF3.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //                    TRF3.CharacterFormat.FontName = "Times New Roman";
            //                    TRF3.CharacterFormat.FontSize = 13;
            //                    TRF3.CharacterFormat.Bold = true;

            //                    tableNoTTKhongHD.ApplyHorizontalMerge(cchdModel.listThue.Count + 1, 0, 2);
            //                    TableRow rowNo = tableNoTTKhongHD.Rows[cchdModel.listThue.Count + 1];
            //                    rowNo.Cells[0].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //                    Paragraph pFNo = rowNo.Cells[0].AddParagraph();
            //                    TextRange TRFNo = pFNo.AppendText("Tổng cộng");
            //                    pFNo.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //                    TRFNo.CharacterFormat.FontName = "Times New Roman";
            //                    TRFNo.CharacterFormat.FontSize = 13;
            //                    TRFNo.CharacterFormat.Bold = true;

            //                    rowNo.Cells[3].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //                    Paragraph pF1No = rowNo.Cells[3].AddParagraph();
            //                    TextRange TRF1No = pF1No.AppendText(cchdModel.tongtientren90ngay > 0 ? cchdModel.tongtientren90ngay.ToString("#,###") : "0");
            //                    pF1No.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //                    TRF1No.CharacterFormat.FontName = "Times New Roman";
            //                    TRF1No.CharacterFormat.FontSize = 13;
            //                    TRF1No.CharacterFormat.Bold = true;

            //                    rowNo.Cells[4].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //                    Paragraph pF2No = rowNo.Cells[4].AddParagraph();
            //                    TextRange TRF2No = pF2No.AppendText(cchdModel.tongsonop > 0 ? cchdModel.tongsonop.ToString("#,###") : "0");
            //                    pF2No.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //                    TRF2No.CharacterFormat.FontName = "Times New Roman";
            //                    TRF2No.CharacterFormat.FontSize = 13;
            //                    TRF2No.CharacterFormat.Bold = true;

            //                    rowNo.Cells[5].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
            //                    Paragraph pF3No = rowNo.Cells[5].AddParagraph();
            //                    TextRange TRF3No = pF3No.AppendText(cchdModel.tongtiencuongche > 0 ? cchdModel.tongtiencuongche.ToString("#,###") : "0");
            //                    pF3No.Format.HorizontalAlignment = HorizontalAlignment.Center;
            //                    TRF3No.CharacterFormat.FontName = "Times New Roman";
            //                    TRF3No.CharacterFormat.FontSize = 13;
            //                    TRF3No.CharacterFormat.Bold = true;
            //                    #endregion

            //                    bodyTTCoHD.ChildObjects.Remove(paragraphTTCoHD);
            //                    bodyTTCoHD.ChildObjects.Insert(indexTTCoHD, tableNoTTCoHD);

            //                    bodyTTKhongHD.ChildObjects.Remove(paragraphTTKhongHD);
            //                    bodyTTKhongHD.ChildObjects.Insert(indexTTKhongHD, tableNoTTKhongHD);

            //                    #region Save File
            //                    // Check Cty nằm trong Chứng từ đã nộp và trong CCHD trên TMS
            //                    // Save QĐ+TB có hoá đơn
            //                    docQDCoHD.SaveToFile(fileQDCoHD, FileFormat.Doc);
            //                    // Save: QĐ+TB không có hoá đơn
            //                    docQDKhongHD.SaveToFile(fileQDKhongHD, FileFormat.Doc);
            //                    // Save: Tờ trình có hoá đơn
            //                    docTTCoHD.SaveToFile(fileTTCoHD, FileFormat.Doc);
            //                    // Save: Tờ trình không có hoá đơn
            //                    docTTKhongHD.SaveToFile(fileTTKhongHD, FileFormat.Doc);
            //                    Common.Common.success("Tạo báo cáo thành công!");
            //                    #endregion
            //                }
            //                #endregion
            //            }
            //            else
            //            {
            //                Common.Common.error("Không tồn tại dữ liệu trong Cưỡng chế hoá đơn trên TMS!");
            //            }
            //        }
            //    }
            //}
            #endregion
            var chinhanh = CCHD_listChiNhanh.Text;
            var chicuctruong = tbCCHD_tenChiCucTruong.Text;
            var chicucpho = tbCCHD_tenPhoChiCucTruong.Text;
            var doitruong = tbCCHD_TenDoiTruong.Text;
            var canbo = CCHD_TenCanBo.Text;
            var tendoi = CCHD_listTenDoi.Text;
            var ngaycohieuluc = CCHD_DateTime.Value;
            var ngayhethieuluc = CCHD_DateTime.Value.AddYears(1);
            var textSearchMST = tbCCHD_Search.Text;
            bool isCreated = CCHDController.CCHD_CreateReport(textSearchMST, chinhanh, canbo, doitruong, chicuctruong, chicucpho, ngaycohieuluc, ngayhethieuluc);
            if (isCreated)
            {
                Common.Common.success("Tạo báo cáo thanh công!");
            }

        }

        public void setData_chiNhanh(String tenChiNhanh)
        {
            listChiCucThue_comboBox.Text = tenChiNhanh;
            cbCCTK_Chinhanh.Text = tenChiNhanh;
            CCHD_listChiNhanh.Text = tenChiNhanh;
            GTTK_listChiNhanh.Text = tenChiNhanh;
            GTHD_listChiNhanh.Text = tenChiNhanh;
        }

        public void setData_tenDoiTruong(String tenDoiTruong)
        {
            tenDoiTruong_txt.Text = tenDoiTruong;
            tbCCHD_TenDoiTruong.Text = tenDoiTruong;
            tbCCTK_TenDoiTruong.Text = tenDoiTruong;
            tbGTTK_TenDoiTruong.Text = tenDoiTruong;
            tbGTHD_TenDoiTruong.Text = tenDoiTruong;
        }

        public void setData_tenDoi(String tenDoi)
        {
            listTenDoi_comboBox.Text = tenDoi;
            cbCCTK_tenDoi.Text = tenDoi;
            CCHD_listTenDoi.Text = tenDoi;
            GTTK_listTenDoi.Text = tenDoi;
            GTHD_listTenDoi.Text = tenDoi;
        }

        public void setData_tenCanBo(String tenCanBo)
        {
            tenCanBo_txt.Text = tenCanBo;
            tbCCTK_TenCB.Text = tenCanBo;
            CCHD_TenCanBo.Text = tenCanBo;
            GTTK_TenCanBo.Text = tenCanBo;
            GTHD_TenCanBo.Text = tenCanBo;
        }

        public void setData_tenChiCucTruong (String tenChiCucTruong)
        {
            tbCCTK_tenChiCucTruong.Text = tenChiCucTruong;
            tbCCHD_tenChiCucTruong.Text = tenChiCucTruong;
            tbGTTK_tenChiCucTruong.Text = tenChiCucTruong;
            tbGTHD_tenChiCucTruong.Text = tenChiCucTruong;
            tb_tenChiCucTruong.Text = tenChiCucTruong;
        }

        public void setData_tenPhoChiCucTruong (String tenPhoChiCucTruong)
        {
            tbCCTK_tenPhoChiCucTruong.Text = tenPhoChiCucTruong;
            tbCCHD_tenPhoChiCucTruong.Text = tenPhoChiCucTruong;
            tbGTTK_tenPhoChiCucTruong.Text = tenPhoChiCucTruong;
            tbGTHD_tenPhoChiCucTruong.Text = tenPhoChiCucTruong;
            tb_tenPhoChiCucTruong.Text = tenPhoChiCucTruong;
        }

        private void saveTenDoi_btn_Click(object sender, EventArgs e)
        {
            var tenDoi = listTenDoi_comboBox.Text;
            var isSuccessfully = DonViController.SaveTenDoi(tenDoi);
            if (isSuccessfully)
            {
                setData_tenDoi(tenDoi);
                MessageBox.Show("Lưu thông tin tên dội thành công !", "Thông báo");
            }
            else
            {
                MessageBox.Show("Có lỗi xảy ra. Lưu thông tin tên đội thất bại !", "Lỗi");
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(1);
        }

        private void btnCCTK_openForm_NhapFileNguon_Click(object sender, EventArgs e)
        {
            InsertFileNguon_CCTK_Form insertForm = new InsertFileNguon_CCTK_Form();
            insertForm.ShowDialog();
        }

        private void btnCCTK_checkFile_Click(object sender, EventArgs e)
        {
            fileNguonStatusModel = FileNguonStatusController.get_FileNguonStatus();
            var fileNguonStatusString = FileNguonStatusController.get_tinhTrangCacFileNguonCCTK(fileNguonStatusModel);

            txbCCTK_fileNguonStatus.Text = fileNguonStatusString;
        }

        //Chỉnh sửa mẫu biểu CCHĐ
        private void button24_Click(object sender, EventArgs e)
        {
            var folderPath = Environment.CurrentDirectory + @"\documents\input\CCHD";
            Common.Common.OpenFolderExplorer(folderPath);
        }

        // Giải tỏa tài khoản
        private void button30_Click(object sender, EventArgs e)
        {
            var folderPath = Environment.CurrentDirectory + @"\documents\input\GTTK";
            Common.Common.OpenFolderExplorer(folderPath);
        }

        // Giải tỏa hóa đơn
        private void button36_Click(object sender, EventArgs e)
        {
            var folderPath = Environment.CurrentDirectory + @"\documents\input\GTHD";
            Common.Common.OpenFolderExplorer(folderPath);
        }

        private void button23_Click(object sender, EventArgs e)
        {
            fileNguonStatusModel = FileNguonStatusController.get_FileNguonStatus();
            var fileNguonStatusString = FileNguonStatusController.get_tinhTrangCacFileNguonCCHĐ(fileNguonStatusModel);

            CCHD_FileNguonStatus.Text = fileNguonStatusString;
        }

        // Search trong Tab Giải tỏa tài khoản
        private void button28_Click(object sender, EventArgs e)
        {
            if (!File.Exists(pathS12File))
            {
                Common.Common.error("Không tìm thấy tài liệu S12.XLSX! Vui lòng nhập tài liệu");
            }
            else
            {
                if (string.IsNullOrWhiteSpace(GTTK_MST_textBox.Text))
                {
                    SearchAllDataS12(pathS12File, GTTK_dsCongTy_GridView);
                }
                else
                {
                    SearchS12byMST(pathS12File, GTTK_MST_textBox.Text.Trim(), GTTK_dsCongTy_GridView);
                }
            }
        }

        //Tạo báo cáo giải tỏa tài khoản
        private void button32_Click(object sender, EventArgs e)
        {
            var tenChiNhanh = GTTK_listChiNhanh.Text;
            var tenDoi = GTTK_listTenDoi.Text;
            var tenCanBo = GTTK_TenCanBo.Text;
            var tenDoiTruong = tbGTTK_TenDoiTruong.Text;
            var tenChiCucTruong = tbGTTK_tenChiCucTruong.Text;
            var tenPhoChiCucTruong = tbGTTK_tenPhoChiCucTruong.Text;

            DonViController.SaveThongTinDonVi(tenChiNhanh, tenCanBo, tenDoi, tenDoiTruong, tenChiCucTruong, tenPhoChiCucTruong);
            setData_chiNhanh(tenChiNhanh);
            setData_tenCanBo(tenCanBo);
            setData_tenDoi(tenDoi);
            setData_tenDoiTruong(tenDoiTruong);
            setData_tenChiCucTruong(tenChiCucTruong);
            setData_tenPhoChiCucTruong(tenPhoChiCucTruong);

            var createdDate_string = GTTK_DateTime.Text;
            var createdDate_stringArray = createdDate_string.Split('/');
            GTTKController.Create_BaoCao(GTTK_MST_textBox.Text.Trim(), createdDate_stringArray);
        }

        // update công ty CCHĐ
        private void button22_Click_1(object sender, EventArgs e)
        {
            if (!File.Exists(pathS12File))
            {
                Common.Common.error("Không tìm thấy tài liệu S12.XLSX! Vui lòng nhập tài liệu");
            }
            else
            {
                if (string.IsNullOrWhiteSpace(tbCCHD_Search.Text))
                {
                    SearchAllDataS12(pathS12File, CCHD_GridView);
                }
                else
                {
                    SearchS12byMST(pathS12File, tbCCHD_Search.Text.Trim(), CCHD_GridView);
                }
            }
        }

        private void button33_Click(object sender, EventArgs e)
        {
            if (!File.Exists(pathS12File))
            {
                Common.Common.error("Không tìm thấy tài liệu S12.XLSX! Vui lòng nhập tài liệu");
            }
            else
            {
                if (string.IsNullOrWhiteSpace(MST_GTHD_txtBox.Text))
                {
                    SearchAllDataS12(pathS12File, GTHD_GridView);
                }
                else
                {
                    SearchS12byMST(pathS12File, MST_GTHD_txtBox.Text.Trim(), GTHD_GridView);
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        { }

        private void CreateReport_CCHD_btn_Click(object sender, EventArgs e)
        {
            var tenChiNhanh = GTHD_listChiNhanh.Text;
            var tenDoi = GTHD_listTenDoi.Text;
            var tenCanBo = GTHD_TenCanBo.Text;
            var tenDoiTruong = tbGTHD_TenDoiTruong.Text;
            var tenChiCucTruong = tbGTHD_tenChiCucTruong.Text;
            var tenPhoChiCucTruong = tbGTHD_tenPhoChiCucTruong.Text;

            DonViController.SaveThongTinDonVi(tenChiNhanh, tenCanBo, tenDoi, tenDoiTruong, tenChiCucTruong, tenPhoChiCucTruong);
            setData_chiNhanh(tenChiNhanh);
            setData_tenCanBo(tenCanBo);
            setData_tenDoi(tenDoi);
            setData_tenDoiTruong(tenDoiTruong);
            setData_tenChiCucTruong(tenChiCucTruong);
            setData_tenPhoChiCucTruong(tenPhoChiCucTruong);

            GTHDController.create_Bao_Cao(MST_GTHD_txtBox.Text.Trim());
        }

        private void btnUploadCCTKTMS_Click(object sender, EventArgs e)
        {
            fileCCTKTMS = UploadFile(tbCCTKTMS);
        }

        private void btnImportCCTKTMS_Click(object sender, EventArgs e)
        {
            Common.Common.Import(fileCCTKTMS, "cctkTMSFile", tbCCTKTMS);
            filePhonebook = "";
        }

        private void btnCreateTB_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(GTTK_MST_textBox.Text))
            {
                Common.Common.error("Vui lòng nhập mã số thuế công ty muốn tạo thông báo!");
                GTTK_MST_textBox.Focus();
            } else
            {
                GTTKController.CreateNotifiction(GTTK_MST_textBox.Text, GTTK_listChiNhanh.Text);
            }
        }
        
        private void FileNguonStatusCheck_GTHD_btn_Click(object sender, EventArgs e)
        {
            fileNguonStatusModel = FileNguonStatusController.get_FileNguonStatus();

            var statusString = FileNguonStatusController.get_tinhTrangCacFileNguonGTHD(fileNguonStatusModel);

            tinhTrangFileNguon_GTHD_txt.Text = statusString;
        }

        /// <summary>
        /// Nhập file nguồn cho Tab Giải tỏa hóa đơn
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button37_Click(object sender, EventArgs e)
        {
            InsertFileNguon_GTHD_Form insertForm = new InsertFileNguon_GTHD_Form();
            insertForm.ShowDialog();
        }

        /// <summary>
        /// Nhập file nguồn cho Tab Cưỡng chế hóa đơn
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button25_Click(object sender, EventArgs e)
        {
            InsertFileNguon_CCHD_Form insertForm = new InsertFileNguon_CCHD_Form();
            insertForm.ShowDialog();
        }

        private void addTenDoiTruong_btn_Click(object sender, EventArgs e)
        {
            var tenDoiTruong = tenDoiTruong_txt.Text;
            var isSaveTenDoiTruongSuccessfully = DonViController.SaveTenDoiTruong(tenDoiTruong);
            if (isSaveTenDoiTruongSuccessfully)
            {
                setData_tenDoiTruong(tenDoiTruong);
                MessageBox.Show("Lưu tên đội trưởng thành công !", "Thông báo");
            }
            else
            {
                MessageBox.Show("Có lỗi xảy ra. Lưu tên đội trưởng thất bại !", "Thông báo");
            }
        }

        // tên chi cục trưởng
        private void button27_Click(object sender, EventArgs e)
        {
            var tenChiCucTruong = tb_tenChiCucTruong.Text;
            var isSaveTenChiCucTruongSuccessfully = DonViController.SaveTenChiCucTruong(tenChiCucTruong);
            if (isSaveTenChiCucTruongSuccessfully)
            {
                setData_tenChiCucTruong(tenChiCucTruong);
                MessageBox.Show("Lưu tên chi cục trưởng thành công !", "Thông báo");
            }
            else
            {
                MessageBox.Show("Có lỗi xảy ra. Lưu tên chi cục trưởng thất bại !", "Thông báo");
            }
        }

        // Tên phó chi cục trưởng
        private void button34_Click(object sender, EventArgs e)
        {
            var tenPhoChiCucTruong = tb_tenPhoChiCucTruong.Text;
            var isSaveTenPhoChiCucTruongSuccessfully = DonViController.SaveTenPhoChiCucTruong(tenPhoChiCucTruong);
            if (isSaveTenPhoChiCucTruongSuccessfully)
            {
                setData_tenPhoChiCucTruong(tenPhoChiCucTruong);
                MessageBox.Show("Lưu tên phó chi cục trưởng thành công !", "Thông báo");
            }
            else
            {
                MessageBox.Show("Có lỗi xảy ra. Lưu tên phó chi cục trưởng thất bại !", "Thông báo");
            }
        }
    }
}
