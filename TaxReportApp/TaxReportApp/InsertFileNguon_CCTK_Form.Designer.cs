﻿namespace TaxReportApp
{
    partial class InsertFileNguon_CCTK_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InsertFileNguon_CCTK_Form));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.CCTK_S12_statusMessage = new System.Windows.Forms.Label();
            this.CCTK_S12_deleteBtn = new System.Windows.Forms.Button();
            this.CCTK_S12_loadBtn = new System.Windows.Forms.Button();
            this.CCTK_S12_selectBtn = new System.Windows.Forms.Button();
            this.CCTK_S12_urlTxtBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.CCTK_S14_statusMessage = new System.Windows.Forms.Label();
            this.CCTK_S14_deleteBtn = new System.Windows.Forms.Button();
            this.CCTK_S14_loadBtn = new System.Windows.Forms.Button();
            this.CCTK_S14_selectBtn = new System.Windows.Forms.Button();
            this.CCTK_S14_urlTxtBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.CCTK_ChungTu_statusMessage = new System.Windows.Forms.Label();
            this.CCTK_ChungTu_deleteBtn = new System.Windows.Forms.Button();
            this.CCTK_ChungTu_loadBtn = new System.Windows.Forms.Button();
            this.CCTK_ChungTu_selectBtn = new System.Windows.Forms.Button();
            this.CCTK_ChungTu_urlTxtBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.CCTK_DanhBa_statusMessage = new System.Windows.Forms.Label();
            this.CCTK_DanhBa_deleteBtn = new System.Windows.Forms.Button();
            this.CCTK_DanhBa_loadBtn = new System.Windows.Forms.Button();
            this.CCTK_DanhBa_selectBtn = new System.Windows.Forms.Button();
            this.CCTK_DanhBa_urlTxtBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.CCTK_TKNganHang_statusMessage = new System.Windows.Forms.Label();
            this.CCTK_TKNganHang_deleteBtn = new System.Windows.Forms.Button();
            this.CCTK_TKNganHang_loadBtn = new System.Windows.Forms.Button();
            this.CCTK_TKNganHang_selectBtn = new System.Windows.Forms.Button();
            this.CCTK_TKNganHang_urlTxtBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.CCTK_dsDiaChiNganHang_statusMessage = new System.Windows.Forms.Label();
            this.CCTK_DSDiaChiNganHang_deleteBtn = new System.Windows.Forms.Button();
            this.CCTK_DSDiaChiNganHang_loadBtn = new System.Windows.Forms.Button();
            this.CCTK_dsDiaChiNganHang_selectBtn = new System.Windows.Forms.Button();
            this.CCTK_DSDiaChiNganHang_urlTxtBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.CCTK_S12_statusMessage);
            this.groupBox1.Controls.Add(this.CCTK_S12_deleteBtn);
            this.groupBox1.Controls.Add(this.CCTK_S12_loadBtn);
            this.groupBox1.Controls.Add(this.CCTK_S12_selectBtn);
            this.groupBox1.Controls.Add(this.CCTK_S12_urlTxtBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(26, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(762, 100);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "1. File S12";
            // 
            // CCTK_S12_statusMessage
            // 
            this.CCTK_S12_statusMessage.AutoSize = true;
            this.CCTK_S12_statusMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CCTK_S12_statusMessage.ForeColor = System.Drawing.SystemColors.Highlight;
            this.CCTK_S12_statusMessage.Location = new System.Drawing.Point(121, 28);
            this.CCTK_S12_statusMessage.Name = "CCTK_S12_statusMessage";
            this.CCTK_S12_statusMessage.Size = new System.Drawing.Size(199, 17);
            this.CCTK_S12_statusMessage.TabIndex = 6;
            this.CCTK_S12_statusMessage.Text = "Hiện tại không có file nào.";
            // 
            // CCTK_S12_deleteBtn
            // 
            this.CCTK_S12_deleteBtn.Location = new System.Drawing.Point(679, 57);
            this.CCTK_S12_deleteBtn.Name = "CCTK_S12_deleteBtn";
            this.CCTK_S12_deleteBtn.Size = new System.Drawing.Size(75, 23);
            this.CCTK_S12_deleteBtn.TabIndex = 5;
            this.CCTK_S12_deleteBtn.Text = "Xóa";
            this.CCTK_S12_deleteBtn.UseVisualStyleBackColor = true;
            this.CCTK_S12_deleteBtn.Click += new System.EventHandler(this.CCTK_S12_deleteBtn_Click);
            // 
            // CCTK_S12_loadBtn
            // 
            this.CCTK_S12_loadBtn.Location = new System.Drawing.Point(588, 57);
            this.CCTK_S12_loadBtn.Name = "CCTK_S12_loadBtn";
            this.CCTK_S12_loadBtn.Size = new System.Drawing.Size(75, 23);
            this.CCTK_S12_loadBtn.TabIndex = 4;
            this.CCTK_S12_loadBtn.Text = "Nhập";
            this.CCTK_S12_loadBtn.UseVisualStyleBackColor = true;
            this.CCTK_S12_loadBtn.Click += new System.EventHandler(this.CCTK_S12_loadBtn_Click);
            // 
            // CCTK_S12_selectBtn
            // 
            this.CCTK_S12_selectBtn.Location = new System.Drawing.Point(496, 57);
            this.CCTK_S12_selectBtn.Name = "CCTK_S12_selectBtn";
            this.CCTK_S12_selectBtn.Size = new System.Drawing.Size(75, 23);
            this.CCTK_S12_selectBtn.TabIndex = 3;
            this.CCTK_S12_selectBtn.Text = "Chọn";
            this.CCTK_S12_selectBtn.UseVisualStyleBackColor = true;
            this.CCTK_S12_selectBtn.Click += new System.EventHandler(this.CCTK_S12_selectBtn_Click);
            // 
            // CCTK_S12_urlTxtBox
            // 
            this.CCTK_S12_urlTxtBox.Location = new System.Drawing.Point(122, 57);
            this.CCTK_S12_urlTxtBox.Name = "CCTK_S12_urlTxtBox";
            this.CCTK_S12_urlTxtBox.Size = new System.Drawing.Size(355, 22);
            this.CCTK_S12_urlTxtBox.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Đường dẫn:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "File hiện tại:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.CCTK_S14_statusMessage);
            this.groupBox2.Controls.Add(this.CCTK_S14_deleteBtn);
            this.groupBox2.Controls.Add(this.CCTK_S14_loadBtn);
            this.groupBox2.Controls.Add(this.CCTK_S14_selectBtn);
            this.groupBox2.Controls.Add(this.CCTK_S14_urlTxtBox);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(26, 118);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(762, 100);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "2. File S14";
            // 
            // CCTK_S14_statusMessage
            // 
            this.CCTK_S14_statusMessage.AutoSize = true;
            this.CCTK_S14_statusMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CCTK_S14_statusMessage.ForeColor = System.Drawing.SystemColors.Highlight;
            this.CCTK_S14_statusMessage.Location = new System.Drawing.Point(121, 28);
            this.CCTK_S14_statusMessage.Name = "CCTK_S14_statusMessage";
            this.CCTK_S14_statusMessage.Size = new System.Drawing.Size(199, 17);
            this.CCTK_S14_statusMessage.TabIndex = 7;
            this.CCTK_S14_statusMessage.Text = "Hiện tại không có file nào.";
            // 
            // CCTK_S14_deleteBtn
            // 
            this.CCTK_S14_deleteBtn.Location = new System.Drawing.Point(679, 57);
            this.CCTK_S14_deleteBtn.Name = "CCTK_S14_deleteBtn";
            this.CCTK_S14_deleteBtn.Size = new System.Drawing.Size(75, 23);
            this.CCTK_S14_deleteBtn.TabIndex = 5;
            this.CCTK_S14_deleteBtn.Text = "Xóa";
            this.CCTK_S14_deleteBtn.UseVisualStyleBackColor = true;
            this.CCTK_S14_deleteBtn.Click += new System.EventHandler(this.CCTK_S14_deleteBtn_Click);
            // 
            // CCTK_S14_loadBtn
            // 
            this.CCTK_S14_loadBtn.Location = new System.Drawing.Point(588, 57);
            this.CCTK_S14_loadBtn.Name = "CCTK_S14_loadBtn";
            this.CCTK_S14_loadBtn.Size = new System.Drawing.Size(75, 23);
            this.CCTK_S14_loadBtn.TabIndex = 4;
            this.CCTK_S14_loadBtn.Text = "Nhập";
            this.CCTK_S14_loadBtn.UseVisualStyleBackColor = true;
            this.CCTK_S14_loadBtn.Click += new System.EventHandler(this.CCTK_S14_loadBtn_Click);
            // 
            // CCTK_S14_selectBtn
            // 
            this.CCTK_S14_selectBtn.Location = new System.Drawing.Point(496, 57);
            this.CCTK_S14_selectBtn.Name = "CCTK_S14_selectBtn";
            this.CCTK_S14_selectBtn.Size = new System.Drawing.Size(75, 23);
            this.CCTK_S14_selectBtn.TabIndex = 3;
            this.CCTK_S14_selectBtn.Text = "Chọn";
            this.CCTK_S14_selectBtn.UseVisualStyleBackColor = true;
            this.CCTK_S14_selectBtn.Click += new System.EventHandler(this.CCTK_S14_selectBtn_Click);
            // 
            // CCTK_S14_urlTxtBox
            // 
            this.CCTK_S14_urlTxtBox.Location = new System.Drawing.Point(122, 57);
            this.CCTK_S14_urlTxtBox.Name = "CCTK_S14_urlTxtBox";
            this.CCTK_S14_urlTxtBox.Size = new System.Drawing.Size(355, 22);
            this.CCTK_S14_urlTxtBox.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 17);
            this.label3.TabIndex = 1;
            this.label3.Text = "Đường dẫn:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "File hiện tại:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.CCTK_ChungTu_statusMessage);
            this.groupBox3.Controls.Add(this.CCTK_ChungTu_deleteBtn);
            this.groupBox3.Controls.Add(this.CCTK_ChungTu_loadBtn);
            this.groupBox3.Controls.Add(this.CCTK_ChungTu_selectBtn);
            this.groupBox3.Controls.Add(this.CCTK_ChungTu_urlTxtBox);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Location = new System.Drawing.Point(26, 224);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(762, 100);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "3. File Chứng từ";
            // 
            // CCTK_ChungTu_statusMessage
            // 
            this.CCTK_ChungTu_statusMessage.AutoSize = true;
            this.CCTK_ChungTu_statusMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CCTK_ChungTu_statusMessage.ForeColor = System.Drawing.SystemColors.Highlight;
            this.CCTK_ChungTu_statusMessage.Location = new System.Drawing.Point(121, 28);
            this.CCTK_ChungTu_statusMessage.Name = "CCTK_ChungTu_statusMessage";
            this.CCTK_ChungTu_statusMessage.Size = new System.Drawing.Size(199, 17);
            this.CCTK_ChungTu_statusMessage.TabIndex = 9;
            this.CCTK_ChungTu_statusMessage.Text = "Hiện tại không có file nào.";
            // 
            // CCTK_ChungTu_deleteBtn
            // 
            this.CCTK_ChungTu_deleteBtn.Location = new System.Drawing.Point(679, 57);
            this.CCTK_ChungTu_deleteBtn.Name = "CCTK_ChungTu_deleteBtn";
            this.CCTK_ChungTu_deleteBtn.Size = new System.Drawing.Size(75, 23);
            this.CCTK_ChungTu_deleteBtn.TabIndex = 5;
            this.CCTK_ChungTu_deleteBtn.Text = "Xóa";
            this.CCTK_ChungTu_deleteBtn.UseVisualStyleBackColor = true;
            this.CCTK_ChungTu_deleteBtn.Click += new System.EventHandler(this.CCTK_ChungTu_deleteBtn_Click);
            // 
            // CCTK_ChungTu_loadBtn
            // 
            this.CCTK_ChungTu_loadBtn.Location = new System.Drawing.Point(588, 57);
            this.CCTK_ChungTu_loadBtn.Name = "CCTK_ChungTu_loadBtn";
            this.CCTK_ChungTu_loadBtn.Size = new System.Drawing.Size(75, 23);
            this.CCTK_ChungTu_loadBtn.TabIndex = 4;
            this.CCTK_ChungTu_loadBtn.Text = "Nhập";
            this.CCTK_ChungTu_loadBtn.UseVisualStyleBackColor = true;
            this.CCTK_ChungTu_loadBtn.Click += new System.EventHandler(this.CCTK_ChungTu_loadBtn_Click);
            // 
            // CCTK_ChungTu_selectBtn
            // 
            this.CCTK_ChungTu_selectBtn.Location = new System.Drawing.Point(496, 57);
            this.CCTK_ChungTu_selectBtn.Name = "CCTK_ChungTu_selectBtn";
            this.CCTK_ChungTu_selectBtn.Size = new System.Drawing.Size(75, 23);
            this.CCTK_ChungTu_selectBtn.TabIndex = 3;
            this.CCTK_ChungTu_selectBtn.Text = "Chọn";
            this.CCTK_ChungTu_selectBtn.UseVisualStyleBackColor = true;
            this.CCTK_ChungTu_selectBtn.Click += new System.EventHandler(this.CCTK_ChungTu_selectBtn_Click);
            // 
            // CCTK_ChungTu_urlTxtBox
            // 
            this.CCTK_ChungTu_urlTxtBox.Location = new System.Drawing.Point(122, 57);
            this.CCTK_ChungTu_urlTxtBox.Name = "CCTK_ChungTu_urlTxtBox";
            this.CCTK_ChungTu_urlTxtBox.Size = new System.Drawing.Size(355, 22);
            this.CCTK_ChungTu_urlTxtBox.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 60);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 17);
            this.label5.TabIndex = 1;
            this.label5.Text = "Đường dẫn:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "File hiện tại:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.CCTK_DanhBa_statusMessage);
            this.groupBox4.Controls.Add(this.CCTK_DanhBa_deleteBtn);
            this.groupBox4.Controls.Add(this.CCTK_DanhBa_loadBtn);
            this.groupBox4.Controls.Add(this.CCTK_DanhBa_selectBtn);
            this.groupBox4.Controls.Add(this.CCTK_DanhBa_urlTxtBox);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Location = new System.Drawing.Point(26, 330);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(762, 100);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "4. File Danh bạ";
            // 
            // CCTK_DanhBa_statusMessage
            // 
            this.CCTK_DanhBa_statusMessage.AutoSize = true;
            this.CCTK_DanhBa_statusMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CCTK_DanhBa_statusMessage.ForeColor = System.Drawing.SystemColors.Highlight;
            this.CCTK_DanhBa_statusMessage.Location = new System.Drawing.Point(121, 28);
            this.CCTK_DanhBa_statusMessage.Name = "CCTK_DanhBa_statusMessage";
            this.CCTK_DanhBa_statusMessage.Size = new System.Drawing.Size(199, 17);
            this.CCTK_DanhBa_statusMessage.TabIndex = 8;
            this.CCTK_DanhBa_statusMessage.Text = "Hiện tại không có file nào.";
            // 
            // CCTK_DanhBa_deleteBtn
            // 
            this.CCTK_DanhBa_deleteBtn.Location = new System.Drawing.Point(679, 57);
            this.CCTK_DanhBa_deleteBtn.Name = "CCTK_DanhBa_deleteBtn";
            this.CCTK_DanhBa_deleteBtn.Size = new System.Drawing.Size(75, 23);
            this.CCTK_DanhBa_deleteBtn.TabIndex = 5;
            this.CCTK_DanhBa_deleteBtn.Text = "Xóa";
            this.CCTK_DanhBa_deleteBtn.UseVisualStyleBackColor = true;
            this.CCTK_DanhBa_deleteBtn.Click += new System.EventHandler(this.CCTK_DanhBa_deleteBtn_Click);
            // 
            // CCTK_DanhBa_loadBtn
            // 
            this.CCTK_DanhBa_loadBtn.Location = new System.Drawing.Point(588, 57);
            this.CCTK_DanhBa_loadBtn.Name = "CCTK_DanhBa_loadBtn";
            this.CCTK_DanhBa_loadBtn.Size = new System.Drawing.Size(75, 23);
            this.CCTK_DanhBa_loadBtn.TabIndex = 4;
            this.CCTK_DanhBa_loadBtn.Text = "Nhập";
            this.CCTK_DanhBa_loadBtn.UseVisualStyleBackColor = true;
            this.CCTK_DanhBa_loadBtn.Click += new System.EventHandler(this.CCTK_DanhBa_loadBtn_Click);
            // 
            // CCTK_DanhBa_selectBtn
            // 
            this.CCTK_DanhBa_selectBtn.Location = new System.Drawing.Point(496, 57);
            this.CCTK_DanhBa_selectBtn.Name = "CCTK_DanhBa_selectBtn";
            this.CCTK_DanhBa_selectBtn.Size = new System.Drawing.Size(75, 23);
            this.CCTK_DanhBa_selectBtn.TabIndex = 3;
            this.CCTK_DanhBa_selectBtn.Text = "Chọn";
            this.CCTK_DanhBa_selectBtn.UseVisualStyleBackColor = true;
            this.CCTK_DanhBa_selectBtn.Click += new System.EventHandler(this.CCTK_DanhBa_selectBtn_Click);
            // 
            // CCTK_DanhBa_urlTxtBox
            // 
            this.CCTK_DanhBa_urlTxtBox.Location = new System.Drawing.Point(122, 57);
            this.CCTK_DanhBa_urlTxtBox.Name = "CCTK_DanhBa_urlTxtBox";
            this.CCTK_DanhBa_urlTxtBox.Size = new System.Drawing.Size(355, 22);
            this.CCTK_DanhBa_urlTxtBox.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(23, 60);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 17);
            this.label7.TabIndex = 1;
            this.label7.Text = "Đường dẫn:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(22, 28);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 17);
            this.label8.TabIndex = 0;
            this.label8.Text = "File hiện tại:";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.CCTK_TKNganHang_statusMessage);
            this.groupBox5.Controls.Add(this.CCTK_TKNganHang_deleteBtn);
            this.groupBox5.Controls.Add(this.CCTK_TKNganHang_loadBtn);
            this.groupBox5.Controls.Add(this.CCTK_TKNganHang_selectBtn);
            this.groupBox5.Controls.Add(this.CCTK_TKNganHang_urlTxtBox);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Location = new System.Drawing.Point(26, 436);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(762, 100);
            this.groupBox5.TabIndex = 6;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "5. File Tài khoản ngân hàng";
            // 
            // CCTK_TKNganHang_statusMessage
            // 
            this.CCTK_TKNganHang_statusMessage.AutoSize = true;
            this.CCTK_TKNganHang_statusMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CCTK_TKNganHang_statusMessage.ForeColor = System.Drawing.SystemColors.Highlight;
            this.CCTK_TKNganHang_statusMessage.Location = new System.Drawing.Point(121, 28);
            this.CCTK_TKNganHang_statusMessage.Name = "CCTK_TKNganHang_statusMessage";
            this.CCTK_TKNganHang_statusMessage.Size = new System.Drawing.Size(199, 17);
            this.CCTK_TKNganHang_statusMessage.TabIndex = 9;
            this.CCTK_TKNganHang_statusMessage.Text = "Hiện tại không có file nào.";
            // 
            // CCTK_TKNganHang_deleteBtn
            // 
            this.CCTK_TKNganHang_deleteBtn.Location = new System.Drawing.Point(679, 57);
            this.CCTK_TKNganHang_deleteBtn.Name = "CCTK_TKNganHang_deleteBtn";
            this.CCTK_TKNganHang_deleteBtn.Size = new System.Drawing.Size(75, 23);
            this.CCTK_TKNganHang_deleteBtn.TabIndex = 5;
            this.CCTK_TKNganHang_deleteBtn.Text = "Xóa";
            this.CCTK_TKNganHang_deleteBtn.UseVisualStyleBackColor = true;
            this.CCTK_TKNganHang_deleteBtn.Click += new System.EventHandler(this.CCTK_TKNganHang_deleteBtn_Click);
            // 
            // CCTK_TKNganHang_loadBtn
            // 
            this.CCTK_TKNganHang_loadBtn.Location = new System.Drawing.Point(588, 57);
            this.CCTK_TKNganHang_loadBtn.Name = "CCTK_TKNganHang_loadBtn";
            this.CCTK_TKNganHang_loadBtn.Size = new System.Drawing.Size(75, 23);
            this.CCTK_TKNganHang_loadBtn.TabIndex = 4;
            this.CCTK_TKNganHang_loadBtn.Text = "Nhập";
            this.CCTK_TKNganHang_loadBtn.UseVisualStyleBackColor = true;
            this.CCTK_TKNganHang_loadBtn.Click += new System.EventHandler(this.CCTK_TKNganHang_loadBtn_Click);
            // 
            // CCTK_TKNganHang_selectBtn
            // 
            this.CCTK_TKNganHang_selectBtn.Location = new System.Drawing.Point(496, 57);
            this.CCTK_TKNganHang_selectBtn.Name = "CCTK_TKNganHang_selectBtn";
            this.CCTK_TKNganHang_selectBtn.Size = new System.Drawing.Size(75, 23);
            this.CCTK_TKNganHang_selectBtn.TabIndex = 3;
            this.CCTK_TKNganHang_selectBtn.Text = "Chọn";
            this.CCTK_TKNganHang_selectBtn.UseVisualStyleBackColor = true;
            this.CCTK_TKNganHang_selectBtn.Click += new System.EventHandler(this.CCTK_TKNganHang_selectBtn_Click);
            // 
            // CCTK_TKNganHang_urlTxtBox
            // 
            this.CCTK_TKNganHang_urlTxtBox.Location = new System.Drawing.Point(122, 57);
            this.CCTK_TKNganHang_urlTxtBox.Name = "CCTK_TKNganHang_urlTxtBox";
            this.CCTK_TKNganHang_urlTxtBox.Size = new System.Drawing.Size(355, 22);
            this.CCTK_TKNganHang_urlTxtBox.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(23, 60);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 17);
            this.label9.TabIndex = 1;
            this.label9.Text = "Đường dẫn:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(22, 28);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(84, 17);
            this.label10.TabIndex = 0;
            this.label10.Text = "File hiện tại:";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.CCTK_dsDiaChiNganHang_statusMessage);
            this.groupBox6.Controls.Add(this.CCTK_DSDiaChiNganHang_deleteBtn);
            this.groupBox6.Controls.Add(this.CCTK_DSDiaChiNganHang_loadBtn);
            this.groupBox6.Controls.Add(this.CCTK_dsDiaChiNganHang_selectBtn);
            this.groupBox6.Controls.Add(this.CCTK_DSDiaChiNganHang_urlTxtBox);
            this.groupBox6.Controls.Add(this.label11);
            this.groupBox6.Controls.Add(this.label12);
            this.groupBox6.Location = new System.Drawing.Point(26, 542);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(762, 100);
            this.groupBox6.TabIndex = 6;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "6. File danh sách địa chỉ ngân hàng";
            // 
            // CCTK_dsDiaChiNganHang_statusMessage
            // 
            this.CCTK_dsDiaChiNganHang_statusMessage.AutoSize = true;
            this.CCTK_dsDiaChiNganHang_statusMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CCTK_dsDiaChiNganHang_statusMessage.ForeColor = System.Drawing.SystemColors.Highlight;
            this.CCTK_dsDiaChiNganHang_statusMessage.Location = new System.Drawing.Point(121, 28);
            this.CCTK_dsDiaChiNganHang_statusMessage.Name = "CCTK_dsDiaChiNganHang_statusMessage";
            this.CCTK_dsDiaChiNganHang_statusMessage.Size = new System.Drawing.Size(199, 17);
            this.CCTK_dsDiaChiNganHang_statusMessage.TabIndex = 10;
            this.CCTK_dsDiaChiNganHang_statusMessage.Text = "Hiện tại không có file nào.";
            // 
            // CCTK_DSDiaChiNganHang_deleteBtn
            // 
            this.CCTK_DSDiaChiNganHang_deleteBtn.Location = new System.Drawing.Point(679, 57);
            this.CCTK_DSDiaChiNganHang_deleteBtn.Name = "CCTK_DSDiaChiNganHang_deleteBtn";
            this.CCTK_DSDiaChiNganHang_deleteBtn.Size = new System.Drawing.Size(75, 23);
            this.CCTK_DSDiaChiNganHang_deleteBtn.TabIndex = 5;
            this.CCTK_DSDiaChiNganHang_deleteBtn.Text = "Xóa";
            this.CCTK_DSDiaChiNganHang_deleteBtn.UseVisualStyleBackColor = true;
            this.CCTK_DSDiaChiNganHang_deleteBtn.Click += new System.EventHandler(this.CCTK_DSDiaChiNganHang_deleteBtn_Click);
            // 
            // CCTK_DSDiaChiNganHang_loadBtn
            // 
            this.CCTK_DSDiaChiNganHang_loadBtn.Location = new System.Drawing.Point(588, 57);
            this.CCTK_DSDiaChiNganHang_loadBtn.Name = "CCTK_DSDiaChiNganHang_loadBtn";
            this.CCTK_DSDiaChiNganHang_loadBtn.Size = new System.Drawing.Size(75, 23);
            this.CCTK_DSDiaChiNganHang_loadBtn.TabIndex = 4;
            this.CCTK_DSDiaChiNganHang_loadBtn.Text = "Nhập";
            this.CCTK_DSDiaChiNganHang_loadBtn.UseVisualStyleBackColor = true;
            this.CCTK_DSDiaChiNganHang_loadBtn.Click += new System.EventHandler(this.CCTK_DSDiaChiNganHang_loadBtn_Click);
            // 
            // CCTK_dsDiaChiNganHang_selectBtn
            // 
            this.CCTK_dsDiaChiNganHang_selectBtn.Location = new System.Drawing.Point(496, 57);
            this.CCTK_dsDiaChiNganHang_selectBtn.Name = "CCTK_dsDiaChiNganHang_selectBtn";
            this.CCTK_dsDiaChiNganHang_selectBtn.Size = new System.Drawing.Size(75, 23);
            this.CCTK_dsDiaChiNganHang_selectBtn.TabIndex = 3;
            this.CCTK_dsDiaChiNganHang_selectBtn.Text = "Chọn";
            this.CCTK_dsDiaChiNganHang_selectBtn.UseVisualStyleBackColor = true;
            this.CCTK_dsDiaChiNganHang_selectBtn.Click += new System.EventHandler(this.CCTK_dsDiaChiNganHang_selectBtn_Click);
            // 
            // CCTK_DSDiaChiNganHang_urlTxtBox
            // 
            this.CCTK_DSDiaChiNganHang_urlTxtBox.Location = new System.Drawing.Point(122, 57);
            this.CCTK_DSDiaChiNganHang_urlTxtBox.Name = "CCTK_DSDiaChiNganHang_urlTxtBox";
            this.CCTK_DSDiaChiNganHang_urlTxtBox.Size = new System.Drawing.Size(355, 22);
            this.CCTK_DSDiaChiNganHang_urlTxtBox.TabIndex = 2;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(23, 60);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(82, 17);
            this.label11.TabIndex = 1;
            this.label11.Text = "Đường dẫn:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(22, 28);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(84, 17);
            this.label12.TabIndex = 0;
            this.label12.Text = "File hiện tại:";
            // 
            // InsertFileNguon_CCTK_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 697);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "InsertFileNguon_CCTK_Form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Nhập file nguồn CCTK";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label CCTK_S12_statusMessage;
        private System.Windows.Forms.Button CCTK_S12_deleteBtn;
        private System.Windows.Forms.Button CCTK_S12_loadBtn;
        private System.Windows.Forms.Button CCTK_S12_selectBtn;
        private System.Windows.Forms.TextBox CCTK_S12_urlTxtBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label CCTK_S14_statusMessage;
        private System.Windows.Forms.Button CCTK_S14_deleteBtn;
        private System.Windows.Forms.Button CCTK_S14_loadBtn;
        private System.Windows.Forms.Button CCTK_S14_selectBtn;
        private System.Windows.Forms.TextBox CCTK_S14_urlTxtBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label CCTK_ChungTu_statusMessage;
        private System.Windows.Forms.Button CCTK_ChungTu_deleteBtn;
        private System.Windows.Forms.Button CCTK_ChungTu_loadBtn;
        private System.Windows.Forms.Button CCTK_ChungTu_selectBtn;
        private System.Windows.Forms.TextBox CCTK_ChungTu_urlTxtBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label CCTK_DanhBa_statusMessage;
        private System.Windows.Forms.Button CCTK_DanhBa_deleteBtn;
        private System.Windows.Forms.Button CCTK_DanhBa_loadBtn;
        private System.Windows.Forms.Button CCTK_DanhBa_selectBtn;
        private System.Windows.Forms.TextBox CCTK_DanhBa_urlTxtBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label CCTK_TKNganHang_statusMessage;
        private System.Windows.Forms.Button CCTK_TKNganHang_deleteBtn;
        private System.Windows.Forms.Button CCTK_TKNganHang_loadBtn;
        private System.Windows.Forms.Button CCTK_TKNganHang_selectBtn;
        private System.Windows.Forms.TextBox CCTK_TKNganHang_urlTxtBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label CCTK_dsDiaChiNganHang_statusMessage;
        private System.Windows.Forms.Button CCTK_DSDiaChiNganHang_deleteBtn;
        private System.Windows.Forms.Button CCTK_DSDiaChiNganHang_loadBtn;
        private System.Windows.Forms.Button CCTK_dsDiaChiNganHang_selectBtn;
        private System.Windows.Forms.TextBox CCTK_DSDiaChiNganHang_urlTxtBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
    }
}