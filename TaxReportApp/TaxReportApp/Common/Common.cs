﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using TaxReportApp.Controller;
using TaxReportApp.Model;

namespace TaxReportApp.Common
{
    class Common
    {
        public static void error(string message)
        {
            MessageBox.Show(message, "Có lỗi xảy ra");
        }
        public static void warning(string message)
        {
            MessageBox.Show(message, "Cảnh báo");
        }
        public static void success(string message)
        {
            MessageBox.Show(message, "Thành công");
        }

        /// <summary>
        /// Format kiểu database thành 2-demension array để hiển thị ở các GridView list công ty
        /// </summary>
        /// <param name="dataTable">DataTable đầu vào</param>
        /// <returns></returns>
        public static String[,] get_ListCompanyFromDataTable(DataTable dataTable)
        {
            String[,] resultList = new string[6, dataTable.Rows.Count];
            if (dataTable != null)
            {

                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    resultList[0, i] = (i + 1).ToString();
                    resultList[1, i] = dataTable.Rows[i]["F3"]?.ToString() ?? "";
                    resultList[2, i] = dataTable.Rows[i]["F2"]?.ToString() ?? "";
                    resultList[3, i] = dataTable.Rows[i][3]?.ToString() ?? "";
                    resultList[4, i] = dataTable.Rows[i][4]?.ToString() ?? "";
                    resultList[5, i] = dataTable.Rows[i][5]?.ToString() ?? "";
                }
                return resultList;
            }
            else
            {
                return resultList;
            }
        }

        /// <summary>
        /// Mở folder 
        /// </summary>
        /// <param name="folderPath">Đường dẫn folder</param>
        public static void OpenFolderExplorer(String folderPath)
        {
            if (Directory.Exists(folderPath))
            {
                ProcessStartInfo startInfo = new ProcessStartInfo
                {
                    Arguments = folderPath,
                    FileName = "explorer.exe"
                };

                Process.Start(startInfo);
            }
            else
            {
                MessageBox.Show("Không tìm thấy file cần mở !", "Lỗi");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="filePathNew"></param>
        /// <param name="textBox"></param>
        public static void Import(string fileName, string filePathNew, TextBox textBox)
        {
            try
            {
                var dir = Directory.GetCurrentDirectory().ToString() + @"\" + ConfigurationManager.AppSettings["directoryDatabase"];
                if (string.IsNullOrWhiteSpace(fileName) || string.IsNullOrEmpty(fileName))
                {
                    MessageBox.Show("Vui lòng chọn file !", "Lỗi");
                }
                else
                {
                    if (!Directory.Exists(dir))
                    {
                        Directory.CreateDirectory(dir);
                    }
                    string filePath = dir + @"\" + ConfigurationManager.AppSettings[filePathNew];
                    var path = Path.GetExtension(fileName);
                    if (Path.GetExtension(fileName) == ".XLS")
                    {
                        var file = new FileInfo(fileName);
                        var fileXLSX = ConvertXLS_XLSX(file, filePathNew);
                        //File.Copy(fileXLSX, filePath, true);
                    }
                    else
                    {
                        File.Copy(fileName, filePath, true);
                    }

                    textBox.Text = "";
                    MessageBox.Show("Lưu file thành công !", "Thành công");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Có lỗi xảy ra !", "Lỗi");
            }
        }

        /// <summary>
        /// Convert XLS sang XLSX
        /// </summary>
        /// <param name="file"></param>
        /// <param name="filePathNew"></param>
        /// <returns></returns>
        public static string ConvertXLS_XLSX(FileInfo file, string filePathNew)
        {
            var dir = Directory.GetCurrentDirectory().ToString() + @"\" + ConfigurationManager.AppSettings["directoryDatabase"];
            var app = new Microsoft.Office.Interop.Excel.Application();
            var xlsFile = file.FullName;
            var wb = app.Workbooks.Open(xlsFile);
            var xlsxFile = dir + @"\" + ConfigurationManager.AppSettings[filePathNew];
            wb.SaveAs(Filename: xlsxFile, FileFormat: Microsoft.Office.Interop.Excel.XlFileFormat.xlOpenXMLWorkbook);
            wb.Close();
            app.Quit();
            return xlsxFile;
        }

        /// <summary>
        /// Import file - Ghi đè FilePathNew lên nếu FilePath đã có
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="filePathNew"></param>
        /// <returns></returns>
        public static bool ImportFile(string filePath, string filePathNew)
        {
            // Thông báo đang nhập
            LoadingForm loadingForm = new LoadingForm();
            loadingForm.Show();

            try
            {
                var dir = Directory.GetCurrentDirectory().ToString() + @"\" + ConfigurationManager.AppSettings["directoryDatabase"];
                if (!File.Exists(filePath))
                {
                    MessageBox.Show("File vừa nhập không tồn tại !", "Lỗi");
                    loadingForm.Hide();
                    return false;
                }
                else
                {
                    if (!Directory.Exists(dir))
                    {
                        Directory.CreateDirectory(dir);
                    }
                    string filePathToSave = dir + @"\" + ConfigurationManager.AppSettings[filePathNew];
                    if (Path.GetExtension(filePath) == ".XLS")
                    {
                        var file = new FileInfo(filePath);
                        var fileXLSX = ConvertXLS_XLSX(file, filePathNew);
                        //File.Copy(fileXLSX, filePath, true);
                    }
                    else
                    {
                        File.Copy(filePath, filePathToSave, true);
                    }

                    loadingForm.Hide();
                    MessageBox.Show("Nhập file thành công !", "Thành công");
                    return true;
                }
            }
            catch (Exception ex)
            {
                loadingForm.Hide();
                MessageBox.Show(ex.ToString(), "Lỗi");
                return false;
            }
        }

        public static string convertToUnSign2(string s)
        {
            string stFormD = s.Normalize(NormalizationForm.FormD);
            StringBuilder sb = new StringBuilder();
            for (int ich = 0; ich < stFormD.Length; ich++)
            {
                System.Globalization.UnicodeCategory uc = System.Globalization.CharUnicodeInfo.GetUnicodeCategory(stFormD[ich]);
                if (uc != System.Globalization.UnicodeCategory.NonSpacingMark)
                {
                    sb.Append(stFormD[ich]);
                }
            }
            sb = sb.Replace('Đ', 'D');
            sb = sb.Replace('đ', 'd');
            return (sb.ToString().Normalize(NormalizationForm.FormD));
        }

        /// <summary>
        /// Kiểm tra file đã tồn tại chưa
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static bool IsFileExist(String filePath)
        {
            if (File.Exists(filePath))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Kiểm tra folder đã tồn tại chưa
        /// </summary>
        /// <param name="folderPath"></param>
        /// <returns></returns>
        public static bool IsFolderExist(String folderPath)
        {
            if (Directory.Exists(folderPath))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Tạo dataTable GridView với 3 cột: STT, Tiểu mục và Số tiền
        /// </summary>
        /// <param name="listKhoanTien"></param>
        public static DataTable Create_FileExcelNo(List<KhoanTienModel> listKhoanTien)
        {
            decimal tongTien = 0;
            var index = 1;
            System.Data.DataTable table = new DataTable();

            //Thông tin các cột
            table.Columns.Add("STT", typeof(string));
            table.Columns.Add("Tiểu mục", typeof(string));
            table.Columns.Add("Số tiền", typeof(string));

            //Thêm thông tin các hàng
            foreach (var khoanTienItem in listKhoanTien)
            {
                table.Rows.Add(index, khoanTienItem.TieuMuc, khoanTienItem.SoTienTheoSo.ToString("#,###"));
                index++;
                tongTien += khoanTienItem.SoTienTheoSo;
            }

            table.Rows.Add("", "Tổng", tongTien.ToString("#,###"));

            return table;
        }
        /// <summary>
        /// Tạo file excel danh sách nợ từ file s12
        /// </summary>
        /// <param name="maSoThue"></param>
        public static void createExcelFile(String maSoThue)
        {
            // Danh sách link file và folder
            string dirDBFile = Directory.GetCurrentDirectory().ToString() + @"\" + ConfigurationManager.AppSettings["directoryDatabase"];
            string S12_FilePath = dirDBFile + @"\" + ConfigurationManager.AppSettings["s12File"];
            string reportPath = Directory.GetCurrentDirectory().ToString() + @"\" + ConfigurationManager.AppSettings["ReportFile"];

            #region Kiểm tra các file cần thiết
            if (!File.Exists(S12_FilePath))
            {
                MessageBox.Show("Không tìm thấy file S12. Mời nhập lại dữ liệu !");
                return;
            }
            #endregion

            #region Load dữ liệu
            var s12FileContent = S12Controller.SearchMST(S12_FilePath, maSoThue);
            var isS12File_NotEmpty = s12FileContent.AsEnumerable().Any(row => row["F2"] != null);
            if (!isS12File_NotEmpty)
            {
                error("Không tìm thấy dữ liệu !");
                return;
            }
            else
            {

                string timeFolder = S12Controller.SearchMonthReport(S12_FilePath);
                var containerFilePath = reportPath + @"\" + timeFolder + @"\" + maSoThue;
                if (!Directory.Exists(containerFilePath))
                {
                    Directory.CreateDirectory(containerFilePath);
                }


                List<KhoanTienModel> listTienNo = new List<KhoanTienModel>();
                for (int i = 0; i < s12FileContent.Rows.Count; i++)
                {
                    // Các khoản tiền nợ
                    var TienNoItem = new KhoanTienModel();
                    TienNoItem.TenCongTy = s12FileContent.Rows[0]["F3"].ToString();
                    TienNoItem.MaSoThue = s12FileContent.Rows[0]["F2"].ToString();
                    TienNoItem.SoTienTheoSo = Convert.ToDecimal(s12FileContent.Rows[i]["F7"].ToString().Replace(",", ""));
                    TienNoItem.TieuMuc = s12FileContent.Rows[i][5].ToString();
                    TienNoItem.MaChuong = s12FileContent.Rows[i][3].ToString();
                    listTienNo.Add(TienNoItem);
                }

                if (listTienNo.Count > 0)
                {
                    var dataExcelTable = Create_FileExcelNo(listTienNo);

                    Microsoft.Office.Interop.Excel.Application excel;
                    Microsoft.Office.Interop.Excel.Workbook workBook;
                    Microsoft.Office.Interop.Excel.Worksheet workSheet;
                    Microsoft.Office.Interop.Excel.Range cellRange;


                    try
                    {
                        excel = new Microsoft.Office.Interop.Excel.Application();

                        excel.Visible = true;
                        excel.DisplayAlerts = true;
                        workBook = excel.Workbooks.Add(Type.Missing);
                        while (workBook == null)
                        {
                            workBook = excel.Workbooks.Add(Type.Missing);
                        }

                        workSheet = (Microsoft.Office.Interop.Excel.Worksheet)workBook.ActiveSheet; workSheet.Name = "Danh sách  tiền nợ";

                        var rowCount = 1;
                        foreach (DataRow dataRow in dataExcelTable.Rows)
                        {
                            rowCount += 1;
                            for (int i = 1; i <= dataExcelTable.Columns.Count; i++)
                            {
                                if (rowCount == 2)
                                {
                                    workSheet.Cells[1, i] = dataExcelTable.Columns[i - 1].ColumnName;
                                    workSheet.Cells.Font.Color = System.Drawing.Color.Black;
                                }
                                workSheet.Cells[rowCount, i] = dataRow[i - 1].ToString();
                            }
                        }

                        cellRange = workSheet.Range[workSheet.Cells[1, 1], workSheet.Cells[rowCount, dataExcelTable.Columns.Count]];
                        cellRange.EntireColumn.AutoFit();
                        Microsoft.Office.Interop.Excel.Borders border = cellRange.Borders;
                        border.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        border.Weight = 2d;

                        cellRange = workSheet.Range[workSheet.Cells[1, 1], workSheet.Cells[2, dataExcelTable.Columns.Count]];

                        workBook.SaveAs(containerFilePath + @"\Danh sách nợ.xlsx");
                        workBook.Close();
                        excel.Quit();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Có lỗi xảy ra !");
                        return;
                    }
                }
                else
                {
                    return;
                }
            }
            #endregion
        }

        /// <summary>
        /// Tạo file excel danh sách số tiền đã đóng theo tiểu mục từ file Chứng từ
        /// </summary>
        /// <param name="maSoThue"></param>
        public static void createFileTrungGian(String maSoThue)
        {
            // Danh sách link file và folder
            string dirDBFile = Directory.GetCurrentDirectory().ToString() + @"\" + ConfigurationManager.AppSettings["directoryDatabase"];
            string chungTu_FilePath = dirDBFile + @"\" + ConfigurationManager.AppSettings["licenseFile"];
            string S12_FilePath = dirDBFile + @"\" + ConfigurationManager.AppSettings["s12File"];
            string S14_FilePath = dirDBFile + @"\" + ConfigurationManager.AppSettings["s14File"];
            string reportPath = Directory.GetCurrentDirectory().ToString() + @"\" + ConfigurationManager.AppSettings["ReportFile"];
            string CCTKTrenTMS_FilePath = dirDBFile + @"\" + ConfigurationManager.AppSettings["cctkTMSFile"];
            string danhBa_FilePath = dirDBFile + @"\" + ConfigurationManager.AppSettings["phoneBookFile"];

            #region Kiểm tra các file cần thiết
            if (String.IsNullOrEmpty(maSoThue.Trim()))
            {
                return;
            }

            if (!File.Exists(danhBa_FilePath))
            {
                MessageBox.Show("Không tìm thấy file Danh bạ. Mời nhập lại dữ liệu !");
            }

            if (!File.Exists(CCTKTrenTMS_FilePath))
            {
                MessageBox.Show("Không tìm thấy file Cưỡng chế tài khoản trên TMS. Mời nhập lại dữ liệu !");
            }

            if (!File.Exists(chungTu_FilePath))
            {
                MessageBox.Show("Không tìm thấy file Chứng từ. Mời nhập lại dữ liệu !");
                return;
            }

            if (!File.Exists(S12_FilePath))
            {
                MessageBox.Show("Khoong tìm thấy file S12. Mời nhập lại dữ liệu !");
                return;
            }
            #endregion

            #region Load dữ liệu
            var chungTuFileContent = ChungTuController.GetListChungTuByMST(chungTu_FilePath, maSoThue);
            var isChungTuFile_NotEmpty = chungTuFileContent.AsEnumerable().Any(row => row["Mã số thuế"] != null);

            var s12FileContent = S12Controller.SearchMST(S12_FilePath, maSoThue);
            var isS12File_NotEmpty = s12FileContent.AsEnumerable().Any(row => row["F2"] != null);

            var danhBaFileContent = DanhBaController.GetDanhba(danhBa_FilePath, maSoThue);
            var isDanhBaFile_NotEmpty = danhBaFileContent.AsEnumerable().Any(row => row["F2"] != null);

            var s14FileContent = S14Controller.GetDataS14FileByMST(S14_FilePath, maSoThue);
            var isS14File_NotEmpty = s14FileContent.AsEnumerable().Any(row => row["F3"] != null);

            if (!isS12File_NotEmpty)
            {
                error("Không tìm thấy dữ liệu trong file S12 !");
                return;
            }
            else if (!isDanhBaFile_NotEmpty)
            {
                error("Không tìm thấy dữ liệu trong file Danh bạ !");
                return;
            }
            else if (!isS14File_NotEmpty)
            {
                error("Không tìm thấy dữ liệu trong file S14 !");
                return;
            }
            else
            {
                // Tạo folder chứa file trung gian - Folder by Time
                string timeFolder = S12Controller.SearchMonthReport(S12_FilePath);
                var fileTrungGian_FilePath = reportPath + @"\" + timeFolder + @"\File_trung_gian_CCTK.xlsx";

                //Container Path
                var containerFilePath = reportPath + @"\" + timeFolder;
                if (!Directory.Exists(containerFilePath))
                {
                    Directory.CreateDirectory(containerFilePath);
                }

                #region Tạo file trung gian mới khi chưa có file
                if (!File.Exists(fileTrungGian_FilePath))
                {
                    System.Data.DataTable table = new DataTable();

                    //Thông tin các cột
                    table.Columns.Add("MST", typeof(string));
                    table.Columns.Add("NNT", typeof(string));
                    table.Columns.Add("TRỤ SỞ", typeof(string));
                    table.Columns.Add("PHƯỜNG TS", typeof(string));
                    table.Columns.Add("THÔNG BÁO", typeof(string));
                    table.Columns.Add("PHƯỜNG TB", typeof(string));
                    table.Columns.Add("QUẬN TB", typeof(string));
                    table.Columns.Add("TB07", typeof(string));
                    table.Columns.Add("NGÀY TB07", typeof(string));
                    table.Columns.Add("1001TK", typeof(string));
                    table.Columns.Add("1052TK", typeof(string));
                    table.Columns.Add("1701TK", typeof(string));
                    table.Columns.Add("1754TK", typeof(string));
                    table.Columns.Add("1804TK", typeof(string));
                    table.Columns.Add("2862TK", typeof(string));
                    table.Columns.Add("2863TK", typeof(string));
                    table.Columns.Add("2864TK", typeof(string));
                    table.Columns.Add("4254TK", typeof(string));
                    table.Columns.Add("4268TK", typeof(string));
                    table.Columns.Add("4272TK", typeof(string));
                    table.Columns.Add("4917TK", typeof(string));
                    table.Columns.Add("4918TK", typeof(string));
                    table.Columns.Add("4931TK", typeof(string));
                    table.Columns.Add("4934TK", typeof(string));
                    table.Columns.Add("4944TK", typeof(string));
                    table.Columns.Add("4943TK", typeof(string));
                    table.Columns.Add("4949TK", typeof(string));
                    table.Columns.Add("Tổng cộng", typeof(string));
                    //table.Columns.Add("Địa chỉ")
                    table.Rows.Add("MST", "NNT", "TRỤ SỞ", "PHƯỜNG TS", "THÔNG BÁO", "PHƯỜNG TB", "QUẬN TB", "TB07", "NGÀY TB07", "1001TK", "1052TK", "1701TK", "1754TK", "1804TK", "2862TK", "2863TK",
                        "2864TK", "4254TK", "4268TK", "4272TK", "4917TK", "4918TK", "4931TK", "4934TK", "4944TK", "4943TK", "4949TK", "Tổng cộng số tiền CCTK");

                    Microsoft.Office.Interop.Excel.Application excel;
                    Microsoft.Office.Interop.Excel.Workbook workBook;
                    Microsoft.Office.Interop.Excel.Worksheet workSheet;
                    Microsoft.Office.Interop.Excel.Range cellRange;


                    try
                    {
                        excel = new Microsoft.Office.Interop.Excel.Application();
                        excel.Visible = true;
                        excel.DisplayAlerts = true;
                        workBook = excel.Workbooks.Add(Type.Missing);
                        while (workBook == null)
                        {
                            workBook = excel.Workbooks.Add(Type.Missing);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                        return;
                    }
                    workSheet = (Microsoft.Office.Interop.Excel.Worksheet)workBook.ActiveSheet; workSheet.Name = "Danh sách số tiền đã đóng";
                    workSheet.EnableSelection = Microsoft.Office.Interop.Excel.XlEnableSelection.xlNoSelection;

                    var rowCount = 0;
                    foreach (DataRow dataRow in table.Rows)
                    {
                        rowCount += 1;
                        for (int i = 1; i <= table.Columns.Count; i++)
                        {
                            if (rowCount == 2)
                            {
                                workSheet.Cells[1, i] = table.Columns[i - 1].ColumnName;
                                workSheet.Cells.Font.Color = System.Drawing.Color.Black;
                            }
                            workSheet.Cells[rowCount, i] = dataRow[i - 1].ToString();
                        }
                    }

                    cellRange = workSheet.Range[workSheet.Cells[1, 1], workSheet.Cells[rowCount, table.Columns.Count]];
                    cellRange.EntireColumn.AutoFit();
                    Microsoft.Office.Interop.Excel.Borders border = cellRange.Borders;
                    border.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                    border.Weight = 2d;

                    cellRange = workSheet.Range[workSheet.Cells[1, 1], workSheet.Cells[2, table.Columns.Count]];

                    workBook.SaveAs(fileTrungGian_FilePath);
                    workBook.Close();
                    excel.Quit();
                }
                #endregion

                #region Lấy dữ liệu
                var fileTrungGianModel = new FileTrungGianViewModel();

                // Các khoản tiền nợ
                List<KhoanTienModel> listTienNo = new List<KhoanTienModel>();
                for (int i = 0; i < s12FileContent.Rows.Count; i++)
                {
                    var TienNoItem = new KhoanTienModel();
                    TienNoItem.TenCongTy = s12FileContent.Rows[0]["F3"].ToString();
                    TienNoItem.MaSoThue = s12FileContent.Rows[0]["F2"].ToString();
                    TienNoItem.SoTienTheoSo = Convert.ToDecimal(s12FileContent.Rows[i]["F7"].ToString().Replace(",", ""));
                    TienNoItem.TieuMuc = s12FileContent.Rows[i][5].ToString();
                    TienNoItem.MaChuong = s12FileContent.Rows[i][3].ToString();
                    listTienNo.Add(TienNoItem);
                    fileTrungGianModel.TongTienNo += TienNoItem.SoTienTheoSo;
                }

                if (isChungTuFile_NotEmpty)
                {
                    for (int i = 0; i < chungTuFileContent.Rows.Count; i++)
                    {
                        // Các khoản tiền đã đóng
                        var TienDaNopItem = new KhoanTienModel();
                        TienDaNopItem.MaSoThue = chungTuFileContent.Rows[0][0].ToString();
                        TienDaNopItem.SoTienTheoSo = Convert.ToDecimal(chungTuFileContent.Rows[i][3].ToString().Replace(",", ""));
                        TienDaNopItem.TieuMuc = chungTuFileContent.Rows[i][2].ToString();

                        for (int j = 0; j < listTienNo.Count; j++)
                        {
                            if (listTienNo[j].MaSoThue == TienDaNopItem.MaSoThue && listTienNo[j].TieuMuc == TienDaNopItem.TieuMuc)
                            {
                                listTienNo[j].SoTienTheoSo = listTienNo[j].SoTienTheoSo < TienDaNopItem.SoTienTheoSo ? 0 : (listTienNo[j].SoTienTheoSo - TienDaNopItem.SoTienTheoSo);
                                fileTrungGianModel.TongTienNo -= TienDaNopItem.SoTienTheoSo;
                                break;
                            }
                        }
                    }
                }

                fileTrungGianModel.listKhoanNo = listTienNo;

                // Thông tin chung
                fileTrungGianModel.TenCongTy = s12FileContent.Rows[0]["F3"].ToString();
                fileTrungGianModel.MST = s12FileContent.Rows[0]["F2"].ToString();
                fileTrungGianModel.DiaChiThongBao = danhBaFileContent.Rows[0][7].ToString()
                                        + ", " + danhBaFileContent.Rows[0][9].ToString()
                                        + ", " + danhBaFileContent.Rows[0][11].ToString()
                                        + ", " + danhBaFileContent.Rows[0][13].ToString();
                fileTrungGianModel.PhuongThongBao = danhBaFileContent.Rows[0][9].ToString();
                fileTrungGianModel.QuanThongBao = danhBaFileContent.Rows[0][11].ToString();

                fileTrungGianModel.TruSo = danhBaFileContent.Rows[0][7].ToString()
                                        + ", " + danhBaFileContent.Rows[0][9].ToString()
                                        + ", " + danhBaFileContent.Rows[0][11].ToString()
                                        + ", " + danhBaFileContent.Rows[0][13].ToString();
                fileTrungGianModel.PhuongTruSo = danhBaFileContent.Rows[0][9].ToString();

                // Thông tin Quyết định - TMS
                fileTrungGianModel.TB07 = s14FileContent.Rows[0][3].ToString();
                fileTrungGianModel.NgayTB07 = s14FileContent.Rows[0][4].ToString();

                #endregion

                #region Fill dữ liệu và save file
                if (listTienNo.Count > 0)
                {
                    Microsoft.Office.Interop.Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();
                    string myPath = fileTrungGian_FilePath;
                    excelApp.Workbooks.Open(myPath);

                    // Get Worksheet
                    Microsoft.Office.Interop.Excel.Worksheet worksheet = excelApp.Worksheets[1];

                    try
                    {
                        int rowCount = worksheet.UsedRange.Rows.Count + 1;
                        //Microsoft.Office.Interop.Excel.Range line = (Microsoft.Office.Interop.Excel.Range)worksheet.Rows[rowCount];
                        //line.Insert(tienDaNopItem);
                        worksheet.Cells[1][rowCount] = fileTrungGianModel.MST;
                        worksheet.Cells[2][rowCount] = fileTrungGianModel.TenCongTy;
                        worksheet.Cells[3][rowCount] = fileTrungGianModel.TruSo;
                        worksheet.Cells[4][rowCount] = fileTrungGianModel.PhuongTruSo;
                        worksheet.Cells[5][rowCount] = fileTrungGianModel.DiaChiThongBao;
                        worksheet.Cells[6][rowCount] = fileTrungGianModel.PhuongThongBao;
                        worksheet.Cells[7][rowCount] = fileTrungGianModel.QuanThongBao;
                        worksheet.Cells[8][rowCount] = fileTrungGianModel.TB07;
                        worksheet.Cells[9][rowCount] = fileTrungGianModel.NgayTB07;
                        worksheet.Cells[10][rowCount] = fileTrungGianModel.listKhoanNo.FirstOrDefault(p => p.TieuMuc == "1001")?.SoTienTheoSo.ToString("#,###") ?? "-";
                        worksheet.Cells[11][rowCount] = fileTrungGianModel.listKhoanNo.FirstOrDefault(p => p.TieuMuc == "1052")?.SoTienTheoSo.ToString("#,###") ?? "-";
                        worksheet.Cells[12][rowCount] = fileTrungGianModel.listKhoanNo.FirstOrDefault(p => p.TieuMuc == "1701")?.SoTienTheoSo.ToString("#,###") ?? "-";
                        worksheet.Cells[13][rowCount] = fileTrungGianModel.listKhoanNo.FirstOrDefault(p => p.TieuMuc == "1754")?.SoTienTheoSo.ToString("#,###") ?? "-";
                        worksheet.Cells[14][rowCount] = fileTrungGianModel.listKhoanNo.FirstOrDefault(p => p.TieuMuc == "1804")?.SoTienTheoSo.ToString("#,###") ?? "-";
                        worksheet.Cells[15][rowCount] = fileTrungGianModel.listKhoanNo.FirstOrDefault(p => p.TieuMuc == "2862")?.SoTienTheoSo.ToString("#,###") ?? "-";
                        worksheet.Cells[16][rowCount] = fileTrungGianModel.listKhoanNo.FirstOrDefault(p => p.TieuMuc == "2863")?.SoTienTheoSo.ToString("#,###") ?? "-";
                        worksheet.Cells[17][rowCount] = fileTrungGianModel.listKhoanNo.FirstOrDefault(p => p.TieuMuc == "2864")?.SoTienTheoSo.ToString("#,###") ?? "-";
                        worksheet.Cells[18][rowCount] = fileTrungGianModel.listKhoanNo.FirstOrDefault(p => p.TieuMuc == "4254")?.SoTienTheoSo.ToString("#,###") ?? "-";
                        worksheet.Cells[19][rowCount] = fileTrungGianModel.listKhoanNo.FirstOrDefault(p => p.TieuMuc == "4268")?.SoTienTheoSo.ToString("#,###") ?? "-";
                        worksheet.Cells[20][rowCount] = fileTrungGianModel.listKhoanNo.FirstOrDefault(p => p.TieuMuc == "4272")?.SoTienTheoSo.ToString("#,###") ?? "-";
                        worksheet.Cells[21][rowCount] = fileTrungGianModel.listKhoanNo.FirstOrDefault(p => p.TieuMuc == "4917")?.SoTienTheoSo.ToString("#,###") ?? "-";
                        worksheet.Cells[22][rowCount] = fileTrungGianModel.listKhoanNo.FirstOrDefault(p => p.TieuMuc == "4918")?.SoTienTheoSo.ToString("#,###") ?? "-";
                        worksheet.Cells[23][rowCount] = fileTrungGianModel.listKhoanNo.FirstOrDefault(p => p.TieuMuc == "4931")?.SoTienTheoSo.ToString("#,###") ?? "-";
                        worksheet.Cells[24][rowCount] = fileTrungGianModel.listKhoanNo.FirstOrDefault(p => p.TieuMuc == "4934")?.SoTienTheoSo.ToString("#,###") ?? "-";
                        worksheet.Cells[25][rowCount] = fileTrungGianModel.listKhoanNo.FirstOrDefault(p => p.TieuMuc == "4944")?.SoTienTheoSo.ToString("#,###") ?? "-";
                        worksheet.Cells[26][rowCount] = fileTrungGianModel.listKhoanNo.FirstOrDefault(p => p.TieuMuc == "4943")?.SoTienTheoSo.ToString("#,###") ?? "-";
                        worksheet.Cells[27][rowCount] = fileTrungGianModel.listKhoanNo.FirstOrDefault(p => p.TieuMuc == "4949")?.SoTienTheoSo.ToString("#,###") ?? "-";
                        worksheet.Cells[28][rowCount] = fileTrungGianModel.TongTienNo > 0 ? fileTrungGianModel.TongTienNo.ToString("#,###") : "0";

                        worksheet.SaveAs(fileTrungGian_FilePath);
                        excelApp.Workbooks.Close();
                        excelApp.Quit();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Có lỗi xảy ra !");
                        return;
                    }
                }
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Nối nhiều file word thành 1
        /// </summary>
        /// <param name="listInputFiles"></param>
        /// <param name="outputFilePath"></param>
        public static void joinFiles(List<string> listInputFiles, String outputFilePath)
        {
            object missing = System.Type.Missing;
            object pageBreak = Microsoft.Office.Interop.Word.WdBreakType.wdSectionBreakNextPage;
            object outputFile = outputFilePath;

            // Create a new Word application
            Microsoft.Office.Interop.Word._Application wordApplication = new Microsoft.Office.Interop.Word.Application();

            try
            {
                // Create a new file based on our template
                Microsoft.Office.Interop.Word.Document wordDocument = wordApplication.Documents.Add(
                                              ref missing
                                            , ref missing
                                            , ref missing
                                            , ref missing);

                // Make a Word selection object.
                Microsoft.Office.Interop.Word.Selection selection = wordApplication.Selection;

                //Count the number of documents to insert;
                int documentCount = listInputFiles.Count;

                //A counter that signals that we shoudn't insert a page break at the end of document.
                int breakStop = 0;

                // Loop thru each of the Word documents
                foreach (string file in listInputFiles)
                {
                    breakStop++;
                    // Insert the files to our template
                    selection.InsertFile(
                                                file
                                            , ref missing
                                            , ref missing
                                            , ref missing
                                            , ref missing);

                    //Do we want page breaks added after each documents?
                    //if (insertPageBreaks && breakStop != documentCount)
                    //{ 
                    selection.InsertBreak(ref pageBreak);
                    //}
                }

                // Save the document to it's output file.
                wordDocument.SaveAs(
                                ref outputFile
                            , ref missing
                            , ref missing
                            , ref missing
                            , ref missing
                            , ref missing
                            , ref missing
                            , ref missing
                            , ref missing
                            , ref missing
                            , ref missing
                            , ref missing
                            , ref missing
                            , ref missing
                            , ref missing
                            , ref missing);

                // Clean up!
                wordDocument = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi xảy ra khi tạo file Tổng hợp !");
                throw ex;
            }
            finally
            {
                // Finally, Close our Word application
                wordApplication.Quit(ref missing, ref missing, ref missing);
            }
        }

        // Lấy địa chỉ ngân hàng
        public static string getBankAddress(string tenNH)
        {
            var containerFolder = Directory.GetCurrentDirectory();
            var CSDLFolderPath = containerFolder + @"\" + ConfigurationManager.AppSettings["directoryDatabase"];
            var dsVietTat_filePath = CSDLFolderPath + @"\" + ConfigurationManager.AppSettings["DS_TenVietTatNganHang"];
            var dsDiaChiNH_filePath = CSDLFolderPath + @"\" + ConfigurationManager.AppSettings["listAddressBankFile"];

            //format lại tên NH
            tenNH = tenNH.Replace("HN", "Hà Nội").Replace("VN", "Việt Nam").Replace("NNo & PTNT", "Nông nghiệp và Phát triển Nông thôn");

            if (String.IsNullOrEmpty(tenNH.Trim()))
            {
                MessageBox.Show("Tên Ngân hàng bị thiếu !", "Lỗi");
                return "";
            }

            // Bỏ thành phần trong ngoặc
            var formattedTenNH = tenNH;
            if (tenNH.Contains("(") && tenNH.Contains(")"))
            {
                formattedTenNH = tenNH.Remove(tenNH.IndexOf("("), tenNH.IndexOf(")") - tenNH.IndexOf("(") + 1);
            }

            // Lay cac thong tin
            var chiNhanh = "";
            var PGD = "";
            var tenNganHang = "";
            var tenNganHangVietTat = "";

            if (formattedTenNH.Contains(','))
            {
                var tenArr = formattedTenNH.Split(',');
                if (tenArr.Length > 0)
                {
                    tenNganHang = tenArr[0];

                    if (tenArr.Length > 1)
                    {
                        for (int i = 1; i < tenArr.Length; i++)
                        {
                            if (tenArr[i].ToUpper().Contains("CN") || tenArr[i].ToUpper().Contains("CHI NHÁNH"))
                            {
                                chiNhanh = tenArr[i].Trim();
                            } else if (tenArr[i].ToUpper().Contains("PGD") || tenArr[i].ToUpper().Contains("PHÒNG GIAO DỊCH")){
                                PGD = tenArr[i].Trim();
                            }
                        }
                    }
                }
            } else if (formattedTenNH.Contains('-'))
            {
                var tenArr = formattedTenNH.Split('-');
                if (tenArr.Length > 0)
                {
                    tenNganHang = tenArr[0].Trim();

                    if (tenArr.Length > 1)
                    {
                        for (int i = 1; i < tenArr.Length; i++)
                        {
                            if (tenArr[i].ToUpper().Contains("CN") || tenArr[i].ToUpper().Contains("CHI NHÁNH"))
                            {
                                chiNhanh = tenArr[i].Trim();
                            }
                            else if (tenArr[i].ToUpper().Contains("PGD") || tenArr[i].ToUpper().Contains("PHÒNG GIAO DỊCH"))
                            {
                                PGD = tenArr[i].Trim();
                            }
                        }
                    }
                }
            }

            // format các tên NH, chi nhánh, pgd
            tenNganHang = tenNganHang.Replace("NH ", "").Replace("Ngân hàng ", "").Trim();
            chiNhanh = chiNhanh.Replace("CN ", "").Replace("Chi nhánh ", "").Trim();
            PGD = PGD.Replace("PGD ", "").Replace("Phòng giao dịch ", "").Trim();

            var fileTenVietTatContent = DS_VietTatTenNH_Controller.SearchMST(dsVietTat_filePath, tenNganHang);
            if (fileTenVietTatContent.Rows.Count > 0)
            {
                tenNganHangVietTat = fileTenVietTatContent.Rows[0][3].ToString();
            }

            var listDataRowDiaChiNganHang = DS_DiaChiNganHang_Controller.SearchMST(dsDiaChiNH_filePath, tenNganHang, chiNhanh, PGD, tenNganHangVietTat);
            if (listDataRowDiaChiNganHang.Count <= 0)
            {
                return "<Không tìm thấy địa chỉ NH>";
            }
            else
            {
                return listDataRowDiaChiNganHang.FirstOrDefault().Field<string>("DIACHIGUIHOADON");
            }
        }
    }
}

