﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxReportApp.Model
{
    class ChungTu
    {
        public string mst { get; set; }
        public string tieumuc { get; set; }
        public decimal sotientheovnd { get; set; }
        public string sochungtu { get; set; }
        public DateTime ngaynopthue { get; set; }
        public string maphongban { get; set; }
        public string macanbo { get; set; }
        public DateTime ngaykbhachtoan { get; set; }
        public string kyhieugiaodich { get; set; }
        public string maxa { get; set; }
        public string nnt { get; set; }
        public string tennnt { get; set; }
        public string huychungtuso { get; set; }
        public string huychungtu { get; set; }
        public string chuong { get; set; }
        public string solochungtu { get; set; }
        public string trangthailochungtu { get; set; }
        public string taikhoancqt { get; set; }
        public decimal sotien { get; set; }
        public string tte { get; set; }
        public string cqt { get; set; }
        public string coquanthue { get; set; }
        public string taikhoanhachtoan { get; set; }
        public string sokhoan { get; set; }
        public string tenkhoan { get; set; }
        public string mamuc { get; set; }
        public string kyhieuchungtugoc { get; set; }
        public string sochungtugoc { get; set; }
        public string sothamchieu { get; set; }
        public DateTime ngayht { get; set; }
    }
}
