﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxReportApp.Model
{
    class ThongBaoViewModel
    {
        public string chicuc { get; set; }
        public string tennnt { get; set; }
        public string mst { get; set; }
        public string diachinhanthongbao { get; set; }
        public string diachitruso { get; set; }
        public decimal tongno { get; set; }
        public string tuso { get; set; }
        public string denso { get; set; }
        public ThongBaoViewModel()
        {
            chicuc = "";
            tennnt = "";
            mst = "";
            diachinhanthongbao = "";
            diachitruso = "";
            tongno = 0;
            tuso = "";
            denso = "";
        }
    }
}
