﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxReportApp.Model
{
    class NganHangModel
    {
        public String TenNganHang { get; set; }
        public String MaSoThue { get; set; }
        public String DiaChi { get; set; }
        public String TenChiNhanh { get; set; }
    }
}
