﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxReportApp.Model
{
    class ReportSumaryModel
    {
        public List<string> listInputFiles = new List<string>();
        public string reportFileToTrinh { get; set; }
        public string reportFileQD { get; set; }
        public string outputPathFile { get; set; }
        public ReportSumaryModel()
        {
            reportFileToTrinh = "";
            reportFileQD = "";
            outputPathFile = "";
            listInputFiles = new List<string>();
        }
    }
}
