﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxReportApp.Model
{
    class DonViViewModel
    {
        public String tenCanBo { get; set; }
        public String tenChiNhanh { get; set; }
        public String tenDoi { get; set; }
        public String tenDoiTruong { get; set; }
        public String tenChiCucTruong { get; set; }
        public String tenPhoChiCucTruong { get; set; }

        public DonViViewModel() {
            this.tenCanBo = "";
            this.tenChiNhanh = "";
            this.tenDoi = "";
            this.tenDoiTruong = "";
            this.tenChiCucTruong = "";
            this.tenPhoChiCucTruong = "";
        }

        public DonViViewModel(String tenCanBo, string tenChiNhanh, String tenDoi, String tenDoiTruong, String tenChiCucTruong, String tenPhoChiCucTruong)
        {
            this.tenCanBo = tenCanBo;
            this.tenChiNhanh = tenChiNhanh;
            this.tenDoi = tenDoi;
            this.tenDoiTruong = tenDoiTruong;
            this.tenChiCucTruong = tenChiCucTruong;
            this.tenPhoChiCucTruong = tenPhoChiCucTruong;
        }
    }
}
