﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxReportApp.Model
{
    class CCHDModel
    {
        public string chicuc { get; set; }
        public string thongbaoso { get; set; }
        public string ngaythongbao { get; set; }
        public string tennnt { get; set; }
        public string dctbt_diachi { get; set; }
        public string dctbt_phuongxa { get; set; }
        public string dctbt_quanhuyen { get; set; }
        public string dctbt_tinhthanhpho { get; set; }
        public string dcts_diachi { get; set; }
        public string dcts_phuongxa { get; set; }
        public string dcts_quanhuyen { get; set; }
        public string dcts_tinhthanhpho { get; set; }
        public string mst { get; set; }
        public string sodangkykd { get; set; }
        public string ngaycapdangkykd { get; set; }
        public DateTime ngaycohieuluc { get; set; }
        public DateTime ngayhethieuluc { get; set; }
        public string nganhnghekd { get; set; }
        public List<HoaDon> hoadons { get; set; }
        public string ngayqdcctk { get; set; }
        public string soqdcctk { get; set; }
        public string quy { get; set; }
        public string canbo { get; set; }
        public List<Thue> listThue { get; set; }
        public decimal tongtientren90ngay { get; set; }
        public decimal tongsonop { get; set; }
        public decimal tongtiencuongche { get; set; }
        public string chicuctruong { get; set; }
        public string doitruong { get; set; }
        public string chicucpho { get; set; }
        public CCHDModel()
        {
            chicuc = "";
            thongbaoso = "";
            ngaythongbao = "";
            tennnt = "";
            dctbt_diachi = "";
            dctbt_phuongxa = "";
            dctbt_quanhuyen = "";
            dctbt_tinhthanhpho = "";
            dcts_diachi = "";
            dcts_phuongxa = "";
            dcts_quanhuyen = "";
            dcts_tinhthanhpho = "";
            mst = "";
            sodangkykd = "";
            ngaycapdangkykd = "";
            ngaycohieuluc = new DateTime();
            ngayhethieuluc = new DateTime();
            nganhnghekd = "";
            hoadons = new List<HoaDon>();
            ngayqdcctk = "";
            soqdcctk = "";
            quy = "";
            canbo = "";
            listThue = new List<Thue>();
            tongtientren90ngay = 0;
            tongsonop = 0;
            tongtiencuongche = 0;
            chicuctruong = "";
            doitruong = "";
            chicucpho = "";
        }

    }
}
