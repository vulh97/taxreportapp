﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxReportApp.Model
{
    class ChiCuc
    {
        public int ID { get; set; }
        public String ten { get; set; }
        public String diaChi { get; set; }
        public String soDienThoai { get; set; }

        public ChiCuc(String ten, String diaChi, String soDienThoai)
        {
            this.ten = ten;
            this.diaChi = diaChi;
            this.soDienThoai = soDienThoai;
        }
        public ChiCuc()
        {

        }
    }
}
