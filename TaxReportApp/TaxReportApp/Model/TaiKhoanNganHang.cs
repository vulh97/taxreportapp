﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxReportApp.Model
{
    class TaiKhoanNganHang
    {
        public int stt { get; set; }
        public string sotaikhoan { get; set; }
        public string tennganhang { get; set; }
        public string diachi { get; set; }
        public TaiKhoanNganHang()
        {
            stt = 1;
            sotaikhoan = "";
            tennganhang = "";
            diachi = "";
        }
    }
}
