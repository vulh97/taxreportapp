﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxReportApp.Model
{
    class ToTrinh
    {
        public string chicuc { get; set; }
        public string tencanbo { get; set; }
        public string tencongty { get; set; }
        public string masothue { get; set; }
        public string diachi { get; set; }
        public string phuongxa { get; set; }
        public string quanhuyen { get; set; }
        public string tinhthanhpho { get; set; }
        public string dctbt_diachi { get; set; }
        public string dctbt_phuongxa { get; set; }
        public string dctbt_quanhuyen { get; set; }
        public string dctbt_thanhpho { get; set; }
        public List<Thue> listThue { get; set; }
        public string thongbaoso { get; set; }
        public string ngaythongbao { get; set; }
        public decimal tongtientren90ngay { get; set; }
        public decimal tongtiencuongche { get; set; }
        public decimal tongsonop { get; set; }
        public List<TaiKhoanNganHang> listTaiKhoan { get; set; }
        public string ngaycohieuluc { get; set; }
        public string ngayhethieuluc { get; set; }
        public string Email { get; set; }
        public string tendoi { get; set; }
        public string sdt { get; set; }
        public string doitruong { get; set; }
        public string chicuctruong { get; set; }
        public string chicucpho { get; set; }
        public ToTrinh()
        {
            chicuc = "";
            tencanbo = "";
            tencongty = "";
            masothue = "";
            diachi = "";
            phuongxa =  "";
            quanhuyen = "";
            tinhthanhpho = "";
            dctbt_diachi = "";
            dctbt_phuongxa = "";
            dctbt_quanhuyen = "";
            dctbt_thanhpho = "";
            listThue = new List<Thue>();
            thongbaoso = "";
            ngaythongbao = "";
            tongtientren90ngay = 0;
            tongtiencuongche = 0;
            tongsonop = 0;
            ngaycohieuluc = "";
            ngayhethieuluc = "";
            Email = "";
            tendoi = "";
            sdt = "";
            doitruong = "";
            chicuctruong = "";
            chicucpho = "";
        }
    }
}
