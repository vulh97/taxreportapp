﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxReportApp.Model
{
    class FileTrungGianViewModel
    {
        public string MST { get; set; }
        public string TenCongTy { get; set; }
        public string TruSo { get; set; }
        public string PhuongTruSo { get; set; }
        public string DiaChiThongBao { get; set; }
        public string PhuongThongBao { get; set; }
        public string QuanThongBao { get; set; }
        public string SoTB { get; set; }
        public string TB07 { get; set; }
        public string NgayTB07 { get; set; }
        public List<KhoanTienModel> listKhoanNo { get; set; }
        public decimal TongTienNo { get; set; }

        public FileTrungGianViewModel()
        {
            MST = "";
            TenCongTy = "";
            TruSo = "";
            PhuongTruSo = "";
            DiaChiThongBao = "";
            PhuongThongBao = "";
            QuanThongBao = "";
            SoTB = "";
            TB07 = "";
            NgayTB07 = "";
            listKhoanNo = new List<KhoanTienModel>();
            TongTienNo = 0;
        }
    }
}
