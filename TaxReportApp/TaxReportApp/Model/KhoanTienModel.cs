﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxReportApp.Model
{
    class KhoanTienModel
    {
        public String TenCongTy { get; set; }
        public String MaSoThue { get; set; }
        public String SoTienTheoChu { get; set; }
        public decimal SoTienTheoSo { get; set; }
        public String TieuMuc { get; set; }
        public String MaChuong { get; set; }
    }
}
