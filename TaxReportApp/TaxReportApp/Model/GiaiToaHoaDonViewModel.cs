﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxReportApp.Model
{
    class GiaiToaHoaDonViewModel
    {
        public String MaSoThue { get; set; }
        public String TenCty { get; set; }
        public String So_TB07 { get; set; }
        public String Ngay_TB07 { get; set; }
        public String So_QD_HD { get; set; }
        public String So_TB { get; set; }
        public DateTime Ngay_QD { get; set; }
        public String DiaChiNhanThongBao { get; set; }
        public String DiaChiTruSo { get; set; }
        public String So_Dang_ky_kinh_doanh { get; set; }
        public DateTime Ngay_Dang_ky_kinh_doanh { get; set; }
        public String ContainerFolderPath { get; set; }
        /// <summary>
        /// Ngày việc cấm lưu hành hóa đơn có hiệu lực
        /// </summary>
        public DateTime Ngay_Co_hieu_luc { get; set; }
        public DateTime Ngay_Het_hieu_luc { get; set; }
        public decimal TongNo { get; set; }
        public decimal TongTienDaTra { get; set; }
        public List<KhoanTienModel> ListKhoanTienNo_CCHD { get; set; }
        public List<HoaDon> ListHoaDon { get; set; }

        public GiaiToaHoaDonViewModel()
        {
            MaSoThue = "";
            TenCty = "";
            So_TB07 = "";
            So_QD_HD = "";
            So_TB = "";
            DiaChiNhanThongBao = "";
            DiaChiTruSo = "";
            So_Dang_ky_kinh_doanh = "";
            ContainerFolderPath = "";
            TongNo = 0;
            TongTienDaTra = 0;
            ListKhoanTienNo_CCHD = new List<KhoanTienModel>();
            ListHoaDon = new List<HoaDon>();
            Ngay_TB07 = "";
        }
    }
}
