﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxReportApp.Model
{
    class FileNguonStatusModel
    {
        #region 01. Cưỡng chế hợp đồng
        /// <summary>
        /// Cưỡng chế hợp đồng trên TMS
        /// </summary>
        public bool cchdTrenTMS { get; set; }

        /// <summary>
        /// File chứng từ nộp từ 01/07/2020
        /// </summary>
        public bool chungTu { get; set; }

        /// <summary>
        /// Danh sách theo dõi cưỡng chế hợp đồng 2020
        /// </summary>
        public bool dsTheoDoiCCHD { get; set; }
        /// <summary>
        /// CCTK trên TMS
        /// </summary>
        public bool cctkTMS { get; set; }
        #endregion

        #region 02. Cưỡng chế tài khoản
        /// <summary>
        /// Cưỡng chế tài khoản trên TMS
        /// </summary>
        public bool cctkTrenTMS { get; set; }

        /// <summary>
        /// File Chứng từ nộp từ 01/07/2020
        /// </summary>
        //public bool chungTu { get; set; }  => Trùng với file ở cưỡng chế hợp đồng

        /// <summary>
        /// Danh sách theo dõi cưỡng chế tài khoản 2020
        /// </summary>
        public bool dsTheoDoiCCTK { get; set; }

        /// <summary>
        /// Mẫu 14 TB07 T62020
        /// </summary>
        public bool mau14TB07 { get; set; }
        #endregion

        #region 04. Nợ khả năng tăng
        /// <summary>
        /// File 06.CBQLt.XLSX
        /// </summary>
        public bool CBQLT { get; set; }

        /// <summary>
        /// File Sổ thu nộp
        /// </summary>
        public bool soThuNop { get; set; }
        #endregion

        #region 06. Nợ phát sinh hàng tháng
        /// <summary>
        /// File 06.CBQLt.XLSX
        /// </summary>
        //public bool CBQLT { get; set; }  => Trùng với file ở folder Nợ khả năng tăng

        /// <summary>
        /// File 07.Mẫu 09
        /// </summary>
        public bool mau09 { get; set; }

        /// <summary>
        /// File 12.Bieu chấm điểm
        /// </summary>
        public bool bieuChamDiem { get; set; }

        /// <summary>
        /// Chứng từ nộp từ 01/07/2020
        /// </summary>
        //public bool chungTu { get; set; }  => trùng với file ở folder cưỡng chế hợp đồng

        /// <summary>
        /// File Worksheet in Basis (1).xlsx
        /// </summary>
        public bool worksheetInBasic { get; set; }
        #endregion

        #region 07. Kiểm tra tình hình cưỡng chế trên TMS
        /// <summary>
        /// Cưỡng chế tài khoản trên TMS
        /// </summary>
        //public bool cctkTrenTMS { get; set; }  => Trùng với file ở folder Cưỡng chế tài khoản

        /// <summary>
        /// Danh sách theo dõi cưỡng chế tài khoản 2020
        /// </summary>
        //public bool dsTheoDoiCCTK { get; set; }  => Trùng với file ở folder Cưỡng chế tài khoản
        #endregion

        #region 08. Giám sát phân loại nợ
        /// <summary>
        /// File PLN T7.xls
        /// </summary>
        public bool PLNT7 { get; set; }
        #endregion

        #region 09. Theo giao công việc giao công văn
        // {Empty}
        #endregion

        #region 10. Cảnh báo đến hạn cưỡng chế 
        /// <summary>
        /// Danh sách theo dõi cưỡng chế tài khoản 2020
        /// </summary>
        //public bool dsTheoDoiCCTK { get; set; }  => Trùng với file ở folder Cưỡng chế tài khoản

        /// <summary>
        /// Danh sách theo dõi cưỡng chế hợp đồng 2020
        /// </summary>
        //public bool dsTheoDoiCCHD { get; set; }  => Trùng với file ở folder cưỡng chế hợp đồng
        #endregion

        #region Các files để tạo các báo cáo cho nhóm cưỡng chế nợ thuế
        /// <summary>
        /// File chứng từ
        /// </summary>
        public bool CC_ChungTu { get; set; }

        /// <summary>
        /// File danh bạ
        /// </summary>
        public bool CC_DanhBa { get; set; }

        /// <summary>
        /// File danh sách địa chỉ ngân hàng
        /// </summary>
        public bool CC_DSDiaChiNH { get; set; }

        /// <summary>
        /// File S12
        /// </summary>
        public bool CC_S12 { get; set; }

        /// <summary>
        /// File S14
        /// </summary>
        public bool CC_S14 { get; set; }

        /// <summary>
        /// File Tài khoản ngân hàng
        /// </summary>
        public bool CC_TaiKhoanNganHang { get; set; }
        #endregion
        // Danh bạ
    }
}
