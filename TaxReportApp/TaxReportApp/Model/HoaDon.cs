﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxReportApp.Model
{
    class HoaDon
    {
        public int stt { get; set; }
        public string loaihoadon { get; set; }
        public string kyhieumau { get; set; }
        public string kyhieuhoadon { get; set; }
        public string tusodenso { get; set; }
        public string ghichu { get; set; }
    }
}
