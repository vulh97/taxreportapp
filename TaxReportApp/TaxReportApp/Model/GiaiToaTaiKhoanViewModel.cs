﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxReportApp.Model
{
    class GiaiToaTaiKhoanViewModel
    {
        public String TenCongTy { get; set; }
        public String MaSoThue { get; set; }
        public List<KhoanTienModel> listKhoanTienDaDong { get; set; }
        public List<KhoanTienModel> listKhoanTienNo { get; set; }
        public List<NganHangModel> listNganHang { get; set; }
        public decimal tongTienNo { get; set; }
        public decimal tongTienDaDong { get; set; }
        public String DiaChiTruSo { get; set; }
        public String DiaChiNhanThongBao { get; set; }
        public String DanhSachNganHang { get; set; }
        public String ContainterFilePath { get; set; }
        public String SoQuyetDinh { get; set; }
        public String Ngay { get; set; }
        public String Thang { get; set; }
        public String Nam { get; set; }
        public decimal TongTienNoCCTK { get; set; }

        public GiaiToaTaiKhoanViewModel()
        {
            this.TenCongTy = "";
            this.MaSoThue = "";
            listKhoanTienDaDong = new List<KhoanTienModel>();
            listKhoanTienNo = new List<KhoanTienModel>();
            listNganHang = new List<NganHangModel>();

            tongTienDaDong = 0;
            tongTienNo = 0;
            DiaChiTruSo = "";
            DiaChiNhanThongBao = "";
            DanhSachNganHang = "";
            ContainterFilePath = "";
            TongTienNoCCTK = 0;
        }
    }
}
