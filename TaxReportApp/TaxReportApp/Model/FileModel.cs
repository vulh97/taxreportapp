﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxReportApp.Model
{
    class FileModel
    {
        public int stt { get; set; }
        public string filename { get; set; }
        public string filetype { get; set; }
    }
}
