﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxReportApp.Model
{
    class Thue
    {
        public int stt { get; set; }
        public string loaithue { get; set; }
        public string tieumuc { get; set; }
        public decimal notren90ngay { get; set; }
        public decimal sotiennop { get; set; }
        public decimal notren91ngaycuongche { get; set; }
        public decimal notren120ngaycuongche { get; set; }
        public string machuong { get; set; }
        public string kythue { get; set; }
        public Thue()
        {
            stt = 1;
            loaithue = "";
            tieumuc = "";
            notren90ngay = 0;
            sotiennop = 0;
            notren91ngaycuongche = 0;
            notren120ngaycuongche = 0;
            machuong = "";
            kythue = "";
        }
    }
}
