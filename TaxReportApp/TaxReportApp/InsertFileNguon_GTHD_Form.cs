﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TaxReportApp.Controller;

namespace TaxReportApp
{
    public partial class InsertFileNguon_GTHD_Form : Form
    {
        public InsertFileNguon_GTHD_Form()
        {
            InitializeComponent();

            // Kiểm tra các file đã có
            var fileNguonStatus = FileNguonStatusController.get_FileNguonStatus();

            if (fileNguonStatus.CC_S12)
            {
                GTHD_S12_statusMessage.Text = ConfigurationManager.AppSettings["s12File"];
            }

            if (fileNguonStatus.CC_ChungTu)
            {
                GTHD_ChungTu_statusMessage.Text = ConfigurationManager.AppSettings["licenseFile"];
            }

            if (fileNguonStatus.CC_DanhBa)
            {
                GTHD_DanhBa_statusMessage.Text = ConfigurationManager.AppSettings["phoneBookFile"];
            }

            if (fileNguonStatus.dsTheoDoiCCHD)
            {
                CCTK_theoDoiGTHD_statusMessage.Text = ConfigurationManager.AppSettings["dsTheoDoiCCHD"];
            }
        }

        private void InsertFileNguon_CCTK_Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(1);
        }

        private void GTHD_S12_selectBtn_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.RestoreDirectory = true;
                openFileDialog.Title = "Chọn File";
                openFileDialog.Multiselect = false;
                openFileDialog.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";
                openFileDialog.CheckFileExists = true;
                openFileDialog.CheckPathExists = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    GTHD_S12_urlTxtBox.Text = openFileDialog.FileName;
                }
            } catch(Exception ex)
            {
                MessageBox.Show("Có lỗi xảy ra !", "Lỗi");
            }
        }

        private void GTHD_ChungTu_selectBtn_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.RestoreDirectory = true;
                openFileDialog.Title = "Chọn File";
                openFileDialog.Multiselect = false;
                openFileDialog.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";
                openFileDialog.CheckFileExists = true;
                openFileDialog.CheckPathExists = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    GTHD_ChungTu_urlTxtBox.Text = openFileDialog.FileName;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi xảy ra !", "Lỗi");
            }
        }

        private void GTHD_DanhBa_selectBtn_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.RestoreDirectory = true;
                openFileDialog.Title = "Chọn File";
                openFileDialog.Multiselect = false;
                openFileDialog.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";
                openFileDialog.CheckFileExists = true;
                openFileDialog.CheckPathExists = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    GTHD_DanhBa_urlTxtBox.Text = openFileDialog.FileName;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi xảy ra !", "Lỗi");
            }
        }

        private void GTHD_dsTheoDoiGTHD_selectBtn_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.RestoreDirectory = true;
                openFileDialog.Title = "Chọn File";
                openFileDialog.Multiselect = false;
                openFileDialog.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";
                openFileDialog.CheckFileExists = true;
                openFileDialog.CheckPathExists = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    GTHD_dsTheoDoi_urlTxtBox.Text = openFileDialog.FileName;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi xảy ra !", "Lỗi");
            }
        }

        private void GTHD_S12_deleteBtn_Click(object sender, EventArgs e)
        {
            GTHD_S12_urlTxtBox.Text = "";
        }

        private void GTHD_ChungTu_deleteBtn_Click(object sender, EventArgs e)
        {
            GTHD_ChungTu_urlTxtBox.Text = "";
        }

        private void GTHD_DanhBa_deleteBtn_Click(object sender, EventArgs e)
        {
            GTHD_DanhBa_urlTxtBox.Text = "";
        }

        private void GTHD_dsTheoDoi_deleteBtn_Click(object sender, EventArgs e)
        {
            GTHD_dsTheoDoi_urlTxtBox.Text = "";
        }

        private void GTHD_S12_loadBtn_Click(object sender, EventArgs e)
        {
            var filePath = GTHD_S12_urlTxtBox.Text;
            var isSuccessful = Common.Common.ImportFile(filePath, "s12File");

            if (isSuccessful)
            {
                GTHD_S12_statusMessage.Text = "S12.XLSX";
                GTHD_S12_urlTxtBox.Text = "";
            }
        }

        private void GTHD_ChungTu_loadBtn_Click(object sender, EventArgs e)
        {
            var filePath = GTHD_ChungTu_urlTxtBox.Text;
            var isSuccessful = Common.Common.ImportFile(filePath, "licenseFile");

            if (isSuccessful)
            {
                GTHD_ChungTu_statusMessage.Text = "Chứng từ.XLSX";
                GTHD_ChungTu_urlTxtBox.Text = "";
            }
        }

        private void GTHD_DanhBa_loadBtn_Click(object sender, EventArgs e)
        {
            var filePath = GTHD_DanhBa_urlTxtBox.Text;
            var isSuccessful = Common.Common.ImportFile(filePath, "phoneBookFile");

            if (isSuccessful)
            {
                GTHD_DanhBa_statusMessage.Text = "Danh bạ.XLSX";
                GTHD_DanhBa_urlTxtBox.Text = "";
            }
        }

        private void GTHD_dsTheoDoi_loadBtn_Click(object sender, EventArgs e)
        {
            var filePath = GTHD_dsTheoDoi_urlTxtBox.Text;
            var isSuccessful = Common.Common.ImportFile(filePath, "accountBankFile");

            if (isSuccessful)
            {
                CCTK_theoDoiGTHD_statusMessage.Text = "Tài khoản ngân hàng.XLSX";
                GTHD_dsTheoDoi_urlTxtBox.Text = "";
            }
        }
    }
}
